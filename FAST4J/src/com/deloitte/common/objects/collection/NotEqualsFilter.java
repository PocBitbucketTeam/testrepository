package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * Determines if 2 objects are not equals.
 * Returns true if 2 objects are not equals else false.
 * <code>
 * 		boolean isSame = new NotEqualsFilter<Customer>(customer1).include(customer2);	
 * </code>
 * 
 * @author SIDT Framework Team
 */
public class NotEqualsFilter<T> implements Filter<T> {

	private T filterVal;
	
	public NotEqualsFilter(T filterVal){
		this.filterVal = filterVal;
	}

	public boolean include(T val) {
		return !(filterVal.equals(val));
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return filterVal.hashCode() + 22;
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof NotEqualsFilter
			&& filterVal.equals(((NotEqualsFilter<T>)obj).filterVal));
	}
}
