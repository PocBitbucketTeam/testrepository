package com.deloitte.common.objects.collection;

import java.lang.ref.SoftReference;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.deloitte.common.interfaces.Cache;

public abstract class AbstractCache<K, V> implements Cache<K, V>
{
	private int theMaxSize;
	private LinkedHashMap<K, SoftReference<V>> entries;
	
	public AbstractCache(boolean accessOrder)
	{
		this(100, accessOrder);
	}
	
	public AbstractCache(int maxsize, boolean accessOrder){
		this.theMaxSize = maxsize;
		entries = new LinkedHashMap<K, SoftReference<V>>(maxsize, .75f, accessOrder){

			private static final long serialVersionUID = 5515193990013654969L;

			/**
			 * @see LinkedHashMap.removeEldestEntry(Map.Entry)
			 */
			protected final boolean removeEldestEntry(Entry<K, SoftReference<V>> eldest)
			{
		        return size() > theMaxSize;
			}
		};
	}
	

	/**
	 * PAY CLOSE ATTENTION TO THIS MESSAGE
	 * <br><br>
	 * If you are expecting the value from this map to be returned and it is not, it
	 * could be because the Garbage Collector has reclaimed it. This uses SoftReferences
	 * to insure we do not have a lot of objects around that should be GC'd.
	 * 
	 * This means you should go to the original place you found the object, recreate and
	 * place back in the cache.
	 * 
	 */
	public final V get(K index)
	{
		V theAnswer = null;
		SoftReference<V> theReference = (SoftReference<V>)entries.get(index);
		if (theReference != null)
		{	
				theAnswer = (V)theReference.get();
		}
		return theAnswer;
	}

	public final V put(K index, V value)
	{
		SoftReference<V> theReference = entries.put(index, new SoftReference<V>(value));
		V theAnswer = null;
		if (theReference != null)
		{	
				theAnswer = (V)theReference.get();
		}
		return theAnswer;
	}

	public boolean containsKey(K index) {
		return entries.containsKey(index);
	}

	public V remove(K index) {
		SoftReference<V> theReference = entries.remove(index);
		V theAnswer = null;
		if (theReference != null)
		{	
				theAnswer = (V)theReference.get();
		}
		return theAnswer;
	}
	
	public int size(){
		return entries.size();
	}

	public void clear(){
		entries.clear();
	}

}
