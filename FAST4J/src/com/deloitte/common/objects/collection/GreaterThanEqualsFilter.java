package com.deloitte.common.objects.collection;

/**
 * Determines the parameter object is grater than or equals to the constructor object.
 * Returns true if parameter object is greater than or equal else false.
 * <code>
 * 		boolean isSame = new GreaterThanEqualsFilter<Customer>(customer1).include(customer2);<br/>
 * 		boolean isSame = new GreaterThanEqualsFilter<Integer>(20).include(25);<br/>
 * </code>
 * 
 * @author SIDT Framework Team
 */
public class GreaterThanEqualsFilter<T extends Comparable<T>> extends AbstractCompareFilter<T> {

	public GreaterThanEqualsFilter(T filterVal){
		super(filterVal);
	}

	@Override
	public boolean include(T val) {
		return val.compareTo(getFilterVal()) >= 0;
	}

}
