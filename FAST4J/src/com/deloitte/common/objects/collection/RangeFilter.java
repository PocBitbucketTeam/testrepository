package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * Determines the parameter object is in between the range of two objects.
 * Returns true if parameter object is in between the range of two objects else false.
 * <code>
 * 		boolean isSame = new RangeFilter<Customer>(customer1, customer10).include(customer5);<br/>
 * 		boolean isSame = new RangeFilter<Integer>(10,20).include(15);<br/>
 * </code>
 * 
 * @author SIDT Framework Team
 */
public class RangeFilter<T extends Comparable<T>> implements Filter<T> {

	T filterVal1;
	T filterVal2;
	
	public RangeFilter(T filterVal1, T filterVal2){
		this.filterVal1 =  filterVal1;
		this.filterVal2 =  filterVal2;
	}
	
	public boolean include(T val) {
		return (val.compareTo(filterVal1) > 0 && val.compareTo(filterVal2) < 0); 
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return filterVal1.hashCode() + filterVal2.hashCode() + 99;
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof RangeFilter
				&& filterVal1.equals(((RangeFilter<T>)obj).filterVal1)
				&& filterVal2.equals(((RangeFilter<T>)obj).filterVal2));
	}
}
