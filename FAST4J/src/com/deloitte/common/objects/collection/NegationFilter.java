package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * Reverses the result of the filter of any Filter it is used to decorate
 * 
 * @author SIDT Framework Team
 */
public class NegationFilter<T> implements Filter<T> {

	private Filter<T> filter;
	
	public NegationFilter(Filter<T> filter){
		this.filter = filter;
	}

	public boolean include(T theObject) {
		return !(filter.include(theObject));
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return filter.hashCode() + 874;
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof NegationFilter
			&& filter.equals(((NegationFilter<T>)obj).filter));
	}
}
