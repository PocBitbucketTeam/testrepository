package com.deloitte.common.objects.collection;

import java.util.Comparator;

import com.deloitte.common.util.ReflectionHelper;
/**
 * Provides a mechanism to sort a Domain Objects by one of the columns. 
 * Please note the Comparator you provide should be able to handle null values.
 * <br/><br/>
 * Sample usage pattern<br/>
 * 
 * 		Collections.sort(theList,new BeanComparator("TOSTRING", BeanComparator.NULLSLOW));
 * 			null, items...
 *		Collections.sort(theList,new BeanComparator("TOSTRING", BeanComparator.NULLSHIGH));
 *			items...null
 *		Collections.sort(theList,new BeanComparator("TOSTRING", BeanComparator.NULLSLOW_ASCENDING));
 *			null, item 1, item 2
 *		Collections.sort(theList,new BeanComparator("TOSTRING", BeanComparator.NULLSLOW_DESCENDING));
 *			null, item 2, item 1
 *		Collections.sort(theList,new BeanComparator("TOSTRING", BeanComparator.NULLSHIGH_ASCENDING));
 *			item 1, item 2, null
 *		Collections.sort(theList,new BeanComparator("TOSTRING", BeanComparator.NULLSHIGH_DESCENDING));
 *			item 2, item 1, null
 *
 */
@SuppressWarnings({"rawtypes"})
public final class BeanComparator<T> implements Comparator<T> {

	private final String propertyName;
	private final Comparator delegate;
	
	
	@SuppressWarnings("unchecked")
	public static final Comparator NULLSLOW =  new NullSafeComparatorDecorator(new DefaultComparator());//new DefaultComparator(true);
	
	public static  final Comparator NULLSLOW_ASCENDING = NULLSLOW;
	@SuppressWarnings("unchecked")
	public static  final Comparator NULLSLOW_DESCENDING = new NullSafeComparatorDecorator(
																new ReverseComparatorDecorator(new DefaultComparator()));
	@SuppressWarnings("unchecked")
	public static final Comparator NULLSHIGH = new NullSafeComparatorDecorator(new DefaultComparator(), true);
	public static final Comparator NULLSHIGH_ASCENDING = NULLSHIGH;
	
	@SuppressWarnings("unchecked")
	public static final Comparator NULLSHIGH_DESCENDING = new NullSafeComparatorDecorator(
																new ReverseComparatorDecorator(new DefaultComparator()),true);
	
	
	/**
	 * The constructor uses the domain object property name (ie customerName)
	 * The property name is case-insensitive
	 */

	public BeanComparator(String propertyName) {
		this(propertyName, NULLSLOW_ASCENDING);
	}
	
	/**
	 * @deprecated - Please use one of the other two constructors
	 */
	public BeanComparator(String propertyName, boolean ascending)
	{
		this(propertyName);
	}
	
	public BeanComparator(String propertyName, Comparator comparator)	{
		this.propertyName = propertyName;
		this.delegate = comparator;
	}
	
	public String getPropertyName() {
		return propertyName;
	}

	public Comparator getDelegate() {
		return delegate;
	}

	@SuppressWarnings("unchecked")
	public int compare(T theFirstObject, T theSecondObject) {
		
		if ((theFirstObject == null && theSecondObject == null)
				|| theFirstObject == theSecondObject) {
			return 0;
		}

		Object column1 = null;
		Object column2 = null;

		try {
			column1 = ReflectionHelper.invokeAccessorMethod(theFirstObject,ReflectionHelper.getAccessorMethod(theFirstObject,propertyName),null);
		} catch (Exception e) {
			// We are going to proceed with nulls
		}
		
		try {
			column2 = ReflectionHelper.invokeAccessorMethod(theSecondObject,ReflectionHelper.getAccessorMethod(theSecondObject,propertyName),null);
		} catch (Exception e) {
			// We are going to proceed with nulls
		}
	
		return delegate.compare(column1, column2);
	}

	//TODO does this have a test case?
	@Override
	public int hashCode() {
		return propertyName.hashCode() + delegate.hashCode();
	}

	//TODO does this have a test case?
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof BeanComparator
			&& propertyName.equals(((BeanComparator)obj).getPropertyName())
			&& delegate.equals(((BeanComparator)obj).getDelegate()));
	}

	
}
