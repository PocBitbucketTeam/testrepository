package com.deloitte.common.objects.collection;

import java.util.Map;
import java.util.TreeMap;

import com.deloitte.common.interfaces.Cache;

/**
 * This implementation of Cache is a naive implementation of caching, providing
 * an unlimited amount of entries. Use this Cache implementation whenever you
 * want the stored entries to grow without bound. Note that this cache will never
 * release it's contents. If a memory-sensitive cache is appropriate for your
 * implementation, a different implementation such as FifoCache and LRUCache
 * would be more appropriate.<P>
 * 
 * <b>WARNING!!!!</b> - Because this cache's entries will never be garbage collected and because
 * the cache can grow without bound, it <b>should only be used in situations where you
 * will potentially have a finite number of cache entries that will easily fit into
 * memory</b>, and only where the number of cache entries is expressly limited by application
 * logic. In situations where you need a very high-performing cache for small, bounded
 * cache sets this cache is very appropriate, but <b>inappropriate use of it where the number
 * of cache entries will grow without bound will introduce a memory leak into your application
 * and inevitably result in an out-of-memory error!!!!!!</b>
 */
//TODO needs a test case
public class UnlimitedCache<K extends Comparable<K>, V> implements Cache<K, V> {

	private Map<K, V> entries;
	
	public UnlimitedCache() {
		this(100);
	}

	public UnlimitedCache(int expectedCacheSize) {
		super();
		this.entries = new TreeMap<K, V>();
	}

	public V get(K index) {
		return entries.get(index);
	}

	public V put(K index, V o) {
		return entries.put(index, o);
	}

	public boolean containsKey(K index) {
		return entries.containsKey(index);
	}

	public V remove(K index) {
		return entries.remove(index);
	}
	
	public void clear() {
		entries.clear();
	}

}
