package com.deloitte.common.objects.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.deloitte.common.interfaces.TwoWayIterator;

public class PagedCollection<E> implements Collection<E>
{
	public 	static final int DEFAULT_PAGESIZE = 10;
	private transient 	 int thePageSize = DEFAULT_PAGESIZE;

	private Collection<E> 	theUnderlyingCollection = Collections.emptyList();
	private transient 	Collection<List<E>>	thePagedCollection;
	
	public PagedCollection(final Collection<E> c)
	{
		this(c,DEFAULT_PAGESIZE);
	}
	
	public PagedCollection(final Collection<E> aCollection, final int pageSize)
	{
		super();
		if (aCollection != null)
		{
			theUnderlyingCollection = Collections.unmodifiableCollection(aCollection);
		}
		if (pageSize > 0)
		{
			thePageSize = pageSize;	
		}
		bucket();
	}
	
	public Iterator<E> iterator()
	{
		return theUnderlyingCollection.iterator();
	}
	
	public TwoWayIterator<List<E>> twoWayIterator()
	{
		return new PaginationIterator<List<E>>(thePagedCollection);
	}
	
	public ListIterator<List<E>> listIterator()
	{

		return Collections.unmodifiableList(new ArrayList<List<E>>(thePagedCollection)).listIterator();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<E> getPage(final int i) {
		return (Collection<E>)thePagedCollection.toArray()[i];
	}
	
	public int	size()
	{
		return thePagedCollection.size();
	}
	
	public boolean contains(Object o)
	{
		return theUnderlyingCollection.contains(o);
	}

	public boolean containsAll(Collection<?> c)
	{
		return theUnderlyingCollection.containsAll(c);
	}

	public boolean isEmpty()
	{
		return theUnderlyingCollection.isEmpty();
	}

	public Object[] toArray()
	{
		return theUnderlyingCollection.toArray();
	}

	public <T> T[] toArray(T[] a)
	{
		return theUnderlyingCollection.toArray(a);
	}
	


	public boolean add(Object o)
	{
		throw new UnsupportedOperationException();
	}

	public boolean addAll(Collection<? extends E> c)
	{
		throw new UnsupportedOperationException();
	}

	public void clear()
	{
		throw new UnsupportedOperationException();
	}

	public boolean remove(Object o)
	{
		throw new UnsupportedOperationException();
	}

	public boolean removeAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}

	public boolean retainAll(Collection<?> c)
	{
		throw new UnsupportedOperationException();
	}

	private void bucket()
	{
		final List<List<E>> pages = new ArrayList<List<E>>();
		List<E> page = new ArrayList<E>();
		
		final Iterator<E> iter = theUnderlyingCollection.iterator();
		while (iter.hasNext())
		{
			page.add(iter.next());
			if (page.size() == thePageSize)
			{
				pages.add(page);
				page = new ArrayList<E>();
			}
			
		}
		if (page.size() != 0)
		{
			pages.add(page);
		}
		thePagedCollection = Collections.unmodifiableCollection(pages);
	}

}
