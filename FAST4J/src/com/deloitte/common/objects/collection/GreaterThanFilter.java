package com.deloitte.common.objects.collection;


/**
 * Determines the parameter object is grater than to the constructor object.
 * Returns true if parameter object is greater than else false.
 * <code>
 * 		boolean isSame = new GreaterThanFilter<Customer>(customer1).include(customer2);<br/>
 * 		boolean isSame = new GreaterThanFilter<Integer>(20).include(25);<br/>
 * </code>
 * 
 * @author SIDT Framework Team
 */
public class GreaterThanFilter<T extends Comparable<T>> extends AbstractCompareFilter<T> {

	public GreaterThanFilter(T filterVal){
		super(filterVal);
	}

	@Override
	public boolean include(T val) {
		return val.compareTo(getFilterVal()) > 0;
	}

}
