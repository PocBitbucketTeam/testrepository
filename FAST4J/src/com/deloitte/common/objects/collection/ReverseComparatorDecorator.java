package com.deloitte.common.objects.collection;

import java.util.Comparator;

/**
 * Reverses the result of the comparison of any Comparator it is used to decorate. Often
 * used to reverse the order of sorting implemented by the comparator.
 */
public class ReverseComparatorDecorator<T> extends AbstractComparatorDecorator<T> {

	public ReverseComparatorDecorator(Comparator<T> comparator) {
		super(comparator);
	}
	
	public int compare(T o1, T o2) {
		return -1 * getComparator().compare(o1, o2);
	}
	
	//TODO needs test case!!
	@Override
	public int hashCode() {
		return getComparator().hashCode() + 88;
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof ReverseComparatorDecorator
			&& getComparator().equals(((ReverseComparatorDecorator<T>)obj).getComparator()));
	}
}
