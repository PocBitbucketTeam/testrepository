package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * Filter with no filtering logic--will return true for any parameter to {@link #include include(T)}
 */
public final class NullFilter<T> implements Filter<T> {

	public boolean include(T theObject) {
		return true;
	}

	@Override
	public int hashCode() {
		return 793847;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof NullFilter; //all NullFilters are equal to one another
	}

}
