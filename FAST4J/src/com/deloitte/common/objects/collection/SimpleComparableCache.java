package com.deloitte.common.objects.collection;

import java.util.LinkedList;
import java.util.Queue;
import java.util.TreeMap;

import com.deloitte.common.interfaces.Cache;

/**
 * Implements a simple, bounded cache (default size 100) of elements. This cache implementation
 * is not memory sensitive, and will permanently persist all entries. This cache is most
 * appropriate for small caches that will not impose memory burdens where very fast and 
 * efficient access to the underlying cache contents is critical. The cache will perform
 * faster than {@link SimpleCache SimpleCache}, where it can be used.  Because this cache is not
 * memory sensitive, this should only be used for cache sizes which will not introduce memory
 * shortages in the application.
 */
//TODO needs a test case
public class SimpleComparableCache<K extends Comparable<K>, V> implements Cache<K, V> {

	private TreeMap<K, V> entries;
	private Queue<K> entriesOrder; //keeps the list of keys in FIFO order
	private int maxCacheSize;
	
	public SimpleComparableCache() {
		this(10);
	}

	public SimpleComparableCache(int maxCacheSize) {
		super();
		this.entries = new TreeMap<K, V>();
		this.entriesOrder = new LinkedList<K>();
		this.maxCacheSize = maxCacheSize;
	}

	public V get(K index) {
		return entries.get(index);
	}

	public V put(K index, V o) {
		if (entriesOrder.size() >= maxCacheSize){
			entries.remove(entriesOrder.remove());
		}
		return entries.put(index, o);
	}

	public boolean containsKey(K index) {
		return entries.containsKey(index);
	}

	public V remove(K index) {
		return entries.remove(index);
	}

	public void clear() {
		entries.clear();
	}

}
