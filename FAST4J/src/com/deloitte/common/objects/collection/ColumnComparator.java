package com.deloitte.common.objects.collection;


import java.util.Comparator;
import java.util.List;

/**
 * Provides a mechanism to sort a List of Lists by one of the columns. This
 * is typical behavior when constructing a Table of data and wishing to
 * resort the table based on one of the columns.
 * 
 * The column on which the table is being sorted must implement the Comparable
 * interface.
 */
//TODO I think the typing is wrong here.
public class ColumnComparator<T extends List<? extends Comparable<?>>> implements Comparator<T> {
	
	private int theColumn;
	private boolean ascending;
	
	/**
	 * The constructor does not use the Index, it uses the column number(ie 1,2,3)
	 */
	public ColumnComparator(int column)
	{
		this(column,true);
	}
	
	public ColumnComparator(int column, boolean ascending)
	{
		this.theColumn = column;
		this.ascending = ascending;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public int compare(T row1, T row2) {
		Comparable column1 = (Comparable) row1.get(theColumn-1);
		Comparable column2 = (Comparable) row2.get(theColumn-1);
		if (column1 != null && column2 != null) {
			if (!ascending) {
				return -1*column1.compareTo(column2);
			}
			return column1.compareTo(column2);
		}
		return 0;
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return theColumn + (ascending ? 5 : 10);
	}

	//TODO needs test case!!
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof ColumnComparator
				&& (theColumn == ((ColumnComparator)obj).theColumn)
				&& (ascending == ((ColumnComparator)obj).ascending));
	}
}
