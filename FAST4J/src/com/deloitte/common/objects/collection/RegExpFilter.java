package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * 
 * Determines if the parameter string matches with the constructor string expression.
 * 
 * @author SIDT Framework Team
 *
 */
public class RegExpFilter implements Filter<String> {

	String regex;
	
	public RegExpFilter(String regex){
		if (regex == null){
			throw new UncheckedApplicationException(this.getClass(),
						"Regular expression cannot be null");
		}
		this.regex = regex;
	}
	
	public String getRegex() {
		return regex;
	}

	public boolean include(String val) {
		return (val==null)? false: val.toString().matches(regex);
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return regex.hashCode() + 35;
	}

	//TODO needs test case!!
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof RegExpFilter
			&& regex.equals(((RegExpFilter)obj).getRegex()));
	}
}
