package com.deloitte.common.objects.collection;

import java.util.Comparator;

/**
 * This class in Abstract Base class for all the Comparator classes.
 * @author SIDT Framework Team
 */
public abstract class AbstractComparatorDecorator<T> implements Comparator<T> {

	private Comparator<T> comparator;
	
	public AbstractComparatorDecorator(Comparator<T> comparator) {
		super();
		this.comparator = comparator;
	}

	public Comparator<T> getComparator() {
		return comparator;
	}

}
