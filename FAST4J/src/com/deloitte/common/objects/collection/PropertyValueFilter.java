package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;

/**
 * Provides filtering on the specified property name of an object, returning
 * true only when the value of the property matches the provided Condition.
 */
public class PropertyValueFilter<T,S> implements Filter<T> {

	String theProperty;
	Filter<S> filter;
	
	/**
	 * @param theProperty the name of the property that will be tested
	 * @param filter the Filter defining the condition to test the value against
	 */
	public PropertyValueFilter(Filter<S> filter, String theProperty) {
		super();
		this.theProperty = theProperty;
		this.filter = filter;
	}
	
	public boolean include(T theObject) {
		try {
			@SuppressWarnings("unchecked")
			S obj = (S)ReflectionHelper.invokeAccessorMethod(theObject, ReflectionHelper.getAccessorMethod(theObject, theProperty),null);
			return filter.include(obj);
		} catch (CheckedApplicationException e) {
			throw new UncheckedApplicationException(this.getClass(), e);
		}
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return theProperty.hashCode() + filter.hashCode();
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof PropertyValueFilter
				&& theProperty.equals(((PropertyValueFilter<T,S>)obj).theProperty)
				&& filter.equals(((PropertyValueFilter<T,S>)obj).filter));
	}
}
