package com.deloitte.common.objects.collection;

/**
 * Determines the parameter object is less than to the constructor object.
 * Returns true if parameter object is less than else false.
 * <code>
 * 		boolean isSame = new LessThanFilter<Customer>(customer1).include(customer2);<br/>
 * 		boolean isSame = new LessThanFilter<Integer>(20).include(25);<br/>
 * </code>
 * 
 * @author SIDT Framework Team
 */
public class LessThanFilter<T extends Comparable<T>> extends AbstractCompareFilter<T> {

	public LessThanFilter(T filterVal) {
		super(filterVal);
	}

	@Override
	public boolean include(T val) {
		return val.compareTo(getFilterVal()) < 0;
	}

}
