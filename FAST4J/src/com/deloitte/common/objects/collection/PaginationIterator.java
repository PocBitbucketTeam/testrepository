package com.deloitte.common.objects.collection;

import java.util.Collection;
import java.util.NoSuchElementException;

import com.deloitte.common.interfaces.TwoWayIterator;

public class PaginationIterator<E> implements TwoWayIterator<E> {

	private Collection<E> theCollection = java.util.Collections.emptyList();
	private int pointer = -1;
	
	public PaginationIterator(Collection<E> c)
	{
		theCollection = c;
	}

	public boolean hasPrevious() 
	{
		return previousIndex() > -1;
	}

	@SuppressWarnings("unchecked")
	public E previous() 
	{
		if (!hasPrevious())
		{
			throw new NoSuchElementException();
		}
		return (E) theCollection.toArray()[--pointer];
	}

	public boolean hasNext() 
	{
		return nextIndex() <= (theCollection.size()-1);
	}

	@SuppressWarnings("unchecked")
	public E next() 
	{
		if (!hasNext())
		{
			throw new NoSuchElementException();
		}
		return (E) theCollection.toArray()[++pointer];
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("unchecked")
	public E current()
	{
		if (pointer == -1 || pointer > theCollection.size()-1)
		{
			throw new NoSuchElementException();
		}
		return (E) theCollection.toArray()[pointer];
	}

	public int nextIndex()
	{
		return pointer+1;
	}

	public int previousIndex()
	{
		return pointer-1;
	}

	public void add(Object arg0)
	{
		throw new UnsupportedOperationException();
		
	}

	public void set(Object arg0)
	{
		throw new UnsupportedOperationException();
	}
}
