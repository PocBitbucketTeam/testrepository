package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Filter;

/**
 * use {@link NullFilter NullFilter} instead
 */
@Deprecated 
public class DomainObjectFilter<T extends DomainObject> implements Filter<T> {
	
	public boolean include(T theObject)	{
		return true;
	}
}
