package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * This class is Abstract & Base class for all Comparison filters.
 * @author SIDT Framework Team
 */
public abstract class AbstractCompareFilter<T extends Comparable<T>> implements Filter<T> {
	
	private T filterVal;
	
	public AbstractCompareFilter(T filterVal) {
		this.filterVal = filterVal;
	}
	
	public T getFilterVal() {
		return filterVal;
	}

	public abstract boolean include(T val);

	//TODO needs test case!! REALLY, REALLY, REALLY needs a test case!!!
	@Override
	public int hashCode() {
		return this.getClass().hashCode() + filterVal.hashCode();
	}

	//TODO needs test case!! REALLY, REALLY, REALLY needs a test case!!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj.getClass().equals(this.getClass())
			&& filterVal.equals(((AbstractCompareFilter<T>)obj).getFilterVal()));
	}
}