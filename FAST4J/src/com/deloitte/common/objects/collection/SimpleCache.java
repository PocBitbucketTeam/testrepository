package com.deloitte.common.objects.collection;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.deloitte.common.interfaces.Cache;

/**
 * Implements a simple, bounded cache (default size 10) of elements. This cache implementation
 * is not memory sensitive, and will permanently persist all entries. This cache is most
 * appropriate for small caches for non-comparable elements that will not impose memory 
 * burdens. If the Cache keys are comparable, {@link SimpleComparableCache 
 * SimpleComparableCache} will provide better performance. Because this cache is not
 * memory sensitive, this should only be used for cache sizes which will not introduce memory
 * shortages in the application.
 */
//TODO needs a test case
public class SimpleCache<K, V> implements Cache<K, V> {

	private int theMaxSize;
	private LinkedHashMap<K, V> entries;
	
	public SimpleCache(){
		this(10);
	}
	
	public SimpleCache(int maxSize){
		this(maxSize, true); // make it an LRU Cache
	}
	
	protected SimpleCache(int maxsize, boolean accessOrder){
		this.theMaxSize = maxsize;
		entries = new LinkedHashMap<K, V>(maxsize, .75f, accessOrder){

			private static final long serialVersionUID = 5456789013654969L;

			/**
			 * @see LinkedHashMap.removeEldestEntry(Map.Entry)
			 */
			protected final boolean removeEldestEntry(Entry<K, V> eldest)			{
		        return size() > theMaxSize;
			}
		};
	}
	
	public V get(K index) {
		return entries.get(index);
	}

	public V put(K index, V o) {
		return entries.put(index, o);
	}

	public boolean containsKey(K index) {
		return entries.containsKey(index);
	}

	public V remove(K index) {
		return entries.remove(index);
	}

	public void clear() {
		entries.clear();
	}

}
