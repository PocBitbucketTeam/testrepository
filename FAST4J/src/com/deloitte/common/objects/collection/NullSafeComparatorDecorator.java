package com.deloitte.common.objects.collection;

import java.util.Comparator;

/**
 * Handles the position of the null objects of the comparison of any Comparator it is used to decorate. Often
 * used to figure out the order of null objects in sorting implemented by the comparator.
 * 
 * @author SIDT Framework Team
 */
public class NullSafeComparatorDecorator<T> extends AbstractComparatorDecorator<T> {
	
	boolean nullsHigh;

	/**
	 * equivalent to NullSafeComparatorDecorator(comparator, false)
	 */
	public NullSafeComparatorDecorator(Comparator<T> comparator) {
		this(comparator, false);
	}
	
	/**
	 * @param comparator the comparator to decorate
	 * @param nullsHigh when set to true, null values will be considered greater than
	 * any non-null value in the comparison
	 */
	public NullSafeComparatorDecorator(Comparator<T> comparator, boolean nullsHigh) {
		super(comparator);
		this.nullsHigh = nullsHigh;
	}
	
	public int compare(T o1, T o2) {
		if (o1 == null){
			if (o2 == null){
				return 0;
			} else {
				return nullsHigh ? 1 : -1;
			}
		} else if (o2 == null){
			return nullsHigh ? -1 : 1;
		} else {
			return getComparator().compare(o1, o2);
		}
	}
	
	//TODO needs test case!!
	@Override
	public int hashCode() {
		return getComparator().hashCode() + 188;
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof NullSafeComparatorDecorator
			&& getComparator().equals(((NullSafeComparatorDecorator<T>)obj).getComparator()));
	}
}
