package com.deloitte.common.objects.collection;


/**
 * This implementation of the Cache stores members and removes the least recently
 * accessed when the cache has reached its maximum size. The default constructor
 * creates a cache size of 100 entries.
 * <br><br>
 * This Cache implementation is memory sensitive as it uses SoftReferences which will
 * be reclaimed if the Garbage Collector needs to reclaim memory.
 */
public class LruCache<K, V> extends AbstractCache<K, V> {
	
	private static final long serialVersionUID = -4050382573564334719L;

	public LruCache()
	{
		super(true);
	}
	
	public LruCache(int maxsize)
	{
		super(maxsize, true);
	}
}