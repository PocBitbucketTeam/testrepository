package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * A Filter implementation that lets you build more complex filters by composing
 * other existing Filters together.
 */
public class OrFilter<T> implements Filter<T> {
	
	private Filter<T>[] filters;
	
	/**
	 * returns true only if and only if the passed filters are
	 * composed of return true
	 */
	public OrFilter(Filter<T>... filters){
		super();
		this.filters = filters;
	}

	public boolean include(T theObject) {
		boolean result = true;
		for(Filter<T> f: filters){
			result = f.include(theObject);
			if(result){
				break;
			}
		}
		return result;
	}

	//TODO needs test case!!
	//TODO Should compute this once!!!
	@Override
	public int hashCode() {
		int hash = 43;
		for(Filter<T> f: filters){
			hash = hash + f.hashCode();
		}
		return hash;
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (obj==this){
			return true;
		} else if (!(obj instanceof OrFilter)){
			return false;
		} else if (filters.length != ((OrFilter<T>)obj).filters.length ){
			return false;
		} else {
			for (int i = 0 ; i < filters.length ; i++){
				if (!filters[i].equals(((OrFilter<T>)obj).filters[i])){
					return false;
				}
			}
		}
		return true;
	}
}
