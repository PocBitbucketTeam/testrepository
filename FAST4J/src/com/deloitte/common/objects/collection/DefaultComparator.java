package com.deloitte.common.objects.collection;

import java.util.Comparator;

/**
 * 
 * Provides the default implementation for comparison of objects.
 * 
 * @author SIDT Framework Team
 * @param <T> is an object which implements Comparable
 */
public class DefaultComparator<T extends Comparable<T>> implements Comparator<T> {

	public int compare(T o1, T o2) {
		return o1.compareTo(o2);
	}

	@Override
	public int hashCode() {
		return 4004;
	}

	//TODO does this have a test case?
	@Override
	public boolean equals(Object obj) {
		return obj instanceof DefaultComparator;
	}

}
