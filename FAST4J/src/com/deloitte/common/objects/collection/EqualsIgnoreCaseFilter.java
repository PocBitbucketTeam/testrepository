package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * Determines if 2 strings are equal, case-insesitively
 */
public class EqualsIgnoreCaseFilter implements Filter<String> {
	
	protected String filterVal;
	
	public EqualsIgnoreCaseFilter(String filterVal){
		this.filterVal = (String)filterVal;
	}

	public boolean include(String val) {
		return val.equalsIgnoreCase(filterVal);
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return filterVal.hashCode() + 700;
	}

	//TODO needs test case!!
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof EqualsIgnoreCaseFilter
			&& filterVal.equals(((EqualsIgnoreCaseFilter)obj).filterVal));
	}
}
