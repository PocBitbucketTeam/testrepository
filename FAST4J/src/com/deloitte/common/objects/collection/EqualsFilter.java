package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Filter;

/**
 * Determines if 2 objects are equals or not.
 * Returns true if 2 objects are equals else false.
 * <code>
 * 		boolean isSame = new EqualsFilter<Customer>(customer1).include(customer2);	
 * </code>
 * 
 * @author SIDT Framework Team
 */
public class EqualsFilter <T> implements Filter<T> {
	
	protected T filterVal;
	
	public EqualsFilter(T filterVal){
		this.filterVal = filterVal;
	}

	public boolean include(T val) {
		return val.equals(filterVal);
	}

	//TODO needs test case!!
	@Override
	public int hashCode() {
		return filterVal.hashCode() + 60;
	}

	//TODO needs test case!!
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof EqualsFilter
			&& filterVal.equals(((EqualsFilter<T>)obj).filterVal));
	}
}
