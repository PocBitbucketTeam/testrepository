package com.deloitte.common.objects.collection;

import com.deloitte.common.interfaces.Cache;

/**
 * Null Cache implementation which can be used to disable any caching behavior
 * in any consumer of the Cache interface.
 */
//TODO needs a test case
public class NullCache<K, V> implements Cache<K, V> {

	public V get(K index) {
		return null;
	}

	public V put(K index, V o) {
		return null;
	}

	public boolean containsKey(K index) {
		return false;
	}

	public V remove(K index) {
		return null;
	}

	public void clear() { }

}
