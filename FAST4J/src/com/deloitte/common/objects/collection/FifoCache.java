package com.deloitte.common.objects.collection;


/**
 * This implementation of the Cache stores members and removes the first entry entered
 * when the cache has reached its maximum size. The default constructor creates a cache
 * size of 100 entries.
 * <br><br>
 * This Cache implementation is memory sensitive as it uses SoftReferences which will
 * be reclaimed if the Garbage Collector needs to reclaim memory.
 */
public class FifoCache<K, V> extends AbstractCache<K, V> {
	
	private static final long serialVersionUID = 7392147528220309624L;

	public FifoCache()
	{
		super(false);
	}
	
	public FifoCache(int maxsize)
	{
		super(maxsize, false);
	}
	
}
