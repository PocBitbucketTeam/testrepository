package com.deloitte.common.objects.collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.interfaces.Validation;
import com.deloitte.common.objects.framework.Error;

/**
 * Range objects are used to store two Comparable objects that represent a from/to
 * It makes no assumptions that the Ranges are in/exclusive, just that the first object
 * is not greater than the second.
 * 
 * @author SIDT Framework Team
 *
 */
public class Range<T extends Comparable<T>> implements Validation
{
	private T object1;
	private T object2;

	public Range(T object1, T object2)
	{
		this.object1 = object1;
		this.object2 = object2;
	}
	
	public enum ErrorType{
		NULLOBJECT, OBJECT2LESSTHAN
	}
	
	public Collection<Error> getValidationErrors(Intent intent)
	{
		return getValidationErrors();
	}
	
	/**
	 *  Will return Errors with the following types:
	 * 		<br>-NULLOBJECT - Object is null (either object1 or object2)
	 * 		<br>-OBJECT2LESSTHAN - Object2 is less than Object1
	 */
	protected Collection<Error> getValidationErrors()
	{
		List<Error> theErrors = new ArrayList<Error>();
		if (object1 == null || object2 == null)
		{
			theErrors.add(new Error(ErrorType.NULLOBJECT.name()) );
		}
		else if (object1.compareTo(object2) > 0)
		{
			theErrors.add(new Error(ErrorType.OBJECT2LESSTHAN.name()) );
		}
		return theErrors;
	}

}
