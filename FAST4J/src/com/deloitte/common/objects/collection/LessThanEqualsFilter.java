package com.deloitte.common.objects.collection;

/**
 * Determines the parameter object is less than or equals to the constructor object.
 * Returns true if parameter object is less than or equal else false.
 * <code>
 * 		boolean isSame = new LessThanEqualsFilter<Customer>(customer1).include(customer2);<br/>
 * 		boolean isSame = new LessThanEqualsFilter<Integer>(20).include(25);<br/>
 * </code>
 * 
 * @author SIDT Framework Team
 */
public class LessThanEqualsFilter<T extends Comparable<T>> extends AbstractCompareFilter<T> {

	public LessThanEqualsFilter(T filterVal) {
		super(filterVal);
	}

	@Override
	public boolean include(T val) {
		return val.compareTo(getFilterVal()) <= 0;
	}

}
