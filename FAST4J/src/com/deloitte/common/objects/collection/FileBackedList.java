package com.deloitte.common.objects.collection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * An implementation of the List interface which is backed by a temporary flat
 * file. This allows population of a List with a number of entries which would
 * otherwise exceed the available heap space in the JVM and thus create an
 * OutOfMemoryError.
 * <P>
 * 
 * This class should be used sparingly. Because it accesses an underlying file
 * for it's entries, access is much slower than with regular in-memory
 * collections and concurrent use many instances could have a very adverse
 * impact on performance due to the large amount of disk I/O created. Use of
 * this class should be reserved only for exception cases where massive amounts
 * of data need to be populated in a collection.
 * <P>
 * 
 * All objects passed to the List must be serializable, as the serialization
 * process is used to write the objects to the underlying temporary file. Per
 * the documentation for OutputStreamWriter, the reset() method needs to be
 * called periodically to prevent all the passed contents being stored in the
 * OutputStreamWriter write cache. The interval at which this is done can be set
 * by the constructors, and specifies that after the nth Object is added to the
 * List via add() or addAll() (n corresponding to the reset interval), the
 * OutputStreamWriter cache will be flushed via reset().
 * <P>
 * 
 * If not otherwise specified in the constructors, the temporary file created
 * will have a prefix of "list_" and a suffix of ".dat". It will also have a
 * default reset interval of 1000. If a directory is not specified to store the
 * temp file, it will be stored in the default temp directory in accordance with
 * the File.createTempFile() methods.
 * <P>
 * 
 * The List and the Iterator implementation returned by iterator() are very
 * limited. Calling anything other than add(), addAll(), isEmpty(), size(),
 * iterator(), or clear() methods from the List interface or hasNext() or next()
 * on its Iterator will throw an UnsupportedOperationException. In essence, all
 * you can accomplish with this List implementation is to populate it and then
 * iterate through it in a forward-only, read-only fashion, and then clear all
 * entries when done. Using this class, you must finish adding all your objects
 * to it before you start reading. When you create the list, it is open for
 * writes via add() and addAll(). Once you retrieve the iterator or call
 * closeForWriting(), however, any calls to add() or addAll() with throw an
 * exception.
 * <P>
 * 
 * Because instances of this class are backed by files, they are not serializable
 * and cannot be used in any disconnected fashion. In addition, once objects are
 * added to the List, any changes to the added objects will not be reflected in
 * what you will later retrieve from the List (i.e. there are no side-effects).
 * This is because when you add to this List the object is immediately
 * serialized and written to a file to be de-serialized and returned later. Thus
 * modifying the object will not be reflected in the underlying file storage,
 * and the Object will be returned from the iterator in the exact same state it
 * was when when added to the List.
 * <P>
 * 
 * When done using the List, you should call clear() on it to remove the
 * temporary file used to back the list. Under some execution scenarios the
 * class will automatically be able to clean up after itself, and some JVMs will
 * be able to automatically delete the temp file when they shut down, but in
 * many environments and in some execution scenarios the deleteFile() method
 * must be called in order for proper cleanup. Once the jvm has terminated, the
 * temp file may also be manually deleted from the file system.
 */
public class FileBackedList<E> implements List<E> {
	
	private File file;
	private int resetInterval;

	private ObjectOutputStream out;
	private ObjectInputStream in;
	private long size = 0;
	private boolean isClosedForWriting = false;
	private boolean calledIteartor = false;
	
	private static final int DEFAULT_RESET_INTERVAL = 1000;
	

	public FileBackedList() {
		this(null);
	}
	
	public FileBackedList(int resetInterval) {
		this(null, resetInterval);
	}
	
	public FileBackedList(String directoryName) {
		this(directoryName, DEFAULT_RESET_INTERVAL);
	}
	
	public FileBackedList(String directoryName, String prefix, String suffix) {
		this(directoryName, prefix, suffix, DEFAULT_RESET_INTERVAL);
	}
	
	public FileBackedList(String directoryName, int resetInterval) {
		this(directoryName, "list_", ".dat", resetInterval);
	}
	
	/**
	 * @param directoryName the directory in which to create the temp file used to back the List
	 * @param prefix the prefix to use for the temp file
	 * @param suffix the suffix to use for the temp file
	 * @param resetInterval the reset interval for the file writing process
	 */
	public FileBackedList(String directoryName, String prefix, String suffix, int resetInterval) {
		File tempFileDirectory = null;
		if (directoryName != null){
			tempFileDirectory = new File(directoryName);
			if(!tempFileDirectory.exists()){
				throw new UncheckedApplicationException(this.getClass(),
						"The provided directory path does not exist");
			}
			if(!tempFileDirectory.isDirectory()){
				throw new UncheckedApplicationException(this.getClass(),
						"The provided directory path does not represent a directory");
			}
		}
		try {
			file = File.createTempFile(prefix, suffix, tempFileDirectory);
			file.deleteOnExit(); //this often doesn't work, but it can't hurt
			this.resetInterval = resetInterval;
			out = new ObjectOutputStream(new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			throw new UncheckedApplicationException(this.getClass(),
					"An unexpected exception occured while opening the file for writing",
					e);
		} catch (IOException e) {
			throw new UncheckedApplicationException(this.getClass(),
					"An unexpected exception occured while opening the file for writing",
					e);
		}
	}
	
	/*
	 * Closed the OutputStream used to write the file and disposes of it properly.
	 * Also records that the List is now closedForWriting and that any further additions
	 * to the list will result in an error.
	 */
	private void closeForWriting() {
		try {
			if (!isClosedForWriting){
				out.flush();
				out.close();
				out = null;
			}
		} catch (IOException e) {
			throw new UncheckedApplicationException(this.getClass(),
					"An unexpected exception occured while closing the file for writing",
					e);
		}
		isClosedForWriting = true;
	}
	
	private void cleanupInputStreamAndFile() {
		if (in != null){
			try {
				in.close();
			} catch (IOException e) {
				throw new UncheckedApplicationException(this.getClass(),
						"An unexpected exception occured while closing the input stream",
						e);
			}
			in = null;
		}
		file.delete();
	}
	
	/**
	 * Allows access to the underlying file. Modification of the file while
	 * still accessing the FileBackedList may result in unpredictable results.
	 * 
	 * @return a handle to the temp file used to store the List contents
	 */
	public File getFile(){
		return file;
	}

	/**
	 * @see java.util.List#size()
	 */
	public int size() {
		if (size > Integer.MAX_VALUE){
			return Integer.MAX_VALUE;
		} else {
			return (int)size;
		}
	}

	/**
	 * @see java.util.List#isEmpty()
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public boolean contains(Object o) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see java.util.List#iterator()
	 */
	public Iterator<E> iterator() {
		if (calledIteartor){
			throw new UncheckedApplicationException(FileBackedList.class,
					"iterator() can only be called once on a FileBackedList");
		}
		calledIteartor = true;
		return new Iterator<E>(){
			
			private long pointerLocation = 1;
			
			{
				try {
					closeForWriting();
					if (isEmpty()){
						//if collection is empty, delete the temp file now
						cleanupInputStreamAndFile();						
					} else {
						//open the file for reading
						in = new ObjectInputStream(new FileInputStream(file));
					}
				} catch (IOException e) {
					throw new UncheckedApplicationException(FileBackedList.class,
							"There was an error while initializing the read stream",
							e);
				}
			}
			
			/**
			 * @see java.util.Iterator#hasNext()
			 */
			public boolean hasNext() {
				return pointerLocation <= size;
			}

			/**
			 * @see java.util.Iterator#next()
			 */
			@SuppressWarnings("unchecked")
			public E next() {
				try {
					if (pointerLocation > size){
						throw new NoSuchElementException("There are no more elements in the List");
					}
					pointerLocation++;
					return (E)in.readObject();
				} catch (IOException e) {
					throw new UncheckedApplicationException(FileBackedList.class,
							"There was an error while reading the file",
							e);
				} catch (ClassNotFoundException e) {
					throw new UncheckedApplicationException(FileBackedList.class,
							"Unable to locate class that was previously stored",
							e);
				} finally {
					//if we just read the last element from the file, close and delete the file
					if (pointerLocation > size){
						cleanupInputStreamAndFile();
					}
				}
			}

			/**
			 * Throws and UnsupportedOperationException
			 */
			public void remove() {
				throw new UnsupportedOperationException();
			}
			
		};
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public <T> T[] toArray(T[] a) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see java.util.List#add(java.lang.Object)
	 */
	public boolean add(E o) {
		if (isClosedForWriting){
			throw new UncheckedApplicationException(this.getClass(),
					"The file is now closed for writing and elements can no longer be added.");
		}
		try {
			out.writeObject(o);
			if (resetInterval != 0 && (size % resetInterval == 0)){
				out.reset();
			}
		} catch (IOException e) {
			throw new UncheckedApplicationException(FileBackedList.class,
					"There was an error while writing to the file",
					e);
		}
		size++;
		return true;
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public boolean containsAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	public boolean addAll(Collection<? extends E> c) {
		boolean changed = false;
		for (E object : c){
			add(object);
			changed = true;
		}
		return changed;
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public boolean addAll(int index, Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Deletes the temporary backing file from the file system. It is
	 * recommended to call this in a finally block in your program when you are
	 * done with the list. If not done, the file may be left on the file system
	 * and must be deleted manually (depending on your JVM).
	 */
	public void clear() {
		closeForWriting();
		cleanupInputStreamAndFile();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public E get(int index) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public E set(int index, E element) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public void add(int index, E element) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public E remove(int index) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public int indexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public int lastIndexOf(Object o) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public ListIterator<E> listIterator() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public ListIterator<E> listIterator(int index) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Throws and UnsupportedOperationException
	 */
	public List<E> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException();
	}
	
	protected void finalize() throws Throwable {
		try {
			clear();
		} finally {
			super.finalize();
		}
	}

}
