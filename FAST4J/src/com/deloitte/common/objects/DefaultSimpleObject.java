package com.deloitte.common.objects;

import com.deloitte.common.interfaces.SimpleObject;


/**
 * Use {@link com.deloitte.common.objects.reftable.DefaultReferenceTableRow DefaultReferenceTableRow}
 * instead <br><br>
 * 
 * A Simple immutable Object that only provides name/value pair information. This object
 * is considered a second class business object as it can not be persisted by
 * itself or provide unique identification. It can be case sensitive or case insensitive
 * NOTE: Setters are private as these objects are immutable!!!
 * <br><br>
 * Example Usage: 
 * 	SimpleObject so = new DefaultSimpleObject("State","AK","Alaska");
 * <br>-or-<br>
 *  SimpleObject so = new DefaultSimpleObject("State","AK","Alaska",false);
 *  <br><br>
 * @author SIDT Framework Team
 *
 */
@Deprecated
public final class DefaultSimpleObject implements SimpleObject
{
	private String type;
	private String name;
	private String value;
	private boolean casesensitive;
	private String thisString;
	private int		hashcode = -1;
	
	public DefaultSimpleObject(String type, String name,String value)
	{
		this(type,name,value,true);
	}
	
	public DefaultSimpleObject(String type, String name, String value, boolean casesensitive)
	{
		setType(type);
		setName(name);
		setValue(value);
		setCaseSensitive(casesensitive);
	}
	
	public final String getType()
	{
		return type;
	}
	
	public final String getName()
	{
		return name;
	}

	public final String getValue()
	{
		return value;
	}


	private void setType(String type)
	{
		this.type = type;
	}
	
	private void setName(String name)
	{
		this.name = name;
	}


	private void setValue(String value)
	{
		this.value = value;
	}

	private void setCaseSensitive(boolean casesensitive)
	{
		this.casesensitive = casesensitive;
	}
	
	public final boolean equals(Object o)
	{
		if (o == null || !(o instanceof DefaultSimpleObject)) {
			return false;
		} else {
			return this.compareTo((DefaultSimpleObject)o)==0;
		}
	}
	
	public final int hashCode()
	{
		if (hashcode == -1) 
		{
			if (!casesensitive)
			{
				hashcode = 17;
				hashcode = 37 * hashcode + getType().toLowerCase().hashCode();
				hashcode = 37 * hashcode + getName().toLowerCase().hashCode();
				hashcode = 37 * hashcode + getValue().toLowerCase().hashCode();
			} else {
				hashcode = 17;
				hashcode = 37 * hashcode + getType().hashCode();
				hashcode = 37 * hashcode + getName().hashCode();
				hashcode = 37 * hashcode + getValue().hashCode();
		    }
		}
		return hashcode;
	}

	public int compareTo(SimpleObject o)
	{
		if (thisString == null)
		{
			thisString = getType()+getName()+getValue();
			thisString = (thisString == null ? "" : thisString);
		}
		String thatString = o.getType() + o.getName() + o.getValue();
		thatString = (thatString == null ? "" : thatString);
		
		if (!casesensitive)
		{
			return thisString.toLowerCase().compareTo(thatString.toLowerCase());
		}
		return thisString.compareTo(thatString);
	}
	
	public String toString()
	{
		return getType() + "/" + getName() + "/" + getValue();
	}
	
}
