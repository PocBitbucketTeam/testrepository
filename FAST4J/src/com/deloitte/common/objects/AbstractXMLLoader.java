package com.deloitte.common.objects;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * This class build a XML Document object by reading a XML File. This class
 * also provides default implementations of registering 
 * or loading from a Configurations file.
 * 
 * @author SIDT Framework Team
 *
 */
public abstract class AbstractXMLLoader implements Loader {

	private String fileName; 
	private Document document;
	
	public AbstractXMLLoader(String fileName){
		this.fileName = fileName;
	}
	
	public AbstractXMLLoader(Document document){
		this.document = document;
	}
	
	public Document getDocument(){
		if(document==null){
			try {
				DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				document = builder.parse(getClass().getResourceAsStream(fileName));
			} catch (ParserConfigurationException e) {
				throw new UncheckedApplicationException(getClass(), "Parser Configuration Error ", e);
			} catch (IOException e) {
				throw new UncheckedApplicationException(getClass(), "Parser Configuration Error ", e);
			} catch (SAXException e) {
				throw new UncheckedApplicationException(getClass(), "Parser Configuration Error ", e);
			}
		}
		return document;
	}
	
	public void loadValues() throws CheckedApplicationException {
		loadValues(getDocument());
	}
	
	protected abstract void  loadValues(Document document) throws CheckedApplicationException;

}
