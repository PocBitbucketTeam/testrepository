package com.deloitte.common.objects.framework;

import java.util.logging.Logger;

public class UncheckedApplicationException extends RuntimeException 
{
	private static final long serialVersionUID = 1L;
	protected final static String DEFAULT_WARNING = "An unknown error has occured";
	
	public UncheckedApplicationException(Class<?> c)
	{
		super(DEFAULT_WARNING);
		Logger.getLogger(c.getName()).severe(DEFAULT_WARNING);
	}
	

	public UncheckedApplicationException(Class<?> c,String message)
	{
		super(message);
		Logger.getLogger(c.getName()).severe(message);
	}

	public UncheckedApplicationException(Class<?> c,Throwable theCause)
	{
        super(theCause.getMessage(),theCause);
		Logger.getLogger(c.getName()).severe(theCause.getMessage());
 	}
	
	public UncheckedApplicationException(Class<?> c,String message,Throwable theCause)
	{
        super(message,theCause);
		Logger.getLogger(c.getName()).severe(message + ", " + theCause.getMessage());
 	}
	
	protected UncheckedApplicationException(String message,Exception theCause)
	{
		super(message,theCause);
		Logger.getLogger(this.getClass().getName()).severe(message + ", " + theCause.getMessage());

	}

}
