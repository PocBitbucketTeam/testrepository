package com.deloitte.common.objects.framework;

import com.deloitte.common.interfaces.Intent;

/**
 * The Validation Intent enum describes the type of Validation the Domain Object
 * should provide. 
 * 
 * As an example, in cases where the DomainObject might be about to be
 * persisted, versus when it is being deleted will require different types
 * of rules to be enforced.
 * 
 */
public enum ValidationIntent implements Intent {
	NONE,
	READ, 
	DELETE, 
	UPDATE, 
	INSERT;
}