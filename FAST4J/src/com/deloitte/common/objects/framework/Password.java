package com.deloitte.common.objects.framework;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.interfaces.Validation;
import com.deloitte.common.util.Arrays;
import com.deloitte.common.util.Strings;
import com.deloitte.common.objects.framework.Error;

/**
 * Password class provides a method of securely handling passwords. It will
 * not return a clear text password to a caller, only a Message Digest (one way hash).
 * This allows passwords to be compared, but not compromised if used in the following way:<br><br>
 * Password stored = new Password("my Password");<br>
 * Password checking = new Password("my Password");<br>
 * boolean isValid = checking.equals(stored);<br>
 * @author SIDT Framework Team
 *
 */
public class Password implements Validation
{
	private int 	hashcode;
	private String 	clearTextPassword;
	private String 	hashedPassword;
	private boolean shouldValidate = true;
	
	public enum ErrorType{
		INVALIDPWDLEN, PWDSPCLCHAR, INVALIDPWDCASE
	}
	
	/**
	 * Assumes the password provided is in clear text, it will be hashed when
	 * it is received.
	 */
	public Password(String password)
	{
		this(password,true);
	}
	

	public Password(String password,boolean clearText)
	{
		if (clearText)
		{
			clearTextPassword = password;
			getPassword();
		} else
		{
			shouldValidate = false;
			hashedPassword = password;
		}
	}
	
	public Collection<Error> getValidationErrors(Intent intent)
	{
		return getValidationErrors();
	}
	
	/**
	 * Validation rules only invoked when a cleartext password is available
	 * <br><br> 
	 * Simple Password validation rules:<br><br>
	 * <li>Password Length greater than 6
	 * <li>Must contain 1 special character
	 * <li>Must contain mixed case
	 * <br><br>Rules can be overriden by application by overriding method.
	 * <br><br>
	 * Will return Errors with the following types:
	 * 		<br>-INVALIDPWDLEN - the length of the password is less than 6 characters
	 * 		<br>-PWDSPCLCHAR - special characters in the password 
	 * 		<br>-INVALIDPWDCASE - character case of the password (upper and lower case)
	 */
	protected Collection<Error> getValidationErrors() {
		List<Error> theErrors = new ArrayList<Error>();
		if ( shouldValidate )
		{
		
			if (Strings.isEmpty(clearTextPassword) || clearTextPassword.length() < 6)
			{	
				// Check Length great than 6
				theErrors.add(new Error(ErrorType.INVALIDPWDLEN.name()));
			} else
			{
				// Check for Special Characters, lower/upper case
				boolean containsSpecialCharacter = false;
				boolean containsLowerCase = false;
				boolean containsUpperCase = false;
				
				char[] specialCharacters = Strings.getSpecialCharacters();
				char[] lowerCase = Strings.getLowerCaseCharacters();
				char[] upperCase = Strings.getUpperCaseCharacters();
				
				for (int i=0; i < specialCharacters.length && containsSpecialCharacter == false; i++)
				{
					containsSpecialCharacter = Arrays.contains(clearTextPassword.toCharArray(),specialCharacters[i]) > 0 ? true : false;
				}
				
				for (int i=0; i < lowerCase.length && containsLowerCase == false; i++)
				{
					containsLowerCase = Arrays.contains(clearTextPassword.toCharArray(),lowerCase[i]) > 0 ? true : false;
					
				}
				
				for (int i=0; i < upperCase.length && containsUpperCase == false; i++)
				{
					containsUpperCase = Arrays.contains(clearTextPassword.toCharArray(),upperCase[i]) > 0 ? true : false;
				}
				
				if (!containsSpecialCharacter)
				{
					theErrors.add(new Error(ErrorType.PWDSPCLCHAR.name()));
				}
				
				if (! (containsLowerCase && containsUpperCase))
				{
					theErrors.add(new Error(ErrorType.INVALIDPWDCASE.name()));
				}
			}
		}
		return theErrors;
	}

	public String getPassword()
	{
		if (Strings.isEmpty(hashedPassword))
		{
				
			MessageDigest md = null;
			try 
			{
				md = MessageDigest.getInstance("SHA");
			} catch(NoSuchAlgorithmException nsae)
			{
				throw new UncheckedApplicationException(getClass(),"SHA Algorithm not found",nsae);
			}
			md.update(clearTextPassword.getBytes());
			byte[] digest = md.digest();
			
			StringBuffer hexString = new StringBuffer();
			String plainText = "";
			for (int i = 0; i < digest.length; i++) 
			{
			        plainText = Integer.toHexString(0xFF & digest[i]).toUpperCase();
				if (plainText.length() < 2) 
				{
					plainText = "0" + plainText;
			    }
				hexString.append(plainText);
			}
			hashedPassword = hexString.toString();
		}

		return hashedPassword;
	}
	
	public boolean equals(Object o)
	{
		return o != null ? getPassword().equals(((Password)o).getPassword()) : false;
	}

	public int hashCode()
	{
		if (hashcode == 0)
		{
			hashcode = getPassword().hashCode(); 
		}
		return hashcode;
	}
}
