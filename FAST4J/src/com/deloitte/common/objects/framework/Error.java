package com.deloitte.common.objects.framework;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
/**
 * Error class is used to return non-Exception conditions, but conditions that
 * were anticipated and can be resolved programatically. An example would be a
 * Data Validation problem with input from a user.
 * Suitable constructor can be used to create the object, based on the parameters available.
 */
public final class Error {
	
	private final String entity;
	private final String property; 
	private final String errorType;
	private final List<Object> params;
	
	public enum ErrorType{
		REQUIRED, MAXLENGTH, MINLENGTH, TOOBIG, TOOSMALL, WRONGFORMAT, EMPTY, INVALID, NONZERO, NONNEGATIVE, NOTSUPPORTED
	}
	
	public Error(String errorType){
		this(errorType, Collections.emptyList());
	}
	
	public Error(String errorType, List<Object> params){
		this(null, errorType, params);
	}
	
	public Error(String entity, String errorType){
		this(entity, errorType, Collections.emptyList());
	}
	
	public Error(String entity, String errorType, List<Object> params){
		this(entity, null, errorType, params);
	}
	
	public Error(String entity, String property, String errorType){
		this(entity, property, errorType, Collections.emptyList());
	}
	
	/**
	 * This Constructor accepts these parameters to generate the message key
	 * @param entity	the base name of the resource bundle, a fully qualified class name
	 * @param property	the field name 
	 * @param errorType error type from ErrorType class
	 * @param params	list of values for the place holders in message.
	 */
	public Error(String entity, String property, String errorType, List<Object> params){
		if (errorType == null){
			throw new UncheckedApplicationException(this.getClass(), "errorType cannot be null");
		}
		if (params == null){
			throw new UncheckedApplicationException(this.getClass(), "params cannot be null");
		}
		this.entity = entity;
		this.property = property;
		this.errorType = errorType;
		this.params = params;
	}
	
	public String getProperty(){
		return this.property;
	}
	
	public String getEntity(){
		return this.entity;
	}
	
	public String getErrorType(){
		return this.errorType;
	}	
	
	public List<Object> getParams(){
		return this.params;
	}
	
	/**
	 * @return a key defining the Error that occured in the format
	 * 		[&lt;entity&gt;.][&lt;property&gt;.]&lt;errorType&gt;, where entity and property
	 * 		will be omitted if null. For example, an Error with entity value of
	 * 		"order", a null property property, and error code property of "INVALID"
	 * 		will return "order.INVALID"
	 */
	public String getMessageKey(){
		StringBuffer key = new StringBuffer("");
		if (entity != null){
			key.append(entity);
			key.append(".");
		}
		if (property != null){
			key.append(property);
			key.append(".");
		}
		key.append(errorType);
		return key.toString();
	}
	
	/**
	 * This method takes n number of parameters, adds those values to a list and returns the list of Objects.
	 * @param values objects 
	 * @return list of objects
	 */
	public static List<Object> createParams(Object... values){
		List<Object> params = new ArrayList<Object>();
		for(Object paramValue : values){
			params.add(paramValue.toString());
		}
		return params;
	}
	
	public String toString(){
		return "Error with key=" + getMessageKey() + " and "
						+ params.size() + " parameters";
	}
}
