package com.deloitte.common.objects.framework;

import java.util.logging.Logger;

public class CheckedApplicationException extends Exception 
{
	private static final long serialVersionUID = 1L;
	protected final static String DEFAULT_WARNING = "An unknown error has occured";
	
	CheckedApplicationException()
	{
		// Can not instantiate without a classname
	}
	
	
	public CheckedApplicationException(Class<?> c)
	{
		super(DEFAULT_WARNING);
		Logger.getLogger(c.getName()).severe(DEFAULT_WARNING);
	}
	
	public CheckedApplicationException(Class<?> c,String message)
	{
		super(message);
		Logger.getLogger(c.getName()).severe(message);
	}

	public CheckedApplicationException(Class<?> c,String message,Exception theCause)
	{
        super(message,theCause);
		Logger.getLogger(c.getName()).severe(message + ", " + theCause.getMessage());
 	}

	// Should be the rarest used Constructor, no specificity in the creator.
	protected CheckedApplicationException(String message)
	{
		super(message);
		Logger.getLogger(this.getClass().getName()).severe(message);
	}

	protected CheckedApplicationException(String message,Exception theCause)
	{
		super(message,theCause);
		Logger.getLogger(this.getClass().getName()).severe(message + ", " + theCause.getMessage());

	}
	
}
