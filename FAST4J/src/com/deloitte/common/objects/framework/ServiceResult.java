package com.deloitte.common.objects.framework;

import java.util.Collection;

/**
 * Used to model the return type of an invocation which can return either a Java object
 * result (such as a domain object or collection) or a collection of Error objects
 * @param <T> the resulting return 
 */
public class ServiceResult <T> {

	T result;
	Collection<Error> errors;
	
	public ServiceResult(T result) {
		this(result, null);
	}

	public ServiceResult(Collection<Error> errors) {
		this(null, errors);
	}

	public ServiceResult(T result, Collection<Error> errors) {
		super();
		this.result = result;
		this.errors = errors;
	}

	public T getResult() {
		return result;
	}

	public Collection<Error> getErrors() {
		return errors;
	}
	
	/**
	 * @return true if there is an error is the service execution
	 */
	public boolean isError(){
		return (this.errors != null) && (this.errors.size() > 0);
	}

}
