package com.deloitte.common.objects.framework;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.Generation;
import com.deloitte.common.interfaces.UUID;

public final class UUIDGenerator implements Generation<UUID>
{
		private static final Logger logger = Logger.getLogger(UUIDGenerator.class.getName());

		private static Map<String, UUIDGenerator> generators = new HashMap<String, UUIDGenerator>();
		private static int DefaultMaximumSuffix = 9999;
		private static int DefaultMaximumRandom = 99;
		
		private static final Object constructionLock = new Object(); /*lock to prevent multiple versions
											of UUIDGenerator for same pattern being created */
		private final Object incrementLock = new Object(); //lock to ensure safe increment of counter

		private String[] padArray;
		private transient int suffix;
		private Random random = new Random();
		private int maxSuffix;
		private int maxRandom;


		private UUIDGenerator(int maximumSuffix, int maximumRandom)
		{
			maxSuffix = maximumSuffix;
			maxRandom = maximumRandom;
			initiatializePadArray();
		}

		/**
		 * This method is synchronized to avoid the race condition in creating new Instance
		 */
		public static UUIDGenerator getInstance(int maxSuffix, int maxRandom)
		{
			String key = maxSuffix + "/" + maxRandom;

			synchronized (constructionLock){
				if (generators.get(key) == null){
					logger.info("Creating new UUIDGenerator for type = " + key);
					generators.put(key, new UUIDGenerator(maxSuffix,maxRandom));
				}
			}

			return (UUIDGenerator)generators.get(key);
		}

		public static UUIDGenerator getInstance()
		{
			return getInstance(DefaultMaximumSuffix,DefaultMaximumRandom);
		}

		public UUID getNextValue()
		{
			return new OID(getNext());
		}

		/**
		 * This method is  synchronized to avoid the race condition in creating UUIDs
		 */
		protected  String getNext() {
			long timestamp ;
			int tempSuffix;
			String part2 ;
			String part3 ;

			timestamp = System.currentTimeMillis();
			
			synchronized(incrementLock){
				if (suffix < maxSuffix){
					suffix++;
				} else {
					suffix = 0;
				}
				tempSuffix = suffix;
			}
			
			part2  = (maxSuffix > 0 ? format(tempSuffix, String.valueOf(maxSuffix).length()) : "");
			part3  = (maxRandom > 0 ? format(getRandom(), String.valueOf(maxRandom).length()) : "");

			if (logger.isLoggable(Level.FINE)){
				logger.fine("Part 1 = " + timestamp +" Part 2 = "+ part2 + " Part 3 = " + part3);
			}

			return timestamp + part2 + part3;
		}

		private int getRandom(){
			return random.nextInt(maxRandom);
		}

		private String format(int theInt, int theLength)
		{
			int intLength = ("" + theInt).length();
			return padArray[theLength-intLength]+theInt;
		}

		private void initiatializePadArray()
		{
			int padSize = maxRandom > maxSuffix ? ("" + maxRandom).length() : (""+maxSuffix).length();
			System.err.println("Maximum PadSize needed " + padSize);
				padArray = new String[padSize];
				padArray[0] = "";
				for (int i=1; i < padSize; i++)
				{
					padArray[i] = padArray[i-1] + "0";
				}
		}
		
		@Override
		public String toString() {
			return "UUIDGenerator [maxSuffix=" + maxSuffix + ", maxRandom=" + maxRandom + "]";
		}

		
}
