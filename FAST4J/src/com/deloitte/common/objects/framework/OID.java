package com.deloitte.common.objects.framework;

import java.io.Serializable;
import java.math.BigDecimal;

import com.deloitte.common.interfaces.UUID;

public class OID implements UUID,Serializable
{
	private Object s;
	private int		hashcode;
	private static UUIDGenerator generator = UUIDGenerator.getInstance();

	public OID() {
		this.s = generator.getNext();
	}

	public OID(Object o)
	{
		this.s = o;
	}

	public OID(String s)
	{
		this.s = s;
	}

	public OID(long l)
	{
		this(l+"");
	}

	public OID(BigDecimal bd)
	{
		this(bd.longValue()+"");
	}

	public Object getValue()
	{
		return s;
	}

	public int compareTo(UUID o)
	{
		return this.getValue().toString().compareTo(o.getValue().toString());
	}


	public int hashCode()
	{
		if (hashcode == 0)
		{
			hashcode = s.hashCode();
		}
		return hashcode;
	}

	public String toString()
	{
		return s.toString();
	}

	public boolean equals(Object o) {
		if (o == null || !(o instanceof OID)) {
			return false;
		} else {
			return this.compareTo((UUID)o)==0;
		}
	}

	/**
	   * Determines if a de-serialized file is compatible with this class.
	   *
	   * Maintainers must change this value if and only if the new version
	   * of this class is not compatible with old versions. See Sun docs
	   * for <a href=http://java.sun.com/products/jdk/1.1/docs/guide
	   * /serialization/spec/version.doc.html> details. </a>
	   */
	private static final long serialVersionUID = -4656612676319318190L;

}
