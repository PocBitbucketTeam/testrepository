package com.deloitte.common.objects.framework;

import com.deloitte.common.interfaces.UUID;

/**
 * 
 * A default implementation of UUID to represent no Identification (Null) of a DomainObject.
 *
 */
public final class NullUUID implements UUID {

	private static final long serialVersionUID = 8349344680930207002L;
	
	private int hashCode = 0;

	public NullUUID() {
	}

	public Object getValue() {
		return null;
	}

	public int compareTo(UUID o) {
		return (o.getValue()==this.getValue())?0:-1;
	}
	
	public boolean equals(Object o){
		if (this == o){
			return true;
		} else if (o == null || !(o instanceof NullUUID)) {
			return false;
		} else {
			return this.compareTo((UUID)o)==0;
		}
	}
	
	public int hashCode() {
		if (hashCode == 0) {
			hashCode = super.hashCode();
		}
		return hashCode;
	}
	
}
