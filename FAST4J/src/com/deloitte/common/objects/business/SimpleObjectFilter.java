package com.deloitte.common.objects.business;

import com.deloitte.common.interfaces.SimpleFilter;
import com.deloitte.common.interfaces.SimpleObject;
import com.deloitte.common.util.Strings;

@Deprecated
public class SimpleObjectFilter implements SimpleFilter
{
	private String type;
	private boolean caseSensitive;
	private String pattern;
	
	public SimpleObjectFilter()
	{
		this(null);
	}
	
	public SimpleObjectFilter(String type)
	{
		this(type,true);
	}
	
	public SimpleObjectFilter(String type, String valuePattern)
	{
		this(type,valuePattern,true);
	}
	
	public SimpleObjectFilter(String type, String valuePattern, boolean caseSensitive)
	{
		this(type,caseSensitive);
		this.pattern = valuePattern;
	}
	
	public SimpleObjectFilter(String type, boolean typeCaseSensitive)
	{
		if (type != null)
		{
			this.type = type;
		}
		this.caseSensitive = typeCaseSensitive;
	}
	
	public boolean include(SimpleObject theObject)
	{
		boolean theAnswer = false;
		if (type == null)
		{
			theAnswer = true;
		}
		else if (!Strings.isEmpty(pattern))
		{
			if (caseSensitive)
			{
				theAnswer = type.equals(theObject.getType()) && theObject.getValue().startsWith(pattern);
			}
			else 
			{
				theAnswer = type.equals(theObject.getType()) && theObject.getValue().toLowerCase().startsWith(pattern.toLowerCase());
			}
		} else
		{
			if (caseSensitive)
			{
				theAnswer = type.equals(theObject.getType());
			} else
			{
				theAnswer = type.equalsIgnoreCase(theObject.getType());	

			}

		}
		return theAnswer;
	}
}
