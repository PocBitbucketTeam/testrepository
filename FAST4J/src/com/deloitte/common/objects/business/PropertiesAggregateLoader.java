package com.deloitte.common.objects.business;

import java.util.Properties;
import java.util.StringTokenizer;

import com.deloitte.common.objects.AbstractPropertiesLoader;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Uses either external properties file or Properties object to load the Loaders.
 * 
 * The format of the properties file must be
 * <pre>
 * loader1=com.deloitte.common.objects.business.UnitedStates
 * </pre>
 * @author SIDT Framework Team
 *
 */
public class PropertiesAggregateLoader extends AbstractPropertiesLoader {

	public PropertiesAggregateLoader(Properties props){
		super(props);
	}
	
	public PropertiesAggregateLoader(String fileName) {
		super(fileName);
	}
	
	protected void loadValues(String propKey, String propVal) throws CheckedApplicationException{
		if(propKey.toLowerCase().startsWith("loader")) {
			StringTokenizer stk = new StringTokenizer(propVal,",");
			new LoaderConfiguration(stk.nextToken()).getLoader().loadValues();
		}
	}

}
