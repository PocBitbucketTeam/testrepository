package com.deloitte.common.objects.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.deloitte.common.interfaces.SimpleFilter;
import com.deloitte.common.interfaces.SimpleObject;
import com.deloitte.common.interfaces.SimpleObjectAccess;

/**
 * Use {@link com.deloitte.common.objects.reftable.ReferenceTable ReferenceTable}
 * and {@link com.deloitte.common.objects.reftable.ReferenceTableRepository
 * ReferenceTableReferenceTableRepository} instead <br><br>
 */
@Deprecated
public class SimpleObjectAccessor implements SimpleObjectAccess {
	
	private final static Set<SimpleObject> list =  new LinkedHashSet<SimpleObject>();
	private static SimpleObjectAccessor me = new SimpleObjectAccessor();
	
	private SimpleObjectAccessor() {
		// Singleton, only construct from getInstance();
	}
	
	public void clearAll() {
		list.clear();
	}
	
	public void clear(String type) {
		//SimpleObject[] array = (SimpleObject[])list.toArray(new SimpleObject[0]);
		Iterator<SimpleObject> i = list.iterator();
		SimpleObject s = null;
		while(i.hasNext()){
			s = i.next();
			if(s.getType().equals(type)){
				i.remove();
			}
		}
	}
	
	public SimpleObject get(String type,String name)
	{
		Collection<SimpleObject> filtered = getAll(new SimpleObjectFilter(type));
		SimpleObject theReturnObject = null;
		for (SimpleObject theObject:filtered) {
			if ( theObject.getName().equals(name)) {
				theReturnObject = theObject; 
				break;
			}
		}
		return theReturnObject;
	}

	public Collection<SimpleObject> getAll(){
		return Collections.unmodifiableCollection(list);
	}
	
	public Collection<SimpleObject> getAll(SimpleFilter filter)	{
		List<SimpleObject>  values = new ArrayList<SimpleObject> ();
			for (SimpleObject theObject:list) {
				if (filter.include(theObject)) {
					values.add(theObject);
				}
			}
		return Collections.unmodifiableCollection(values);
	}

	public Collection<String> getAllTypes(SimpleFilter filter)
	{
		Set<String> values = new HashSet<String>();
		for (SimpleObject theObject:list) {
			if (filter.include(theObject))
			{
				values.add(theObject.getType());
			}
		}
		return Collections.unmodifiableCollection(values);
	}

	public Collection<String> getAllNames(SimpleFilter filter)
	{
		List<String> values = new ArrayList<String>();
		for (SimpleObject theObject:list) {
			if (filter.include(theObject))
			{
				values.add(theObject.getName());
			}
		}
		return Collections.unmodifiableCollection(values);
	}

	public Collection<String> getAllValues(SimpleFilter filter)
	{
		List<String> values = new ArrayList<String>();
		for (SimpleObject theObject:list ) {
			if (filter.include(theObject))
			{
				values.add(theObject.getValue());
			}
		}
		return Collections.unmodifiableCollection(values);
	}
	
	public boolean add(SimpleObject simpleObject) {
		//TODO Make a copy for immutability?
		//DefaultSimpleObject theObject = new DefaultSimpleObject(simpleObject.getType(),simpleObject.getName(),simpleObject.getValue());
		return list.add(simpleObject);
	}
	
	public boolean addAll(Collection<SimpleObject> simpleObjects) {
		boolean theAnswer = true;	
		synchronized (list) { // Must had this to maintain the order while mutliple threads adding
			for (SimpleObject theObject:simpleObjects) {			
				theAnswer = theAnswer && list.add(theObject);			
			}
		}
		return theAnswer;
	}
	
	public static SimpleObjectAccessor getInstance() {
		return me;
	}
}
