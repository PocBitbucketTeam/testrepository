package com.deloitte.common.objects.business;

import java.text.Format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.deloitte.common.interfaces.DomainAttribute;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.util.Arrays;
import com.deloitte.common.util.EmployerIdentificationNumberFormat;
import com.deloitte.common.util.Strings;

/**
 * Common Business Object to represent US Employer Identification Numbers.
 * 
 * @author SIDT Framework Team
 * 
 */
public class EmployerIdentificationNumber implements DomainAttribute, Comparable<EmployerIdentificationNumber> {

	private String ein;

	private static Set<String> invalidPrefixes = new HashSet<String>();

	static {
		invalidPrefixes.add("07");
		invalidPrefixes.add("08");
		invalidPrefixes.add("09");
		invalidPrefixes.add("17");
		invalidPrefixes.add("18");
		invalidPrefixes.add("19");
		invalidPrefixes.add("28");
		invalidPrefixes.add("29");
		invalidPrefixes.add("49");
		invalidPrefixes.add("69");
		invalidPrefixes.add("70");
		invalidPrefixes.add("78");
		invalidPrefixes.add("79");
		//invalidPrefixes.add("95");
		invalidPrefixes.add("96");
		invalidPrefixes.add("97");

	}

	public enum ErrorType{
		INVALIDLENGTH, INVALIDCHAR, INVALIDPREFIX
	}
	
	public EmployerIdentificationNumber(String ein) {
		if (null == ein) {
			throw new IllegalArgumentException(
					"Unable to create null or Empty Employer Identification Number");
		}
		this.ein = ein;
	}

	public Collection<Error> getValidationErrors(Intent intent) {
		return getValidationErrors();
	}

	/**
	 * Will return Errors with the following types:
	 * 		<br>-INVALIDLENGTH - the number does not contain the required 9 characters (too long or too short)
	 * 		<br>-INVALIDCHAR - the value contains characters other than '-' and numerics
	 * 				<br>--- param[0] - comma-separated list of the bad characters in the value
	 * 		<br>-INVALIDPREFIX - the prefix (first two characters) are not a valid prefix value for an EIN
	 * 				<br>--- param[0] - the invalid prefix value
	 */
	public Collection<Error> getValidationErrors() {
		Collection<Error> theList = new ArrayList<Error>();
		char[] charArray = Arrays.copyWithout(ein.toCharArray(), '-');
		if (charArray.length != 9) {
			// Considered fatal
			theList.add(new Error(ErrorType.INVALIDLENGTH.name()));
			return theList;
		}
		StringBuffer invalidChars = new StringBuffer("");
		boolean found = false;
		String seperator = "";
		for (int i = 0; i < charArray.length; i++) {
			if (!Strings.isNumber(charArray[i])) {
				found = true;
				invalidChars.append(seperator + charArray[i]);
				seperator = ",";
			}
		}
		if (found) {
			theList.add(new Error(ErrorType.INVALIDCHAR.name(), Error.createParams(invalidChars.toString())));
		}
		String prefix = "" + charArray[0] + charArray[1];
		if (invalidPrefixes.contains(prefix)) {
			theList.add(new Error(ErrorType.INVALIDPREFIX.name(), Error.createParams(prefix)));
		}
		return theList;
	}

	public String toDisplay() {
		return new EmployerIdentificationNumberFormat("##-#######")
				.format(this);
	}

	public String toDisplay(Format format) {
		return format.format(this);
	}

	public String toString() {
		return ein;
	}

	public Object getValue() {
		char[] charArray = Arrays.copyWithout(ein.toCharArray(), '-');
		return Arrays.get(charArray, "");
	}

	public int compareTo(EmployerIdentificationNumber o) {
		EmployerIdentificationNumber that = (EmployerIdentificationNumber) o;
		return ((String) this.getValue()).compareTo((String) that.getValue());
	}
}
