package com.deloitte.common.objects.business;


import java.text.Format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.util.Arrays;
import com.deloitte.common.util.CollectionHelper;
import com.deloitte.common.util.CreditCardFormat;
import com.deloitte.common.util.Luhn;
import com.deloitte.common.util.Strings;

/**
 * Provided an implementation for the 5 Major Credit cards issued. Allows the
 * card to be validated and queried as to its type.
 * 
 * @author dasisk
 *
 */
public final class CreditCard extends AbstractDomainAttribute
{
	private String input;
	private String ccNumber;
	private char[] cArray;
	private CreditCardAttributes creditCard;
	List<Error> theErrors;

	public enum ErrorType{
		INVALIDCCNLEN, INVALIDCCNCHARS, UNKNOWNCCNTYPE, INVALIDCCN
	}

	CreditCardAttributes AMERICANEXPRESS = new AmericanExpress();
	CreditCardAttributes MASTERCARD = new MasterCard();
	CreditCardAttributes VISA 		= new Visa();
	CreditCardAttributes DISCOVER 	= new Discover();
	CreditCardAttributes DINERSCLUB = new DinersClub();
	
	public CreditCard(String creditCardNumber)
	{
		
		if (null == creditCardNumber)
		{
			throw new IllegalArgumentException("Unable to create null or Empty Credit Card");
		}
	  input = creditCardNumber;
	  cArray = Arrays.copyWithout(Arrays.copyWithout(creditCardNumber.toCharArray(),'-'),' ');
	  this.ccNumber = Arrays.get(cArray,"");
	  if (isMasterCard())
	  {
		  creditCard = MASTERCARD;
	  } else if (isVisa())
	  {
		  creditCard = VISA;
	  } else if (isDiscover())
	  {
		  creditCard = DISCOVER;
	  } else if (isDinersClub())
	  {
		  creditCard = DINERSCLUB;
	  } else if (isAmericanExpress())
	  {
		  creditCard = AMERICANEXPRESS;
	  }
	}
	
	/**
	 * Will return Errors with the following types:
	 * 		<br>-INVALIDCCNLEN - the number does not contain the required 9 characters (too long or too short)
	 * 				<br>--- param[0] - the invalid / bad credit card data value
	 * 		<br>-INVALIDCCNCHARS - invalid characters in credit card number
	 * 				<br>--- param[0] - invalid / bad characters
	 * 		<br>-UNKNOWNCCNTYPE - unknown credit card type
	 * 				<br>--- param[0] - the bad CCN type data value
	 * 		<br>-INVALIDCCN - invalid credit card number
	 * 				<br>--- param[0] - the invalid / bad credit card data value
	 */
	public Collection<Error> getValidationErrors()
	{
		if (theErrors == null)
		{
			theErrors = new ArrayList<Error>();
			if (Strings.isEmpty(ccNumber) || ccNumber.length() < 13 || !Strings.isInteger(ccNumber)){	
				if (Strings.isEmpty(ccNumber) || ccNumber.length() < 13){
					theErrors.add(new Error(ErrorType.INVALIDCCNLEN.name(), Error.createParams(ccNumber)));
				}else {
					theErrors.add(new Error(ErrorType.INVALIDCCNCHARS.name(), Error.createParams(ccNumber)));
				}
				return theErrors;
			} else 
			{
				if (! (
						isMasterCard() || 
						isVisa() || 
						isAmericanExpress() || 
						isDinersClub() || 
						isDiscover())
					   )
				{
					theErrors.add(new Error(ErrorType.UNKNOWNCCNTYPE.name(), Error.createParams(ccNumber)) );
				} 
				else 
				{
					if (!Luhn.isValidMod10(ccNumber)){
		
						theErrors.add(new Error(ErrorType.INVALIDCCN.name(), Error.createParams(ccNumber)) );
					}
				}
			}
		}
		return theErrors;
	}
	
	public boolean isMasterCard()
	{
		return MASTERCARD.isType(ccNumber);
	}
	
	public boolean isVisa()
	{
		return VISA.isType(ccNumber);
	}
	
	public boolean isAmericanExpress()
	{
		return AMERICANEXPRESS.isType(ccNumber);
	}
	
	public boolean isDinersClub()
	{
		return DINERSCLUB.isType(ccNumber);
	}
	
	public boolean isDiscover()
	{
		return DISCOVER.isType(ccNumber);
	}
	
	public Object getValue()
	{
		return ccNumber;
	}
	
	public String toDisplay(Format format)
	{
		return format.format(ccNumber);
	}
	
	public String toDisplay()
	{	
		if (CollectionHelper.isEmpty(getValidationErrors()))
		{
			return new CreditCardFormat(creditCard.getFormat()).format(this);
		} 
		return input;
	}
	
	public boolean equals(Object o)
	{
		return o != null ? getValue().equals(((CreditCard)o).getValue()) : false;
	}
	public String toString()
	{
		return  getValue().toString();
	}
	
	private abstract class CreditCardAttributes
	{
		private int length1;
		private int length2;
		protected List<String> prefixes = new ArrayList<String>();
		private Map<String, String> format = new HashMap<String, String>();
		
		boolean isType(String creditCardNumber)
		{
			return matchesLength(creditCardNumber,length1,length2) && matchesPrefix(creditCardNumber,(String[])prefixes.toArray(new String[0]));
		}
		
		boolean matchesLength(String creditCardNumber,int length1,int length2)
		{
			return creditCardNumber.length() == length1 || creditCardNumber.length() == length2;
		}

		boolean matchesPrefix(String creditCardNumber,String[] prefixes)
		{
			boolean theAnswer = false;
			for (int i=0; i<prefixes.length && theAnswer == false; i++)
			{
				theAnswer = creditCardNumber.startsWith(prefixes[i]);
			}
			return theAnswer;
		}
		
		int  getLength1()
		{
			return this.length1;
		}
		
		int getLength2()
		{
			return this.length2;
		}
		
		void setLength1(int length)
		{
			this.length1 = length;
		}

		void setLength2(int length)
		{
			this.length2 = length;
		}

		void addPrefix(String prefix)
		{
			prefixes.add(prefix);
		}
		
		void setFormat(int length,String format)
		{
			this.format.put(""+length,format);
		}
		
		String getFormat()
		{
			return (String)format.get(ccNumber.length()+"");
		}
	}
	
	private class MasterCard extends CreditCardAttributes
	{
		MasterCard()
		{
			setLength1(16);
			setLength2(16);
			addPrefix("51");
			addPrefix("52");
			addPrefix("53");
			addPrefix("54");
			addPrefix("55");
			setFormat(16,"####-####-####-####");
		}
	}
	
	private class Visa extends CreditCardAttributes
	{
		Visa()
		{
			setLength1(13);
			setLength2(16);
			addPrefix("4");
			setFormat(13,"####-#####-####");
			setFormat(16,"####-####-####-####");
			
		}
	}

	private class AmericanExpress extends CreditCardAttributes
	{
		AmericanExpress()
		{
			setLength1(15);
			setLength2(15);
			addPrefix("34");
			addPrefix("37");
			setFormat(15,"####-######-#####");
		}
	}

	private class DinersClub extends CreditCardAttributes
	{
		
		DinersClub()
		{
			setLength1(14);
			setLength2(14);
			addPrefix("300"); 
			addPrefix("301");
			addPrefix("302");
			addPrefix("303");
			addPrefix("304");
			addPrefix("305");
			addPrefix("36");
			addPrefix("38");
			setFormat(14,"####-######-####");
		}
	}

	private class Discover extends CreditCardAttributes
	{
		
		Discover()
		{
			setLength1(16);
			setLength2(16);
			addPrefix("6011");
			setFormat(16,"####-####-####-####");
		}
	}
	
}
