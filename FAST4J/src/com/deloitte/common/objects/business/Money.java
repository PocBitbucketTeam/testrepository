package com.deloitte.common.objects.business;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Collection;
import java.util.Collections;
import java.util.Currency;

import com.deloitte.common.objects.framework.Error;

/**
 * Money is an immutable value object used to represent a monetary value.  Use this object
 * to avoid the rounding problems and formatting problems associated with using
 * primitive types or BigDecimals directly to represent money.
 * 
 * @author SIDT Framework Team
 *
 */
public class Money extends AbstractDomainAttribute implements Comparable<Money>
{
	private final BigDecimal theAmount;
	private final Currency theCurrency;
	
	private int hashcode;
	private Collection<Error> errors;
	
	/**
	 * 
	 * Constructs Money with the given amount value and currency.
	 * 
	 * @param amount The amount in a format that can be parsed as a number. 
	 * @param currency The currency for this Money.
	 * 
	 */
	public Money(String amount,Currency currency)
	{
		this.theCurrency = currency;
		this.theAmount = new BigDecimal(amount).setScale(currency.getDefaultFractionDigits(),BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 
	 * Constructs Money with the given amount value and currency.
	 * 
	 * @param amount The amount as a BigDecimal. 
	 * @param currency The currency for this Money.
	 * 
	 */
	public Money(BigDecimal amount,Currency currency)
	{
		this.theCurrency = currency;
		this.theAmount = amount.setScale(currency.getDefaultFractionDigits(),BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 
	 * Factory method to create Money with the given amount value and of currency type US Dollar.
	 * 
	 * @param amount The amount in a format that can be parsed as a number. 
	 */
	public static Money USDollars(String amount)
	{
		return new Money(amount,Currency.getInstance("USD"));
	}

	/**
	 * 
	 * Factory method to create Money with the given amount value and of currency type US Dollar.
	 * 
	 * @param amount The amount as a BigDecimal.
	 *  
	 */
	public static Money USDollars(BigDecimal amount)
	{
		return new Money(amount,Currency.getInstance("USD"));
	}

	/**
	 * 
	 * Returns Money whose value is (this + thatMoney) and whose currency is this.currency()
	 * 
	 * @param thatMoney value to be added to this Money
	 * @return Money whose value is (this + thatMoney) and whose currency is this.currency()
	 * @throws IllegalArgumentException when the Moneys to add are not of the same currency.
	 */
	public Money add(Money thatMoney)
	{	
		assertSameCurrencies(thatMoney);
		return new Money(theAmount.add((BigDecimal)(thatMoney.getValue())),theCurrency);
	}
	
	/**
	 * 
	 * Returns Money whose value is (this - thatMoney) and whose currency is this.currency()
	 * 
	 * @param thatMoney value to be subtracted from this Money
	 * @return Money whose value is (this - thatMoney) and whose currency is this.currency()
	 * @throws IllegalArgumentException when the Moneys to subtract are not of the same currency.
	 */
	public Money subtract(Money thatMoney)
	{	
		assertSameCurrencies(thatMoney);
		return new Money(theAmount.subtract((BigDecimal)(thatMoney.getValue())),theCurrency);
	}
	
	/**
	 * 
	 * Returns Money whose value is (this * theMultiplier) and whose currency is this.currency()
	 * 
	 * @param theMultiplier value to be multiplied by this Money
	 * @return Money whose value is (this * theMultiplier) and whose currency is this.currency()
	 */
	public Money multiply(BigDecimal theMultiplier)
	{
		return new Money(theAmount.multiply(theMultiplier),theCurrency);
	}

	/**
	 * 
	 * Returns Money whose value is (this / theDivider) and whose currency is this.currency()
	 * 
	 * @param theDivider value by which this Money is to be divided
	 * @return Money whose value is (this / theDivider) and whose currency is this.currency()
	 */
	public Money divide(BigDecimal theDivider)
	{
		return new Money(theAmount.divide(theDivider,BigDecimal.ROUND_HALF_UP),theCurrency);
	}

	// Throws a runtime exception when trying to perform operations on two Moneys of different currencies
	private void assertSameCurrencies(Money thatMoney)
	{
		if(!this.currency().equals(thatMoney.currency()))
		{
			throw new IllegalArgumentException("Money currency mismatch");
		}
	}

	/**
	 * 
	 * Returns an empty list of errors.
	 * 
	 * @return an empty list of errors.
	 * 
	 */
	public Collection<Error> getValidationErrors()
	{
		if (errors == null)
		{
			errors = Collections.emptyList();
		}
		return errors;
	}

	/**
	 * Returns the string representation of this Money using the proper 
	 * currency symbol and locale-specific formatting.
	 * 
	 * @return the string representation of this Money.  The Money's currency symbol will be used
	 * and the number format will be that of the default Locale.
	 * 
	 */
	public String toDisplay()
	{
		DecimalFormat format = (DecimalFormat)DecimalFormat.getCurrencyInstance();
		// The following sets the currency symbol but does not set the DecimalFormat
		// The decimal format used will be the one for the default locale
		format.setCurrency(theCurrency);
		return toDisplay(format);
	}

	/**
	 * Returns the string representation of this Money using the given Format.
	 * 
	 * @return the string representation of this Money using the given Format.
	 * 
	 */
	public String toDisplay(Format format)
	{
		return format.format(theAmount);
	}

	/**
	 * Returns the string representation of this Money; identical to toDisplay().
	 * 
	 * @return the string representation of this Money.
	 * 
	 */
	public String toString()
	{
		return toDisplay();
	}
	
	/**
	 * 
	 * Returns the amount value for this Money.
	 * 
	 * @return the BigDecimal value for this Money.
	 */
	public Object getValue()
	{
		return theAmount;
	}
	
	/**
	 * 
	 * Returns the currency for this Money.
	 * 
	 * @return the currency for this Money.
	 */
	public Currency currency()
	{
		return theCurrency;
	}

	/**
	 * Compares this Money object with the specified Object for equality.  Moneys are equal
	 * if they are of equal value and of equal currency.
	 * 
 	 * @param o Object to which this BigDecimal is to be compared.
	 * @return true if and only if the specified Object is a Money whose value and currency are equal to this Money's.
	 */
	public boolean equals(Object o)
	{
		if(o instanceof Money)
		{
			return this.getValue().equals(((Money)o).getValue()) && this.currency().equals(((Money)o).currency());
		}
		return false;
	}
	
	/**
	 * Returns the hash code for this Money.
	 * 
	 * @return hash code for this Money.
	 */
	public int hashCode()
	{
		if(hashcode == 0)
		{
			hashcode = this.getValue().hashCode();
		}
		return hashcode;
	}

	/**
	 * Compares this Money with the specified Object. If the Object is not of type Money, this method 
	 * throws an IllegalArgumentException (as Money is comparable only to Money).
	 * 
	 *  @return If the two Moneys are of the same currency, returns a negative number, zero, 
	 *  or a positive number as this Money is numerically less than, equal to, or greater than o.
	 *  If the two Moneys are of different currencies, returns a negative number, zero, 
	 *  or a positive number as this Money's currency Code is alphabetically less than, equal to, or
	 *  greater than o's curency code.
	 */
	public int compareTo(Money that)
	{
/*		if(o instanceof Money)
		{
			Money that = (Money)o;*/
			if(this.currency().equals(that.currency())){
				return ((BigDecimal)this.getValue()).compareTo((BigDecimal)(that.getValue()));
			}	
			else{
				return this.currency().getCurrencyCode().compareTo(that.currency().getCurrencyCode());
			}	
		/*}
		throw new IllegalArgumentException("Argument to compare is not of type Money");*/
	}
}
