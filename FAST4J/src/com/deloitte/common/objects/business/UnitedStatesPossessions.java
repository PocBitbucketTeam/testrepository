package com.deloitte.common.objects.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.interfaces.SimpleFilter;
import com.deloitte.common.interfaces.SimpleObject;
import com.deloitte.common.objects.DefaultSimpleObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Use a {@link com.deloitte.common.objects.reftable.ReferenceTable ReferenceTable}
 * instead <br><br>
 */
@Deprecated
public class UnitedStatesPossessions implements Loader
{
	private boolean valuesLoaded = false;
	private static Loader me = new UnitedStatesPossessions();
	
	public static final DefaultSimpleObject AMERICAN_SAMOA 	 = new DefaultSimpleObject(getType(),"AS","American Samoa");
	public static final DefaultSimpleObject FEDERATED_STATES_OF_MICRONESIA = new DefaultSimpleObject(getType(),"FM","Federated States of Micronesia");
	public static final DefaultSimpleObject GUAM  		 	 = new DefaultSimpleObject(getType(),"GU","Guam");
	public static final DefaultSimpleObject MARSHALL_ISLANDS = new DefaultSimpleObject(getType(),"MH","Marshall Islands");
	public static final DefaultSimpleObject NORTHERN_MARIANA_ISLANDS = new DefaultSimpleObject(getType(),"MP","Northern Mariana Islands");
	public static final DefaultSimpleObject PALAU 		 	 = new DefaultSimpleObject(getType(),"PW","Palau");
	public static final DefaultSimpleObject PUERTO_RICO 	 = new DefaultSimpleObject(getType(),"PR","Puerto Rico");
	public static final DefaultSimpleObject VIRGIN_ISLANDS 	 = new DefaultSimpleObject(getType(),"VI","Virgin Islands");

	protected UnitedStatesPossessions()
	{
		super();
	}
	public final void loadValues() throws CheckedApplicationException
	{
		if (!valuesLoaded)
		{
			for (SimpleObject simpleObject : getValues())
			{
				SimpleObjectAccessor.getInstance().add(simpleObject);
			}
			valuesLoaded = true;
		}
	}

	public static Loader getInstance() {
		return me;
	}
	
	public static String getType()
	{
		return "UnitedStatesPossesions";
	}
	
	public static final SimpleFilter getFilter()
	{
		return new SimpleObjectFilter(getType());
	}
	
	public static final Collection<SimpleObject> getValues()
	{
		List<SimpleObject> list = new ArrayList<SimpleObject>();
		list.add(AMERICAN_SAMOA);
		list.add(FEDERATED_STATES_OF_MICRONESIA);
		list.add(GUAM);
		list.add(MARSHALL_ISLANDS);
		list.add(NORTHERN_MARIANA_ISLANDS);
		list.add(PALAU);
		list.add(PUERTO_RICO);
		list.add(VIRGIN_ISLANDS);
		return list;
	}

}
