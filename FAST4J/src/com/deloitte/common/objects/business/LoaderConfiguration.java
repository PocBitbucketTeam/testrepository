package com.deloitte.common.objects.business;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;

final class LoaderConfiguration {

	private String loaderName;
	
	public LoaderConfiguration(String loaderName) {
		this.loaderName = loaderName;
	}
	
	public Loader getLoader() throws CheckedApplicationException { 
		Class<?> theClass = ReflectionHelper.createFor(loaderName);
		return ReflectionHelper.invokeAccessorMethod(null,ReflectionHelper.getAccessorMethod(theClass,"Instance"),ReflectionHelper.EMPTY_ARRAY);
	}
	
	public String toString(){
		return "[Loader=" + loaderName + "]";
	}
}
