package com.deloitte.common.objects.business;

import java.util.Collection;

import com.deloitte.common.interfaces.DomainAttribute;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.Error;

public abstract class AbstractDomainAttribute implements DomainAttribute
{

	private int hashcode = -1;
	
	public Collection<Error> getValidationErrors(Intent intent)
	{
		return getValidationErrors();
	}
	
	public boolean equals(Object o)
	{
		DomainAttribute that = (DomainAttribute) o;
		if (this == that)
		{
			return true;
		}
		
		return that != null ? getValue().equals(((DomainAttribute)that).getValue()) : false;
	}
	
	public int hashCode()
	{
		if (hashcode == -1)
		{
			hashcode = getValue().hashCode();
		}
		return hashcode;
	}
}
