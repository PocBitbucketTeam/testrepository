package com.deloitte.common.objects.business;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * ConfigurationLoader uses either external files to initialize items
 * within the system.
 * 
 * TODO Database Backed Loader?
 * 
 * The format of the properties file is 
 * dao1=com.deloitte.common.interfaces.DomainObject,com.deloitte.common.persistence.HsqldbConnectionFactory,com.deloitte.common.persistence.DomainObjectDAO
 *
 * The format of the xml file is
 * <Configuration>
 * <Loader>
 *	<Item type="SimpleObject" 
 *	  	  loader="com.deloitte.common.objects.business.UnitedStates"/> 
 *	</Loader>
 *  </Configuration> 
 * @author SIDT Framework Team
 *
 */

/**
 * @deprecated Provided  backward-compatibility.
 * @see XMLAggregateLoader
 * @see PropertiesAggregateLoader
 */
public class SimpleObjectLoader implements Loader {
	
	private static SimpleObjectLoader me;
	private String fileName;
	
	protected SimpleObjectLoader(String fileName) {
		super();
		this.fileName = fileName;
	}
	
	public static SimpleObjectLoader getInstance(String fileName) {
		if (me == null) {
			me = new SimpleObjectLoader(fileName);
			me.fileName = fileName;
		}
		return me;
	}

	public static void reset() {
		me.fileName = null;
		me = null;
	}
	
	public final void loadValues() throws CheckedApplicationException {
		if (fileName.endsWith(".xml")) {
			XMLAggregateLoader loader = new XMLAggregateLoader(fileName);
			loader.loadValues();
		} else {
			PropertiesAggregateLoader loader = new PropertiesAggregateLoader(fileName);
			loader.loadValues();
		}
	}
	
}
