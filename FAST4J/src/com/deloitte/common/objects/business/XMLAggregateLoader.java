package com.deloitte.common.objects.business;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.deloitte.common.objects.AbstractXMLLoader;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Uses either external XML file or Document object to load the Loaders.
 * 
 * The format of the xml file must be
 * <pre>
 * <Configuration>
 * <Loader>
 *	<Item type="SimpleObject" 
 *	  	  loader="com.deloitte.common.objects.business.UnitedStates"/> 
 *	</Loader>
 *  </Configuration>
 *  </pre> 
 * @author SIDT Framework Team
 *
 */
public class XMLAggregateLoader extends AbstractXMLLoader {
	
	public XMLAggregateLoader(Document document){
		super(document);
	}
	public XMLAggregateLoader(String fileName) {
		super(fileName);
	}
	
	protected void loadValues(Document document) throws CheckedApplicationException {
		NodeList theLoaders = document.getElementsByTagName("loader");
		Node node = null;
		NamedNodeMap map = null;
		String loaderClass = null;
		for (int i=0; i < theLoaders.getLength(); i++) {
			node = theLoaders.item(i);
			map = node.getAttributes();
			loaderClass = map.getNamedItem("classname").getNodeValue();
			new LoaderConfiguration(loaderClass).getLoader().loadValues();
		}
	}

}
