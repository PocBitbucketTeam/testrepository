package com.deloitte.common.objects.business;

import java.text.Format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.deloitte.common.interfaces.DomainAttribute;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.util.Arrays;
import com.deloitte.common.util.SocialSecurityNumberFormat;
import com.deloitte.common.util.Strings;

/**
 * Common Business Object to represent US Social Security Numbers.
 * 
 * @author SIDT Framework Team
 * 
 */
public class SocialSecurityNumber implements DomainAttribute, Comparable<SocialSecurityNumber> {

	private String ssn;

	private static Set<String> invalid = new HashSet<String>();
	
	static {
		invalid.add("111111111");
		invalid.add("123456789");
		invalid.add("333333333");
		invalid.add("078051120");
	}
	
	public enum ErrorType{
		SSNINVALIDLENGTH, SSNINVALIDSSNCHARS, SSNINVALIDPREFIX0, SSNINVALIDPREFIX6, SSNINVALIDSUFFIX0, SSNINVALIDPREFIX89, SSNINVALID
	}
	
	public SocialSecurityNumber(String ssn) {
		if (null == ssn) {
			throw new IllegalArgumentException(
					"Unable to create null or Empty Social Security Number");
		}
		this.ssn = ssn;
	}

	public Collection<Error> getValidationErrors(Intent intent) {
		return getValidationErrors();
	}

	/**
	 *  Will return Errors with the following types:
	 * 		<br>-SSNINVALIDLENGTH - length of the SSN is not valid(length 9 in not reached)
	 * 		<br>-SSNINVALIDSSNCHARS - invalid characters in the SSN
	 * 			<br>--- param[0] - comma-separated list of the bad characters in the value
	 * 		<br>-SSNINVALIDPREFIX0 - the prefix (first three characters) are not a valid(000) prefix value for an SSN
	 * 		<br>-SSNINVALIDPREFIX6 - the prefix (first three characters) are not a valid(666) prefix value for an SSN
	 * 		<br>-SSNINVALIDSUFFIX0 - the suffix (last four characters) are not a valid(0000) suffix value for an SSN
	 * 		<br>-SSNINVALIDPREFIX89 - the starting (character) is not a valid prefix(8 or 9) value for an SSN
	 *  	<br>-SSNINVALID - not a valid SSN number
	 *  		<br>--- param[0] - invalid / bad value
	 */
	public Collection<Error> getValidationErrors() {
		Collection<Error> theList = new ArrayList<Error>();
		char[] charArray = Arrays.copyWithout(ssn.toCharArray(), '-');
		if (charArray.length != 9) {
			// Considered fatal
			theList
					.add(new Error(ErrorType.SSNINVALIDLENGTH.name()));
			return theList;
		}
		StringBuffer invalidChars = new StringBuffer("");
		boolean found = false;
		String seperator = "";
		for (int i = 0; i < charArray.length; i++) {
			if (!Strings.isNumber(charArray[i])) {
				found = true;
				invalidChars.append(seperator + charArray[i]);
				seperator = ",";
			}
		}
		if (found) {
			theList.add(new Error(ErrorType.SSNINVALIDSSNCHARS.name(), Error.createParams(invalidChars)));
		}

		String areaNumber = "" + charArray[0] + charArray[1] + charArray[2];
		String groupCode = "" + charArray[3] + charArray[4];
		String serialNumber = "" + charArray[5] + charArray[6] + charArray[7]
				+ charArray[8];

		if ("000".equals(areaNumber)) {
			theList.add(new Error(ErrorType.SSNINVALIDPREFIX0.name()));
		}
		if ("666".equals(areaNumber)) {
			theList.add(new Error(ErrorType.SSNINVALIDPREFIX6.name()));
		}

		if ("0000".equals(serialNumber)) {
			theList.add(new Error(ErrorType.SSNINVALIDSUFFIX0.name()));
		}

		if (areaNumber.startsWith("8") || areaNumber.startsWith("9")) {
			theList.add(new Error(ErrorType.SSNINVALIDPREFIX89.name()));
		}

		String fullNumber = Arrays.get(charArray, "");
		if (invalid.contains(fullNumber)) {
			StringBuffer invalidSSN = new StringBuffer();
			invalidSSN.append(areaNumber + "-");
			invalidSSN.append(groupCode + "-" + serialNumber);
			theList.add(new Error(ErrorType.SSNINVALID.name(), Error.createParams(invalidSSN)));
		}

		return theList;
	}

	public String toDisplay() {
		return new SocialSecurityNumberFormat("###-##-####").format(this);
	}

	public String toDisplay(Format format) {
		return format.format(this);
	}

	public String toString() {
		return ssn;
	}

	public Object getValue() {
		char[] charArray = Arrays.copyWithout(ssn.toCharArray(), '-');
		return Arrays.get(charArray, "");
	}

	public int compareTo(SocialSecurityNumber that) {
		return ((String) this.getValue()).compareTo((String) that.getValue());
	}

}
