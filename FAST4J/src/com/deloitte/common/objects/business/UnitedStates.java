package com.deloitte.common.objects.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.interfaces.SimpleFilter;
import com.deloitte.common.interfaces.SimpleObject;
import com.deloitte.common.objects.DefaultSimpleObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Use a {@link com.deloitte.common.objects.reftable.ReferenceTable ReferenceTable}
 * instead <br><br>
 */
@Deprecated
public class UnitedStates implements Loader
{
	//private boolean valuesLoaded = false;
	private static Loader me = new UnitedStates();
	
	protected UnitedStates()
	{
		super();
	}
	
	public static final String getType()
	{
		return "UnitedStates";
	}

	public static final SimpleFilter getFilter()
	{
		return new SimpleObjectFilter(getType());
	}
	
	public void loadValues() throws CheckedApplicationException	{
		for (SimpleObject simpleObject:getValues()) {
			SimpleObjectAccessor.getInstance().add(simpleObject);
		}
	}

	public static Loader getInstance() {
		return me;
	}
	
	private static Collection<SimpleObject> getValues()
	{
		List<SimpleObject> list = new ArrayList<SimpleObject>();
		
		list.add(ALABAMA);
		list.add(ALASKA);
		list.add(ARIZONA);
		list.add(ARKANSAS);
		list.add(CALIFORNIA);
		list.add(COLORADO);
		list.add(CONNECTICUT);
		list.add(DELAWARE);
		list.add(DISTRICT_OF_COLUMBIA);
		list.add(FLORIDA);
		list.add(GEORGIA);
		list.add(HAWAII);
		list.add(IDAHO);
		list.add(ILLINOIS);
		list.add(INDIANA);
		list.add(IOWA);
		list.add(KANSAS);
		list.add(KENTUCKY);
		list.add(LOUISIANA);
		list.add(MAINE);
		list.add(MARYLAND);
		list.add(MASSACHUSETTS);
		list.add(MICHIGAN);
		list.add(MINNESOTA);
		list.add(MISSISSIPPI);
		list.add(MISSOURI);
		list.add(MONTANA);
		list.add(NEBRASKA);
		list.add(NEVADA);
		list.add(NEW_HAMPSHIRE);
		list.add(NEW_JERSEY);
		list.add(NEW_MEXICO);
		list.add(NEW_YORK);
		list.add(NORTH_CAROLINA);
		list.add(NORTH_DAKOTA);
		list.add(OHIO);
		list.add(OKLAHOMA);
		list.add(OREGON);
		list.add(PENNSYLVANIA);
		list.add(RHODE_ISLAND);
		list.add(SOUTH_CAROLINA);
		list.add(SOUTH_DAKOTA);
		list.add(TENNESSEE);
		list.add(TEXAS);
		list.add(UTAH);
		list.add(VERMONT);
		list.add(VIRGINIA);
		list.add(WASHINGTON);
		list.add(WEST_VIRGINIA);
		list.add(WISCONSIN);
		list.add(WYOMING);
		return list;
	}
	public static final DefaultSimpleObject ALABAMA 	 = new DefaultSimpleObject(getType(),"AL","Alabama");
	public static final DefaultSimpleObject ALASKA 	     = new DefaultSimpleObject(getType(),"AK","Alaska");
	public static final DefaultSimpleObject ARIZONA  	 = new DefaultSimpleObject(getType(),"AZ","Arizona");
	public static final DefaultSimpleObject ARKANSAS 	 = new DefaultSimpleObject(getType(),"AR","Arkansas");
	public static final DefaultSimpleObject CALIFORNIA   = new DefaultSimpleObject(getType(),"CA","California");
	public static final DefaultSimpleObject COLORADO     = new DefaultSimpleObject(getType(),"CO","Colorado");
	public static final DefaultSimpleObject CONNECTICUT  = new DefaultSimpleObject(getType(),"CT","Connecticut");
	public static final DefaultSimpleObject DELAWARE 	 = new DefaultSimpleObject(getType(),"DE","Deleware");
	public static final DefaultSimpleObject DISTRICT_OF_COLUMBIA = new DefaultSimpleObject(getType(),"DC","District of Columbia");
	public static final DefaultSimpleObject FLORIDA 	 = new DefaultSimpleObject(getType(),"FL","Florida");
	public static final DefaultSimpleObject GEORGIA 	 = new DefaultSimpleObject(getType(),"GA","Georgia");
	public static final DefaultSimpleObject HAWAII 	 	 = new DefaultSimpleObject(getType(),"HI","Hawaii");
	public static final DefaultSimpleObject IDAHO 	 	 = new DefaultSimpleObject(getType(),"ID","Idaho");
	public static final DefaultSimpleObject ILLINOIS 	 = new DefaultSimpleObject(getType(),"IL","Illinois");
	public static final DefaultSimpleObject INDIANA 	 = new DefaultSimpleObject(getType(),"IN","Indiana");
	public static final DefaultSimpleObject IOWA 		 = new DefaultSimpleObject(getType(),"IA","Iowa");
	public static final DefaultSimpleObject KANSAS 	 	 = new DefaultSimpleObject(getType(),"KS","Kansas");
	public static final DefaultSimpleObject KENTUCKY 	 = new DefaultSimpleObject(getType(),"KY","Kentucky");
	public static final DefaultSimpleObject LOUISIANA  	 = new DefaultSimpleObject(getType(),"LA","Louisiana");
	public static final DefaultSimpleObject MAINE 	 	 = new DefaultSimpleObject(getType(),"ME","Maine");
	public static final DefaultSimpleObject MARYLAND 	 = new DefaultSimpleObject(getType(),"MD","Maryland");
	public static final DefaultSimpleObject MASSACHUSETTS = new DefaultSimpleObject(getType(),"MA","Massacusetts");
	public static final DefaultSimpleObject MICHIGAN 	 = new DefaultSimpleObject(getType(),"MI","Michigan");
	public static final DefaultSimpleObject MINNESOTA  	 = new DefaultSimpleObject(getType(),"MN","Minnesota");
	public static final DefaultSimpleObject MISSISSIPPI  = new DefaultSimpleObject(getType(),"MS","Mississippi");
	public static final DefaultSimpleObject MISSOURI 	 = new DefaultSimpleObject(getType(),"MO","Missouri");
	public static final DefaultSimpleObject MONTANA 	 = new DefaultSimpleObject(getType(),"MT","Montana");
	public static final DefaultSimpleObject NEBRASKA 	 = new DefaultSimpleObject(getType(),"NE","Nebraska");
	public static final DefaultSimpleObject NEVADA 	 	 = new DefaultSimpleObject(getType(),"NV","Nevada");
	public static final DefaultSimpleObject NEW_HAMPSHIRE = new DefaultSimpleObject(getType(),"NH","New Hampshire");
	public static final DefaultSimpleObject NEW_JERSEY 	 = new DefaultSimpleObject(getType(),"NJ","New Jersey");
	public static final DefaultSimpleObject NEW_MEXICO 	 = new DefaultSimpleObject(getType(),"NM","New Mexico");
	public static final DefaultSimpleObject NEW_YORK 	 = new DefaultSimpleObject(getType(),"NY","New York");
	public static final DefaultSimpleObject NORTH_CAROLINA = new DefaultSimpleObject(getType(),"NC","North Carolina");
	public static final DefaultSimpleObject NORTH_DAKOTA = new DefaultSimpleObject(getType(),"ND","North Dakota");
	public static final DefaultSimpleObject OHIO 		 = new DefaultSimpleObject(getType(),"OH","Ohio");
	public static final DefaultSimpleObject OKLAHOMA 	 = new DefaultSimpleObject(getType(),"OK","Oklahoma");
	public static final DefaultSimpleObject OREGON 	 	 = new DefaultSimpleObject(getType(),"OR","Oregon");
	public static final DefaultSimpleObject PENNSYLVANIA = new DefaultSimpleObject(getType(),"PA","Pennsylvania");
	public static final DefaultSimpleObject RHODE_ISLAND = new DefaultSimpleObject(getType(),"RI","Rhode Island");
	public static final DefaultSimpleObject SOUTH_CAROLINA = new DefaultSimpleObject(getType(),"SC","South Carolina");
	public static final DefaultSimpleObject SOUTH_DAKOTA = new DefaultSimpleObject(getType(),"SD","South Dakota");
	public static final DefaultSimpleObject TENNESSEE    = new DefaultSimpleObject(getType(),"TN","Tennessee");
	public static final DefaultSimpleObject TEXAS 	 	 = new DefaultSimpleObject(getType(),"TX","Texas");
	public static final DefaultSimpleObject UTAH 		 = new DefaultSimpleObject(getType(),"UT","Utah");
	public static final DefaultSimpleObject VERMONT 	 = new DefaultSimpleObject(getType(),"VT","Vermont");
	public static final DefaultSimpleObject VIRGINIA     = new DefaultSimpleObject(getType(),"VA","Virginia");
	public static final DefaultSimpleObject WASHINGTON   = new DefaultSimpleObject(getType(),"WA","Washington");
	public static final DefaultSimpleObject WEST_VIRGINIA = new DefaultSimpleObject(getType(),"WV","West Virginia");
	public static final DefaultSimpleObject WISCONSIN    = new DefaultSimpleObject(getType(),"WI","Wisconsin");
	public static final DefaultSimpleObject WYOMING 	 = new DefaultSimpleObject(getType(),"WY","Wyoming");
		
	
}
