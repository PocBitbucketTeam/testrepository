package com.deloitte.common.objects.business;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import com.deloitte.common.objects.framework.Error;

/**
 * 
 * Important Note: This class can roll the day forward until it is 
 * 				   a Western business Day(ie M-F)
 * 
 * <br/><br/>
 * @todo Implement Holiday Calendars (Resource Bundle?)
 * @todo Implement Worldwide Workweek (Middle East Work Week Sun-Thu)
 */
public class BusinessDate extends AbstractDomainAttribute implements Comparable<BusinessDate>
{
	private Calendar date;
	private Collection<Error> errors;
	private static String dateFormat = "MM/dd/yyyy";
	
	public enum ErrorType{
		INVALIDDATE, GENERALERROR, INVALIDBUSINESSDAY
	}
	
	public BusinessDate(Calendar c)
	{
		this(c,false);
	}
	
	/**
	 * Make a copy of the Calendar Object to disallow any changes to the
	 * underlying object.
	 * Will return Errors with the following types:
	 * 		<br>-INVALIDDATE - invalid date with invalid Lenient value
	 * 		<br>-GENERALERROR - when exception occurs for any reason
	 * 		<br>-INVALIDBUSINESSDAY - date containing the invalid business day (SUNDAY or SATURDAY)
	 */
	public BusinessDate(Calendar cal, boolean rollTheDate)
	{
		if (null == cal)
		{
			throw new IllegalArgumentException("Unable to create null or Empty Business Date");
		}

		date = Calendar.getInstance();
		date.setLenient(cal.isLenient());
		try {
				date.setTimeInMillis(cal.getTimeInMillis());
		} catch (IllegalArgumentException e)
		{
			if (!date.isLenient())
			{
				getValidationErrors().add(new Error(ErrorType.INVALIDDATE.name()));
			}
			else {
				getValidationErrors().add(new Error(ErrorType.GENERALERROR.name()));
			}
		}
		if (rollTheDate)
		{
			while (!isWesternBusinessDay(date))
			{
				date.add(Calendar.DATE,1);
			}
		}
		if (!isWesternBusinessDay(date))
		{
				getValidationErrors().add(new Error(ErrorType.INVALIDBUSINESSDAY.name()));
		}
	}
	
	
	private boolean isWesternBusinessDay(Calendar c)
	{
		
		int i = c.get(Calendar.DAY_OF_WEEK);
		boolean theAnswer = true;
		if (Calendar.SATURDAY == i || Calendar.SUNDAY == i)
		{
			theAnswer = false;
		}
		return theAnswer;
	}
	
	public Collection<Error> getValidationErrors()
	{
		if (errors == null)
		{
			errors = new ArrayList<Error>();
		}
		return errors;
	}
	
	/*
	 * Return a copy to keep anyone from changing the underlying
	 * implementation to an invalid date.
	 */
	
	public final Object getValue()
	{
		Calendar copy = Calendar.getInstance();
		copy.setTimeInMillis(date.getTimeInMillis());
		return copy;
	}
	
	public boolean equals(Object o)
	{
		BusinessDate that = (BusinessDate)o;
		return that != null && this.getValue().equals(that.getValue());
	}
	

	public int compareTo(BusinessDate that)
	{
		long diff = date.getTimeInMillis() - ((Calendar)that.getValue()).getTimeInMillis();
		if (diff > 0)
		{
			return 1;
		} else if (diff < 0)
		{
			return -1;
		}
			
		return 0;
	}
	
	public String toString()
	{
		return toDisplay();
	}
	
	public String toDisplay()
	{
		return (new SimpleDateFormat(dateFormat)).format(date.getTime());
	}

	public String toDisplay(Format format)
	{
		return format.format(date.getTime());
	}
	
}
