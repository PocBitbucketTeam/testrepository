package com.deloitte.common.objects.decorator;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Mapper;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.objects.mapper.DefaultPropertyMapper;
import com.deloitte.common.util.ReflectionHelper;

/**
 * By providing a list of property names via the constructor this class will decorate a DomainObject
 * and only return the specified properties in the getProperties() call.
 * <br/>
 * This can be helpful when you only want to retrieve a limited set of properties from the object. One example might be in the 
 * persistence layer where you only want to update a subset of columns in a table. By coupling 
 * this with the SQLUpdateHelper you could automatically generate sql statements.
 * <br/><br/>
 * Note that if a property value is null, by default it will not be included in the property map. This condition
 * can occur if a specified property is provided that does not exist, or if the property does not have
 * a value on the underlying domain object.
 * Set includeNullProperties property to true to include properties with null value.
 * Sample Usage:<br/>
 * <br/>
 * <pre>
 *  List theProperties = new ArrayList();
 *	theProperties.add("Property1");
 *	theProperties.add("Property2");
 *	DomainObjectDecorator theDecorator = new SpecifiedPropertyDecorator(theObject,aMapper,theProperties);
 * </pre>
 * In this case, the getProperties() call would return a map containing [("PROPERTY1",value),("PROPERTY2",value)] or whatever
 *  Property1/2 had been mapped to via the mapper provided. Note that the Mapper is applied after the
 *  properties are tested for inclusion in the Map. This means the the specified properties passed
 *  in the list to the constructor should match the property name BEFORE the mapping is executed. For
 *  example, if you have a DOB property on the domain object and provide a mapper that maps this property
 *  to the property dateOfBirth, you should add "DOB" to the specified properties in the list passed to the 
 *  constructor.
 *  
 */
public class SpecifiedPropertyDecorator extends AbstractDomainObjectDecorator
{
	Mapper theMapper = new DefaultPropertyMapper();
	Map<String, Object> theProperties = new HashMap<String, Object>();
	Set<String> theSpecifiedProperties = new TreeSet<String>();
	private boolean includeNullProperties = false;
	
	public SpecifiedPropertyDecorator(DomainObject theObject,Mapper theMapper, List<String> theProperties)
	{
		this(theObject,theMapper);
		setSpecifiedProperties(theProperties);
		
	}
	public SpecifiedPropertyDecorator(DomainObject theObject, List<String> theProperties)
	{
		this(theObject);
		setSpecifiedProperties(theProperties);
	}
	
	/**
	 * @param theObject the DomainObject to be decorated
	 * @param theMapper the Mapper to use to map the property names with in the resulting properties map
	 * @param theProperties specifies which properties should be included in resulting property map
	 * you get from getProperties(). Should include the name of the property (without the get/is prefix)
	 * and does a case-insensitive comparison
	 * @param includeNullProperties indicates if properties will null values should be included in the resulting
	 * properties map. False by default, if not specified in the constructor.
	 */
	public SpecifiedPropertyDecorator(DomainObject theObject, Mapper theMapper,
			List<String> theProperties, boolean includeNullProperties) {
		this(theObject, theMapper, theProperties);
		setIncludeNullProperties(includeNullProperties);
	}
	
	public SpecifiedPropertyDecorator(DomainObject theObject, List<String> theProperties,
			boolean includeNullProperties) {
		this(theObject, theProperties);
		setIncludeNullProperties(includeNullProperties);
	}
	
	/**
	 * Using this constructor will cause prevent any properties in the object from
	 * being populated in the map, because no properties are specified.
	 * @deprecated
	 */
	public SpecifiedPropertyDecorator(DomainObject theObject, Mapper theMapper)
	{
		this(theObject);
		this.theMapper = theMapper;
	}
	
	/**
	 * Using this constructor will cause prevent any properties in the object from
	 * being populated in the map, because no properties are specified.
	 * @deprecated
	 */
	public SpecifiedPropertyDecorator(DomainObject theObject)
	{
		super(theObject);
	}
	

	public Map<String, ?> getProperties()
	{
		execute();
		return theProperties;
	}
	
	protected void execute(){
		List<Method> theGetters = ReflectionHelper.getAccessorMethods(getObject());
		for (Method theMethod: theGetters ){
			String originalMethodName = theMethod.getName();
			String propertyName = this.getPropertyName(originalMethodName);
			try {
				if (!originalMethodName.equals("getClass") && theSpecifiedProperties.contains(propertyName)){
					Object theValue = ReflectionHelper.invokeAccessorMethod(getObject(), theMethod, null);
					if ((includeNullProperties || theValue != null)){
						theProperties.put(theMapper.getName(getObject().getClass(), propertyName), theValue);
					}
				}
				
			} catch (CheckedApplicationException e){
				throw new UncheckedApplicationException(getClass(),"Unable to find Object/Property getObject(" + getObject() + ")",e);

			}
		}
	}

	private void setSpecifiedProperties(List<String> theKeys) {	
		for(String key: theKeys){			
				theSpecifiedProperties.add(key.toUpperCase());			
		}
	}
	
	public void setIncludeNullProperties(boolean includeNullProperties) {
		this.includeNullProperties = includeNullProperties;
	}
	
	String getPropertyName(String methodName){
		String propertyName;
		if (methodName.length() > 3 && "get".equalsIgnoreCase(methodName.substring(0,3))){
			propertyName = methodName.substring(3).toUpperCase();
		}else if (methodName.length() > 2 && "is".equalsIgnoreCase(methodName.substring(0,2))){
			propertyName = methodName.substring(2).toUpperCase();
		}else{
			propertyName = methodName.toUpperCase();
		}
		return propertyName;
	}
	
}
