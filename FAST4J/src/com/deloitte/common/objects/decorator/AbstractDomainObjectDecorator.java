package com.deloitte.common.objects.decorator;

import java.util.Collection;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.DomainObjectDecorator;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.framework.Error;

/**
 * AbstractDomainObjectDecoratork<br><br>
 * Delegates the DomainObject calls to the underlying object<br><br>
 * @author SIDT Team
 *
 */

public abstract class AbstractDomainObjectDecorator implements DomainObjectDecorator
{
	private DomainObject theObject;
	
	public AbstractDomainObjectDecorator(DomainObject theObject)
	{
		this.theObject = theObject;
	}

	public Object getObject()
	{
		return theObject;
	}
	
	public UUID getID() 
	{
		return theObject.getID();
	}

	public Collection<Error> getValidationErrors(Intent intent) 
	{
		return theObject.getValidationErrors(intent);
	}

	public int compareTo(DomainObject o) 
	{
		return theObject.compareTo(o);
	}

}
