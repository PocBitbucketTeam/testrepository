package com.deloitte.common.objects.decorator;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Mapper;
import com.deloitte.common.objects.mapper.DefaultPropertyMapper;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;
/**
 * PopulatedPropertyMask<br><br>
 * Accesses the getters and returns a property map of the methods that return a non-null value.<br>
 * By default ignores the getClass property<br><br>
 * Example:<br><br>
 *   David returns from getFirstName()<br>
 *   null  returns from getBirthDate()<br>
 *   <br><br>
 *   Map will contain: "FIRSTNAME", "David"<br><br>
 * @author SIDT Team
 *
 */

public class PopulatedPropertyDecorator extends AbstractDomainObjectDecorator
{
	Mapper theMapper = new DefaultPropertyMapper();
	Map<String, Object> theProperties = new HashMap<String, Object>();
	
	public PopulatedPropertyDecorator(DomainObject theObject, Mapper theMapper)
	{
		this(theObject);
		this.theMapper = theMapper;
	}
	
	public PopulatedPropertyDecorator(DomainObject theObject)
	{
		super(theObject);
	}
	

	public Map<String, ?> getProperties() {
		execute();
		return theProperties;
	}
	
	protected void execute(){
		List<Method> theGetters = ReflectionHelper.getAccessorMethods(getObject());
		for (Method theMethod: theGetters){
			try {
				Object theValue = ReflectionHelper.invokeAccessorMethod(getObject(), theMethod, null);
				if (theValue != null && !theMethod.getName().equals("getClass")){
					// Checking Method length is inevitable for 2 cases
					// 1. If there is a method "is" or "get" available then the method name becomes empty
					// 2. If there is a method "is" or "get" available then it throws StringIndexOutOfBound Exception 
					if (theMethod.getName().length() > 3 && "get".equalsIgnoreCase(theMethod.getName().substring(0,3))){
						theProperties.put(theMapper.getName(getObject().getClass(), theMethod.getName().substring(3).toUpperCase()), theValue);
					}else if (theMethod.getName().length() > 2 && "is".equalsIgnoreCase(theMethod.getName().substring(0,2))){
						theProperties.put(theMapper.getName(getObject().getClass(), theMethod.getName().substring(2).toUpperCase()), theValue);
					}else{
						theProperties.put(theMapper.getName(getObject().getClass(), theMethod.getName().toUpperCase()), theValue);
					}
				}
			}catch (CheckedApplicationException e){
				throw new UncheckedApplicationException(getClass(),"Unable to perform operation on the Object/method required",e);
			}
		}
	}
	

}
