package com.deloitte.common.objects.decorator;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.DomainObjectDecorator;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.interfaces.Mapper;
import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.objects.mapper.DefaultPropertyMapper;
import com.deloitte.common.util.ReflectionHelper;

/**
 * This class will return all the properties on an object in the properties map returned by
 * getProperties(). There are various constructors that allow the contents of the properties map
 * to be manipulated. Properties with null values can be optionally included or excluded in the map.
 * Be default, null property values will be excluded. Also, supplying a Mapper object to the
 * PropertyDecorator will cause the property names to be populated in the map with their mappings applied.
 * This functions the same as SpecifiedPropertyDecorator, which may be referenced for examples.
 * 
 * In contrast to SpecifiedPropertyDecorator, the list of property names supplied to the instance
 * identifies which properties should be skipped and not populated on the map. Not passing or passing
 * an empty skipList will cause all properties on the DomainObject to be populated on the map.
 */
public class PropertyDecorator implements DomainObjectDecorator {

	private final SpecifiedPropertyDecorator spd;
	
	/**
	 * @param theObject the DomainObject to be decorated
	 * @param theMapper a Mapper object to use to translate the property names which
	 * will be populated in the map
	 * @param skipList a List containing any properties in the object which should be skipped
	 * and not populated on the map
	 * @param includeNullProperties indicates if properties with null values should be included
	 * in the properties map
	 */
	public PropertyDecorator(DomainObject theObject, Mapper theMapper,
			List<String> skipList, boolean includeNullProperties) {
		spd = new SpecifiedPropertyDecorator(theObject, theMapper, getIncludedPropertyNames(theObject, skipList), includeNullProperties);
	}

	public PropertyDecorator(DomainObject theObject, Mapper theMapper,
			List<String> skipList) {
		this(theObject, theMapper, skipList, false);
	}

	public PropertyDecorator(DomainObject theObject, Mapper theMapper) {
		this(theObject, theMapper, Collections.EMPTY_LIST);
	}

	public PropertyDecorator(DomainObject theObject, List<String> skipList,
			boolean includeNullProperties) {
		this(theObject, new DefaultPropertyMapper(), skipList, includeNullProperties);
	}

	public PropertyDecorator(DomainObject theObject, List<String> skipList) {
		this(theObject, skipList, false);
	}

	public PropertyDecorator(DomainObject theObject) {
		this(theObject, Collections.EMPTY_LIST);
	}

	public PropertyDecorator(DomainObject theObject, boolean includeNullProperties) {
		this(theObject, Collections.EMPTY_LIST, includeNullProperties);
	}

	
	
	private List<String> getIncludedPropertyNames(DomainObject theObject,
			List<String> skipList){
		List<String> properties = getAllPropertyNames(theObject);
		for (String skippedProp : skipList){
			properties.remove(skippedProp.toUpperCase());
		}
		return properties;
	}
	
	private List<String> getAllPropertyNames(DomainObject theObject){
		//populate the full list of accessor methods
		List<Method> methods = ReflectionHelper.getAccessorMethods(theObject.getClass());
		List<String> specifiedProps = new ArrayList<String>();
		for (Method method : methods){
			specifiedProps.add(propertyifyAccessorName(method.getName()));
		}
		
		return specifiedProps;
	}
	
	private String propertyifyAccessorName(String methodName){
		String propertyName;
		if (methodName.length() > 3 && methodName.toLowerCase().startsWith("get")){
			propertyName = methodName.substring(3).toUpperCase();
		}else if (methodName.length() > 2 && methodName.toLowerCase().startsWith("is")){
			propertyName = methodName.substring(2).toUpperCase();
		}else{
			propertyName = methodName.toUpperCase();
		}
		return propertyName;
	}

	
	
	//delegated methods handled by wrapped SpecifiedPropertyDecorator
	public int compareTo(DomainObject o) {
		return spd.compareTo(o);
	}

	public UUID getID() {
		return spd.getID();
	}

	public Collection<Error> getValidationErrors(Intent intent) {
		return spd.getValidationErrors(intent);
	}

	public Map<String, ?> getProperties() {
		return spd.getProperties();
	}

	public Object getObject() {
		return spd.getObject();
	}
}
