package com.deloitte.common.objects.domain;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;

import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * This class is represents the Clob/Text datatype
 * When you want to Persist the Character data (like Unlimited Text or char[])
 * then the object of this class should be passed as a bind parameter to the DAO.<br/>
 * SampleUsage:<br/>
 * TextDocument textDoc = new TextDocument(char[]); <br/> Pass a char[] as argument <br/>
 * TextDocument textDoc = new TextDocument(String); <br/> Pass a String as argument <br/>
 * TextDocument textDoc = new textDoc(Reader); <br/> 
 * Pass a Reader as a argument like FileReader <br/>
 * 
 * @author SIDT Framework Team
 *
 */
public class TextDocument extends AbstractDocument
{
	
	private static final String DEFAULT_TYPE = "text/text";
	
	private static final long serialVersionUID = -1332661467899929085L;
	private char[] theCharacters;
	private static final int BUFFER_SIZE = 2048;
	
	public enum ErrorType{
		NULLTXTDOC
	}
	
	public TextDocument(char[] theCharacters){
		super(DEFAULT_TYPE);
		this.theCharacters=theCharacters;
	}
	
	public TextDocument(String string){
		this(string.toCharArray());
	}

	public TextDocument(char[] theCharacters, String fileName){
		super(DEFAULT_TYPE, fileName);
		this.theCharacters=theCharacters;
	}
	
	public TextDocument(String string, String fileName){
		this(string.toCharArray(), fileName);
	}

	public TextDocument(Reader reader){
		this(reader, null);
	}
	
	public TextDocument(Reader reader, String fileName){
		super(DEFAULT_TYPE, fileName);
        char[] buf = new char[BUFFER_SIZE]; 
	    CharArrayWriter theWriter = new CharArrayWriter(BUFFER_SIZE); 
        int count=0; 
	    try {
			while ((count=reader.read(buf)) != -1){ 
				theWriter.write(buf, 0, count); 
			}
		} catch (IOException e) {
			throw new UncheckedApplicationException(this.getClass(),"Unable to read from Reader", e);
		}
        theCharacters = theWriter.toCharArray(); 
	}
	
	public int length()
	{
		return theCharacters.length;
	}

	public InputStream getStream()
	{
		return new ReaderInputStream(new StringReader(new String(theCharacters)));
	}
	
	/**
	 *  Will return Errors with the following types:
	 * 		<br>-NULLTXTDOC - the text document is null
	 */
	public Collection<Error> getValidationErrors() {
		Collection<Error> theErrors = new ArrayList<Error>();
		if(theCharacters == null)
		{
			theErrors.add(new Error(ErrorType.NULLTXTDOC.name()));
		}
		return theErrors;
	}

	public Collection<Error> getValidationErrors(Intent intent) {
		return getValidationErrors();
	}
	
	public String toString()
	{
		return new String(theCharacters);
	}
	
	static class ReaderInputStream extends InputStream 
	{

		private Reader reader;

		public ReaderInputStream(Reader reader)
		{
			this.reader = reader;
		}

		public int read() throws IOException
		{
			return reader.read();
		}
	}

}
