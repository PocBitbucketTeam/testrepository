package com.deloitte.common.objects.domain;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.objects.framework.OID;

/**
 * Simple implementation of a DomainObject that applications can
 * extend for basic behavior.
 * <br/>
 * Note that Equals/Hashcode/CompareTo have been override to provide
 * semantic equivalents based on the UUID.
 * <br/> 
 * @author SIDT Framework Team
 *
 */
public abstract class AbstractDomainObject implements DomainObject, Serializable
{
	private static final long serialVersionUID=7889137800060154077L;
	private UUID    id;
	private int		hashcode = -1;
	
	protected AbstractDomainObject()
	{
		this(new OID());
	}
	
	protected AbstractDomainObject(UUID uuid)
	{
		setID(uuid);
	} 
	
	public UUID getID()
	{
		return this.id;
	}
	
	public void setID(UUID uuid)
	{
		this.id = uuid;
	}
	
	/**
	 * Returns true if the two domain objects represent the same logical
	 * domain entry, identified by having the same UUID value
	 */
	public boolean equals(Object o)	{
		if (this == o)		{
			return true;
		} else if (o == null || !(o instanceof DomainObject)) {
			return false;
		} else {
			return this.compareTo((DomainObject)o)==0;
		}
	}
	
	public int hashCode()
	{
		if (hashcode == -1)
		{
			hashcode = getID().getValue().hashCode(); 
		}
		return hashcode;
	}

	public int compareTo(DomainObject arg0) 
	{
		return getID().compareTo((arg0).getID());
	}

	public Collection<Error> getValidationErrors(Intent intent) 
	{
		return Collections.emptyList();
	}

	private void readObject( ObjectInputStream aStream ) throws IOException, ClassNotFoundException 
	{
		aStream.defaultReadObject();
		UUID id  = (UUID)aStream.readObject();
		setID( id );	}

	private void writeObject( ObjectOutputStream aStream ) throws IOException 
	{
		aStream.defaultWriteObject();
		aStream.writeObject( getID() );
	}
}
