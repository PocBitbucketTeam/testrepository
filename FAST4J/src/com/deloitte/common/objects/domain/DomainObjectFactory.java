package com.deloitte.common.objects.domain;

import java.lang.reflect.Proxy;

import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.util.ReflectionHelper;

/**
 * 
 * This class and DomainObjectProxy are BETA functionality. They are designed
 * to use Dynamic Proxies, use with care.
 * 
 * @deprecated
 */
@SuppressWarnings("unchecked")
public class DomainObjectFactory 
{
	
	public static Object create(Class theClass) 
	{

		return 	Proxy.newProxyInstance(
				theClass.getClassLoader(),
				ReflectionHelper.getDeclaredInterfaces(theClass), 
				new DomainObjectProxy(theClass));
	}

	// How do we determine if the Object is in the Backing Data Store
	public static Object create(Class theClass,UUID uuid )
	{
		return 	Proxy.newProxyInstance(
				theClass.getClassLoader(),
				ReflectionHelper.getDeclaredInterfaces(theClass), 
				new DomainObjectProxy(theClass,uuid));
	}

	public static void main(String[] args)
	{
	}
}
