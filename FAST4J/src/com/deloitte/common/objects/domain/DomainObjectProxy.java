package com.deloitte.common.objects.domain;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * 
 * This class and DomainObjectFactory are BETA functionality. They are designed
 * to use Dynamic Proxies, use with care.
 * 
 * @deprecated
 */
@SuppressWarnings("unchecked")
public class DomainObjectProxy implements InvocationHandler {

	private DomainObject delegate;

	/*
	 * The Cast to DomainObject ensures the correct behaviour later in the
	 * process. It is not necessary to do this.
	 */

	DomainObjectProxy(Class delegate) 
	{
		try {
			this.delegate = (DomainObject)delegate.newInstance();
		} catch (InstantiationException e) 
		{
			throw new UncheckedApplicationException(getClass(),"Unable to instantiate " + delegate,e);

		} catch (IllegalAccessException e) 
		{
			throw new UncheckedApplicationException(getClass(),"Unable to access " + delegate + " due to access restrictions",e);
		}
	}

	// @TODO, Could/Should set Constructor (Class c, Object[] parms) to allow
	//        instantiation of any Constructor
	DomainObjectProxy(Class delegate, UUID uuid) 
	{
		Class[] parameterClass = {UUID.class};
		Object[] parameterObject = {uuid};
		try 
		{
			Constructor constructor = delegate.getDeclaredConstructor(parameterClass);
			this.delegate = (DomainObject)constructor.newInstance(parameterObject);
		} 
		catch (SecurityException e) 		{ throw new RuntimeException(e);} 
		catch (NoSuchMethodException e) 	{ throw new RuntimeException(e);} 
		catch (IllegalArgumentException e)  { throw new RuntimeException(e);} 
		catch (InvocationTargetException e) { throw new RuntimeException(e);} 
		catch (InstantiationException e)    { throw new RuntimeException(e);} 
		catch (IllegalAccessException e)    { throw new RuntimeException(e);}
	}

	public Object invoke(Object proxy, Method method, Object[] args)
		throws Throwable {

		try {
			Object result = method.invoke(delegate, args);
			if (method.getName().startsWith("set") &&
			   !method.getName().startsWith("setDirty") &&
			   !method.getName().startsWith("setClean"))
			{
				//((DomainObject)delegate).setDirty(); 
			}
			return result;
		} catch (InvocationTargetException e) 
		{
			throw e.getTargetException();
		}
	}

}
