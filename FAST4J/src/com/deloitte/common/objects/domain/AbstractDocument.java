package com.deloitte.common.objects.domain;

import com.deloitte.common.interfaces.Document;

public abstract class AbstractDocument implements Document {

	private static final long serialVersionUID = -3247805983274598374L;

	private String type;
	private String fileName;
	
	AbstractDocument(String type){
		this(type, null);
	}

	AbstractDocument(String type, String fileName){
		this.type = type;
		this.fileName = fileName;
	}

	public String getType() {
		return type;
	}

	public String getFileName() {
		return fileName;
	}

}
