package com.deloitte.common.objects.domain;

public class XMLContainer extends AbstractDomainObject {

	private static final long serialVersionUID = 6780083851662832084L;
	
	private String xml;

	public XMLContainer()
	{
		super();
	}
	
	public XMLContainer(String xml)
	{
		setXml(xml);
	}
	
	public final String getXml()
	{
		return xml;
	}

	public final void setXml(String xml)
	{
		this.xml = xml;
	}
	
}
