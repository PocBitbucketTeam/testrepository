package com.deloitte.common.objects.domain;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * This class is represents the Blob datatype
 * When you want to Persist the Binary data (like Image, Video, Audo clips OR byte[])
 * then the object of this class should be passed as a bind parameter to the DAO.<br/>
 * SampleUsage:<br/>
 * BinaryDocument binaryDoc = new BinaryDocument(byte[]); <br/> Pass a byte[] as argument <br/>
 * BinaryDocument binaryDoc = new BinaryDocument(InputStream); <br/> 
 * Pass a inputstream as a argument like FileInputStream <br/>
 * 
 * @author SIDT Framework Team
 *
 */
public class BinaryDocument extends AbstractDocument
{
	private static final long serialVersionUID = -6280730334601523459L;
	
	private byte[] theBytes;
	
	private static final String DEFAULT_TYPE = "application/octet-stream";

	public enum ErrorType{
		NULLDOC, NULLTYPE
	}
	
	public BinaryDocument(byte[] bytes) {
		this(bytes, DEFAULT_TYPE);
	}
	
	public BinaryDocument(byte[] bytes, String type) {
		this(bytes, type, null);
	}
	
	public BinaryDocument(byte[] bytes, String type, String fileName) {
		super(type, fileName);
		this.theBytes = bytes;
	}
	
	public BinaryDocument(InputStream stream){
		this(stream, DEFAULT_TYPE);
	}

	public BinaryDocument(InputStream stream, String type) {
		this(stream, type, null);
	}

	public BinaryDocument(InputStream stream, String type, String fileName) {
		super(type, fileName);
		final int chunkSize = 2048; 
        byte[] buf = new byte[chunkSize]; 
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(chunkSize); 
        int count=0; 
        try {
			while ((count=stream.read(buf)) != -1) {
			    byteStream.write(buf, 0, count); 
			}
		} catch (IOException e) {
			throw new UncheckedApplicationException(this.getClass(),"Unable to read from InputStream", e);
		}

		this.theBytes = byteStream.toByteArray();
	}

	public int length()
	{
		return theBytes.length;
	}

	public InputStream getStream()
	{
		return new ByteArrayInputStream(theBytes);
	}
	
	public byte[] getBytes()
	{
		return theBytes;
	}
	
	/**
	 *  Will return Errors with the following types:
	 * 		<br>-NULLDOC - the document is null
	 */
	public Collection<Error> getValidationErrors() {
		Collection<Error> theErrors = new ArrayList<Error>();
		if(theBytes == null){
			theErrors.add(new Error(ErrorType.NULLDOC.name()));
		}
		if(getType() == null || getType().equals("")){
			theErrors.add(new Error(ErrorType.NULLTYPE.name()));
		}
		return theErrors;
	}

	public Collection<Error> getValidationErrors(Intent intent) {
		return getValidationErrors();
	}

	public String toString(){
		return "BinaryDocument[type=" + getType() + ", fileName=" + getFileName() + ", content length=" + getBytes().length + "]";
	}
}