package com.deloitte.common.objects.domain;

import java.math.BigInteger;

import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.util.Strings;


public class MockDomainObject extends AbstractDomainObject {
	
	private static final long serialVersionUID = 4343578455145399977L;
	
	private String value;
	private int primitive;
	private BigInteger wrapper;
	private boolean theBoolean;
	
	public boolean isTheBoolean() {
		return theBoolean;
	}

	public void setTheBoolean(boolean theBoolean) {
		this.theBoolean = theBoolean;
	}

	public MockDomainObject()
	{
		super();
	}
	
	public MockDomainObject(UUID uuid)
	{
		super(uuid);
	}

	public String toString()
	{
		return this.getClass().getName() + "[ID=" + getID() + ",value=" + getValue() + ",primitive="
				+ getPrimitive() + ",theBoolean=" + isTheBoolean() + ",wrapper=" + getWrapper() + "]";
	}

	public String getValue()
	{
		return value;
	}

	public int getPrimitive()
	{
		return this.primitive;
	}
	
	public void setPrimitive(int i)
	{
		this.primitive = i;
	}
	
	public void setValue(String value)
	{
		this.value = value;
	}
	
	public boolean isValueSet()
	{
		return !Strings.isEmpty(getValue());
	}
	
	public int getValueLength()
	{
		if (isValueSet())
		{
			return getValue().length();
		}
		return 0;
	}

	public BigInteger getWrapper() 
	{
		return wrapper;
	}

	public void setWrapper(BigInteger wrapper) {
		this.wrapper = wrapper;
	}
	
}
