package com.deloitte.common.objects;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.deloitte.common.interfaces.Population;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;
/**
 * ObjectPopulator allows attributes to be copied from One object to another or from a Map to an Object.
 * This could be used to "auto-populate" instead of hand-coding values moving between locations.
 * <br/><br/>
 * Example: ObjectPopulator op = new ObjectPopulator(fromobject,toObject,<Population>);
 * @see DefaultPropertyPopulator
 */
public class ObjectPopulator
{
	private Object to;
	private Object from;
	private Population populator;
	private List<Property> ignoreList;
	private static final List<String> EMPTY_STR_LIST = Collections.emptyList();	
	
	public ObjectPopulator(Object from, Object to, Population populator, List<String> ignoreList)
	{
		this.to = to;
		this.from = from;
		this.populator = populator;
		this.ignoreList = new ArrayList<Property>();

		if (ignoreList != null)
		{
			for(String ignoreKey:ignoreList){
				Property property = new Property(ignoreKey,from.getClass());
				this.ignoreList.add(property);
			}
		} 

	}

	public ObjectPopulator(Object from, Object to, Population populator)
	{
		this(from,to,populator, EMPTY_STR_LIST);
	}

	
//	TODO think this not a good way to remove the warning here
	@SuppressWarnings("unchecked")
	public final void execute() throws CheckedApplicationException
	{
		List<Property> properties = null;
		if (Map.class.isAssignableFrom(from.getClass()))
		{
			properties = getProperties((Map<String,?>)from);
		} else
		{
			properties = getProperties(from);
		}

		for (Property property:properties){
			if (populator.accepts(to,property.getPropertyValue(),property.getPropertyName()))
			{
				populator.populate(to,property.getPropertyValue(),property.getPropertyName());
			}
		}
	}
	
	private List<Property> getProperties(Map<String, ?> m) throws CheckedApplicationException
	{
		List<Property> properties = new ArrayList<Property>();
		
		for (String propertyName : m.keySet()){
			if (!ignoreList.contains(new Property(propertyName,null))){
				properties.add(new Property(propertyName,m.get(propertyName)));
			}
		}
		return properties;
	}

	private List<Property> getProperties(Object from) throws CheckedApplicationException
	{
		List<Method> accessors = ReflectionHelper.getAccessorMethods(from);
		List<Property> properties = new ArrayList<Property>();

		for (Method m:accessors){
			
			if (!ignoreList.contains(new Property(m.getName(),null))){
				if(m.getName().startsWith("get")){
					properties.add(new Property(m.getName().substring(3),ReflectionHelper.invokeAccessorMethod(from,m,null)));
				} else {
					properties.add(new Property(m.getName().substring(2),ReflectionHelper.invokeAccessorMethod(from,m,null)));
				}
			}
		}
		return properties;
	}
	
	private static class Property
	{
		String propertyName;
		Object propertyValue;
		
		Property(String propertyName, Object propertyValue)
		{
			setPropertyName(propertyName);
			setPropertyValue(propertyValue);
		}
		public String getPropertyName()
		{
			return propertyName;
		}
		public Object getPropertyValue()
		{
			return propertyValue;
		}
		public void setPropertyName(String propertyName)
		{
			if (propertyName.startsWith("get"))
			{
				this.propertyName = propertyName.substring(3);
			} else
			{
				this.propertyName = propertyName;
			}
		}
		public void setPropertyValue(Object propertyValue)
		{
			this.propertyValue = propertyValue;
		}
		
		public boolean equals(Object o)
		{
			Property p = (Property)o;
			String string = p.getPropertyName();
			if (string.startsWith("get"))
			{
				string = string.substring(3);
			}
			if (getPropertyName().equalsIgnoreCase(string))
			{
				return true;
			}
			return false;
		}
		
		public String toString()
		{
			return "[PropertyName=" + getPropertyName() + ", Value="+getPropertyValue() + "]";
		}
		
	}
}
