package com.deloitte.common.objects;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * This class build a java.util.Properties object by reading a properties File. This class
 * also provides default implementations of registering 
 * or loading from a properties file.
 * 
 * @author SIDT Framework Team
 *
 */
public abstract class AbstractPropertiesLoader implements Loader {

	private String fileName; 
	private Properties props;
	
	public AbstractPropertiesLoader(Properties props){
		super();
		this.props = props;
	}
	public AbstractPropertiesLoader(String fileName) {
		super();
		this.fileName = fileName;
	}
	
	public Properties getProperties(){
		if(props == null){
			props = new Properties();
			try {
				props.load(getClass().getResourceAsStream(fileName));
			} catch (IOException e) {
				throw new UncheckedApplicationException(this.getClass(),"Unable to retrieve properties from " + fileName, e);
			}
		}
		return props;
	}
	
	public void loadValues() throws CheckedApplicationException {
		Set<Map.Entry<Object,Object>> keys = getProperties().entrySet();
		for (Map.Entry<Object,Object> entry:keys) {
			loadValues(entry.getKey().toString(), entry.getValue().toString());
		}
	}
	
	protected abstract void loadValues(String propKey, String propVal) throws CheckedApplicationException;

}
