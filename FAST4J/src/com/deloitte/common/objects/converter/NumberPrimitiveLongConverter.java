package com.deloitte.common.objects.converter;

/**
 * NumberPrimitiveLongConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Long.MIN_VALUE > number < Long.MAX_VALUE<br>
 *  Output:     long<br>
 *  
 *  @author SIDT Framework Team
 */
public final class NumberPrimitiveLongConverter extends NumberLongConverter 
{
	public Class<?> getReturnType()
	{
		return long.class;
	}


}
