package com.deloitte.common.objects.converter;

import java.sql.Timestamp;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * StringDateConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Must be parseable in the DateFormat Supplied(Default @see AbstractCalendarConverter.DEFAULT_DATE_FORMAT)<BR>
 *  Output:     Timestamp<br>
 */
public final class StringTimestampConverter extends StringDateConverter 
{

	public StringTimestampConverter()
	{
		super();
	}

	public StringTimestampConverter(String format)
	{
		super(format);
	}
	
	public Class<?> getReturnType() 
	{
		return Timestamp.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		return new Timestamp(((java.util.Date)super.convert(from)).getTime());
		
	}

}
