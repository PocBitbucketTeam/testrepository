package com.deloitte.common.objects.converter;

import java.sql.Timestamp;
import java.util.Calendar;


import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * CalendarDateConverter<br><br>
 * 	Input:      Calendar<br>
 *  Conditions: ......<br>
 *  Output:     Timestamp<br>
 *  
 *   @author SIDT Framework Team
 */
public final class CalendarTimestampConverter extends AbstractCalendarDateConverter 
{
	public Class<?> getReturnType()
	{
		return Timestamp.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Calendar theObject = (Calendar)from;
		return new Timestamp(theObject.getTimeInMillis());
	}
}
