package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringByteConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: -128 > integer < 127<br>
 *  Output:     Byte<br>
 *  
 *  @author SIDT Framework Team
 */
public class StringByteConverter extends AbstractStringNumberConverter
{
	public Class<?> getReturnType()
	{
		return Byte.class;
	}
	
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Byte((String)from);
	}	
}
