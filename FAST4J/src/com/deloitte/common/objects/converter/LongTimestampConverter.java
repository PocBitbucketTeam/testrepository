package com.deloitte.common.objects.converter;

import java.sql.Timestamp;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * LongSQLDateConverter<br><br>
 * 	Input:      Long or long<br>
 *  Conditions: <br>
 *  Output:    Timestamp<br>
 *  @author SIDT Framework Team
 */
public final class LongTimestampConverter extends AbstractLongDateConverter
{
	public Class<?> getReturnType() 
	{
		return Timestamp.class;
	}

	public Object convert(Object from) throws CheckedApplicationException 
	{
		checkAndThrowException(from);
		return new Timestamp(new Long(from.toString()).longValue());
	}

		
}