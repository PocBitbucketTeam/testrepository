package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringFloatConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Float.MIN_VALUE > number < Float.MAX_VALUE<br>
 *  Output:     Float<br>
 *  
 *  @author SIDT Framework Team
 */
public  class StringFloatConverter extends AbstractStringNumberConverter 
{
	
	
	public Class<?> getReturnType() 
	{
		return Float.class;
	}

	
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Float((String)from);
	}	

}
