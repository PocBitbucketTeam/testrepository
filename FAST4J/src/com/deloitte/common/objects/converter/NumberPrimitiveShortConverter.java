package com.deloitte.common.objects.converter;

/**
 * NumberPrimitiveShortConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Short.MIN_VALUE > number < Short.MAX_VALUE<br>
 *  Output:     short<br>
 *  
 *  @author SIDT Framework Team
 */
public final class NumberPrimitiveShortConverter extends NumberShortConverter 
{


	public Class<?> getReturnType() 
	{
		return short.class;
	}


}
