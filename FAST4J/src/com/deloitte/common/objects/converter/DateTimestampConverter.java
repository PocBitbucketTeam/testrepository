package com.deloitte.common.objects.converter;

/**
 * DateTimestampConverter<br><br>
 * 	Input:      Util Date/SQL Date<br>
 *  
 *  Output:     Timestamp<br>
 *  
 *  @author SIDT Framework Team
 */

import java.sql.Timestamp;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public final class DateTimestampConverter extends AbstractDateConverter {

	public Object convert(Object from) throws CheckedApplicationException 
	{
		checkAndThrowException(from);
		return new Timestamp(((java.util.Date)from).getTime());
	}
	public Class<?> getReturnType() 
	{
		return Timestamp.class;
	}
}
