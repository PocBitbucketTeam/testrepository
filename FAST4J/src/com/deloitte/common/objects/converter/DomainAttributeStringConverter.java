package com.deloitte.common.objects.converter;

import java.text.Format;

import com.deloitte.common.interfaces.DomainAttribute;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 *  This Converter is responsible to convert DomainAttribute to a String. This converter will accept
 *  various parameters in its constructors to determine which method in the DomainAttribute implementation 
 *  class should be called to return the respective String representation.
 *  
 *  <p>
 *  1) No Argument constructor (Default Converter added to DefaultPropertyPopulator) - toString() is called. <br>
 *  2) DomainAttributeStringConverter(true) - same as above and 
 *     DomainAttributeStringConverter(false) - uses toDisplay() method <br>
 *  3) DomainAttributeStringConverter(Format format) - uses toDispaly(format) method.
 *  </p> 
 *   
 * 
 * @author SIDT Framework Team
 *
 */

public class DomainAttributeStringConverter extends BasicStringConverter {

	private Format domainAttributeFormat;
	private boolean plainDisplay=false;
	
	public DomainAttributeStringConverter()
	{
		this(true);
	}
	public DomainAttributeStringConverter(boolean plainDisplay) 
	{
		super();
		this.plainDisplay = plainDisplay;
	}
	
			
	public DomainAttributeStringConverter(Format format) 
	{
		this.domainAttributeFormat = format;
	}
	
	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { DomainAttribute.class };
		return c;
	}
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		DomainAttribute attribute=(DomainAttribute)from;
		if(plainDisplay) return from.toString();
		else if(domainAttributeFormat==null)return attribute.toDisplay();
		else return attribute.toDisplay(domainAttributeFormat);
	}	

}
