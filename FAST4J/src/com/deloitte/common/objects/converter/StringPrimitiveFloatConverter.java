package com.deloitte.common.objects.converter;


/**
 * StringFloatConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Float.MIN_VALUE > number < Float.MAX_VALUE<br>
 *  Output:     float<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringPrimitiveFloatConverter extends StringFloatConverter
{
	public Class<?> getReturnType() 
	{
		return float.class;
	}
	
}
