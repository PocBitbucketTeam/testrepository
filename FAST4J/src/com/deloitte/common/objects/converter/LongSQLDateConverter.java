package com.deloitte.common.objects.converter;

import java.sql.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * LongSQLDateConverter<br><br>
 * 	Input:      Long or long<br>
 *  Conditions: <br>
 *  Output:    sql Date<br>
 *  @author SIDT Framework Team
 */
public final class LongSQLDateConverter extends AbstractLongDateConverter
{
	public Class<?> getReturnType() 
	{
		return Date.class;
	}

	public Object convert(Object from) throws CheckedApplicationException 
	{
		checkAndThrowException(from);
		return new Date(new Long(from.toString()).longValue());
	}

		
}