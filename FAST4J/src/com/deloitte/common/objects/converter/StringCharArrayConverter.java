package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;


/**
 * StringCharArrayConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: should be a string as input<br>
 *  Output:     char[]<br>
 *  
 *  @author SIDT Framework Team
 */
public  class StringCharArrayConverter extends AbstractStringObjectConverter 
{
	public Class<?> getReturnType()
	{
		return char[].class;
	}
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return from.toString().toCharArray();
	}	
	
}
