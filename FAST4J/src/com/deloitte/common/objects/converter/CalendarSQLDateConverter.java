package com.deloitte.common.objects.converter;

import java.sql.Date;
import java.util.Calendar;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * CalendarSQLDateConverter<br><br>
 * 	Input:      Calendar<br>
 *  Conditions: ......<br>
 *  Output:     SQL Date<br>
 *  
 *   @author SIDT Framework Team
 */
public final class CalendarSQLDateConverter extends AbstractCalendarDateConverter 
{
	
	public Class<?> getReturnType()
	{
		return Date.class;
	}
	
	
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Calendar theObject = (Calendar)from;
		return new Date(theObject.getTimeInMillis());
	}
	
	
}
