package com.deloitte.common.objects.converter;

import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.logging.Logger;

import com.deloitte.common.objects.domain.BinaryDocument;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * This class is used to Convert the BinaryDocument to an Object (SerializedObject)
 * The Passed Object must be implement the Serialization OR Externalization
 * 
 * @author SIDT Framework Team
 *
 */
public final class SerializableObjectBinaryDocumentConverter extends AbstractConverter {

	private static final Logger log = Logger.getLogger(SerializableObjectBinaryDocumentConverter.class.getName());
	
	public Object convert(Object from) throws CheckedApplicationException {
		checkAndThrowException(from);
		BinaryDocument serializableDoc = null;
		ByteArrayOutputStream binaryOutputStream = null;
		ObjectOutputStream os = null;
		try{
			binaryOutputStream = new ByteArrayOutputStream();
			os = new ObjectOutputStream(binaryOutputStream);
			os.writeObject(from);
			serializableDoc = new BinaryDocument(binaryOutputStream.toByteArray());
		} catch (IOException e) {
			log.warning("IOException Occured while converting the Object to BinaryDocument");
			throw new CheckedApplicationException(this.getClass(),"Unable to Serailize the Object", e);
		}finally{
			try{
				if(os != null){
					os.flush();
					os.reset();
					os.close();
				}
				if(binaryOutputStream != null){
					binaryOutputStream.flush();
					binaryOutputStream.reset();
					binaryOutputStream.close();
				}
			}catch (IOException e) {
				log.warning("IOException Occured while  Close/Flush/Rese the ObjectOutputStream/ByteArrayOutputStream ");
				throw new CheckedApplicationException(this.getClass(),"Unable to Close/Flush/Reset the OutputStream", e);
			}
		}
		return serializableDoc;
	}

	public Class<?> getReturnType() {
		return BinaryDocument.class;
	}
	
	public Class<?>[] getParameterTypes(){
		Class<?>[] c = { Serializable.class, Externalizable.class} ;
		return c;
	}
}
