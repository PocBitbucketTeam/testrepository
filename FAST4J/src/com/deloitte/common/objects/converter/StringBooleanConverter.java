package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * StringBooleanConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Can be any class<br>
 *  Output:     Boolean(false if invalid input)<br>
 *  
 *  @author SIDT Framework Team
 */

public  class StringBooleanConverter extends AbstractStringObjectConverter 
{
	public Class<?> getReturnType()
	{
		return Boolean.class;
	}
	
		// Booleans can be created from any string, no further check required
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Boolean((String)from);
		
	}	

}
