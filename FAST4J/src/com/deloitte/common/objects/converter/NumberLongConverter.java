package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberLongConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Target property type should be primitive or wrapper for long.<br>
 *  Output:     Long which can also be mapped back to primitive by Reflection <br>
 *  
 *  @author SIDT Framework Team
 */

public class NumberLongConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return Long.class;
	}
	

	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Number n=(Number)from;
		return new Long(n.longValue());		
	}
		
}
