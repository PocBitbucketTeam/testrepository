package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringIntegerConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Integer.MIN_VALUE > integer < Integer.MAX_VALUE<br>
 *  Output:     Integer<br>
 *  
 *  @author SIDT Framework Team
 */
public  class StringIntegerConverter extends AbstractStringNumberConverter 
{


	public Class<?> getReturnType() 
	{
		return Integer.class;
	}

	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Integer((String)from);
	}	
}
