package com.deloitte.common.objects.converter;


import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringLongConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Long.MIN_VALUE > integer < Long.MAX_VALUE<br>
 *  Output:     Long<br>
 *  
 *  @author SIDT Framework Team
 */
public  class StringLongConverter extends AbstractStringNumberConverter 
{
		
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Long((String)from);
	}	
	
	public Class<?> getReturnType()
	{		
		return Long.class;
	}
	
}
