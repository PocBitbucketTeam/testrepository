package com.deloitte.common.objects.converter;

import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * StringCalendarConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Must be parseable in the DateFormat Supplied(Default @see AbstractCalendarConverter.DEFAULT_DATE_FORMAT)<BR>
 *  Output:     Calendar<br>
 */
public final class StringCalendarConverter extends AbstractCalendarConverter 
{

	public StringCalendarConverter()
	{
		super();
	}
	
	public StringCalendarConverter(String format)
	{
		super(format);
	}
	
	public Class<?> getReturnType()
	{
	 	return Calendar.class;
	}
	
	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { String.class };
		return c;
	}
	
	public boolean accepts(Object from, Class<?> to)
	{
		return super.accepts(from,to) &&
				 createDateFormat().parseObject((String)from,new ParsePosition(0)) != null; 
				 
	}
		
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Calendar theObject = Calendar.getInstance();
		theObject.setTime((Date)createDateFormat().parseObject((String)from,new ParsePosition(0)));
		return theObject;

	}
}
