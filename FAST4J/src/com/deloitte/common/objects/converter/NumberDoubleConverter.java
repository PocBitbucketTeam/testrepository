package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberDoubleConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Target property type should be primitive or wrapper for double.<br>
 *  Output:     Double which can also be mapped back to primitive by Reflection <br>
 *  
 *  @author SIDT Framework Team
 */

public class NumberDoubleConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return Double.class;
	}
	
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);	
		Number n=(Number)from;
		return new Double(n.doubleValue());		
	}	
	
}
