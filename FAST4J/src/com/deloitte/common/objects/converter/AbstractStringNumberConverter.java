package com.deloitte.common.objects.converter;

/**
 *  The base class that by default implements the boundary checks for different numeric types
 *  during the conversion process to its respective numeric type from String.  It is applicable for all the 
 *  primitive as well as the Wrapper targets from Source strings.
 * 
 * 	@author SIDT Framework team
 */


public abstract class AbstractStringNumberConverter extends AbstractConverter
{
	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { String.class };
		return c;

	}

	public boolean accepts(Object from, Class<?> to)
	{
	return super.accepts(from,to)&&NumberConversionHelper.accepts(from, to);
	}



}
