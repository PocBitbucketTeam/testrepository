package com.deloitte.common.objects.converter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;


public abstract class AbstractCalendarConverter extends AbstractConverter 
{
	public final static String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";
	
	private final String format;
	
	protected AbstractCalendarConverter(){
		this(DEFAULT_DATE_FORMAT);
	}
	
	protected AbstractCalendarConverter(String format){
		this.format = format;
	}
	
	protected DateFormat createDateFormat(){
		return new SimpleDateFormat(format);
	}
}
