package com.deloitte.common.objects.converter;


import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.Arrays;

/**
 * DateLongConverter<br><br>
 * 	Input:      Date<br>
 *  Conditions: ....
 *  Output:     Long / long<br>
 *  
 *   @author SIDT Framework Team
 */
public final class DateLongConverter extends AbstractConverter 
{
	
	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { Date.class};
		return c;
	}
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Date d=(Date)from;
		return new Long(d.getTime());
	}	
	
	public Class<?> getReturnType()
	{
		return Long.class;
	}
	
	public boolean accepts(Object from, Class<?> to)
	{
	return (from != null && to != null) &&
			   (Arrays.isOfType(from.getClass(),getParameterTypes())) &&
			   		   (to==Long.class||to==long.class);

	}
	
	
}
