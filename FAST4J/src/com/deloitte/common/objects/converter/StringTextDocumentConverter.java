package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.domain.TextDocument;
import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * StringTextDocumentConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Target class is a TextDocument<br>
 *  Output:     TextDocument<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringTextDocumentConverter extends AbstractStringObjectConverter 
{


	public Class<?> getReturnType() 
	{
		return TextDocument.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new TextDocument((String)from);
	}	
}
