package com.deloitte.common.objects.converter;



import java.math.BigDecimal;
import java.math.BigInteger;

import com.deloitte.common.util.NumberConverterUtility;
import com.deloitte.common.util.Strings;

/**
 * Helper class that can be used by both String to Number/Primitive Converters as well as
 * converters that are sub classes of AbstractNumericConverter, to find whether the types are compatible for
 * conversion using the accepts method. Preferred Delegation over inheritence to have multiple types of converters
 *  used to convert to numbers,  rather than just from String to Number.
 *  
 *  @author SIDT Framework Team
 * 
 */

public class NumberConversionHelper 
{
	public static boolean accepts(Object from, Class<?> to)
	{
			if(from==null||to==null)return false;
		
			else if(to.equals(Integer.class)||to.equals(int.class))
			{
				return NumberConverterUtility.isInteger(from.toString());
			}
			else if(to.equals(Byte.class)||to.equals(byte.class))
			{
				return NumberConverterUtility.isByte(from.toString());
			}
			else if(to.equals(Short.class)||to.equals(short.class))
			{
				return NumberConverterUtility.isShort(from.toString());
			}
			else if(to.equals(Float.class)||to.equals(float.class))
			{
				return NumberConverterUtility.isFloat(from.toString());
			}
			
			else if(to.equals(Double.class)||to.equals(double.class))
			{
				return NumberConverterUtility.isDouble(from.toString());
			}
			else if(to.equals(Long.class)||to.equals(long.class))
			{
				return NumberConverterUtility.isLong(from.toString());
			}
			else if(to.equals(BigDecimal.class))
			{
				return (Strings.isDecimal(from.toString()) || Strings.isInteger(from.toString()));
			}
			else if(to.equals(BigInteger.class))
			{
				return Strings.isInteger(from.toString());
			}
			else
			{
		return false;
			}
	}
	
	
	
	
	
}
