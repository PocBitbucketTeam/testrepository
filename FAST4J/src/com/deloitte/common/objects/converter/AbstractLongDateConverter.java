package com.deloitte.common.objects.converter;

/**
 * 
 * Abstract super class for converters that convert long (time in millisecs) to Date and its subclasses
 * 
 * @author SIDT Framework team
 */

public abstract class AbstractLongDateConverter extends AbstractConverter {

	public Class<?>[] getParameterTypes() 
	{
		Class<?>[] c = { Long.class,long.class };
		return c;
	}
	
}
