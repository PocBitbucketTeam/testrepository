package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.business.CreditCard;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringCreditCardConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: ......<br>
 *  Output:     CreditCard &ltDomain Attribute&rt<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringCreditCardConverter extends AbstractStringObjectConverter 
{
	

	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new CreditCard((String)from);
	}	
	
	
	public Class<?> getReturnType()
	{
		return CreditCard.class;
	}

	
}
