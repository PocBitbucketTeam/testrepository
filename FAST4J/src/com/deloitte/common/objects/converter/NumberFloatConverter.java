package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberFloatConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Target property type should be primitive or wrapper for float.<br>
 *  Output:     Float which can also be mapped back to primitive by Reflection <br>
 *  
 *  @author SIDT Framework Team
 */

public class NumberFloatConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return Float.class;
	}
	
		
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Number n=(Number)from;
		return new Float(n.floatValue());		
	}
	
}
