package com.deloitte.common.objects.converter;


import java.math.BigInteger;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberBigIntegerConverter<br><br>
 * 	Input:      Number<br>
 *  Output:     BigInteger </br>
 *  @author SIDT Framework Team
 */

public final class NumberBigIntegerConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return BigInteger.class;
	}
	

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		String s=(""+from).trim();
		return new BigInteger(s);
		
 }
	
}
