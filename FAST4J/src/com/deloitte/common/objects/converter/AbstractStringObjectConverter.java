package com.deloitte.common.objects.converter;

/**
 * 
 * The base class for all possible Object types as target of conversion from source String.
 * 
 * @author SIDT Framework Team
 *
 */

public abstract class AbstractStringObjectConverter extends AbstractConverter 
{

	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { String.class };
		return c;

	}

}
