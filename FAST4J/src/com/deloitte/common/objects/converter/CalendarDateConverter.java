package com.deloitte.common.objects.converter;

import java.util.Calendar;
import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * CalendarDateConverter<br><br>
 * 	Input:      Calendar<br>
 *  Conditions: ......<br>
 *  Output:     util Date<br>
 *  
 *   @author SIDT Framework Team
 */
public final class CalendarDateConverter extends AbstractCalendarDateConverter 
{
	public Class<?> getReturnType()
	{
		return Date.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Calendar theObject = (Calendar)from;
		return new Date(theObject.getTimeInMillis());
	}
}
