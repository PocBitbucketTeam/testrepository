package com.deloitte.common.objects.converter;

/**
 * StringIntegerConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Integer.MIN_VALUE > number < Integer.MAX_VALUE<br>
 *  Output:     int<br>
 *  
 *  @author SIDT Framework Team
 */



public final class StringPrimitiveIntConverter extends StringIntegerConverter
{
	public Class<?> getReturnType()
	{
		return int.class;
	}
}
