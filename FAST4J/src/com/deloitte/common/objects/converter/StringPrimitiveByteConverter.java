package com.deloitte.common.objects.converter;

/**
 * StringPrimitiveByteConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: -128 > number < 127<br>
 *  Output:     byte<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringPrimitiveByteConverter extends StringByteConverter 
{
	public Class<?> getReturnType()
	{
		return byte.class;
	}
	
}
