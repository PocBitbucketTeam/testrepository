package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public class StringToEnumConverter extends AbstractConverter {
	
	@SuppressWarnings("rawtypes")
	private Class enumClass;
	
	public StringToEnumConverter(Class<? extends Enum<?>> enumClass) {
		super();
		this.enumClass = enumClass;
	}

	@SuppressWarnings("unchecked")
	public Object convert(Object from) throws CheckedApplicationException {
		checkAndThrowException(from);
		return Enum.valueOf(enumClass, (String)from);	
	}

	public Class<?> getReturnType() {
		return enumClass;
	}

	public Class<?>[] getParameterTypes() {
		Class<?>[] classes = {String.class};
		return classes;
	}

}
