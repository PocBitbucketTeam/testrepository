package com.deloitte.common.objects.converter;

import java.math.BigDecimal;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringBigDecimalConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Must be a number<br>
 *  Output:     BigDecimal<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringBigDecimalConverter extends AbstractStringNumberConverter 
{


	public Class<?> getReturnType() 
	{
		return BigDecimal.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new BigDecimal((String)from);
	}	
}
