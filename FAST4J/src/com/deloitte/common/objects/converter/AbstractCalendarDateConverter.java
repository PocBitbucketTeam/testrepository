package com.deloitte.common.objects.converter;

import java.util.Calendar;
/**
 * 
 * Super abstract class for all the converters that convert from Calendar to Date and its subclasses
 * 
 * @author SIDT Framework team
 *
 */

public abstract class AbstractCalendarDateConverter extends AbstractConverter {

	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { Calendar.class };
		return c;
	}
	
}
