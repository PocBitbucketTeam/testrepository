package com.deloitte.common.objects.converter;

import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * LongDateConverter<br><br>
 * 	Input:      Long or long<br>
 *  Conditions: <br>
 *  Output:    util Date<br>
 *  @author SIDT Framework Team
 */
public final class LongDateConverter extends AbstractLongDateConverter
{
	public Class<?> getReturnType() 
	{
		return Date.class;
	}

	public Object convert(Object from) throws CheckedApplicationException 
	{
		checkAndThrowException(from);
		return new Date(new Long(from.toString()).longValue());
	}
	
}