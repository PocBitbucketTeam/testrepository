package com.deloitte.common.objects.converter;

/**
 * StringLongConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Long.MIN_VALUE > number < Long.MAX_VALUE<br>
 *  Output:     long<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringPrimitiveLongConverter extends StringLongConverter 
{
	public Class<?> getReturnType()
	{
		return long.class;
	}

}
