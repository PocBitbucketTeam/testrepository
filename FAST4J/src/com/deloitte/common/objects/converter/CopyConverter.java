package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * CopyConverter<br><br>
 * 	Input:      Any Class<br>
 *  Conditions: Input class and target class must the same or assignable<br>
 *  Output:     Same Class as Input<br>
 *   @author SIDT Framework Team
 */

public class CopyConverter extends AbstractConverter
{

	public Class<?> getReturnType()
	{
		return Object.class; // Hmmm what do I do?
	}
	
	public boolean accepts(Object from, Class<?> to)
	{
		return from != null &&
				 to != null &&
				 to.isAssignableFrom(from.getClass());
	}
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return from;	// No conversion necessary, types are naturally compatible or can be assigned
	}
		
}

