package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * WrapperPrimitiveSymmetricConverter <br>
 * If a wrapper is invoked as input to a primitive method signature, Java reflection will performing the
 * "un/boxing" for you. As a result this converter merely ensures the match of wrappers to primitives. This
 * is a little bit of a hack in that it makes a big assumption that you will be using Reflection to populate
 * your data.
 * <br>
 * 	Input:      Primitive (or) Wrapper for Char and boolean<br>
 *  Conditions: Primtive Wrapper matches primitive converting to (eg Boolean --> boolean<br>
 *  Output:      Wrapper(or)primitive  via Reflection<br>
 *  
 *  @author SIDT Framework Team
 */

public final class WrapperPrimitiveSymmetricConverter extends AbstractConverter 
{
	
	public Class<?> getReturnType()
	{
		return Object.class;
	}
	
	public Class<?>[] getParametersTypes()
	{
		Class<?>[] c = { Boolean.class, Character.class,boolean.class,char.class };
		return c;
	}

	public boolean accepts(Object from, Class<?> to)
	{
			boolean theAnswer = false;
			if (
			   (from != null && to != null) &&
			   ((Boolean.class  == from.getClass() && boolean.class == to)||
			   (Character.class  == from.getClass() && char.class == to) ||
			   (Boolean.class  == to && boolean.class == from.getClass())||
			   (Character.class  == to && char.class == from.getClass())))
			   {
				   theAnswer = true;
			   }
			return theAnswer;
	}
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		
		return from;	// No conversion necessary, types are naturally compatible or can be assigned
	}
}
