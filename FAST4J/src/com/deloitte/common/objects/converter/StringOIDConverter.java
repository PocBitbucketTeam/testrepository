package com.deloitte.common.objects.converter;

import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.OID;
/**
 * StringOIDConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: From class sub class of java.langNumber or<br> 
 *  			From class string and integer value<br>
 *  			with a UUID target class<br>
 *  Output:     OID<br>
 *  
 *  @author SIDT Framework Team
 */
public class StringOIDConverter extends AbstractConverter
{
	
	public Class<?> getReturnType() 
	{
		return UUID.class;
	}

	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = {UUID.class, Number.class, String.class };
		return c;
	}
	
	public boolean accepts(Object from, Class<?> to)
	{
		return super.accepts(from,to) ;
	}
		
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new OID((String)from.toString());
	}	
}