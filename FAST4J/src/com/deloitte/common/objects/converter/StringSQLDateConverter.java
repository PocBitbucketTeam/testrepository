package com.deloitte.common.objects.converter;

import java.sql.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * StringDateConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Must be parseable in the DateFormat Supplied(Default @see AbstractCalendarConverter.DEFAULT_DATE_FORMAT)<BR>
 *  Output:     SQL Date<br>
 */
public final class StringSQLDateConverter extends StringDateConverter 
{

	public StringSQLDateConverter()
	{
		super();
	}

	public StringSQLDateConverter(String format)
	{
		super(format);
	}
	
	public Class<?> getReturnType() 
	{
		return Date.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		return new Date(((java.util.Date)super.convert(from)).getTime());
	}

}
