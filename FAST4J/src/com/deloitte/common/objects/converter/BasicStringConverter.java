package com.deloitte.common.objects.converter;

import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.domain.TextDocument;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	StringConverter <br><br>
 * 	Input: Number and its subclasses as well as the primitives including character and boolean,
 *  TextDocument,UUID (OID) object of the FAST4J framework.
 * 	Other FAST4J Objects that are subclasses of UUID,DomainAttribute and Date Objects are covered by the subclasses 
 *  of this converter.<br><br>
 *  Conditions: The from class should cleanly over ride the toString method.<br> <p>
 *  <b>Example:&nbsp&nbsp</b> From a DomainObject if the data type is BigDecimal and in a struts form its String
 *  this type of conversion matters. <br>
 *  Output:     String <br>
 *  
 *  @author SIDT Framework Team
 *  
 */
public class BasicStringConverter extends AbstractConverter 
{
	

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return from.toString();
	}	

	public Class<?> getReturnType()
	{
		return String.class;
	}

	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = {Number.class,int.class,float.class,double.class,byte.class,
				short.class,long.class,Character.class,Boolean.class,boolean.class,
				char.class,TextDocument.class,UUID.class,char[].class,Character[].class};
		return c;
	}
}
