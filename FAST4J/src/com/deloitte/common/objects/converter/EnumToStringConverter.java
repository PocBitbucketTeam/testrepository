package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public class EnumToStringConverter extends AbstractConverter {

	public Object convert(Object from) throws CheckedApplicationException {
		checkAndThrowException(from);
		return ((Enum<?>)from).name();
	}

	public Class<?> getReturnType() {
		return String.class;
	}

	public Class<?>[] getParameterTypes() {
		Class<?>[] classes = {Enum.class};
		return classes;
	}

}
