package com.deloitte.common.objects.converter;

import java.text.ParsePosition;
import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * StringDateConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Must be parseable in the DateFormat Supplied(Default @see AbstractCalendarConverter.DEFAULT_DATE_FORMAT)<BR>
 *  Output:     Util Date<br>
 */
public class StringDateConverter extends AbstractCalendarConverter 
{

	public StringDateConverter()
	{
		super();
	}
	
	public StringDateConverter(String format)
	{
		super(format);
	}
	
	public Class<?>[] getParameterTypes() 
	{
		Class<?>[] c = { String.class };
		return c;
	}

	public Class<?> getReturnType() 
	{
		return Date.class;
	}

	public boolean accepts(Object from, Class<?> to)
	{
		return super.accepts(from, to) &&
				 createDateFormat().parseObject((String)from,new ParsePosition(0)) != null;
	}
		
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return (Date)createDateFormat().parseObject((String)from,new ParsePosition(0));
	}

}
