package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberIntegerConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Target property type should be primitive or wrapper for int.<br>
 *  Output:     Integer which can also be mapped back to primitive by Reflection <br>
 *  
 *  @author SIDT Framework Team
 */

public class NumberIntegerConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return Integer.class;
	}
	
		
	public Object convert(Object from) throws CheckedApplicationException
	{	
		checkAndThrowException(from);
		Number n=(Number)from;
		return new Integer(n.intValue());		
	}
		
}
