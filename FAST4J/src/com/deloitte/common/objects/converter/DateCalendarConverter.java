package com.deloitte.common.objects.converter;

import java.util.Calendar;
import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * 	DateCalendarConverter<br><br>
 * 	Input:      Date or its subclasses<br>
 *  Output:     Calendar<br>
 *  
 *   @author SIDT Framework Team
 */
public final class DateCalendarConverter extends AbstractConverter 
{
	
	public DateCalendarConverter()
	{
		super();
	}
	
	public Class<?> getReturnType()
	{
	 	return Calendar.class;
	}
	
	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { java.util.Date.class};
		return c;
	}
		
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Calendar theObject = Calendar.getInstance();
		Date theDate = (Date)from;
		theObject.setTimeInMillis(theDate.getTime());
		return theObject;

	}

}
