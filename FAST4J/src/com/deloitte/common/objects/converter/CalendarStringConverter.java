package com.deloitte.common.objects.converter;

import java.util.Calendar;
import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * CalendarStringConverter<br><br>
 * 	Input:     Calendar <br>
 *  Conditions: ....<br>
 *  Output:     String<br>
 */
public final class CalendarStringConverter extends AbstractCalendarConverter 
{

	public CalendarStringConverter()
	{
		super();
	}
	
	public CalendarStringConverter(String format)
	{
		super(format);
	}
	
	public Class<?> getReturnType()
	{
	 	return String.class;
	}
	
	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { Calendar.class };
		return c;
	}
	
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		Calendar theObject = (Calendar)from;
		Date date=theObject.getTime();
		return createDateFormat().format(date);

	}
}
