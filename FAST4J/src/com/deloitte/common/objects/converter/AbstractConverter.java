package com.deloitte.common.objects.converter;

import com.deloitte.common.interfaces.Converter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.Arrays;

/**
 * Abstract Super class for all the Converters. 
 * 
 * @author SIDT Framework team
 *
 */


public abstract class AbstractConverter implements Converter {

	public boolean equals(Object o)
	{
		Converter that = (Converter)o;
		return getClass().equals(that.getClass());
	}

	public int hashCode()
	{
		return this.getClass().hashCode();
	}
	
	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { Object.class } ;
		return c;
	}
	
	protected void checkAndThrowException(Object from) throws CheckedApplicationException
	{
		if(!accepts(from,getReturnType()))
		{
			if (from != null)
			{
				throw new CheckedApplicationException(getClass(),"Could not convert the value " + from + " of class = " + from.getClass().getName() + " to a " + getReturnType().getName());	
			}
			throw new CheckedApplicationException(getClass(),"Could not convert a null value to a " + getReturnType().getName());
		}

	}
	
	public boolean accepts(Object from, Class<?> to)
	{
		/*
		String message = "+++++++++++ Accepts: ";
		message = message + "\n\t\t From == null: " + (from != null;
		message = message + "\n\t\t To   == null: " + (to != null);
		message = message + "\n\t\t Arrays of type: " + (Arrays.isOfType(from.getClass(),getParameterTypes()));
		message = message + "\n\t\t Return type   : To="+ to + " ReturnType "+ getReturnType() + " " + (to.equals(getReturnType()));
		logger.info(message);
		*/
		return from != null &&
				  to != null &&
				  Arrays.isOfType(from.getClass(),getParameterTypes()) &&
				  (to.equals(getReturnType())
			);
	}


}
