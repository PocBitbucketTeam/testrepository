package com.deloitte.common.objects.converter;

/**
 * NumberPrimitiveByteConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: -128 > value < 127<br>
 *  Output:     byte<br>
 *  
 *  @author SIDT Framework Team
 */
public final class NumberPrimitiveByteConverter extends NumberByteConverter 
{
	public Class<?> getReturnType()
	{
		return byte.class;
	}
	
	
}
