package com.deloitte.common.objects.converter;

import java.util.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * StringDateConverter<br><br>
 * 	Input:      util/sql Date<br>
 *  Conditions: ......<BR>
 *  Output:     String<br>
 */
public final class DateStringConverter extends AbstractCalendarConverter 
{

	public DateStringConverter()
	{
		super();
	}
	
	public DateStringConverter(String format)
	{
		super(format);
	}
	
	public Class<?>[] getParameterTypes() 
	{
		Class<?>[] c = { Date.class };
		return c;
	}

	public Class<?> getReturnType() 
	{
		return String.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return createDateFormat().format((Date)from);
	}
	
	

}
