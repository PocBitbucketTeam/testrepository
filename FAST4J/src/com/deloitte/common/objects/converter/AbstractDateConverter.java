package com.deloitte.common.objects.converter;

import java.util.Date;

/**
 * Super abstract class for converters that convert from subclasses of Date to Date 
 * 
 * @author SIDT Framework team 
 */

public abstract class AbstractDateConverter extends AbstractConverter {

	public Class<?>[] getParameterTypes() 
	{
		Class<?>[] c = { Date.class };
		return c;
	}

}
