package com.deloitte.common.objects.converter;

/**
 * StringBooleanConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Can be any class<br>
 *  Output:     boolean(false if invalid input)<br>
 *  
 *  @author SIDT Framework Team
 */

public final class StringPrimitiveBooleanConverter extends StringBooleanConverter
{
	public Class<?> getReturnType()
	{
		return boolean.class;
	}
}
