package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberShortConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Target property type should be primitive or wrapper for short.<br>
 *  Output:     Short which can also be mapped back to primitive by Reflection <br>
 *  
 *  @author SIDT Framework Team
 */


	public class NumberShortConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return Short.class;
	}
	
		
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);	
		Number n=(Number)from;
		return new Short(n.shortValue());
	}	
	
}
