package com.deloitte.common.objects.converter;

/**
 *  DateSQLDateConverter<br><br>
 * 	Input:      Util Date/Timestamp<br>
 *  
 *  Output:     SQL Date<br>
 *  
 *   @author SIDT Framework Team
 */

import java.sql.Date;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public final class DateSQLDateConverter extends AbstractDateConverter {

	public Object convert(Object from) throws CheckedApplicationException 
	{
		checkAndThrowException(from);
		return new Date(((java.util.Date)from).getTime());
	}
	public Class<?> getReturnType() 
	{
		return Date.class;
	}
	


}
