package com.deloitte.common.objects.converter;


/**
 * StringDoubleConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Double.MIN_VALUE >	number < Double.MAX_VALUE<br>
 *  Output:     double<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringPrimitiveDoubleConverter extends StringDoubleConverter {

	public Class<?> getReturnType()	{
		return double.class;
	}
}
