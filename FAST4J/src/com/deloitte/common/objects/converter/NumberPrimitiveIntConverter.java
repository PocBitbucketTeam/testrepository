package com.deloitte.common.objects.converter;

/**
 * NumberPrimitiveIntConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Integer.MIN_VALUE > number < Integer.MAX_VALUE<br>
 *  Output:     int<br>
 *  
 *  @author SIDT Framework Team
 */



public final class NumberPrimitiveIntConverter extends NumberIntegerConverter
{
	public Class<?> getReturnType()
	{
		return int.class;
	}
}
