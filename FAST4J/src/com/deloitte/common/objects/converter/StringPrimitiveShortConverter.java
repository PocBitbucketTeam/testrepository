package com.deloitte.common.objects.converter;

/**
 * StringShortConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Short.MIN_VALUE > number < Short.MAX_VALUE<br>
 *  Output:     short<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringPrimitiveShortConverter extends StringShortConverter 
{
	public Class<?> getReturnType() 
	{
		return short.class;
	}

}
