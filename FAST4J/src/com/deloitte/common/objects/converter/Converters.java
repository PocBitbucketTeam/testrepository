package com.deloitte.common.objects.converter;

import java.util.ArrayList;
import java.util.Collection;

import com.deloitte.common.interfaces.Converter;

/**
 * This class exclusively consists of static method getDefaultConverters() that
 * returns the Collection of all the available converters in the framework and any
 * application specific custom converters that are added using a static method addConverter(). 
 * The Collection returned by this method is typesafe - All are Implementation
 * classes for the Converter framework.
 * 
 * The static method addConverter() allows the users to add converters, that will be available
 * throughout the application and provides a way to register the custom converter globally
 * 
 * @author SIDT Framework team
 * 
 */
public final class Converters {
	
	private static Collection<Converter> customConverters = new ArrayList<Converter>();
	
	private Converters() 
	{
	
	}
	

	/**
	 * @return Collection of default framework converters and custom converters
	 */
	public final static Collection<Converter> getDefaultConverters() {
		Collection<Converter> converters = new ArrayList<Converter>();
		converters.addAll(getFrameworkDefaultConverters());
		converters.addAll(customConverters);		
		return converters;		
	}

	/**
	 * Only default framework converters must be added to this method
	 * 
	 * @return Collection of default framework converter implementations 
	 */
	private static Collection<Converter> getFrameworkDefaultConverters(){
		Collection<Converter> converters = new ArrayList<Converter>();
		converters.add(new CopyConverter());
		// only for char and boolean primitives and their respective wrappers.
		converters.add(new WrapperPrimitiveSymmetricConverter()); 
		// most commonly used Converter
		converters.add(new BasicStringConverter()); 
		// String to DomainAttribute Conversion ... Perhaps, This simplifies and
		// fosters the usage of DomainAttributes instead of Strings.

		converters.add(new StringSSNConverter());
		converters.add(new StringCreditCardConverter());

		// Family of String to Object Converters

		converters.add(new StringOIDConverter());
		converters.add(new StringTextDocumentConverter());

		// Family of String to Number Converters

		converters.add(new StringBigDecimalConverter());
		converters.add(new StringBigIntegerConverter());

		converters.add(new StringDoubleConverter());
		converters.add(new StringFloatConverter());
		converters.add(new StringIntegerConverter());
		converters.add(new StringLongConverter());
		converters.add(new StringShortConverter());
		converters.add(new StringByteConverter());

		// String to other non numeric primitive wrappers

		converters.add(new StringBooleanConverter());

		// Example usage : Middle Initial Name from struts form : String to char
		// in the DomainObject.

		converters.add(new StringCharacterConverter());

		// Primitive Converter family that allows String to primitive population

		converters.add(new StringPrimitiveLongConverter());
		converters.add(new StringPrimitiveFloatConverter());
		converters.add(new StringPrimitiveIntConverter());
		converters.add(new StringPrimitiveCharConverter());
		converters.add(new StringPrimitiveDoubleConverter());
		converters.add(new StringPrimitiveByteConverter());
		converters.add(new StringPrimitiveShortConverter());
		converters.add(new StringPrimitiveBooleanConverter());

		// String to character arrays conversion

		converters.add(new StringCharArrayConverter());
		converters.add(new StringCharacterArrayConverter());

		converters.add(new CalendarDateConverter());
		converters.add(new CalendarSQLDateConverter());
		converters.add(new CalendarTimestampConverter());

		// Date to Calendar converter
		converters.add(new DateCalendarConverter());

		// Date to/from its subclasses converters
		converters.add(new DateSQLDateConverter());
		converters.add(new DateTimestampConverter());
		

		// Long to Date and its subclasses Timestamp/SQLDate

		converters.add(new LongDateConverter());
		converters.add(new LongSQLDateConverter());
		converters.add(new LongTimestampConverter());

		// Date and its subclasses to be converted to Long/long
		converters.add(new DateLongConverter());

		// When a String can be converted into different Object, the Object
		// should also should also be converted back to String with a valid format. 
		
		converters.add(new DomainAttributeStringConverter());

		/* Enhanced from primitive Converter ... These converters are capable of
		   Adapting the non wrapper Number subclasses like BigDecimal to be converted into a specific
		   wrapper or primitive and vice versa.
		*/
		converters.add(new NumberBigDecimalConverter());
		converters.add(new NumberBigIntegerConverter());
		converters.add(new NumberByteConverter());
		converters.add(new NumberDoubleConverter());
		converters.add(new NumberFloatConverter());
		converters.add(new NumberIntegerConverter());
		converters.add(new NumberLongConverter());
		converters.add(new NumberShortConverter());
		converters.add(new NumberPrimitiveLongConverter());
		converters.add(new NumberPrimitiveFloatConverter());
		converters.add(new NumberPrimitiveIntConverter());
		converters.add(new NumberPrimitiveDoubleConverter());
		converters.add(new NumberPrimitiveByteConverter());
		converters.add(new NumberPrimitiveShortConverter());

		//Enum to be converted into String
		converters.add(new EnumToStringConverter());
		
		return converters;

	}
	
	/**
	 * Allows the user to add custom converters. The converters that are added here
	 * will be available throughout the application.
	 * 
	 * @param converter a custom converter
	 */
	public final static void addConverter(Converter converter){
		if(customConverters.contains(converter))customConverters.remove(converter);
		customConverters.add(converter);
	}	

}
