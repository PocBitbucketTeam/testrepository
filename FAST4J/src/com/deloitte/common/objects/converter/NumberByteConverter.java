package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberByteConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Target property type should be primitive or wrapper for byte.<br>
 *  Output:     Byte which can also be mapped back to primitive by Reflection <br>
 *  @author SIDT Framework Team
 */

public class NumberByteConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return Byte.class;
	}
	

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);	
		Number n=(Number)from;
		return new Byte(n.byteValue());
		
 }
	
	
	
}
