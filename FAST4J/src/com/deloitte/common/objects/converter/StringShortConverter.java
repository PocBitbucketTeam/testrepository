package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringShortConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Short.MIN_VALUE > number < Short.MAX_VALUE<br>
 *  Output:     Short<br>
 *  
 *  @author SIDT Framework Team
 */
public  class StringShortConverter extends AbstractStringNumberConverter 
{
	public Class<?> getReturnType() 
	{
		return Short.class;
	}

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Short((String)from);
	}	
}
