package com.deloitte.common.objects.converter;


/**
 * NumberPrimitiveDoubleConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Double.MIN_VALUE >	number < Double.MAX_VALUE<br>
 *  Output:     double<br>
 *  
 *  @author SIDT Framework Team
 */
public final class NumberPrimitiveDoubleConverter extends NumberDoubleConverter {


	public Class<?> getReturnType() 
	{
		return double.class;
	}


}
