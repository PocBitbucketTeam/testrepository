package com.deloitte.common.objects.converter;

/**
 * StringCharacterConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: should be a single charactered string<br>
 *  Output:     char<br>
 *  
 *  @author SIDT Framework Team
 */


public final class StringPrimitiveCharConverter extends StringCharacterConverter
{
	public Class<?> getReturnType()
	{
		return char.class;
	}
}
