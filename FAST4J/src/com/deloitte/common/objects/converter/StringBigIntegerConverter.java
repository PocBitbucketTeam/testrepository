package com.deloitte.common.objects.converter;

import java.math.BigInteger;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * StringBigIntegerConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Must be an integer<br>
 *  Output:     BigInteger<br>
 *  
 *  @author SIDT Framework Team
 */

public final class StringBigIntegerConverter extends AbstractStringNumberConverter 
{

	public Class<?> getReturnType()
	{
		return BigInteger.class;
	}
	
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new BigInteger((String)from);
	}
	
}
