package com.deloitte.common.objects.converter;

/**
 * AbstractNumericConverter <br>
 * If a Number (and its sub classes BigDecimal, BigInteger,Integer,Byte,Short, Long, Float, Double)
 * is invoked as input to a primitive/Wrapper method signature, Java reflection will performing the
 * "un/boxing" or "Adaption between different sub classes of Number" for you. 
 * As a result this converter merely ensures the match of Number to wrappers or primitives. 
 * This is a little bit of a hack in that it makes a big assumption that you will be using Reflection to populate
 * your data.... copied from PrimitiveConveter.
 * <br>
 * 	Input:      Number or primitive numeric types<br>
 *  Conditions: Sub classes of Number including Primtive Wrapper matches primitive converting to 
 *  (eg BigDecimal --> int or BigDecimal ---> Integer<br>
 *  Output:     Wrapper or primitive via Reflection<br>
 *  
 *  @author SIDT Framework team
 */



public abstract class AbstractNumericConverter extends AbstractConverter 
{


	public Class<?>[] getParameterTypes()
	{
		Class<?>[] c = { Number.class,int.class,float.class,double.class,long.class,short.class,byte.class};

		return c;
	}


	public boolean accepts(Object from, Class<?> to)
	{
		return (super.accepts(from,to) && NumberConversionHelper.accepts(from, to));

	}


}




