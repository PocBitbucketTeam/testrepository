package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;


/**
 * StringCharacterConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: should be a single charactered string<br>
 *  Output:     Character<br>
 *  
 *  @author SIDT Framework Team
 */
public  class StringCharacterConverter extends AbstractStringObjectConverter 
{
	public Class<?> getReturnType()
	{
		return Character.class;
	}
	
	
	public boolean accepts(Object from, Class<?> to)
	{
		return super.accepts(from,to) && from.toString().toCharArray().length==1;	
	}
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Character(from.toString().charAt(0));
	}	
	
}
