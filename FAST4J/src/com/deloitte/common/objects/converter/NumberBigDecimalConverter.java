package com.deloitte.common.objects.converter;

import java.math.BigDecimal;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 	NumberBigDecimalConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Target property type should be primitive or wrapper for byte.<br>
 *  Output:     BigDecimal which can also be mapped back to primitive by Reflection <br>
 *  @author SIDT Framework Team
 */

public final class NumberBigDecimalConverter extends AbstractNumericConverter 
{

	public Class<?> getReturnType()
	{
		return BigDecimal.class;
	}
	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		String s=(""+from).trim();
		return new BigDecimal(s);
		
 }
	
	
	
	
}
