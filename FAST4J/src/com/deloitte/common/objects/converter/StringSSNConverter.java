package com.deloitte.common.objects.converter;


import com.deloitte.common.objects.business.SocialSecurityNumber;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 *  StringSSNConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Must adhere to the validation around SSN<br>
 *  Output:     SSN &ltDomain Attribute&rt<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringSSNConverter extends AbstractStringObjectConverter 
{

	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new SocialSecurityNumber((String)from);
	}	

	public Class<?> getReturnType()
	{
		return SocialSecurityNumber.class;
	}


}
