package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;


/**
 * StringCharArrayConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: should be a string as input<br>
 *  Output:     Character[]<br>
 *  
 *  @author SIDT Framework Team
 */
public final class StringCharacterArrayConverter extends StringCharArrayConverter 
{
	public Class<?> getReturnType()
	{
		return Character[].class;
	}
	
	public Object convert(Object from) throws CheckedApplicationException
	{
	char[] array=(char[])super.convert(from);
	Character[] newArray = new Character[array.length];
	for (int i=0; i < array.length; i++)
	{
		newArray[i] = new Character(array[i]);
	}
	return newArray;
	}	
	
	}
