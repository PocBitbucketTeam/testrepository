package com.deloitte.common.objects.converter;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 *  StringDoubleConverter<br><br>
 * 	Input:      String<br>
 *  Conditions: Double.MIN_VALUE >	number < Double.MAX_VALUE<br>
 *  Output:     Double<br>
 *  
 *  @author SIDT Framework Team
 */
public  class StringDoubleConverter extends AbstractStringNumberConverter
 {

	
	public Class<?> getReturnType() 
	{
		return Double.class;
	}

	
	public Object convert(Object from) throws CheckedApplicationException
	{
		checkAndThrowException(from);
		return new Double((String)from);
	}	

}
