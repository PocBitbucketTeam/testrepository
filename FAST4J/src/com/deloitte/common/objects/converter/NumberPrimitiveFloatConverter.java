package com.deloitte.common.objects.converter;


/**
 * NumberPrimitiveFloatConverter<br><br>
 * 	Input:      Number<br>
 *  Conditions: Float.MIN_VALUE > number < Float.MAX_VALUE<br>
 *  Output:     float<br>
 *  
 *  @author SIDT Framework Team
 */
public final class NumberPrimitiveFloatConverter extends NumberFloatConverter
{


	public Class<?> getReturnType() 
	{
		return float.class;
	}


}
