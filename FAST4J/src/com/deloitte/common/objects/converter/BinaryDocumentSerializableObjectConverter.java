package com.deloitte.common.objects.converter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.Converter;
import com.deloitte.common.objects.domain.BinaryDocument;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.Arrays;

/**
 * This class is used to Convert the BinaryDocument to an Object (SerializedObject)
 * <br/><p>
 * When you use with populators the returnType should be same as the
 * type of the attribute that is going to be converted
 * 
 * For Example, If you have a Collection Object as a attribute at DomainObject
 * and this Collection object will be populated through the populator the 
 * Convertor should be initialized as below
 * <code>
 * DefaultPropertyPopulator populator = new DefaultPropertyPopulator();
 * populator.add(new BinaryDocumentSerializableObjectConverter(Collection.class));
 * </code>
 * </p>
 * 
 * @author SIDT Framework Team
 *
 */
public final class BinaryDocumentSerializableObjectConverter extends AbstractConverter {

	private static final Logger log = Logger.getLogger(BinaryDocumentSerializableObjectConverter.class.getName());
	
	private Class<?> returnType;
	private int hashCode = -1;
	
	public BinaryDocumentSerializableObjectConverter(Class<?> returnType) {
		this.returnType = returnType;
	}

	public Object convert(Object from) throws CheckedApplicationException {
		checkAndThrowException(from);
		Object theObject = null;
		ObjectInputStream ois=null;
		try{
			ois = new ObjectInputStream(((BinaryDocument)from).getStream());
			theObject = ois.readObject();
			if(!Arrays.isOfType(theObject.getClass(),new Class[]{getReturnType()})){
				throw new CheckedApplicationException(this.getClass(),"Could not Convert"+from+" to Serialazable Object "+getReturnType());
			}
		} catch (IOException e) {
			log.warning("IOException Occured while converting BinaryDocument to "+returnType );
			throw new CheckedApplicationException(this.getClass(),"Could not Convert "+from+" to Serialazable Object "+getReturnType(), e);
		} catch (ClassNotFoundException e) {
			log.warning("ClassNotFoundException Occured while converting BinaryDocument to "+returnType );
			throw new CheckedApplicationException(this.getClass(),"Could not Convert "+from+" to Serialazable Object "+getReturnType(), e);
		}finally{
			if(ois!=null){
				try {
					ois.close();
				} catch (IOException e) {
					log.warning("IOException Occured while closing the  ObjectInputStream ");
					throw new CheckedApplicationException(this.getClass(),"Unable to Close the Stream", e);
				}
			}
		}
		return theObject;
	}

	public Class<?> getReturnType() {
		return returnType;
	}

	public Class<?>[] getParameterTypes(){
		Class<?>[] c = { BinaryDocument.class } ;
		return c;
	}
	
	public int hashCode(){
		if(hashCode==-1){
			hashCode = super.hashCode()+ returnType.hashCode();
		}
		return hashCode;
	}
	
	public boolean equals(Object o){
		if(this == o){
			return true;
		}else if(o==null || (this.getClass()!=o.getClass()) ){
			return false;
		}else{
			Converter o1 =(Converter)o;
			return (o1.getReturnType().equals(returnType));
		}
	}
}

