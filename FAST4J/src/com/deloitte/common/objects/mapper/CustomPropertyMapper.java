package com.deloitte.common.objects.mapper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Map based implementation of Mapper.
 * <br><br>
 * Example:
 * <pre>
 * <code>
   Map<String, String> mappings = new HashMap<String, String>();
	mappings.put("intFrom", "intTo");
	mappings.put("bigDecimalFrom", "BIGDECIMALTO");
	mappings.put("collectionFrom", "CoLLFroM");		
   ManualPropertyMapper mpm = new ManualPropertyMapper(mappings);
   </code>
 * or <code>
   ManualPropertyMapper mpm = new ManualPropertyMapper();
 	mpm.setMappings(mappings);
   </code>
 * </pre>
 * <br> 
 * The String representation of all mapping values will be returned in upper case.
 * 
 * @deprecated Use {@link DefaultPropertyMapper} instead. Since 1.6.3 (Dec 2012)
 **/
public class CustomPropertyMapper extends DefaultPropertyMapper {

	private Map<String, String> mappings;

	public CustomPropertyMapper() {
		mappings = new HashMap<String, String>();
	}

	public CustomPropertyMapper(Map<String, String> theMap) {
		this();
		setMappings(theMap);
	}

	public Map<String, String> getMappings() {
		return mappings;
	}

	public void setMappings(Map<String, String> theMap) {
		Iterator<Entry<String,String>> i = theMap.entrySet().iterator();
		while (i.hasNext()) {
		    Entry<String, String> e = i.next();
		    if (e.getKey() != null && e.getValue()!=null) {
			    this.mappings.put(e.getKey().toUpperCase(), e.getValue().toUpperCase());
			}
		}
	}

	public void addMapping(String from, String to) {
		mappings.put(from.toUpperCase(), to.toUpperCase());
	}

	public void removeMapping(String key) {
		mappings.remove(key.toUpperCase());
	}

	public String getName(Class<?> klass, String name) {
		String theAnswer = super.getName(klass, name);
		String mappedProperty = mappings.get(name.toUpperCase());
		if (mappedProperty != null && !mappedProperty.trim().equals("")) {
			theAnswer = mappedProperty;
		}
		return theAnswer;
	}

}
