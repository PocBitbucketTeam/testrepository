package com.deloitte.common.objects.mapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.interfaces.Mapper;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;

/**
  * Singleton pattern that implements Mapper as well as Loader. This allows there to be one consistent
  * set of mapping across the JVM, if necessary the class can reload mappings 
  * <br><br>
  * Example:<br>
  * FileBasedDatabaseMapper fbdm = FileBasedDatabaseMapper.getInstance("/mapper.xml");
  * <br><br>
  * The mapper.xml file format should have the fully qualified name of the TARGET object, this is the
  *	object that is being populated. The "from" attribute will have the name of the property from
  *	the SOURCE. The "to" attribute will be the mutator name on the TARGET object.<br><br>Example<br>
  * <mappers>
  *    <property class="com.deloitte.example.businessobjects.ExampleBusinessObject">
  *	      <map from="REGION" to="rejin"/>
  *	      <map from="item" to="IDEM"/>
  *       <map from="id" to="TRANSACTIONID"/>
  *    </property>
  * </mappers>
  * 
  * The String representation of all mapping values will be returned in uppercase.
  * 
  * @deprecated Use {@link DefaultPropertyMapperLoader} in conjunction with
  * {@link DefaultPropertyMapper} instead. Since 1.6.3 (Dec 2012)
  **/
public final class FilePropertyMapper implements Mapper, Loader
{
	private static final Logger logger = Logger.getLogger(FilePropertyMapper.class.getName());

	//@TODO Figure out how to do a Factory of singletons
	private static FilePropertyMapper me;
	private String fileName;
	private Map<Class<?>, Map<String, ClassMapper>> theClassMappings;  // ClappMapper entries are stored in a Map of Maps, 
								// top-level Map is keyed by the class, second-level 
								// Map is keyed by the uppercase property name
	
	protected FilePropertyMapper()
	{
		super();
	}
	
	public String getName(Class<?> klass, String name) 
	{
		logger.finest("Mapper looking for " + klass + "/" + name);

		String theAnswer = name;
		Map<String, ClassMapper> thePropertyMappings = theClassMappings.get(klass);
		if (thePropertyMappings != null)
		{
			ClassMapper theMapper = (ClassMapper)thePropertyMappings.get(name.toUpperCase());
			if (theMapper != null){
				theAnswer = theMapper.getTo();
				logger.finest("Mapper found " + theAnswer);
			}
		}
		return theAnswer;
	}
	
	/**
	 * Add an entry to the internal mappings
	 */
	private void putEntry(Class<?> klass, String name, ClassMapper theClassMapper){
		Map<String, ClassMapper> thePropertyMappings = theClassMappings.get(klass);
		if (thePropertyMappings == null)
		{
			thePropertyMappings = new HashMap<String, ClassMapper>();
			theClassMappings.put(klass, thePropertyMappings);
		}
		thePropertyMappings.put(name.toUpperCase(), theClassMapper);
	}
	
	public static FilePropertyMapper getInstance(String fileName) throws CheckedApplicationException
	{
		if (me == null)
		{
			me = new FilePropertyMapper();
			me.fileName = fileName;
			me.theClassMappings = new HashMap<Class<?>, Map<String, ClassMapper>>();
			me.loadValues();
		}
		return me;
	}
	
	/**
	 * @param klass class to find the property mappings for
	 * @return List of ClassMappers with the property mapping information. All entries
	 * are in uppercase
	 */
	public Map<String, ClassMapper> getMappedProperties(Class<?> klass)
	{
		logger.finest("Mapper getting all properties for " + klass);
		Map<String, ClassMapper> thePropertyMappings = theClassMappings.get(klass);
		if (thePropertyMappings == null)
			return Collections.emptyMap();
		else
			return Collections.unmodifiableMap(thePropertyMappings);
	}
	
	/**
	 * @param klass class to find the property mappings for
	 * @return List of Strings of the property keys. All entries
	 * are in uppercase
	 */
	public List<String> getMappedPropertyKeys(Class<?> klass)
	{
		return new ArrayList<String>(getMappedProperties(klass).keySet());
	}
	
	private InputStream getFile() throws CheckedApplicationException
	{
		return getClass().getResourceAsStream(fileName);
	}	
	
	public void loadValues() throws CheckedApplicationException 
	{
		loadXMLValues();
	}
	

	private void loadXMLValues() throws CheckedApplicationException
	{
		try 
		{
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document d = builder.parse(getFile());
		
		NodeList theMappers = d.getElementsByTagName("mappers");
		if (theMappers.getLength() == 1)
		{
			Node theRoot = theMappers.item(0);
			Node theProperty = null;
			Node theMap = null;
			for (int i=0; i < theRoot.getChildNodes().getLength();i++)
			{
				 
				theProperty = theRoot.getChildNodes().item(i);
				Class<?> theClass = null;
				if (theProperty.getNodeType() == Node.ELEMENT_NODE)
				{
					ClassMapper m = null;
					theClass = ReflectionHelper.createFor(theProperty.getAttributes().getNamedItem("class").getNodeValue());
					for (int j=0; j < theProperty.getChildNodes().getLength(); j++)
					{
						theMap = theProperty.getChildNodes().item(j);
						if (theMap.getNodeType() == Node.ELEMENT_NODE)
						{
							m = new ClassMapper();
							m.setTheClass(theClass);
							m.setFrom(theMap.getAttributes().getNamedItem("from").getNodeValue());
							m.setTo(theMap.getAttributes().getNamedItem("to").getNodeValue());
							putEntry(m.getTheClass(), m.getFrom(), m);
						}
					}
				}
				
			}
		}
		//TODO replace with a usefule comment here
		//logger.info("Loaded " + theClassMappers.size() + " mappers\n\t" + CollectionHelper.format(theClassMappers,false,"\n\t"));
		} catch (ParserConfigurationException e)
		{
			throw new CheckedApplicationException(getClass(),"Parser Error ",e);
		} catch (IOException e)
		{
			throw new CheckedApplicationException(getClass(),"IO/Error Error ",e);
		} catch (SAXException e)
		{
			throw new CheckedApplicationException(getClass(),"SAX Error ",e);
		}
	}

	static class ClassMapper
	{
		private Class<?> theClass;
		private String from;
		private String to;
		public String getFrom() {
			return from;
		}
		public void setFrom(String from) {
			this.from = from.toUpperCase();
		}
		public Class<?> getTheClass() {
			return theClass;
		}
		public void setTheClass(Class <?>theClass) {
			this.theClass = theClass;
		}
		public String getTo() {
			return to.toUpperCase();
		}
		public void setTo(String to) {
			this.to = to;
		}
		
		public String toString()
		{
			return "Object = " + theClass + " from " + from + " to " + to;
		}
	}

}
