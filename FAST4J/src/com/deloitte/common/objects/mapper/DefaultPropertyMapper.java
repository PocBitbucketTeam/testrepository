package com.deloitte.common.objects.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.Mapper;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * 
 * An implementation of Mapper which can be manually loaded with property mappings.
 * If a matching property mapping does not exist to the call to {@link #getName(Class, String)}
 * the return value will be the input property name.
 * <br><br>
 * Example:
 * <pre><code>
 * DefaultPropertyMapper dpm = new DefaultPropertyMapper();
 * dpm.addMapping(MyDomainObject.class, "intFrom", "intTo");
 * dpm.addMapping(MyDomainObject.class, "dateOfBirth", "dob");
 * dpm.addMapping(MyDomainObject.class, "address", "customerAddress");		
    </code></pre>
 * In this case, a call to getName(MyDomainObject.class, "intFrom") will return "INTTO",
 * getName(MyDomainObject.class, "dateOfBirth") will return "DOB". Both the class and 
 * fromVal have to match, so a call to getName(SomeOtherObject.class, "intFrom") will
 * return "intFrom", and getName(MyDomainObject.class, "propWithNoMapping") will return
 * "propWithNoMapping" (because any properties with no mappings return the fromVal as
 * the result.
 * <br> 
 * The String representation of all mapping values will be returned in upper case.
 */
public class DefaultPropertyMapper implements Mapper {

	private static final Logger logger = Logger.getLogger(DefaultPropertyMapper.class
			.getName());

	private Map<Class<?>, Map<String, String>> theClassMappings = new HashMap<Class<?>, Map<String, String>>();
	// The map entries are stored in a Map of Maps, top-level Map is keyed 
	// by the class, second-level Map is keyed by the uppercase property name

	/**
	 * Will return the mapping for a particular class and string combination, if
	 * it exists (i.e. has been added via {@link #addMapping(Class, String, String)}.
	 * If no matching mapping exists, it will return the same value as provided in the
	 * name parameter
	 */
	public String getName(Class<?> klass, String name) {
		logger.finest("Mapper looking for " + klass + "/" + name);

		String theAnswer = name;
		Map<String, String> thePropertyMappings = theClassMappings.get(klass);
		if (thePropertyMappings != null) {
			String mappingVal = thePropertyMappings.get(name.toUpperCase());
			if (mappingVal != null) {
				theAnswer = mappingVal;
				logger.finest("Mapper found " + theAnswer);
			} else {
				logger.finest("Class " + klass
						+ " has mappings, but none defined for property "
						+ name);
			}
		} else {
			logger.finest("No mappings for class " + klass);
		}
		return theAnswer;
	}

	/**
	 * Add an entry to the internal mappings. The searches on entries are not 
	 * case-sensitive and everything is returned in uppercase, so the case used
	 * does not matter.
	 */
	public void addMapping(Class<?> klass, String fromVal, String toVal) {
		// ClassMapper theClassMapper = new ClassMapper(klass, name,
		// mappingVal);
		if (klass == null){
			throw new UncheckedApplicationException(this.getClass(),
					"Cannot insert null mappings, class is null");
		}
		if (fromVal == null || fromVal.trim().equals("")){
			throw new UncheckedApplicationException(this.getClass(),
			"Cannot insert null mappings, property from name is null");
		}
		if (toVal == null || toVal.trim().equals("")){
			throw new UncheckedApplicationException(this.getClass(),
			"Cannot insert null mappings, property to name is null");
		}
		
		Map<String, String> thePropertyMappings = theClassMappings.get(klass);
		if (thePropertyMappings == null) {
			thePropertyMappings = new LinkedHashMap<String, String>();
			theClassMappings.put(klass, thePropertyMappings);
		}
		
		thePropertyMappings.put(fromVal.toUpperCase(), toVal.toUpperCase());
	}
	
	/**
	 * Gets the Map object of property mappings defined for a particular class.
	 * @param klass class to find the property mappings for
	 * @return Map of String with the property mapping information. Key values
	 * contain the from property name, the values are the to property name. All
	 * entries are in uppercase
	 */
	public Map<String, String> getMappedProperties(Class<?> klass) {
		logger.finest("Mapper getting all properties for " + klass);
		Map<String, String> thePropertyMappings = theClassMappings.get(klass);
		if (thePropertyMappings == null) {
			return Collections.emptyMap();
		} else {
			return Collections.unmodifiableMap(thePropertyMappings);
		}
	}
	
	/**
	 * Gets the List object of containing all the properties that have mappings
	 * defined for a particular class.
	 * @param klass class to find the property mappings for
	 * @return List of Strings of the property keys with defined mappings
	 * (the from values). All entries are in uppercase
	 */
	public List<String> getMappedPropertyKeys(Class<?> klass) {
		return new ArrayList<String>(getMappedProperties(klass).keySet());
	}

}
