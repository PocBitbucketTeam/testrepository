package com.deloitte.common.objects.mapper;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;

/**
  * Can be used to load mappings from an XML configuration file into a
  * {@link DefaultPropertyMapper}. The values will be loaded by calling
  * {@link #loadValues(DefaultPropertyMapper, String)}.
  * <br><br>
  * The file name passed to loadValues() should be the fully qualified name
  * of the file. The file should be formatted as follows:<br><pre>
  * &lt;mappers&gt;
  *    &lt;property class="com.deloitte.example.businessobjects.ExampleBusinessObject"&gt;
  *	      &lt;map from="REGION" to="rejin"/&gt;
  *	      &lt;map from="item" to="IDEM"/&gt;
  *       &lt;map from="id" to="TRANSACTIONID"/&gt;
  *    &lt;/property&gt;
  * &lt;/mappers&gt;
  * </pre>
  * In this case, the class property corresponds to the class for which the mappings will
  * be created. The 'from' attribute in the map element specifies the fromVal, and the 'to'
  * attribute specifies the toVal. This would be equivalent to calling the following in code
  * against the DefaultPropertyMapper:
  * <pre>
  * mapper.addMapping(MyDomainObject.class, "REGION", "rejin");
  * mapper.addMapping(MyDomainObject.class, "item", "IDEM");
  * mapper.addMapping(MyDomainObject.class, "id", "TRANSACTIONID");		
  * </pre>
 */
public class DefaultPropertyMapperLoader {
	
	private DefaultPropertyMapperLoader(){}

	public static void loadValues(DefaultPropertyMapper mapper, String fileName) {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document d = builder.parse(DefaultPropertyMapperLoader.class.getResourceAsStream(fileName));

			NodeList theMappers = d.getElementsByTagName("mappers");
			if (theMappers.getLength() == 1) {
				Node theRoot = theMappers.item(0);
				Node theProperty = null;
				Node theMap = null;
				for (int i = 0; i < theRoot.getChildNodes().getLength(); i++) {

					theProperty = theRoot.getChildNodes().item(i);
					Class<?> theClass = null;
					if (theProperty.getNodeType() == Node.ELEMENT_NODE) {
						theClass = ReflectionHelper.createFor(theProperty
								.getAttributes().getNamedItem("class")
								.getNodeValue());
						for (int j = 0; j < theProperty.getChildNodes()
								.getLength(); j++) {
							theMap = theProperty.getChildNodes().item(j);
							if (theMap.getNodeType() == Node.ELEMENT_NODE) {
								String fromVal = theMap.getAttributes()
										.getNamedItem("from").getNodeValue();
								String toVal = theMap.getAttributes()
										.getNamedItem("to").getNodeValue();
								mapper.addMapping(theClass, fromVal, toVal);
							}
						}
					}

				}
			}
		} catch (ParserConfigurationException e) {
			throw new UncheckedApplicationException(DefaultPropertyMapperLoader.class,
					"Unexpected error while parsing the mapping XML", e);
		} catch (IOException e) {
			throw new UncheckedApplicationException(DefaultPropertyMapperLoader.class,
					"Unexpected error while parsing the mapping XML", e);
		} catch (SAXException e) {
			throw new UncheckedApplicationException(DefaultPropertyMapperLoader.class,
					"Unexpected error while parsing the mapping XML ", e);
		}
	}

}
