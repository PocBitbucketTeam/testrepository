package com.deloitte.common.objects;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.Converter;
import com.deloitte.common.interfaces.Mapper;
import com.deloitte.common.interfaces.Population;
import com.deloitte.common.objects.converter.Converters;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.mapper.DefaultPropertyMapper;
import com.deloitte.common.util.ReflectionHelper;
/**
 * DefaultPopulator handles the movement of data between one object and another. There are basic conversion
 * also provided for simple object translation. This can be useful, when you are coming from a web input
 * of strings and need to convert to an object, such as Date, or BigDecimal.
 * <br/><br/>
 * A Mapper can be added by invoking the method, set Mapper. In the case of Mappers, there is only one
 * currently allowed. A Mapper allows a Property(method name) to be mapped to another name.<br><br>
 * Example:<br>
 * Property = Foo<br>
 * Object Method Name = setBar<br>
 * Invoke the setBar method with Foo
 * <br><br>
 * Mapping occurs before Conversion
 * <br><br>
 * Converters can be appended to the internal converter list, or the basic set provided can be used as-is.
 * Example: <br>
 * Converter myConverter = new MyConverter();
 * List converters = new DefaultPropertyPopulator().getConverters();
 * converters.add(new MyConverter());
 * Population populator = new DefaultPropertyPopulator(converters);
 * <br><br>- or -<br><br>
 * DefaultPropertyPopulation dpp = new DefaultPropertyPopulation();
 * dpp.add(new MyConverter());
 * <br/></br>
 *
 */
public final class DefaultPropertyPopulator implements Population {
	private static final Logger logger = Logger.getLogger(DefaultPropertyPopulator.class.getName());

	List<Converter> theConverters = new ArrayList<Converter>();
	Mapper theMapper = new DefaultPropertyMapper();

	public DefaultPropertyPopulator() {		
	getConverterCollection().addAll(Converters.getDefaultConverters());		
	}
	
	public DefaultPropertyPopulator(Mapper mapper) {
		this();
		theMapper = mapper;
	}
	
	public DefaultPropertyPopulator(Collection<Converter> converters) {
		for (Converter converter : converters) {
				addConverter(converter);
		}
	}

	public Collection<Converter> getConverters() {
		return Collections.unmodifiableCollection(getConverterCollection());
	}
	
	public void setMapper(Mapper mapper) {
		theMapper = mapper;
	}
	
	public boolean addConverter(Converter converter) {
		Collection<Converter> converters=getConverterCollection();
		if(converters.contains(converter))converters.remove(converter);
		return converters.add(converter);
	}
	
	public boolean removeConverter(Converter converter) {
		return getConverterCollection().remove(converter);
	}
	
	private Collection<Converter> getConverterCollection() {
		return theConverters;
	}

	/**
	 * @see com.deloitte.common.interfaces.Population#accepts(java.lang.Object, java.lang.Object, java.lang.String)
	 */
	public boolean accepts(Object toObject, Object fromValue,String propertyName) 
	{
		if (toObject == null || fromValue == null) {
			return false;
		} else if (toObject instanceof Map) {
			return true;
		} else {
			try {
				Method mutator = ReflectionHelper.getMutatorMethod(toObject,
						theMapper.getName(toObject.getClass(), propertyName));
				if (mutator == null) {
					return false;
				}
				Class<?> toType = mutator.getParameterTypes()[0];
				boolean canConvert = false;
				for (Iterator<Converter> i = theConverters.iterator(); i
						.hasNext() && canConvert == false;) {
					Converter c = (Converter) i.next();
					canConvert = c.accepts(fromValue, toType);
				}
				if (!canConvert) {
					logger.info(fromValue.getClass().getName() + "("
							+ fromValue + ") No Converter=" + " --> " + toType);
				}
				return canConvert;
			} catch (CheckedApplicationException ce) {
				return false;
			}
		}
	}

	/**
	 * Important Note:  Silent failure if the mutator is not found
	 * 
	 * @see com.deloitte.common.interfaces.Population#populate(java.lang.Object, java.lang.Object, java.lang.String)
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void populate(Object toObject, Object fromValue, String propertyName) throws CheckedApplicationException 
	{
		if (toObject instanceof Map) {
			((Map) toObject).put(theMapper.getName(toObject.getClass(), propertyName), fromValue);
		} else {
			boolean canConvert = false;
			Object toValue = fromValue;
			Method mutator = ReflectionHelper.getMutatorMethod(toObject,theMapper.getName(toObject.getClass(), propertyName));
			if (mutator == null) {
				return;
			} else {
				Iterator<Converter> i;
				Converter c = null;
				Class<?> toType = mutator.getParameterTypes()[0];

				for (i = theConverters.iterator(); i.hasNext() && canConvert == false;) {
					c = (Converter) i.next();
					canConvert = c.accepts(fromValue, toType);
				}
				if (canConvert) {
					toValue = c.convert(fromValue);
					ReflectionHelper.invokeMutatorMethod(toObject, mutator, toValue);
				}
			}
	    }
 	}
	
}
