package com.deloitte.common.objects.reftable;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * Immutable type that represents a row from a reference table. Additional
 * columns are stored in <see cref="AdditionalColumns"/> as a read-only
 * dictionary.
 */
public class DefaultReferenceTableRow implements ReferenceTableRow {

	private final String key;
	private final String displayValue;
	private final Date effectiveStartDate;
	private final Date effectiveEndDate;
	private final String localeId;
	private final Map<String, ?> additionalColumns; 

	String classString;

	@SuppressWarnings("unchecked")
	private static final Map<String, ?> emptyColumns = Collections.unmodifiableMap(Collections.EMPTY_MAP);
	
	public DefaultReferenceTableRow(String key, String displayValue,
			Date effectiveStartDate, Date effectiveEndDate, String localeId,
			Map<String, ?> additionalColumns) {
		super();

		if (key == null){
			throw new UncheckedApplicationException(this.getClass(), "Key cannot be null");
		}
		this.key = key;
		this.displayValue = displayValue;
		this.effectiveStartDate = effectiveStartDate;
		this.effectiveEndDate = effectiveEndDate;
		this.localeId = localeId;
		this.additionalColumns = Collections.unmodifiableMap(additionalColumns);
	}

	public DefaultReferenceTableRow(String key, String displayValue,
			Date effectiveStartDate, Date effectiveEndDate) {
		this(key, displayValue, effectiveStartDate, effectiveEndDate, NO_LOCALE, emptyColumns);
	}

	public DefaultReferenceTableRow(String key, String displayValue,
			Date effectiveStartDate, Date effectiveEndDate, String localeId) {
		this(key, displayValue, effectiveStartDate, effectiveEndDate, localeId, emptyColumns);
	}

	public DefaultReferenceTableRow(String key, String displayValue,
			Date effectiveStartDate, Date effectiveEndDate, Map<String, ?> additionalColumns) {
		this(key, displayValue, effectiveStartDate, effectiveEndDate, NO_LOCALE, additionalColumns);
	}

	public DefaultReferenceTableRow(String key, String displayValue, String localeId) {
		this(key, displayValue, null, null, localeId);
	}

	public DefaultReferenceTableRow(String key, String displayValue) {
		this(key, displayValue, NO_LOCALE);
	}

	public String getKey() {
		return key;
	}

	public String getDisplayValue() {
		return displayValue;
	}

	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public Date getEffectiveEndDate() {
		return effectiveEndDate;
	}

	public String getLocaleId() {
		return localeId;
	}

	public Map<String, ?> getAdditionalColumns() {
		return additionalColumns;
	}

	@SuppressWarnings("unchecked")
	public <T> T getAdditionalColumn(String columnName) {
		return (T) additionalColumns.get(columnName);
	}
	
	//may want to remove this or redesign the internals for more efficient access
//	@SuppressWarnings("unchecked")
//	public <T> T getAdditionalColumn(int position) {
//		int i = 1;
//		for (Object columnValue : additionalColumns.values()){
//			if (i == position){
//				return (T) columnValue;
//			}
//			i++;
//		}
//		throw new UncheckedApplicationException(this.getClass(), "Column position " + position + " out of bounds for ReferenceTableRow with "
//				+ additionalColumns.size() + " additional columns");
//	}
	

	public int compareTo(ReferenceTableRow o) {
		return this.getKey().compareTo(o.getKey());
	}

	public int hashCode() {
		return key.hashCode();
	}

	public boolean equals(Object obj) {
		return obj instanceof ReferenceTableRow &&
			((ReferenceTableRow)obj).getKey().equals(this.getKey());
	}

	public String toString() {
		if (classString == null){
			String columnString = "";
			for (String columnName : additionalColumns.keySet()){
				columnString += ", " + columnName + "=" + additionalColumns.get(columnName);
			}
			classString = "ReferenceTableRow: [key=" + getKey()
											+ ", localeId=" + getLocaleId()
											+ ", displayValue=" + getDisplayValue()
											+ ", effectiveStartDate=" + getEffectiveStartDate()
											+ ", effectiveEndDate=" + getEffectiveEndDate()
											+ columnString;
		}
		return classString;
	}

}
