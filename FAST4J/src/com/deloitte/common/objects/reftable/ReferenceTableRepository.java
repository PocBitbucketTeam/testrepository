package com.deloitte.common.objects.reftable;


public interface ReferenceTableRepository {

	ReferenceTable getTable(String tableName);

}
