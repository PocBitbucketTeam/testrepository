package com.deloitte.common.objects.reftable;

import java.util.Comparator;

import com.deloitte.common.objects.collection.DefaultComparator;

/**
 * Can be used to compare two ReferenceTableRows using the additional column of the specified
 * name. The column used for the comparison must implement the Comparable interface, or the
 * Comparator will throw a runtime exception
 */

public final class RefTableColumnComparator implements Comparator<ReferenceTableRow>{

	final String columnName;
	@SuppressWarnings("rawtypes")
	private Comparator delegate;

	//these are calculated once to optimize hashCode and toString methods
	int hashCode;
	String classString;
	
	public RefTableColumnComparator(String columnName, @SuppressWarnings("rawtypes") Comparator delegate) {
		super();
		this.columnName = columnName;
		this.delegate = delegate;
	}

	@SuppressWarnings("rawtypes")
	public RefTableColumnComparator(String columnName) {
		this(columnName, new DefaultComparator());
	}

	public String getColumnName() {
		return columnName;
	}
	
	@SuppressWarnings("rawtypes")
	public Comparator getDelegate() {
		return delegate;
	}

	@SuppressWarnings("unchecked")
	public int compare(ReferenceTableRow o1, ReferenceTableRow o2) {
		return delegate.compare(o1.getAdditionalColumn(columnName), o2.getAdditionalColumn(columnName));
	}

	@Override
	public int hashCode() {
		if (hashCode == 0){
			hashCode = columnName.hashCode() + delegate.hashCode();
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof RefTableColumnComparator
			&& columnName.equals(((RefTableColumnComparator)obj).getColumnName())
			&& delegate.equals(((RefTableColumnComparator)obj).getDelegate()));
	}

	@Override
	public String toString() {
		if (classString == null){
			classString = "RefTableColumnComparator with sort column '" + columnName 
				+ "' and Comparator delegate " + delegate + "'";
		}
		return classString;
	}
	
}
