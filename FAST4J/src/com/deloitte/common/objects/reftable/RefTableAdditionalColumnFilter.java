package com.deloitte.common.objects.reftable;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.collection.EqualsFilter;

/**
 * Filters ReferenceTableRows based on the value of an identified additional
 * column, using a Filter delegate to define the filtering logic against the value
 */
public final class RefTableAdditionalColumnFilter<T> implements Filter<ReferenceTableRow> {

	final String filterColumn;
	private Filter<T> delegate;

	//these are calculated once to optimize hashCode and toString methods
	int hashCode;
	String classString;
	
	/**
	 * Defines the filter using an EqualsFilter, so that include() will return
	 * true only when the specified column is equal to the provideed value
	 * @param filterColumn the name of the column to apply the filtering logic to
	 * @param filterValue the value to compare the additional column entry to
	 */
	public RefTableAdditionalColumnFilter(String filterColumn, T filterValue) {
		this(filterColumn, new EqualsFilter<T>(filterValue));
	}

	/**
	 * @param filterColumn the name of the column the filtering logic will be applied to
	 * @param delegate a Filter which will be used to define the filtering logic which will be applied
	 * to the column entry
	 */
	public RefTableAdditionalColumnFilter(String filterColumn, Filter<T> delegate) {
		super();
		this.filterColumn = filterColumn;
		this.delegate = delegate;
	}

	public String getFilterColumn() {
		return filterColumn;
	}

	public Filter<T> getDelegate() {
		return delegate;
	}

	@SuppressWarnings("unchecked")
	public boolean include(ReferenceTableRow theObject) {
		return delegate.include((T)theObject.getAdditionalColumn(filterColumn));
	}

	@Override
	public int hashCode() {
		if (hashCode == 0){
			hashCode = filterColumn.hashCode() + delegate.hashCode() + 50;
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof RefTableAdditionalColumnFilter
		&& delegate.equals(((RefTableAdditionalColumnFilter<?>)obj).getDelegate())
		&& filterColumn.equals(((RefTableAdditionalColumnFilter<?>)obj).getFilterColumn()));
	}

	@Override
	public String toString() {
		if (classString == null){
			classString = "RefTableAdditionalColumnFilter with filter column '" + 
					filterColumn + "' and filter delegate '" + delegate + "'";
		}
		return classString;
	}
	
	
	
}
