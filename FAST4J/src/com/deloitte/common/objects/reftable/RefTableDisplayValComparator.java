package com.deloitte.common.objects.reftable;

import java.util.Comparator;

import com.deloitte.common.objects.collection.DefaultComparator;


public class RefTableDisplayValComparator  implements Comparator<ReferenceTableRow> {

	private Comparator<String> delegate;

	//these are calculated once to optimize hashCode and toString methods
	int hashCode;
	String classString;
	
	public RefTableDisplayValComparator(Comparator<String> delegate) {
		super();
		this.delegate = delegate;
	}

	public RefTableDisplayValComparator() {
		this(new DefaultComparator<String>());
	}

	public Comparator<String> getDelegate() {
		return delegate;
	}

	public int compare(ReferenceTableRow o1, ReferenceTableRow o2) {
		return delegate.compare(o1.getDisplayValue(), o2.getDisplayValue());
	}

	@Override
	public int hashCode() {
		if (hashCode == 0){
			hashCode = delegate.hashCode() + 44;
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof RefTableDisplayValComparator
			&& delegate.equals(((RefTableDisplayValComparator)obj).getDelegate()));
	}

	@Override
	public String toString() {
		if (classString == null){
			classString = "RefTableDisplayValComparator with Comparator delegate '" + delegate + "'";
		}
		return classString;
	}
	
}
