package com.deloitte.common.objects.reftable;

import java.util.Comparator;
import java.util.List;

import com.deloitte.common.interfaces.Filter;

public interface ReferenceTable {

	public String getName();

	ReferenceTableRow getRow(String key);
	
	ReferenceTableRow getRow(String key, String localeId);
	
	List<ReferenceTableRow> getRows(Filter<ReferenceTableRow> filter);
	
	List<ReferenceTableRow> getRows(Comparator<ReferenceTableRow> comparator);
	
	List<ReferenceTableRow> getRows(Filter<ReferenceTableRow> filter, Comparator<ReferenceTableRow> comparator);
	
	List<ReferenceTableRow> getRows();

}