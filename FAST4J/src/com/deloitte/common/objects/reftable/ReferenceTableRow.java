package com.deloitte.common.objects.reftable;

import java.util.Date;
import java.util.Map;

public interface ReferenceTableRow extends Comparable<ReferenceTableRow> {
	
	public static final String NO_LOCALE = "-";

	public String getKey();

	public String getDisplayValue();

	public Date getEffectiveStartDate();

	public Date getEffectiveEndDate();

	public String getLocaleId();

	public Map<String, ?> getAdditionalColumns();

	/**
	 * @param columnName
	 * @return the value of the identified column. The type conversion is unchecked,
	 * so if the requested column is not compatible with the dyanmic return type
	 * a RuntimeException will be encountered
	 */
	public <T> T getAdditionalColumn(String columnName);

	/*
	 * @param position the 1-based ordinal position of the additional column
	 * @return the value of the column identified by the ordinal position. The 
	 * type conversion is unchecked, so if the requested column is not compatible
	 * with the dyanmic return type a RuntimeException will be encountered.
	 * 
	 * For reference tables of any non-trivial size, name-based access will be faster.
	 * It is recommended to use getAdditionalColumn(String columnName) for any reference
	 * table with more than two additional columns.
	public <T> T getAdditionalColumn(int position);
	 */

}