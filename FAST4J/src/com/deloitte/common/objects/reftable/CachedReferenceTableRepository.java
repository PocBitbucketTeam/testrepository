package com.deloitte.common.objects.reftable;

import java.util.List;

import com.deloitte.common.interfaces.Cache;
import com.deloitte.common.objects.collection.SimpleCache;
import com.deloitte.common.objects.collection.UnlimitedCache;

/**
 * Will add caching behavior to any ReferenceTableRepository it is used to decorate.
 * This caching will occur at two levels. First, the returned ReferenceTables will be
 * caches so that repeated calls to {@link #getTable(String)} will return the already
 * loaded reference table instance. In addition, it will also return ReferenceTable
 * implementations that are able to cache their query results. This means that reapeated
 * calls to the different {@link ReferenceTable#getRows(com.deloitte.common.interfaces.Filter, java.util.Comparator)
 * methods will themselves be cached, so if the methods are called again with the same Filter
 * conditions the result will be returned directly from the cache, avoiding the need to apply
 * the sorting and filtering to the query again.
 */
public class CachedReferenceTableRepository implements ReferenceTableRepository {

	ReferenceTableRepository repos;
	Cache<String, CachedReferenceTable> cache;
	QueryCacheFactory queryCacheFactory;
	
	/**
	 * Decorates the provided {@link ReferenceTableRepository} with a
	 * ReferenceTable cache using the cache instance provided, and which
	 * will use the provided QueryCacheFactory to retrieve caches for each
	 * reference table
	 * @param repos the {@link ReferenceTableRepository} instance to add
	 * caching behavior to
	 * @param cache the Cache to use for the reference table cache
	 * @param queryCacheFactory the {@link QueryCacheFactory} which will be used
	 * to provide the cache for each reference table
	 */
	public CachedReferenceTableRepository(ReferenceTableRepository repos,
			Cache<String, CachedReferenceTable> cache, QueryCacheFactory queryCacheFactory) {
		super();
		this.repos = repos;
		this.cache = cache;
		this.queryCacheFactory = queryCacheFactory;
	}

	/**
	 * Decorates the provided {@link ReferenceTableRepository} with a
	 * ReferenceTable cache using the cache instance provided, and a query 
	 * cache of size 10 for each {@link ReferenceTable} using 
	 * {@link com.deloitte.common.objects.collection.SimpleCache SimpleCache}
	 * @param repos the {@link ReferenceTableRepository} instance to add
	 * caching behavior to
	 * @param cache the Cache to use for the reference table cache
	 */
	public CachedReferenceTableRepository(ReferenceTableRepository repos,
			Cache<String, CachedReferenceTable> cache) {
		this(repos, cache, new QueryCacheFactory(){
			public Cache<CachedReferenceTable.RefTblQueryCriterion, List<ReferenceTableRow>> getQueryCache(String tableName){
				return new SimpleCache<CachedReferenceTable.RefTblQueryCriterion, List<ReferenceTableRow>>(10);
			}
		});
	}

	/**
	 * Decorates the provided {@link ReferenceTableRepository} with a
	 * ReferenceTable cache of unlimited size and the provide QueryCacheFactory
	 * @param repos the {@link ReferenceTableRepository} instance to add
	 * caching behavior to
	 * @param queryCacheFactory the {@link QueryCacheFactory} which will be used
	 * to provide the cache for each reference table
	 */
	public CachedReferenceTableRepository(ReferenceTableRepository repos,
			QueryCacheFactory queryCacheFactory) {
		this(repos, new UnlimitedCache<String, CachedReferenceTable>(), queryCacheFactory);
	}

	/**
	 * Decorates the provided {@link ReferenceTableRepository} with a
	 * ReferenceTable cache of unlimited size, and a query cache of size 10
	 * using {@link com.deloitte.common.objects.collection.SimpleCache SimpleCache}
	 * @param repos the {@link ReferenceTableRepository} instance to add
	 * caching behavior to
	 */
	public CachedReferenceTableRepository(ReferenceTableRepository repos) {
		this(repos, new UnlimitedCache<String, CachedReferenceTable>());
	}

	public ReferenceTable getTable(String tableName) {
		if(cache.containsKey(tableName)){
			return cache.get(tableName);
		} else {
			CachedReferenceTable table = new CachedReferenceTable(repos.getTable(tableName), queryCacheFactory.getQueryCache(tableName));
			cache.put(tableName, table);
			return table;
		}
	}
	
	/**
	 * Updates the contents of the CachedReferenceTable with the current state of the table
	 * as defined by the ReferenceTableRepository, allowing any updates to the ref table contents
	 * to be dynamically pushed out to the application. Will cause any new queries to that 
	 * ReferenceTable to return the updated ReferenceTableRows
	 */
	public void updateCache(String tableName){
		if(cache.containsKey(tableName)){
			//retrieve the existing entry from the cache and update with new underlying data
			CachedReferenceTable cachedEntry = cache.get(tableName);
			cachedEntry.updateReferenceTable(repos.getTable(tableName));
		} else {
			//table being updated has not yet been loaded
			this.getTable(tableName);
		}
	}
	
	
	/**
	 * Indicates if the specified table exists in the reference table cache
	 */
	public boolean isTableCached(String tableName){
		return cache.containsKey(tableName);
	}

	/**
	 * Returns the number of cache hits to the <B>query cache</B> for the specified table
	 */
	public long getCacheHits(String tableName){
		CachedReferenceTable crt = cache.get(tableName);
		return (crt == null) ? 0 : crt.getCacheHits();
	}

	/**
	 * Returns the number of cache misses to the <B>query cache</B> for the specified table
	 */
	public long getCacheMisses(String tableName){
		CachedReferenceTable crt = cache.get(tableName);
		return (crt == null) ? 0 : crt.getCacheMisses();
	}

	//TODO do we want other management operations? (getCachedTableKeys, getCacheStats,....???)
//	public List<String> getCachedTableKeys(){
//		//would need to extend the cache interface for this!
//		return Collections.emptyList();
//	}
	
	
	/**
	 * Factory interface which defines the strategy by which filter caches will be created
	 * when {@link CachedReferenceTable CachedReferenceTables} are instantiated by the 
	 * {@link CachedReferenceTableRepository CachedReferenceTableRepository}. Can be used
	 * to provide customized logic defining the size and type of the query cache each
	 * reference table will get.
	 */
	public static interface QueryCacheFactory {
		
		public Cache<CachedReferenceTable.RefTblQueryCriterion, List<ReferenceTableRow>> getQueryCache(String tableName);
	
	}
}
