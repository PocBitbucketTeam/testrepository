package com.deloitte.common.objects.reftable;

import com.deloitte.common.interfaces.Filter;

public class RefTableLocaleIdFilter implements Filter<ReferenceTableRow>{

	final String localeId;

	//these are calculated once to optimize hashCode and toString methods
	int hashCode;
	String classString;
	
	public RefTableLocaleIdFilter(String localeId) {
		super();
		this.localeId = localeId;
	}

	public String getLocaleId() {
		return localeId;
	}

	public boolean include(ReferenceTableRow theObject) {
		return localeId.equals(theObject.getLocaleId());
	}

	@Override
	public int hashCode() {
		if (hashCode == 0){
			hashCode = localeId.hashCode() + 8;
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof RefTableLocaleIdFilter
		&& localeId.equals(((RefTableLocaleIdFilter)obj).getLocaleId()));
	}

	@Override
	public String toString() {
		if (classString == null){
			classString = "RefTableLocaleIdFilter with local ID '" + localeId + "'";
		}
		return classString;
	}
	
}
