package com.deloitte.common.objects.reftable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.deloitte.common.interfaces.Cache;
import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.collection.NullCache;
import com.deloitte.common.objects.collection.NullFilter;

/**
 * Decorator class for ReferenceTable objects that allows the ReferenceTableRepository
 * to push updates to the underlying reference table out. Allows the ReferenceTable
 * contents to be dynamically updated in a transparent fashion to anyone consuming the
 * ReferenceTable.
 */
public class CachedReferenceTable implements ReferenceTable {
	
	ReferenceTable table;
	Cache<RefTblQueryCriterion, List<ReferenceTableRow>> queryCache;
	
	private long cacheHits;
	private long cacheMisses;
	
	
	//constants used in the query cache for storing queries without parts of the criterion
	private static final Filter<ReferenceTableRow> theNullFilter = new NullFilter<ReferenceTableRow>();
	private static final Comparator<ReferenceTableRow> theNullComparator = new Comparator<ReferenceTableRow>(){
		public int compare(ReferenceTableRow o1, ReferenceTableRow o2) { return 0; }
		public int hashCode() { return 5; }
		public boolean equals(Object obj) { return obj == this; }
	};
	private static final RefTblQueryCriterion theNullCriterion = new RefTblQueryCriterion(theNullFilter, theNullComparator);

	
	
	public CachedReferenceTable(ReferenceTable table) {
		this(table, new NullCache<RefTblQueryCriterion, List<ReferenceTableRow>>());
	}
	
	public CachedReferenceTable(ReferenceTable table,
			Cache<RefTblQueryCriterion, List<ReferenceTableRow>> filterCache) {
		super();
		this.table = table;
		this.queryCache = filterCache;
	}

	/**
	 * Allows the ReferenceTableRepository to push updates to the row contents
	 * out to the CachedReferenceTables
	 * @param table new table with updated rows
	 */
	public void updateReferenceTable(ReferenceTable table) {
		this.table = table;
		this.queryCache.clear();
		cacheHits = 0;
		cacheMisses = 0;
	}
	
	public long getCacheHits() {
		return cacheHits;
	}

	public long getCacheMisses() {
		return cacheMisses;
	}

	public String getName() {
		return table.getName();
	}

	public ReferenceTableRow getRow(String key) {
		return table.getRow(key);
	}
	
	public ReferenceTableRow getRow(String key, String localeId) {
		return table.getRow(key, localeId);
	}
	
	/**
	 * caches the rows returned keyed on the Filter object
	 */
	public List<ReferenceTableRow> getRows(Filter<ReferenceTableRow> filter, Comparator<ReferenceTableRow> comparator) {
		RefTblQueryCriterion cacheKey = new RefTblQueryCriterion(filter, comparator);
		if (queryCache.containsKey(cacheKey)){
			cacheHits++;
			return queryCache.get(cacheKey);
		} else {
			List<ReferenceTableRow> result = Collections.unmodifiableList(table.getRows(filter, comparator));
			queryCache.put(cacheKey, result);
			cacheMisses++;
			return result;
		}
	}

	public List<ReferenceTableRow> getRows(Filter<ReferenceTableRow> filter) {
		/* duplicating caching logic and calling getRows() slightly more efficient
		 * than calling getRows(Filter, Comparator) (avoids the sorting overhead) */
		RefTblQueryCriterion cacheKey = new RefTblQueryCriterion(filter, theNullComparator);
		if (queryCache.containsKey(cacheKey)){
			cacheHits++;
			return queryCache.get(cacheKey);
		} else {
			List<ReferenceTableRow> result = Collections.unmodifiableList(table.getRows(filter));
			queryCache.put(cacheKey, result);
			cacheMisses++;
			return result;
		}
	}

	public List<ReferenceTableRow> getRows(Comparator<ReferenceTableRow> comparator) {
		/* duplicating caching logic and calling getRows() slightly more efficient
		 * than calling getRows(Filter, Comparator) (avoids the filter overhead) */
		RefTblQueryCriterion cacheKey = new RefTblQueryCriterion(theNullFilter, comparator);
		if (queryCache.containsKey(cacheKey)){
			cacheHits++;
			return queryCache.get(cacheKey);
		} else {
			List<ReferenceTableRow> result = Collections.unmodifiableList(table.getRows(comparator));
			queryCache.put(cacheKey, result);
			cacheMisses++;
			return result;
		}
	}

	public List<ReferenceTableRow> getRows() {
		/* duplicating caching logic and calling getRows() slightly more efficient
		 * than calling getRows(Filter) (avoids the null filtering overhead */
		if (queryCache.containsKey(theNullCriterion)){
			cacheHits++;
			return queryCache.get(theNullCriterion);
		} else {
			List<ReferenceTableRow> result = Collections.unmodifiableList(table.getRows());
			queryCache.put(theNullCriterion, result);
			cacheMisses++;
			return result;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof CachedReferenceTable && table.equals(obj);
	}

	@Override
	public int hashCode() {
		return table.hashCode();
	}

	@Override
	public String toString(){
        return "CachedReferenceTable which decorates: [" + table.toString() + "]";
	}

	
	
	/**
	 * Internal class used to create keys for the filter cache lookups
	 */
	public final static class RefTblQueryCriterion {
		
		private final Filter<ReferenceTableRow> filter;
		private final Comparator<ReferenceTableRow> comparator;
		
		public RefTblQueryCriterion(Filter<ReferenceTableRow> filter,
										Comparator<ReferenceTableRow> comparator) {
			super();
			this.comparator = comparator;
			this.filter = filter;
		}

		public Comparator<ReferenceTableRow> getComparator() {
			return comparator;
		}

		public Filter<ReferenceTableRow> getFilter() {
			return filter;
		}

		@Override
		public int hashCode() {
			return filter.hashCode() + comparator.hashCode() + 20;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof RefTblQueryCriterion){
				RefTblQueryCriterion criterion = (RefTblQueryCriterion)obj;
				return filter.equals(criterion.getFilter()) && comparator.equals(criterion.getComparator());
			} else {
				return false;
			}
		}

	}
	
}
