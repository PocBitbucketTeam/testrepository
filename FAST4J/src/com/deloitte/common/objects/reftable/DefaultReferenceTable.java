package com.deloitte.common.objects.reftable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

public class DefaultReferenceTable implements ReferenceTable {

	private final String name;
	private final Map<RefTblLangRowKey, ReferenceTableRow> rows;

	public DefaultReferenceTable(String name, List<ReferenceTableRow> rows) {
		super();

        if (name==null || name.equals("")){
            throw new UncheckedApplicationException(this.getClass(), "Name is null or empty");        	
        }
        if (rows == null){
            throw new UncheckedApplicationException(this.getClass(), "Rows is null");
        }
        if (rows.size() == 0){
            throw new UncheckedApplicationException(this.getClass(), "Rows does not contain any elements.");
        }

        this.name = name;
        //put the rows in a order-preserving map, keyed by the row key and locale ID
        Map<RefTblLangRowKey, ReferenceTableRow> rowsMap = new LinkedHashMap<DefaultReferenceTable.RefTblLangRowKey, ReferenceTableRow>(rows.size() * 2, .5f);
        for (ReferenceTableRow row : rows){
        	rowsMap.put(new RefTblLangRowKey(row.getKey(), row.getLocaleId()), row);
        }
		this.rows = Collections.unmodifiableMap(rowsMap);
	}
	
	public String getName() {
		return name;
	}
	
	public ReferenceTableRow getRow(String key) {
		return this.getRow(key, ReferenceTableRow.NO_LOCALE);
	}

	public ReferenceTableRow getRow(String key, String localeId) {
		ReferenceTableRow theRow = rows.get(new RefTblLangRowKey(key, localeId));
		if (theRow == null){
			//this may be too harsh. May want to just log a warning and return null instead
			throw new UncheckedApplicationException(this.getClass(), "Invalid row key " + key);
		}
		return theRow;
	}

	public List<ReferenceTableRow> getRows(Filter<ReferenceTableRow> filter, Comparator<ReferenceTableRow> comparator) {
		List<ReferenceTableRow> filteredRows = this.getRows(filter);
		Collections.sort(filteredRows, comparator);
		return filteredRows;
	}
	
	public List<ReferenceTableRow> getRows(Comparator<ReferenceTableRow> comparator) {
		List<ReferenceTableRow> rows = this.getRows();
		Collections.sort(rows, comparator);
		return rows;
	}
	
	public List<ReferenceTableRow> getRows(Filter<ReferenceTableRow> filter) {
		List<ReferenceTableRow> filteredRows = new ArrayList<ReferenceTableRow>();
		Collection<ReferenceTableRow> tempRowsList = rows.values();
		for (ReferenceTableRow row : tempRowsList){
			if (filter.include(row)){
				filteredRows.add(row);
			}
		}
		return filteredRows;
	}
	
	public List<ReferenceTableRow> getRows() {
		return new ArrayList<ReferenceTableRow>(rows.values());
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof DefaultReferenceTable
			&& ((DefaultReferenceTable)obj).getName().equals(this.getName());
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode() + 5000;
	}

	@Override
	public String toString(){
        return "Reference table name: " + this.getName() + "; Number of rows: " + this.getRows().size();
	}


	/**
	 * Internal class used to represent keys for ReferenceTableRows
	 */
	protected final static class RefTblLangRowKey {
		
		private final String rowKey;
		private final String rowLocalId;
		

		public RefTblLangRowKey(String rowKey, String rowLocalId) {
			super();
			this.rowKey = rowKey;
			this.rowLocalId = rowLocalId;
		}

		public String getRowKey() {
			return rowKey;
		}

		public String getRowLocalId() {
			return rowLocalId;
		}

		@Override
		public int hashCode() {
			return rowKey.hashCode() + rowLocalId.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof RefTblLangRowKey){
				RefTblLangRowKey langRowKey = (RefTblLangRowKey)obj;
				return rowKey.equals(langRowKey.getRowKey()) && rowLocalId.equals(langRowKey.getRowLocalId());
			} else {
				return false;
			}
		}

		@Override
		public String toString() {
			return "RefTblLangRowKey[" + this.getRowKey() + "," + this.rowLocalId + "]";
		}

	}
}
