package com.deloitte.common.objects.reftable;

import java.util.Calendar;
import java.util.Date;

import com.deloitte.common.interfaces.Filter;

public final class RefTableEffectiveDateFilter implements Filter<ReferenceTableRow> {

	final Date filterDate;
	
	//these are calculated once to optimize hashCode and toString methods
	int hashCode;
	String classString;
	
	public RefTableEffectiveDateFilter() {
		this(new Date());
	}

	public RefTableEffectiveDateFilter(Date filterDate) {
		super();
		
		Calendar cal = Calendar.getInstance();   
		cal.setTime(filterDate);   
		  
		// Set time fields to zero   
		cal.set(Calendar.HOUR_OF_DAY, 0);   
		cal.set(Calendar.MINUTE, 0);   
		cal.set(Calendar.SECOND, 0);   
		cal.set(Calendar.MILLISECOND, 0);   
		  
		// Put it back in the Date object   
		this.filterDate = cal.getTime();
	}

	public Date getFilterDate() {
		return filterDate;
	}

	public boolean include(ReferenceTableRow theObject) {
		return theObject.getEffectiveStartDate().getTime() <= filterDate.getTime()
				&& filterDate.getTime() < theObject.getEffectiveEndDate().getTime();
	}

	@Override
	public int hashCode() {
		if (hashCode == 0){
			hashCode = this.filterDate.hashCode() + 29;
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object obj) {
		return obj==this ||
		(obj instanceof RefTableEffectiveDateFilter
		&& getFilterDate().equals(((RefTableEffectiveDateFilter)obj).getFilterDate()));
	}

	@Override
	public String toString() {
		if (classString == null){
			classString = "RefTableEffectiveDateFilter with filter date '" + getFilterDate() + "'";
		}
		return classString;
	}

	
}
