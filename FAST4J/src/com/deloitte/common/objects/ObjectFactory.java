package com.deloitte.common.objects;

import com.deloitte.common.interfaces.Factory;
import com.deloitte.common.objects.framework.CheckedApplicationException;

public class ObjectFactory implements Factory
{
	private static ObjectFactory me = new ObjectFactory();
	
	public Object createFor(Object theClass) throws CheckedApplicationException
	{
		Object theObject = null;
		try 
		{
			if (theClass instanceof Class)
			{
				theObject = ((Class<?>)theClass).newInstance();
			} else
			{
				theObject = theClass.getClass().newInstance();
			}
			} catch (InstantiationException e) 
			{
				throw new CheckedApplicationException(e.getClass(),e.toString());
			} catch (IllegalAccessException e) 
			{
				throw new CheckedApplicationException(e.getClass(),e.toString());
		}
		return theObject;
	}
	
	public static Factory getInstance()	{
		return me;
	}

}
