package com.deloitte.common.presentation;

import java.util.HashMap;
import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.decorator.AbstractDomainObjectDecorator;

public class DomainObjectPresentationDecorator extends AbstractDomainObjectDecorator 
{
	private static Map<String, Map<String, ?>> properties;
	
	public DomainObjectPresentationDecorator(DomainObject theObject)
	{
		super(theObject);
	}

	public static void loadProperties(String theKey, Map<String, ?> theProperties) {
		if (properties == null) {
			properties = new HashMap<String, Map<String, ?>>();
		}
		properties.put(theKey, theProperties);
	}

	public static void removeProperties(String theKey)
	{
		properties.remove(theKey);
	}
	
	public Map<String, ?> getProperties() {
		Map<String, Object> theReturnValues = new HashMap<String, Object>();
		Map<String, ?> theProperties = properties.get(getObject().getClass().getName());
		if (theProperties != null) theReturnValues.putAll(theProperties);
		return theReturnValues;
	}
	
}
