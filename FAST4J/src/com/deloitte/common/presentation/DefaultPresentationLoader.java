package com.deloitte.common.presentation;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.DebugHelper;

/**
 * ConfigurationLoader uses either external files to initialize items
 * within the system.
 * 
 * TODO Database Backed Loader?
 * 
 * The format of the properties file is 
 * dao1=com.deloitte.common.interfaces.DomainObject,com.deloitte.common.persistence.HsqldbConnectionFactory,com.deloitte.common.persistence.DomainObjectDAO
 *
 * The format of the xml file is
 * <configuration>
 * <presentation>
 *	<object key="com.deloitte.common.objects.domain.MockDomainObject" 
 *	 <property name="value" size="20" maxlength="28" label="Value Label" readonly="false" tooltip="Value"/>
 *	 <property name="wrapper" size="20" maxlength="20" label="Value Label" readonly="false" tooltip="BigDecimal"/>
 *	</object>
 *  </presentation> 
 * @author SIDT Framework Team
 *
 */
/**
 * 
 */
public class DefaultPresentationLoader implements Loader
{
	private static final Logger logger = Logger.getLogger(DefaultPresentationLoader.class.getName());

	private static DefaultPresentationLoader me;
	private String fileName;
	
	
	private DefaultPresentationLoader(String fileName)
	{
		super();
		this.fileName = fileName;
	}
	
	public static DefaultPresentationLoader getInstance(String fileName)
	{
		
		if (me == null)
		{
			me = new DefaultPresentationLoader(fileName);
		}
		return me;
	}


	private InputStream getFile() throws CheckedApplicationException
	{
		return getClass().getResourceAsStream(fileName);
	}
	
	
	public final void loadValues() throws CheckedApplicationException
	{
		loadXMLValues();
		StringBuffer sb = new StringBuffer();
		for (int i=0; i < DebugHelper.getLocalLog().length; i++)
		{
			sb.append(DebugHelper.getLocalLog()[i] + "\n");
		}
		if (sb.length() > 0) logger.info(sb.toString());
	}
	
	
	/**
	 * Find the Metadata in XML document
	 */
	private void loadXMLValues() throws CheckedApplicationException {
		
		try 
		{
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.parse(getFile());

			NodeList theDecorators = document.getElementsByTagName("decorator");
			for (int outerLoop = 0; outerLoop < theDecorators.getLength(); outerLoop++)
			{
				Node theKeyNode = theDecorators.item(outerLoop);
				String theKey = theKeyNode.getAttributes().getNamedItem("key").getNodeValue();
				Node thePropertyNodes = null;
				Map<String, String> theProperties = new HashMap<String, String>(theKeyNode.getChildNodes().getLength());

				for (int innerLoop = 0; innerLoop < theKeyNode.getChildNodes().getLength(); innerLoop++) 
				{
					thePropertyNodes = theKeyNode.getChildNodes().item(innerLoop);
						for (int j =0; j < thePropertyNodes.getChildNodes().getLength(); j++)
						{
							if (thePropertyNodes.getChildNodes().item(j).getNodeType() == Node.ELEMENT_NODE)
							{
								String theName = thePropertyNodes.getAttributes().getNamedItem("name").getNodeValue();
								theProperties.put(theName.toUpperCase() + "." + thePropertyNodes.getChildNodes().item(j).getNodeName().toUpperCase(), thePropertyNodes.getChildNodes().item(j).getFirstChild().getNodeValue());
							}
						}
				}
				DomainObjectPresentationDecorator.loadProperties(theKey, theProperties);
			}

		} catch (ParserConfigurationException e) 
		{
			throw new CheckedApplicationException(getClass(), "Parser Error ",
					e);
		} catch (IOException e) {
			throw new CheckedApplicationException(getClass(),
					"IO/Error Error ", e);
		} catch (SAXException e) {
			throw new CheckedApplicationException(getClass(), "SAX Error ", e);
		}

	}

}
