package com.deloitte.common.presentation.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.deloitte.common.util.ReflectionHelper;
/**
 * Display object will "walk" an object graph to return the
 * lowest value. If a Null object is encountered along the 
 * way it will return a zero length string.
 * @author SIDT Framework Team
 *
 */
public class DisplayObject extends TagSupport
{
	private static final long serialVersionUID = 1L;
	private String theName;
	private String theValue;

	public void setName(String theName)
	{
		this.theName = theName;
	}
	
	public void setValue(String theValue)
	{
		this.theValue = theValue;
	}
	
	public String getValue()
	{
		return theValue;
	}
	public String getName()
	{
		return theName;
	}
	
	public int doStartTag() throws JspException
	{
		Object o = pageContext.getAttribute(getName());
		String theReturn = ReflectionHelper.invokeNestedAccessorMethods(o,getValue());
		try {
			pageContext.getOut().print(theReturn);
		} catch (IOException ioe)
		{
			throw new JspException(ioe);
		}

		return SKIP_BODY;
	}
}
