package com.deloitte.common.presentation.sessionmanagement;

/**
 * <p>
 * Stores all HttpSession objects in one location so that they may be queried.
 * Currently in J2EE you can only get a handle on the HttpSession object by 
 * calling the HttpRequest.getSession(boolean b) method. This means there is no
 * centralized location to get handles on the HttpSessions. 
 * <p>
 * Having the handle available allows you to implement behavior like clearing a
 * user's session from the system(invalidate) or querying it to determine how 
 * many things have been put into the session.
 * <p>
 * Note the web.xml will need to be updated to register the listener.
 * <p>
 * <pre>
 * <servlet>
 * ...
 * 	<listener>
 *		<listener-class>com.deloitte.common.presentation.sessionmanagement.HttpSessionListener</listener-class>
 *	</listener>
 *...
 * </servlet>
 * </pre>
 */

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

public class HttpSessionListener implements javax.servlet.http.HttpSessionListener {

	public HttpSessionListener()
	{
		super();
	}
	public void sessionCreated(final HttpSessionEvent event) 
	{
		HttpSession theSession = event.getSession();
		Map<String, HttpSession> theMap = getSessionMap(theSession);
		theMap.put(theSession.getId(),theSession);
	}

	public void sessionDestroyed(final HttpSessionEvent event) 
	{
		HttpSession theSession = event.getSession();
		Map<String, HttpSession> theMap = getSessionMap(theSession);
		theMap.remove(theSession.getId());
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, HttpSession> getSessionMap(final HttpSession theSession) {
		Map<String, HttpSession> theMap = (Map<String, HttpSession>) theSession.getServletContext().getAttribute("theSessions");
		if (theMap == null)	{
			theMap = new HashMap<String, HttpSession>();
			theSession.getServletContext().setAttribute("theSessions",theMap);
		}
		return theMap;
	}

}
