package com.deloitte.common.presentation.sessionmanagement;

/**
 * This method should be used when placing things in the HttpSession. It will
 * assist in managing the size and number of objects in the HttpSession.
 * 
 */
import java.io.Serializable;

public interface SessionObject extends Serializable 
{
	public long size();
}
