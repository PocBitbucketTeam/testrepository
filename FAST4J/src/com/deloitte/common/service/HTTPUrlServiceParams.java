package com.deloitte.common.service;

public class HTTPUrlServiceParams extends GenericServiceParams {

	private String endpoint;
	private String qname;
	private String namespace;
	private String protocol;

	public HTTPUrlServiceParams() {
		super();
	}

	public HTTPUrlServiceParams(String endpoint, String qname,
			String namespace, String protocol) {
		this.endpoint = endpoint;
		this.qname = qname;
		this.namespace = namespace;
		this.protocol = protocol;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getQname() {
		return qname;
	}

	public void setQname(String qname) {
		this.qname = qname;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " endpoint : " + endpoint + " qname : "
				+ qname + " protocol : " + protocol + " namespace : "
				+ namespace;
	}

}
