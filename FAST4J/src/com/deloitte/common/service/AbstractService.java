package com.deloitte.common.service;

import com.deloitte.common.service.interfaces.Service;
import com.deloitte.common.service.interfaces.ServiceParams;

/**
 * All the service Implementation classes can extend this class or directly
 * implement the Service interface.
 * 
 * @author Framework Team
 * 
 */
public abstract class AbstractService implements Service {

	private ServiceParams propeties;

	public void setServiceParams(ServiceParams props) {
		this.propeties = props;

	}

	public ServiceParams getServiceParams() {
		return propeties;
	}
}
