package com.deloitte.common.service;

import com.deloitte.common.service.interfaces.ServiceParams;
import com.deloitte.common.util.ReflectionHelper;

final class ServiceConfiguration {

	private String key;
	private String artificialKey;
	private String serviceName;
	private ServiceParams serviceParams;

	public ServiceConfiguration(String key, String artificialKey, String serviceName,
			ServiceParams serviceParams) {
		this.key = key;
		this.serviceName = serviceName;
		this.serviceParams = serviceParams;
		this.artificialKey= artificialKey;
	}

	@SuppressWarnings("unchecked")
	public <T> T getService() {
		T theService = (T) ReflectionHelper.createFor(
				serviceName, new Object[0]);
		return theService;
	}

	public String getServiceName() {
		return serviceName;
	}

	public ServiceParams getServiceParams() {
		return serviceParams;
	}

	@SuppressWarnings("unchecked")
	public <T> Class<T> getKey() {
		Class<T> theKey = (Class<T>) ReflectionHelper
				.createFor(key);
		return theKey;
	}
	
	
	public String getArtificialKey() {
		return artificialKey;
	}
	
	public String toString() {
		return ",\n\tKey=" + getKey() + ",\n\tServiceName="
				+ getServiceName() + ",\n\tConfig Properties="
				+ getServiceParams();
	}
}
