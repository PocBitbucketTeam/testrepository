package com.deloitte.common.service;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.deloitte.common.objects.AbstractXMLLoader;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.service.interfaces.ServiceParams;

/**
 * This Loader is part of the Loader hierarchy can be bootstrapped in your
 * application for examples; Spring startup, a servlet or ejb startup class.
 * 
 * <code>
 * 	XMLServiceLoader loader = new XMLServiceLoader(fileName);
 * 	loader.loadValues();
 * </code>
 * 
 * Loads the service from a XML file
 * <p>
 * Example Configuration:
 * </p>
 * 
 * <pre>
 * &lt;services&gt;
 *  &lt;!-- any services that may not require initial properties can be added like this --!&gt;
 *  &lt;service  key=&quot;com.deloitte.common.service.MockService&quot;
 *  	serviceName=&quot;com.deloitte.common.service.MockServiceImpl&quot; artificialKey=&quot;someservice&quot; &gt;		
 *  &lt;/service&gt;
 * 
 *  &lt;!-- any web services that may have some initial properties can be added like this --!&gt;
 *  &lt;httpservice key=&quot;com.deloitte.common.service.UserService&quot;
 *  	serviceName=&quot;com.deloitte.common.service.UserService&quot;&gt;
 * 	 &lt;endpoint&gt;http://ushydbkarthik4.us.deloitte.com:9080/CUBSWebServicesPOC/services/UserLoginWS&lt;/endpoint&gt;
 * 	 &lt;qname&gt; etUserResponse&lt;/qname&gt;
 * 	 &lt;namespace&gt;http://services.deloitte.com&lt;/namespace&gt;
 * 	 &lt;protocol&gt;POST &lt;/protocol&gt;
 *  &lt;/httpservice&gt;
 * 
 *  &lt;messagingservice key=&quot;com.deloitte.common.service.OrderMessageService&quot;
 *  	serviceName=&quot;com.deloitte.common.service.OrderMessageServiceImpl&quot;&gt;
 *  	&lt;connectionFactory&gt;testConnectionFactory&lt;/connectionFactory&gt;
 * 	 &lt;destination&gt;queue&lt;/destination&gt;		
 *  &lt;/messagingservice&gt;
 * &lt;/services&gt;
 * </pre>
 * 
 * @author SIDT Framework Team
 * 
 */
public class XMLServiceLoader extends AbstractXMLLoader {

	private static final Logger logger = Logger.getLogger(XMLServiceLoader.class.getName());
	
	public XMLServiceLoader(Document document){
		super(document);
	}
	
	public XMLServiceLoader(String fileName) {
		super(fileName);
	}

	protected void  loadValues(Document document) throws CheckedApplicationException{
		NodeList theServices = document.getDocumentElement().getChildNodes();
		if (theServices != null && theServices.getLength() !=0 ) {
			Node node = null; 
			NamedNodeMap map = null;
			ServiceParams params = null;
			ServiceConfiguration configuration = null;			
			for (int i = 0; i < theServices.getLength(); i++) {
				node = theServices.item(i);
				String key=null, serviceName=null, artificialKey = null;
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;				
					map = node.getAttributes();
					key = map.getNamedItem("key").getNodeValue();
					serviceName = map.getNamedItem("serviceName").getNodeValue();
					if (map.getNamedItem("artificialKey") != null) {
						artificialKey = map.getNamedItem("artificialKey").getNodeValue();
					}
					if (("service".equals(element.getNodeName()))) {
						params = new GenericServiceParams();
						((GenericServiceParams)params).setProps(loadGeneralProperties(element));
					}else if (("httpService".equals(element.getNodeName()))) {
						params = loadHTTPUrlServiceConfiguration(element);
					}else if (("webService".equals(element.getNodeName()))) {
						params = loadWebServiceConfiguration(element);
					}else if (("messagingService".equals(element.getNodeName()))) {
						params = loadMessageServiceConfiguration(element);
					}
					configuration = new ServiceConfiguration(key, artificialKey, serviceName, params);
					//If any artificial key is configured register with that key.
					if (configuration.getArtificialKey() == null) {
						logger.info("\n Registering a " + configuration.getKey()
								+ " Service ..." + configuration);
						DefaultServiceFactory.getInstance().register(
								configuration.getKey(),
								configuration.getServiceParams(),
								configuration.getService());
					} else {
						logger.info("\n Registering with a artificial key " + configuration.getArtificialKey()
								+ " Service ..." + configuration);
						DefaultServiceFactory.getInstance()
								.registerByArtificialKey(
										configuration.getArtificialKey(),
										configuration.getServiceParams(),
										configuration.getService());
					}
				}
			}		
		}
	}

	private ServiceParams loadWebServiceConfiguration(Element element) {
		WebServiceParams params =null;
		NodeList subNodes = element.getChildNodes();
		String wsdl = null, namespace = null, qname = null;
		for (int j = 0; j < subNodes.getLength(); j++) {
			Node subNode = subNodes.item(j);
			if (subNode.getNodeType() == Node.ELEMENT_NODE) {
				if (subNode.getNodeName().equals("wsdl")) {
					wsdl = trim(subNode.getChildNodes().item(0).getNodeValue());
				} else if (subNode.getNodeName().equals("namespace")) {
					namespace = trim(subNode.getChildNodes().item(0)
							.getNodeValue());
				} else if (subNode.getNodeName().equals("qname")) {
					qname = trim(subNode.getChildNodes().item(0).getNodeValue());
				}
			}			
		}
		params = new WebServiceParams(wsdl,qname, namespace);
		params.setProps(loadGeneralProperties(element));
		return params;
	}
	private ServiceParams loadHTTPUrlServiceConfiguration(Element element) {
		HTTPUrlServiceParams params = null;
		NodeList subNodes = element.getChildNodes();
		String endpoint = null, namespace = null, qname = null, protocol = null;;
		for (int j = 0; j < subNodes.getLength(); j++) {
			Node subNode = subNodes.item(j);
			if (subNode.getNodeType() == Node.ELEMENT_NODE) {
				if (subNode.getNodeName().equals("endpoint")) {
					endpoint= trim(subNode.getChildNodes().item(0).getNodeValue());
				}else if (subNode.getNodeName().equals("namespace")) {
					namespace= trim(subNode.getChildNodes().item(0).getNodeValue());
				}else if (subNode.getNodeName().equals("qname")) {
					qname= trim(subNode.getChildNodes().item(0).getNodeValue());
				}else if (subNode.getNodeName().equals("protocol")) {
					protocol= trim(subNode.getChildNodes().item(0).getNodeValue());
				}
			}
			
		}
		params = new HTTPUrlServiceParams(endpoint,qname,namespace,protocol);
		params.setProps(loadGeneralProperties(element));
		return params;
	}
	private ServiceParams loadMessageServiceConfiguration(Element element) {
		MessageServiceParams params = null;
		NodeList subNodes = element.getChildNodes();
		Properties contextProperties=null;
		String connectionFactory = null, destination = null;
		int acknowledgeMode =1;
		for (int j = 0; j < subNodes.getLength(); j++) {
			Node subNode = subNodes.item(j);
			if (subNode.getNodeType() == Node.ELEMENT_NODE) {
				if (subNode.getNodeName().equals("connectionFactory")) {
					connectionFactory= trim(subNode.getChildNodes().item(0).getNodeValue());
				}else if (subNode.getNodeName().equals("destination")) {
					destination= trim(subNode.getChildNodes().item(0).getNodeValue());
				}else if (subNode.getNodeName().equals("acknowledgeMode")) {
					try {
						acknowledgeMode = Integer.parseInt(trim(subNode.getChildNodes().item(0).getNodeValue()));
					} catch (Exception e) {
						if (logger.isLoggable(Level.WARNING)) {
							logger.warning("AcknowledgeMode is not set properly,  Setting to default 1 " + acknowledgeMode);
						}
					}					
				}else if (subNode.getNodeName().equals("context")) {
					Element contextElement = (Element) subNode;
					contextProperties = loadGeneralProperties(contextElement);
				}
			}							
		}
		params = new MessageServiceParams(connectionFactory, destination,acknowledgeMode);		
		params.setContextProperties(contextProperties);
		params.setProps(filterProps(loadGeneralProperties(element), contextProperties) );
		return params;
	}
	private Properties filterProps(Properties allProps, Properties additionalProps) {
		for (Object key : additionalProps.keySet()) {
			allProps.remove(key);
		}
		return allProps;
	}
	private Properties loadGeneralProperties(Element element) {
		NodeList theConfigProps = element.getElementsByTagName("param");
		Properties properties = new Properties();
		if (theConfigProps != null
				&& theConfigProps.getLength() != 0) {
			NamedNodeMap propertiesMap = null;			
			Node propNode = null;
			String propKey = null, propValue = null;
			for (int j = 0; j < theConfigProps.getLength(); j++) {
				propNode = theConfigProps.item(j);
				propertiesMap = propNode.getAttributes();
				propKey = propertiesMap.getNamedItem("key")
						.getNodeValue();
				propValue = propertiesMap.getNamedItem("value")
						.getNodeValue();
				properties.put(trim(propKey), trim(propValue));
			}
		}
		return properties;
	}
	private String trim(String property) {
		if (property != null) {
			return property.trim();
		}
		return property;
	}
}
