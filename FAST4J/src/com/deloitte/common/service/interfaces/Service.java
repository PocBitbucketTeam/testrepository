package com.deloitte.common.service.interfaces;


/**
 * Represents a basic Service that sets some initial properties.
 */

public interface Service {

	public void setServiceParams(ServiceParams props);

}
