package com.deloitte.common.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Abstract implementation to call a webservice method using HttpURLConnection
 * This class sends a SOAP request over HTTP and recives a SOAP response,
 * Implementation class is responsible to create the SOAP request message and
 * parse the SOAP response back to DomainObjects.
 * 
 * @see #getBody(Map)
 * @see #map(Map)
 * 
 * @author SIDT Framewok Team
 * 
 */
public abstract class AbstractHTTPUrlService extends AbstractService {

	private static final Logger logger = Logger
			.getLogger(AbstractHTTPUrlService.class.getName());

	@Override
	public HTTPUrlServiceParams getServiceParams() {
		// TODO Auto-generated method stub
		return (HTTPUrlServiceParams)super.getServiceParams();
	}
	
	public Collection<?> invoke(Map<?, ?> parameters) throws CheckedApplicationException {

		long start = System.currentTimeMillis();
		URL url = getURL();
		HttpURLConnection httpConn = getConnection(url);
		int responseCode = -1;
		String inputLine;
		StringBuffer sb = new StringBuffer();
		BufferedReader in = null;
		try {
			prepareConnection(httpConn, parameters);
			InputStreamReader isr = null;
			responseCode = httpConn.getResponseCode();
			if (HttpURLConnection.HTTP_OK == responseCode) {
				isr = new InputStreamReader(httpConn.getInputStream());
			} else {
				isr = new InputStreamReader(httpConn.getErrorStream());
			}
			in = new BufferedReader(isr);
			while ((inputLine = in.readLine()) != null) {
				sb.append(inputLine);
			}
		} catch (IOException ioe) {
			throw new CheckedApplicationException(this.getClass(),
					"An I/O Exception occured", ioe);
		} finally {
			try {
				logger.info("Completed " + this.getServiceParams()
						+ " with a Response Code of " + responseCode
						+ ", Duration =  "
						+ (System.currentTimeMillis() - start)
						+ " milliseconds");

				if (in != null)
					in.close();
			} catch (IOException ignoreThis) {
			}
		}
		return handleDocument(getServiceParams().getQname(),
				getDocument(sb.toString()));
	}

	private URL getURL() throws CheckedApplicationException {
		URL url = null;
		try {
			url = new URL(getServiceParams().getEndpoint());
		} catch (MalformedURLException me) {
			throw new CheckedApplicationException(this.getClass(),
					"Unable to parse URL " + url, me);
		}
		return url;
	}

	private HttpURLConnection getConnection(URL url)
			throws CheckedApplicationException {
		HttpURLConnection httpConn = null;
		try {
			httpConn = (HttpURLConnection) url.openConnection();

		} catch (IOException io) {
			throw new CheckedApplicationException(this.getClass(),
					"Unable to open HttpConnection" + url, io);
		}
		return httpConn;
	}

	private void prepareConnection(HttpURLConnection httpConn, Map<?, ?> parameters)
			throws CheckedApplicationException {
		try {
			httpConn.setRequestMethod(getServiceParams().getProtocol());
			httpConn.setDoOutput(true);
			if ("POST".equals(getServiceParams().getProtocol())) {
				byte[] payload = getMessage(parameters).getBytes();

				httpConn.setDoInput(true);
				httpConn.setRequestProperty("Content-Length", String
						.valueOf(payload.length));
				httpConn.setRequestProperty("Content-Type",
						"text/xml; charset=utf-8");
				//httpConn.setRequestProperty("SOAPAction", "SOAPAction");
				for (Iterator<String> iterator = httpHeaders.keySet().iterator(); iterator
						.hasNext();) {
					String key = iterator.next();
					httpConn.setRequestProperty(key, httpHeaders.get(key));
					
				}
				OutputStream out = httpConn.getOutputStream();
				out.write(payload);
				out.close();
			}
		} catch (ProtocolException pe) {
			throw new CheckedApplicationException(this.getClass(), getServiceParams().getProtocol()
					+ " is an invalid protocol", pe);
		} catch (IOException ioe) {
			throw new CheckedApplicationException(this.getClass(), getServiceParams().getProtocol()
					+ " exception in writing payload", ioe);
		}
	}

	protected abstract DomainObject map(Map<?, ?> parameters)
			throws CheckedApplicationException;

	/**
	 * Gets the return Node, and treats the tree like rows and columns of data
	 * like a JDBC result set.Converts the SOAP response to the list of Java
	 * Objects. If a webservice returns a Array result this method creates
	 * corresponding domain Objects.
	 * 
	 */
	protected List<?> handleDocument(String nodeName, Document document)
			throws CheckedApplicationException {
		logger.info("Received Document, starting at Node =  " + nodeName);

		checkForFault(document);

		// Node theNode = document.getElementsByTagName(nodeName).item(0);
		Node theNode = document.getElementsByTagNameNS(
				getServiceParams().getNamespace(), nodeName).item(
				0);
		if (theNode == null) {
			throw new CheckedApplicationException(this.getClass(),
					"Expecting data in node " + nodeName + " Nothing found");
		}
		NodeList theNodes = theNode.getChildNodes();
		return handleNodes(theNodes);
	}

	/**
	 * Converts all the nodes in to the corresponding DomainObjects, this method
	 * has complete control over the returned SOAP response, override this
	 * method if the Soap response does not return a set of domain objects.
	 * Ex: a webservice can return simple Array of Strings.
	 * 
	 */
	protected List<?> handleNodes(NodeList theNodes)
			throws CheckedApplicationException {
		List<DomainObject> l = new ArrayList<DomainObject>();
		Map<String, String> theColumns = new HashMap<String, String>();
		NodeList elements = null;
		Node row = null;
		Node column = null;
		for (int i = 0; i < theNodes.getLength(); i++) {
			theColumns.clear();
			row = theNodes.item(i);
			elements = row.getChildNodes();
			for (int j = 0; j < elements.getLength(); j++) {
				column = elements.item(j);
				if (column.getFirstChild() != null) {
					theColumns.put(column.getNodeName().toUpperCase(), column
							.getFirstChild().getNodeValue());
				} else {
					theColumns.put(column.getNodeName().toUpperCase(), null);
				}

			}
			l.add(map(theColumns));
		}
		return l;
	}

	/**
	 * Method used for creating entire SOAP message
	 * 
	 */
	protected String getMessage(Map<?, ?> parameters) {
		return getHeader() + getBody(parameters) + getFooter();
	}

	/**
	 * The default parser is the Crimson Parser packaged with J2SE 1.4. If you
	 * intend to use a different parser, make sure you look at the JavaDocs of
	 * java.xml.parsers.DocumentBuilderFactory for information on registration.
	 * <br>
	 * <br>
	 * For example:<br>
	 * System.setProperty("javax.xml.parsers.DocumentBuilderFactory","org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
	 * 
	 */
	protected Document getDocument(String rawXML)
			throws CheckedApplicationException {
		logger.info("Received XML: " + rawXML);
		Document theDocument = null;

		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder;

		try {
			docBuilder = docFactory.newDocumentBuilder();
			logger
					.info("Attempting to parse xml document using: DocumentFactory = "
							+ docFactory.toString()
							+ " DocumentBuilder = "
							+ docBuilder.toString());
			theDocument = docBuilder.parse(new ByteArrayInputStream(rawXML
					.getBytes()));

		} catch (ParserConfigurationException e) {
			throw new CheckedApplicationException(this.getClass(),
					"Parser not configured properly", e);

		} catch (SAXException e) {
			throw new CheckedApplicationException(this.getClass(),
					"Unable to process XML via SAX", e);

		} catch (IOException e) {
			throw new CheckedApplicationException(this.getClass(), "Bad I/O "
					+ e, e);
		}
		return theDocument;
	}

	protected String getHeader() {
		StringBuffer xml = new StringBuffer();
		xml.append("<SOAP-ENV:Envelope ");
		xml
				.append("xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" ");
		xml.append("xmlns:xsi=\"http://www.w3.org/1999/XMLSchema-instance\" ");
		xml.append("xmlns:xsd=\"http://www.w3.org/1999/XMLSchema\">");
		return xml.toString();
	}

	protected abstract String getBody(Map<?, ?> Parameters);

	protected String getFooter() {
		StringBuffer xml = new StringBuffer();
		xml.append("</SOAP-ENV:Envelope>");
		return xml.toString();
	}

	/**
	 * Look for a SOAP Error Node and process the document accordingly.
	 */
	protected void checkForFault(Document theDocument)
			throws CheckedApplicationException {
		StringBuffer theError = new StringBuffer();
		NodeList theNodes = theDocument.getElementsByTagName("SOAP-ENV:Fault");
		if (theNodes == null || theNodes.getLength() == 0) {
			theNodes = theDocument.getElementsByTagName("soapenv:Fault");
		}
		if (theNodes != null && theNodes.getLength() > 0) {
			Node theNode = theNodes.item(0);
			NodeList subList = theNode.getChildNodes();
			theError.append(subList.item(0).getFirstChild().getNodeValue()
					+ " has thrown an Exception "
					+ subList.item(0).getFirstChild().getNodeValue() + "("
					+ subList.item(1).getFirstChild().getNodeValue() + ")");

			throw new CheckedApplicationException(this.getClass(), theError
					.toString());
		}
	}
	
	private Map<String, String> httpHeaders = new HashMap<String, String>();  
	public void setHTTPRequestHeaders(Map<String, String> httpHeaders){
		this.httpHeaders = httpHeaders;
	}
}
