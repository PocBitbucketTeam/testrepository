package com.deloitte.common.service;

import java.util.logging.Logger;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * AbstractJMSSender provides a default implementation to send JMS messages to a destination. 
 * Subclasses used to send simple string or XML messages must override convertToXML method.
 * If the subclass wants to send object or Map to a destination, it must override the handleMessage method. 
 * Also JMS connections, producers, session retrieve/release and execution are handled
 * by this class. 
 * 
 */
import com.deloitte.common.objects.framework.CheckedApplicationException;

public abstract class AbstractJMSSender<T> extends AbstractService {

	private static Logger logger = Logger.getLogger(AbstractJMSSender.class
			.getName());

	@Override
	public MessageServiceParams getServiceParams() {
		return (MessageServiceParams) super.getServiceParams();
	}

	protected AbstractJMSSender() {
	}

	public AbstractJMSSender(MessageServiceParams params) {
		setServiceParams(params);
	}

	public void send(T message) throws CheckedApplicationException {
		ConnectionFactory connectionFactory = (ConnectionFactory) JNDIServiceLocator
				.getJNDIObject(getServiceParams().getConnectionFactory(),
						getServiceParams().getContextProperties());
		Destination destination = (Destination) JNDIServiceLocator
				.getJNDIObject(getServiceParams().getDestination(),
						getServiceParams().getContextProperties());
		Connection connection = null;
		MessageProducer producer = null;
		Session session = null;
		try {
			connection = connectionFactory.createConnection();
			session = connection.createSession(false, getServiceParams()
					.getSessionAcknowledgeMode());
			producer = session.createProducer(destination);					
			producer.send(handleMessage(session, message));
		} catch (JMSException e) {
			logger.severe("Error in sending the " + message + " message "
					+ " to the destination ");
			throw new CheckedApplicationException(AbstractJMSSender.class,
					"Error in sending the " + message + " message "
							+ " to the destination ", e);
		} finally {
			if (connection != null) {
				try {
					// closing the Connection closes it's Session and
					// Producer/Consumer objects also
					connection.close();
				} catch (JMSException e) {
					logger.warning("Error in closing the connection.");
				}
			}
		}

	}
	
	protected Message handleMessage(Session session, T obj) throws JMSException { 
		TextMessage msg = session.createTextMessage();	
        msg.setText(convertToXML(obj)); 
        return msg; 
	}

	protected String convertToXML(T message) {
		return message.toString();
	}

}
