package com.deloitte.common.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.deloitte.common.objects.framework.UncheckedApplicationException;

public class JNDIServiceLocator {

	private static Map<String, Object> jndimap = new HashMap<String, Object>();

	public static synchronized Object getJNDIObject(String jndiName,
			Properties contextProps) {
		if (!jndimap.containsKey(jndiName)) {
			Object returnObject = null;
			InitialContext context;
			try {
				context = new InitialContext(contextProps);
				returnObject = context.lookup(jndiName);
			} catch (NamingException e) {
				throw new UncheckedApplicationException(
						JNDIServiceLocator.class,
						"Error in looking up the JNDI name " + jndiName, e);
			}
			jndimap.put(jndiName, returnObject);
			return returnObject;
		} else {
			return jndimap.get(jndiName);
		}

	}

}
