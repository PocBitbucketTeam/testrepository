package com.deloitte.common.service;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.deloitte.common.service.interfaces.Service;
import com.deloitte.common.service.interfaces.ServiceParams;

/**
 * DefaultServiceFactory provides a lookup for Services, with default behavior
 * for AbstractService where it sets the configuration properties for the service to
 * be invoked.
 * <p>
 * Usage:<br>
 * <li>Step 1 -
 * DefaultServiceFactory.getInstance().register(serviceInterface,properties,
 * serviceImpl);
 * <li>Step 2 - serviceInterface service =
 * DefaultServiceFactory.getInstance().createFor(Class<serviceInterface>);
 * 
 * @author Framework Team
 * 
 */
public class DefaultServiceFactory {

	private static final Logger logger = Logger
			.getLogger(DefaultServiceFactory.class.getName());

	private static Map<Object, Object> services = new HashMap<Object, Object>();

	private static DefaultServiceFactory me = new DefaultServiceFactory();

	protected DefaultServiceFactory() {
		// Singleton, only construct from instance();
	}

	/**
	 * Unregister the Service for the corresponding key value
	 */
	public void deRegister(Object key) {
		services.remove(key);
	}

	/**
	 * This is the preferred method for registering Services. Allows the
	 * Services to be registered with a lookup key of the class of the Interface
	 * type which the Service implements containing the actual methods.
	 * 
	 * @param <T>
	 *            The type of the Interface which the Service is used for
	 * @param <E>
	 *            The type of the Service implementation Class
	 * @param key
	 *            The key which will later be used to look up the Service in the
	 *            various factory methods.
	 * @param props
	 *            Initial configuration properties to be injected in to the service.
	 * @param service
	 *            The Service to register
	 */
	public <T, E extends T> void register(Class<T> key,
			ServiceParams props, E service) {
		registerByArtificialKey(key, props, service);
	}

	/**
	 * Allows Services to be registered using any value as the key. Would allow
	 * one to register the Service by something other than the actual Interface
	 * type the Service is used to access. Useful in scenarios where the same
	 * Service must be registered multiple times (such as with different
	 * implementations).
	 * 
	 * @param key
	 *            The key which will later be used to look up the Service in the
	 *            various factory methods.
	 * @param props
	 *            Initial configuration properties to be injected in to the service.
	 * @param service
	 *            The Service to register
	 */
	public <T> void registerByArtificialKey(Object key,
			ServiceParams props, T service) {
		if (service instanceof Service) {
			((Service) service).setServiceParams(props);
		} 		
		services.put(key, service);
	}

	public static DefaultServiceFactory getInstance() {
		return me;
	}

	@SuppressWarnings("unchecked")
	public <T> T createFor(Object key, Class<T> klass) {
		logger.info("Looking for a Service to handle " + key);
		T theService = (T) services.get(key);
		if (theService == null) {
			logger
					.info("Throwing a RuntimeException, No Service was found to handle "
							+ key);
			throw new RuntimeException(
					"No Service was found to handle "
							+ key
							+ " Check to make sure the Service was registered with the DefaultServiceFactory");
		}

		return theService;
	}

	public <T> T createFor(Class<T> key) {
		return createFor(key, key);
	}
}
