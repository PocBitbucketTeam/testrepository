package com.deloitte.common.service;

/**
 * container to store web service description 
 * */
public class WebServiceParams extends GenericServiceParams {

	private String wsdl;
	private String qname;
	private String namespace;

	public WebServiceParams(String wsdl, String qname, String namespace) {
		this.wsdl = wsdl;
		this.qname = qname;
		this.namespace = namespace;
	}

	public String getWsdl() {
		return wsdl;
	}

	public void setWsdl(String wsdl) {
		this.wsdl = wsdl;
	}

	public String getQname() {
		return qname;
	}

	public void setQname(String qname) {
		this.qname = qname;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " wsdl : " + wsdl + " qname : " + qname
				+ " namespace : " + namespace;
	}

}
