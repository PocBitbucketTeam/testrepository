package com.deloitte.common.service;

import java.util.Properties;

import com.deloitte.common.service.interfaces.ServiceParams;

public class GenericServiceParams implements ServiceParams {

	private Properties props;

	public Properties getProps() {
		return props;
	}

	public void setProps(Properties props) {
		this.props = props;
	}

	@Override
	public String toString() {
		String returnStr = null;
		if (props != null) {
			returnStr = props.toString();
		}
		return returnStr;
	}
}
