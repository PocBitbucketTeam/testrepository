package com.deloitte.common.service;

import java.util.Properties;

/**
 * container to store JMS properties
 * */
public class MessageServiceParams extends GenericServiceParams {

	private String connectionFactory;
	private String destination;
	private int sessionAcknowledgeMode;
	private Properties contextProperties;

	public MessageServiceParams() {
		super();
	}

	public MessageServiceParams(String connectionFactory, String destination,
			int sessionAcknowledgeMode) {
		this.connectionFactory = connectionFactory;
		this.destination = destination;
		this.sessionAcknowledgeMode = sessionAcknowledgeMode;
	}

	public String getConnectionFactory() {
		return connectionFactory;
	}

	public void setConnectionFactory(String connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setSessionAcknowledgeMode(int sessionAcknowledgeMode) {
		this.sessionAcknowledgeMode = sessionAcknowledgeMode;
	}

	public int getSessionAcknowledgeMode() {
		return sessionAcknowledgeMode;
	}

	public void setContextProperties(Properties contextProperties) {
		this.contextProperties = contextProperties;
	}

	public Properties getContextProperties() {
		return contextProperties;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " connectionFactory : " + connectionFactory
				+ " destination : " + destination + " contextProperties "
				+ contextProperties;
	}

}
