package com.deloitte.common.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import com.deloitte.common.objects.framework.Error;

/**
 * MessageBundle class is used to retrieve messages from ResourceBundles,
 * as identified by the messageKey property of the Errors passed to it. It
 * will also insert any params passed in the error into the output message. 
 */
public class MessageBundle {
	
	 private final ResourceBundle messages;
	 private Locale locale;
	 
	 /**
	 * This constructor will load the resource bundle for input parameters
	 * resource and locale.
	 * 
	 * @param resource the base name of the resource bundle as would be passed to
	 * 			ResourceBundle.getBundle(String, Locale)
	 * @param currentLocale the locale for which a resource bundle is desired
	 */
	 public MessageBundle(String resource,  Locale  currentLocale){
		 this.locale = currentLocale;
		 this.messages = ResourceBundle.getBundle(resource, locale);
	 }
	 
	 /**
	 * This method is used to get the error messages from the resource bundle and
	 * apply any necessary formatting to the message. The error messages will be 
	 * retrieved from the resource bundle via the key provided by Error.getMessageKey().
	 * Parameters will then be inserted into the message along the format defined by the
	 * java.text.MessageFormat class. This means that parameters passed in the Error can
	 * be inserted into the message by inserted numbered parameters in the message based
	 * on a zero-based index such as:<br>
	 * &nbsp;&nbsp;&nbsp;			<code>"Here is my first parameter {0} and here is my second {1}"</code><br><br>
	 * For full details, see the MessageFormat javadoc. MessageBundle currently supports
	 * only string and numeric parameters.
	 * 
	 * @param theError
	 *            object of Error class
	 * @return the message corresponding to the messageKey property on the error
	 *         with parameters inserted for each '{}' in the message string
	 */
	 public String getMessage(Error theError){
		Object[] params = theError.getParams().toArray(new Object[theError.getParams().size()]);
        MessageFormat messageFormat = new MessageFormat(messages.getString(theError.getMessageKey()), locale);
        return messageFormat.format(params);
	 }

}