package com.deloitte.common.util;

import java.text.ParsePosition;

import com.deloitte.common.interfaces.DomainAttribute;
import com.deloitte.common.objects.business.SocialSecurityNumber;

public class SocialSecurityNumberFormat extends AbstractFormat
{

	private static final long serialVersionUID = 1L;
	
	public SocialSecurityNumberFormat()
	{
		super();
	}
	public SocialSecurityNumberFormat(String format)
	{
		super(format);
	}
	
	public Object parseObject(String source, ParsePosition pos)
	{
		//logger.fine("Received: " + source);
		DomainAttribute ssn = new SocialSecurityNumber(source);
		if (ssn.getValidationErrors().isEmpty())
		{
			pos.setIndex(source.length()-1);
		}
		return ssn;
	}

}
