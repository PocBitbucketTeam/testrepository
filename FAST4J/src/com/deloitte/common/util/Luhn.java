package com.deloitte.common.util;

/**
 * Algorithm for Luhn(Mod10) Checks
 * Step 1: Double the value of alternate digits of the primary account
 * 		   number beginning with the second digit from the right 
 * 		   (the first right--hand digit is the check digit.) 
 *
 * Step 2: Add the individual digits comprising the products obtained
 * 		   in Step 1 to each of the unaffected digits in the original number. 
 *
 * Step 3: The total obtained in Step 2 must be a number ending in zero 
 * 		   (30, 40, 50, etc.) for the account number to be validated.
 *  
 * @author SIDT Framework Team
 *
 */
public class Luhn
{
	/**
	 * addNumber provides a simple way to do the Check Digit routine. When
	 * the position is multipled by 2, if the value is 2 digits they are added
	 * together (eg 12 = 1+2=3). The Array has the 12th position equal to 3, etc.
	 */
	private static final int[] addNumber 	 = { 0, 1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9 };
	private static final int[] getDifference = { 0,9,8,7,6,5,4,3,2,1 };

	/**
	 * getDifference provides a simple way to finish the Check Digit generation.
	 * When you have the final value of all of the positions prior to the check
	 * digit it will be a vaule from 0-9, and you have to determine how much
	 * added to this number = 10, thereby generating the check digit.The array
	 * has in each position, the difference between it and 10. So if the value
	 * of the positions is 4, position 4 in the the array is 6, equalling 10.
	 * together (eg 12 = 1+2=3). The Array has the 12th position equal to 3, etc.
	 */

	public static boolean isValidMod10(String number)
	{
		if (Strings.isEmpty(number))
		{
			return false;
		}
		char[] cArray = Arrays.copyWithout(Arrays.copyWithout(number.toCharArray(),' '),'-');
		if (!Strings.isInteger(Arrays.get(cArray,"")))
		{
			return false;
		}
		
		int length = cArray.length;

		int value = 0;
		boolean doubleIt = false;
		for (int i = length -1; i >= 0; i-=1)
		{
			value += addValue(cArray,i,doubleIt);
			doubleIt = !doubleIt;
		}
		return (value % 10 == 0);
	}



	public static int generateCheckDigit(String number)
	{
		char[] cArray = Arrays.copyWithout(Arrays.copyWithout(number.toCharArray(),' '),'-');

		int value = 0;
		boolean doubleIt = true;
		int length = cArray.length;
		
		for (int i = length -1; i >= 0; i-=1)
		{
			value += addValue(cArray,i,doubleIt);
			doubleIt = !doubleIt;
		}
		while (value > 9)
		{
			value -= 10;
		}
		return getDifference[value];
	}

	private static int addValue(char[] cArray,int position,boolean doubleIt)
	{
		int valueToAdd = 0;
		
		if (doubleIt)
		{
			valueToAdd = addNumber[Strings.getNumber(cArray[position]) * 2];

		} else
		{
			valueToAdd = Strings.getNumber(cArray[position]);
		}

		return valueToAdd;
	
	}

}
