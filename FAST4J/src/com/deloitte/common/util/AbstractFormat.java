package com.deloitte.common.util;

import java.text.FieldPosition;
import java.text.Format;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.DomainAttribute;

public abstract class AbstractFormat extends Format
{
	private static final long serialVersionUID = -7761643442431755031L;

	private static final Logger logger = Logger.getLogger(AbstractFormat.class.getName());

	protected String thisFormat;
	
	public AbstractFormat()
	{
		super();
	}
	
	public AbstractFormat(String format)
	{
		this.thisFormat = format;
	}
	
	public StringBuffer format(Object obj, StringBuffer toAppendTo,
			FieldPosition pos)
	{
		if (thisFormat == null)
		{
			throw new IllegalArgumentException("Unable to format null object");
		}
		logger.info(" Object Type: " + obj.getClass());
		String value = null;
		if (obj instanceof DomainAttribute)
		{
			value = (String)((DomainAttribute)obj).getValue();
		}
		else
		{
			value = obj.toString();
		}
		logger.info("Value Length: "+ value.length()+ " FormatLength: " + thisFormat.length());

		if (thisFormat.length() < value.length())
		{
			throw new IllegalArgumentException("Unable to format object, Value smaller than Format provided");
		}
		int valuePointer = 0;
		for (int i=0; i < thisFormat.length();i++)
		{
			if (value.length() > valuePointer)
			{
			switch(thisFormat.charAt(i))
			{
				case '#': toAppendTo.append(value.charAt(valuePointer++));continue;
				default: toAppendTo.append(thisFormat.charAt(i));
			}
			}
		}
		return toAppendTo;
	}

}
