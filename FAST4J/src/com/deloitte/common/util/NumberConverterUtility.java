package com.deloitte.common.util;

/**
 * Utility Class that is used by the Converters to see if a String can be converted into a respective
 * Numeric Primitive or a wrapper type. The methods in this class depends on XXX.parseXXX(String)method
 * present in each of the wrapper class. Typically these methods ensures that the input is with in the bounds of
 * a particular type. Eg: Byte.parseByte("132") throws a number format exception as the allowed values are only
 * between -128 to 127.
 * 
 * @author SIDT Framework team
 * 
 */



public class NumberConverterUtility 
{

	/**
	 * Determination of whether or not a string can be assigned to a  Double
	 */
	
	public static boolean isDouble(String s)
	{
		try {
				Double.parseDouble(s);
				return true;
		} catch (NumberFormatException nfe)
		{
			
		}
		return false;
	}
	
	/**
	 * Determination of whether or not a string can be assigned to a  Float
	 */
	
	
	public static boolean isFloat(String s)
	{
		try {
				Float.parseFloat(s);
				return true;
		} catch (NumberFormatException nfe)
		{
			
		}
		return false;
	}

	/**
	 * Determination of whether or not a string can be assigned to a  Long
	 */
	
	
	public static boolean isLong(String s)
	{
		try {
				Long.parseLong(s);
				return true;
		} catch (NumberFormatException nfe)
		{
			
		}
		return false;
	}
	
	
	/**
	 * Determination of whether or not a string can be assigned to a  Integer
	 */
	
	public static boolean isInteger(String s)
	{
		try {
				Integer.parseInt(s);
				return true;
		} catch (NumberFormatException nfe)
		{
			
		}
		return false;
	}
	
	/**
	 * Determination of whether or not a string can be assigned to a  Byte
	 */
	
	
	public static boolean isByte(String s)
	{
		try {
				Byte.parseByte(s);
				return true;
		} catch (NumberFormatException nfe)
		{
			
		}
		return false;
	}

	/**
	 * Determination of whether or not a string can be assigned to a  Short
	 */
	
	
	public static boolean isShort(String s)
	{
		try {
				Short.parseShort(s);
				return true;
		} catch (NumberFormatException nfe)
		{
			
		}
		return false;
	}
	
	
	
	
	
	
	
}
