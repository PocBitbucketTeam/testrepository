package com.deloitte.common.util;


/**
 * Convenience class with helper methods associated with
 * processing java Strings.
 */

public class Strings 
{


	/**
	 * Fast implementation of determining if a character is a number. This is
	 * faster than using the Character.isNumber(char) class
	 */
	public static boolean isNumber(char c)
	{
		return c - '0' > -1 && c -'0' < 10;
	}
	
	/**
	 * Fast implementation of determining if a Strings is a integer or decimal. 
	 * Leading sign(+,-) is allowed a one decimal point(.).
	 * 
	 * This is faster than using the Character.isNumber(char) class
	 */
	public static boolean isDecimal(String s)
	{
		boolean theAnswer = !isEmpty(s+"");
		boolean foundDecimalAlready = false;
		char[] theArray = s.toCharArray();
		for (int i=0; i < s.length() && theAnswer; i++)
		{
			if (isNumber(theArray[i]))
			{
				theAnswer = true;
			} else
			{
				theAnswer = false; // Not a number, Now prove otherwise if we should say true
				if (i == 0 && isSign(theArray[i]))
				{
					theAnswer = true;
				}
				
				if (isDecimalPoint(theArray[i]) && !foundDecimalAlready)
				{
					theAnswer = true;
					foundDecimalAlready = true;
				}
			}
		}
		return theAnswer;
	}
	
	/**
	 * Fast implementation of determining if a Strings is a integer.
	 * Leading sign (+,-) is allowed.
	 *  
	 * This is faster than using the Character.isNumber(char) class
	 */
	public static boolean isInteger(String s)
	{
		boolean theAnswer = !isEmpty(s+"");
		char[] theArray = s.toCharArray();
		for (int i=0; i < s.length() && theAnswer; i++)
		{
			if (i==0 && isSign(theArray[i]))
			{
				theAnswer = true;
			} else
			{
				theAnswer = isNumber(theArray[i]);
			}
		}
		return theAnswer;
	}
	/**
	 * Fast implementation of determining if a character is an Integer or Decimal. 
	 * This is faster than using the Character.isNumber(char) class
	 */
/*	public static boolean isNumber(String s)
	{
		return isInteger(s) || isDecimal(s);
	}
*/	/**
	 * Fast implementation of determining converting a character to a integer. This is
	 * faster than using the Character.isNumber(char) class
	 */

	public static int getNumber(char c)
	{
		if (!isNumber(c))
		{
			throw new IllegalArgumentException(c + " is not a valid number");
		} 
		return c - '0';
	}
	
	public static boolean isSign(char c)
	{
		return (int)c == 43 || (int) c == 45;
	}
	
	public static boolean isDecimalPoint(char c)
	{
		return (int) c == 46;
	}
	
	/**
	 * 
	 * Return an array of the Special characters
	 * 
	 */
	
	public static char[] getSpecialCharacters()
	{
		char[] theArray = new char[34];
		int pointer = 0;
		theArray[pointer++] = (int)' ';
		for (int i=33; i < 48; i++)
		{
			theArray[pointer++] = (char)i; 
		}
		for (int i=58; i < 65; i++)
		{
			theArray[pointer++] = (char)i; 
		}
		for (int i=91; i < 97; i++)
		{
			theArray[pointer++] = (char)i; 
		}
		for (int i=123; i < 127; i++)
		{
			theArray[pointer++] = (char)i; 
		}
		return theArray;
	}
	/**
	 * 
	 * Return an array of the Lower Case characters
	 * 
	 */
	
	public static char[] getLowerCaseCharacters()
	{
		char[] theArray = new char[26];
		int pointer = 0;
		for (int i=97; i < 123; i++)
		{
			theArray[pointer++] = (char)i; 
		}		
		return theArray;
	}
	/**
	 * 
	 * Return an array of the Upper Case characters
	 * 
	 */
	public static char[] getUpperCaseCharacters()
	{
		char[] theArray = new char[26];
		int pointer = 0;
		for (int i=65; i < 91; i++)
		{
			theArray[pointer++] = (char)i; 
		}		
		return theArray;
	}
	/**
	 * Determination of whether or not a string is empty
	 */
	public static boolean isEmpty(String s)
	{
		if (s == null || s.length() == 0)
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Soundex provides a way to determine if two words are similiar enough to be considered the same. This
	 * is used in searching for last names. As an example Robert and Rupert will both return the same soundex
	 * code.
	 * @return 4 character soundex in the form A###
	 */
	public static String getSoundex(String input)
	{
		int[] map = { 0,1,2,3,0,1,2,0,0,2,2,4,5,5,0,1,2,6,2,3,0,1,0,2,0,2};
		char[] theArray = input.toLowerCase().toCharArray();
		StringBuffer theAnswer = new StringBuffer(input.substring(0,1).toUpperCase());
		int lastLetterValue = map[theArray[0]- 'a'];
		int thisLetterValue = 0;
		for (int i=1; i < theArray.length; i++)
		{
			thisLetterValue = map[theArray[i] - 'a'];
			if (thisLetterValue > 0 && thisLetterValue != lastLetterValue)
			{
				theAnswer.append(map[theArray[i] - 'a']);
			}
			lastLetterValue = thisLetterValue;
		}
		
		for (int i = 4 - theAnswer.length(); i > 0; i--)
		{
			theAnswer.append('0');	
		}
		return theAnswer.substring(0,4);
		
	}
	
}
