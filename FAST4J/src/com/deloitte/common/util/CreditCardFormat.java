package com.deloitte.common.util;

import java.text.ParsePosition;

import com.deloitte.common.objects.business.CreditCard;

public class CreditCardFormat extends AbstractFormat
{
	
	private static final long serialVersionUID = 1L;


	public CreditCardFormat()
	{
		super();
	}
	public CreditCardFormat(String format)
	{
		super(format);
	}
	
	public Object parseObject(String source, ParsePosition pos)
	{
		if (source == null || !CollectionHelper.isEmpty(new CreditCard(source).getValidationErrors()))
		{
			throw new IllegalArgumentException();
		}
		CreditCard cc = new CreditCard(source);
		pos.setIndex(source.length()-1);
		return cc;
	}

}
