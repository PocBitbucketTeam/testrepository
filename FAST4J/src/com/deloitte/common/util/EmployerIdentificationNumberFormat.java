package com.deloitte.common.util;

import java.text.ParsePosition;

import com.deloitte.common.interfaces.DomainAttribute;
import com.deloitte.common.objects.business.EmployerIdentificationNumber;

public class EmployerIdentificationNumberFormat extends AbstractFormat {

	private static final long serialVersionUID = 1L;

	public EmployerIdentificationNumberFormat() {
		super();
	}

	public EmployerIdentificationNumberFormat(String format) {
		super(format);
	}

	public Object parseObject(String source, ParsePosition pos) {

		DomainAttribute ein = new EmployerIdentificationNumber(source);
		if (ein.getValidationErrors().isEmpty()) {
			pos.setIndex(source.length() - 1);
		}
		return ein;
	}
}