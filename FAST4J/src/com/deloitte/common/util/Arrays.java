package com.deloitte.common.util;

import com.deloitte.common.interfaces.Mapper;



/**
 * Convenience class with helper methods associated with
 * processing java arrays.
 * 
 * @author SIDT Framework Team
 */
public class Arrays 
{
	private static final char[] CA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
			.toCharArray();

	private static final int[] IA = new int[256];

	static {
		java.util.Arrays.fill(IA, -1);
		for (int i = 0, iS = CA.length; i < iS; i++) {
			IA[CA[i]] = i;
		}
		IA['='] = 0;
	}
	
	public final static byte[] base64Encode(byte[] fromBytes) {
		if (fromBytes == null)
		{
			return null;
		}
		int from; 		// index into source (byteData)
		int to; 		// index into destination (byteDest)
		byte toBytes[] = new byte[((fromBytes.length + 2) / 3) * 4];
		for (from = 0, to = 0; from < fromBytes.length - 2; from += 3) 
		{
			toBytes[to++] = (byte) ((fromBytes[from] >>> 2) & 077);
			toBytes[to++] = (byte) ((fromBytes[from + 1] >>> 4) & 017 | (fromBytes[from] << 4) & 077);
			toBytes[to++] = (byte) ((fromBytes[from + 2] >>> 6) & 003 | (fromBytes[from + 1] << 2) & 077);
			toBytes[to++] = (byte)  (fromBytes[from + 2] & 077);
		}

		if (from < fromBytes.length) {
			toBytes[to++] = (byte) ((fromBytes[from] >>> 2) & 077);
			if (from < fromBytes.length - 1) {
				toBytes[to++] = (byte) ((fromBytes[from + 1] >>> 4) & 017 | (fromBytes[from] << 4) & 077);
				toBytes[to++] = (byte) ((fromBytes[from + 1] << 2) & 077);
			} else
				toBytes[to++] = (byte) ((fromBytes[from] << 4) & 077);
		}

		for (from = 0; from < to; from++) 
		{
			if (toBytes[from] < 26)
			{
				toBytes[from] = (byte) (toBytes[from] + 'A');
			}
			else if (toBytes[from] < 52)
			{
				toBytes[from] = (byte) (toBytes[from] + 'a' - 26);
			}
			else if (toBytes[from] < 62)
			{
				toBytes[from] = (byte) (toBytes[from] + '0' - 52);
			}
			else if (toBytes[from] < 63)
			{
				toBytes[from] = '+';
			}
			else
			{
				toBytes[from] = '/';
			}
		}
		while (from < toBytes.length)
		{
			toBytes[from++] = '=';
		}

		return toBytes;
	}
	/**
	 * Decodes a BASE64 encoded byte array.
	 * 
	 * @param fromBytes
	 *            The source array.
	 * @return The decoded array of bytes.
	 */
	public final static byte[] base64Decode(byte[] fromBytes) {
		if (fromBytes == null) {
			return null;
		}
		int sLen = fromBytes.length;
		int sepCnt = 0;
		for (int i = 0; i < sLen; i++) {
			if (IA[fromBytes[i] & 0xff] < 0) {
				sepCnt++;
			}
		}

		// Check so that legal chars (including '=') are evenly divideable by 4
		if ((sLen - sepCnt) % 4 != 0) {
			return null;
		}
		int pad = 0;
		for (int i = sLen; i > 1 && IA[fromBytes[--i] & 0xff] <= 0;) {
			if (fromBytes[i] == '=') {
				pad++;
			}
		}
		int len = ((sLen - sepCnt) * 6 >> 3) - pad;

		byte[] toBytes = new byte[len]; // Preallocate byte[] of exact length

		for (int s = 0, d = 0; d < len;) {
			// Assemble three bytes into an int from four "valid" characters.
			int i = 0;
			for (int j = 0; j < 4; j++) { // j only increased if a valid char
				// was found.
				int c = IA[fromBytes[s++] & 0xff];
				if (c >= 0) {
					i |= c << (18 - j * 6);
				} else {
					j--;
				}
			}

			// Add the bytes
			toBytes[d++] = (byte) (i >> 16);
			if (d < len) {
				toBytes[d++] = (byte) (i >> 8);
				if (d < len) {
					toBytes[d++] = (byte) i;
				}
			}
		}

		return toBytes;
	}

	/**
	 * Return an array with only the characters provided.
	 * <p>
	 * char[] charArray = ":123-45-6789".toCharArray();<br>
	 * char[] justNumbers = Arrays.copyWith(charArray,"0123456789".toCharArray());<br>
	 * Example Output: 123456789
	 * 
	 * @return A character array
	 */
	
	public static char[] copyWith(char[] charArray, char[] keepArray)
	{
		if (charArray == null)
		{
			return new char[0];
		}
		
		char[] returnArray = new char[contains(charArray,keepArray)];
		int index = 0;
		for (int i=0; i < charArray.length; i++)
		{
			
			if (contains(keepArray,charArray[i]) > 0)
			{
				returnArray[index++] = charArray[i];
			}
		}
		return returnArray;	
		
	}
	
	public static char[] copyWith(char[] charArray, char[] keepArray,Mapper theMapper)
	{
		StringBuffer sb = new StringBuffer();

		for (int i=0; i < charArray.length; i++)
		{
			
			if (contains(keepArray,charArray[i]) > 0)
			{
				sb.append(charArray[i]);
			} else
			{
				sb.append(theMapper.getName(char.class, charArray[i]+""));
			}
		}
		
		return sb.toString().toCharArray();
	}

	
	/**
	 * Return an array without the characters provided.
	 * <p>
	 * char[] charArray = ":123-45-6789".toCharArray();<br>
	 * char[] justNumbers = Arrays.copyWithout(charArray,":-".toCharArray());<br>
	 * Example Output: 123456789
	 * 
	 * @return A character array
	 */
	
	public static char[] copyWithout(char[] charArray, char[] removeArray)
	{
		if (charArray == null)
		{
			return new char[0];
		}
		
		char[] returnArray = new char[charArray.length-contains(charArray,removeArray)];
		int index = 0;
		for (int i=0; i < charArray.length; i++)
		{
			if (contains(removeArray,charArray[i]) == 0)
			{
				returnArray[index++] = charArray[i];
			}
		}
		return returnArray;
	}
	
	
	/**
	 * Return an array without the character provided.
	 * <p>
	 * char[] charArray = "123-45-6789".toCharArray();<br>
	 * char[] justNumbers = Arrays.copyWithout(charArray,'-');<br>
	 * Example Output: 123456789
	 * 
	 * @return A character array
	 */
	
	public static char[] copyWithout(char[] charArray, char c)
	{
		if (charArray == null)
		{
			return new char[0];
		}
		
		char[] returnArray = new char[charArray.length-contains(charArray,c)];
		int index = 0;
		for (int i=0; i < charArray.length; i++)
		{
			if (charArray[i] != c)
			{
				returnArray[index++] = charArray[i];
			}
		}
		return returnArray;
	}
	/**
	 * Given a character array, check for occurrences of the character array provided.
	 * <p>
	 * char[] theArray = "Now is the time".toCharArray(); <br>
	 * int howMany = Arrays.contains(theArray,"ie".toCharArray());<br>
	 * Example Output: 4
	 * @return How many occurrences of the character array were found in the array
	 */

	public static int contains(char[] charArray, char[] theValues)
	{
		int found = 0;
		for (int i=0; i < theValues.length; i++)
		{
			found += contains(charArray,theValues[i]);
		}
		return found;
	}
	/**
	 * Given a character array, check for occurrences of the character provided.
	 * <p>
	 * char[] theArray = "Now is the time".toCharArray(); <br>
	 * int howMany = Arrays.contains(theArray,'i');<br>
	 * Example Output: 2
	 * @return How many occurrences of the character were found in the array
	 */
	public static int contains(char[] charArray, char c)
	{
		int found = 0;

		for (int i=0; i < charArray.length; i++)
		{
			if (charArray[i] == c)
			{
				found++;
			}
		}
		return found;
	}
	/**
	 * Given a Object array, check for occurrences of the object or that
	 * can be assigned
	 * <p>
	 */
	
	public static int contains(Object[] objectArray, Class<?> c)
	{
		int found = 0;
		for (int i=0; i < objectArray.length; i++)
		{
			if (c == objectArray[i].getClass() || 
				c.isAssignableFrom(objectArray[i].getClass()))
			{
				found++;
			}
		}
		return found;
	}
	
	/**
	 * Determines if an Object is one of the types provided in the
	 * class array. For instance a DomainObject would return true
	 * if Validation, Identification, Comparable or DomainObject
	 * were contained within the class array.
	 * 
	 */
	public static boolean isOfType(Class<?> c, Class<?>[] classArray)
	{
		boolean theAnswer = false;
		int i=classArray.length-1;
		while (i > -1 && theAnswer == false)
		{
			//logger.fine("Checking " + classArray[i] + " is Assignable from " + c);
			if (classArray[i] == c  || classArray[i].isAssignableFrom(c))
			{
				theAnswer = true;
			}
			i--;
		}
		return theAnswer;
	}
	
	public static String get(String string)
	{
		return get(string.toCharArray());
	}
	/**
	 * <p>
	 * Format the character for rendering or as a value list.
	 * <p>
	 * 	char[] questions = "?????".toCharArray();<br>
	 *  String formatted = Arrays.get(questions);<br>
	 *  Example Output: ?,?,?,?,?
	 * @param array
	 * @return	"char[0],char[1],char[2]..."
	 */
	public static String get(char[] array)
	{
		return get(array,",");
	}

	/**
	 * <p>
	 * Format the character for rendering or as a value list.
	 * <p>
	 * 	char[] questions = "?????".toCharArray();<br>
	 *  String formatted = Arrays.get(questions,"-");<br>
	 *  Example Output: ?-?-?-?-?
	 * @param array
	 * @return	"char[0]-char[1]-char[2]..."
	 */

	public static String get(char[] array, String s)
	{
		Character[] newArray = new Character[array.length];
		for (int i=0; i < array.length; i++)
		{
			newArray[i] = new Character(array[i]);
		}
		return get(newArray,s);
		
	}

	/**
	 * <p>
	 * Format the character for rendering or as a value list.
	 * <p>
	 * 	Object[] objects = {new BigInteger(1),new BigInteger(2),new BigInteger(3)};<br>
	 *  String formatted = Arrays.get(objects);<br>
	 *  Example Output: 1,2,3
	 * @param array
	 * @return	"object[0].toString(),char[1].toString(),char[2].toString()..."
	 */

	public static String get(Object[] array)
	{
		return get(array,",");
	}

	/**
	 * <p>
	 * Format the character for rendering or as a value list.
	 * <p>
	 * 	Object[] objects = {'?','$','#'};<br>
	 *  String formatted = Arrays.get(objects,"-");<br>
	 *  Example Output : ?-$-#
	 * @param array
	 * @return	"object[0].toString()-char[1].toString()-char[2].toString()..."
	 */
	
	public static String get(Object[] array, String s)
	{
		StringBuffer sb = new StringBuffer();
		String seperator = "";
		for (int i=0; i < array.length; i++)
		{	
			if (array[i] != null)
			{
				sb.append(seperator + array[i]);
				seperator = s;
			}
		}
		return sb.toString();
	}
	/**
	 * <p>
	 * Duplicates the character for rendering or as a value list.
	 * <p>
	 *  String duplicates = Arrays.duplicate(5,'?');<br>
	 *  Example Output: ?????
	 * @return	"char[0]char[1]char[2]..."
	 */
	
	public static String duplicate(int count, char c)
	{
		StringBuffer sb = new StringBuffer();
		for (int i=0; i < count; i++)
		{
			sb.append(c);
		}
		return sb.toString();
	}
	
	
	public static int least(int[] array)
	{
		int theAnswer = 0;
		if (array != null && array.length > 0)
		{
			for (int i=0; i < array.length; i++)
			{
				if (i==0)
				{
					theAnswer = array[i];
				} else
				{
					if (array[i] < theAnswer)
					{
						theAnswer = array[i];
					}
				}
			}
		}
		return theAnswer;
	}	
}
