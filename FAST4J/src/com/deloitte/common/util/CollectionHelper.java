package com.deloitte.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.collection.ColumnComparator;

/**
 * Convenience class with helper methods associated with
 * processing java Collections.
 */
public class CollectionHelper
{

	/**
	 * Return a Map that returns only the values that contain the prefix. It also
	 * removes the prefix from the values prior to return.
	 * <p>
	 * 		Map map = new HashMap();<br>
	 * 		map.put("table1_column1");<br>
	 * 		map.put("table2_columnA");<br>
	 * 		<br><br>
	 * 		Map subMap = CollectionHelper.subMap(map,table2);<br>
	 * 		Example Output: columnA<br>
	 */
	public static <V> Map<String, V> subMap(Map<String, V> map, String prefix) 
	{
		Map<String, V> theMap = new HashMap<String, V>();
		Set<String> keys = map.keySet();
		
		String newKey = null;
		for (String key : keys)
		{
			if (key.startsWith(prefix))
			{
				newKey = key.substring(prefix.length());
				theMap.put(newKey,map.get(key));
			}
		}
		return theMap;
	}
	
	/**
	 * 	Supplies verbose = false
	 * @see #format(Map, boolean)
	 */
	public static String format(Map<?,?> map)
	{
		return format(map,false);
	}
	
	/**
	 * 	Supplies verbose = true
	 * @see #format(Map,boolean)
	 */
	public static String formatVerbose(Map<?,?> map)
	{
		return format(map,true);
	}
	
	public static String format(Map<?,?> map, boolean verbose)
	{
		return format(map,verbose,", ");
	}
	
	/**
	 * Returns a String representation of the elements in the map, formatted nicely.
	 * It either returns verbosely, which prints the getClass() values as well or not.
	 * <br><br>
	 * Map map = new Hashmap();<br>
	 * map.put("Key1","String1");<br>
	 * map.put("Key2","String2");<br>
	 * String formatted = CollectionHelper.format(map,false);<br>
	 * Example Output: [key1=String1,key2=String2]<br>
	 * formatted = CollectionHelper.format(map,true);<br>
	 * Example Output: [key1=java.lang.String String1, key2=java.lang.String String2]<br>
	 */
	public static String format(Map<?,?> map, boolean verbose,String seperator)
	{
		String theSeperator = "";
		StringBuffer sb = new StringBuffer("[");
		Set<?> keys = map.keySet();
		Object key = null;
		for (Iterator<?> i=keys.iterator();i.hasNext();)
		{
			key = i.next();
			if (verbose && map.get(key) != null)
			{
				sb.append(theSeperator + key + "=" + map.get(key).getClass().getName() + " "+ map.get(key));
			} else
			{
				sb.append(theSeperator + key + "=" + map.get(key));
			}
			theSeperator = seperator;
		}
		return sb.toString() + "]";
	}

	/**
	 * 	Supplies verbose = false
	 * @see #format(List,boolean)
	 */
	public static String format(List<?> list)
	{
		return format(list,false);
	}
	
	/**
	 * 	Supplies verbose = true
	 * @see #format(List,boolean)
	 */
	public static String formatVerbose(List<?> list)
	{
		return format(list,true);
	}
	
	/**
	 * Returns a String representation of the elements in the list, formatted nicely.
	 * It either returns verbosely, which prints the getClass() values as well or not.
	 * <br><br>
	 * List list = new ArrayList();<br>
	 * list.add("String1");<br>
	 * list.add("String2");<br>
	 * String formatted = CollectionHelper.format(list,false);<br>
	 * Example Output: [String1,String2]<br>
	 * formatted = CollectionHelper.format(list,true);<br>
	 * Example Output: [java.lang.String String1, java.lang.String String2]<br>
	 */
	public static String format(List<?> list, boolean verbose)
	{
		StringBuffer sb = new StringBuffer("[");
		String seperator = "";
		if (list != null)
		{
			for (Object value: list )
			{
				if (verbose)
				{
					sb.append(seperator + value.getClass().getName() + " " + value);
				} else {
					sb.append(seperator + value);
				}
				seperator = ", ";
			}
		}
		sb.append("]");

		return sb.toString();
	}

	/**
	 * Sorts a List of Lists, essentially a 2way table by the column. Useful
	 * for on-screen displays of data with a Sort by column that can be changed
	 */
	public static <T extends List<? extends Comparable<?>>> void sortTable(List<T> list, int column)
	{
		Collections.sort(list, new ColumnComparator<T>(column));
	}

	
	public static boolean isEmpty(Collection<?> c)
	{
		if (c == null || c.size() == 0)
		{
			return true;
		}
		return false;
	}
	
	public  static <T> List<T> toList(T[] objectArray)
	{
		List<T> theList = new ArrayList<T>();
		for (int i=0; i < objectArray.length; i++)
		{
			if (objectArray[i] != null)
			{
				theList.add(objectArray[i]);
			}
		}
		return theList;
	}
	
	/**
	 * Used to generate a subset collection based on the application of a filter
	 * @param collection the collection to be filtered
	 * @param filter the filter to apply
	 * @return a collection containing only the elements of collection that met the filter
	 * criterion
	 */
	public static <T> Collection<T> filter(Collection<T> collection, Filter<T> filter) {
		Collection<T> result = new ArrayList<T>();
		for (T obj:collection) {
			if (filter.include(obj)) {
				result.add(obj);
			}
		}
		return result;
	}

}
