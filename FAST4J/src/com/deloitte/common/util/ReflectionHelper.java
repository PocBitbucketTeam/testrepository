package com.deloitte.common.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Convenience class with helper methods associated with
 * using java reflection.
 */

public class ReflectionHelper 
{
	private static final Logger logger = Logger.getLogger(ReflectionHelper.class.getName());

	public static final Class<?>[] EMPTY_ARRAY = new Class[0];
	public static final Object[] EMPTY_OBJ_ARRAY = new Object[0];	
	
	public static Class<?> createFor(String className)
	{
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) 
		{
			throw new RuntimeException("Class " + className + " can not be located");
		}
	}
	/**
	 * Factory Method used to create objects, given a String class name, and the
	 * constructor signature. Useful for dynamically creating classes.
	 * <br><br>
	 * Example: <br>
	 *   Class[] array = { "" };<br>
	 *   String stringObject = (String) ReflectionHelper.createFor("java.lang.String",array);
	 */	
	public static Object createFor(String className, Object[] objectArray) {
		Class<?> theClass = null;
		try {
			theClass = Class.forName(className);		
		} catch (ClassNotFoundException cnfe) {
			throw new RuntimeException("Class " + className + " can not be located");
		} 
		return createFor(theClass, objectArray); 
	}
	
	/**
	 * Factory Method used to create objects, given a class object, and the
	 * constructor signature. Useful for dynamically creating classes.
	 * <br><br>
	 * Example: <br>
	 *   Class[] array = { "" };<br>
	 *   String stringObject = (String) ReflectionHelper.createFor(String.class,array);
	 */	
	@SuppressWarnings("unchecked")
	public static <T> T createFor(Class<T> klass, Object[] objectArray) {
		Class<?>[] classArray = null;
		T o = null;
		if (objectArray != null) {
			classArray = new Class[objectArray.length];
			for (int i = 0; i < objectArray.length; i++) {
				classArray[i] = objectArray[i].getClass();
			}
		}
		Constructor<T> c = null;
		try {
			c = getConstructor(klass, classArray);
			if (c != null) {
				o = c.newInstance(objectArray);
			} else {
				throw new RuntimeException("Constructor for  "
						+ klass.getName()
						+ " can not be found or is not accessible");
			}
		} catch (NoSuchMethodException e) {
			// Let's try the Factory Method
			logger.info("An Exception was encountered, trying Factory for "
					+ klass.getName());
			try {
				Method m = klass.getMethod("getInstance", classArray);
				if (klass != null) {
					o = (T)m.invoke(null, objectArray);
				}
			} catch (NoSuchMethodException nsme) {
				throw new RuntimeException("Constructor: " + klass.getName()
						+ "(" + Arrays.get(classArray)
						+ ") can not be found or is not accessible");
			} catch (InvocationTargetException ite) {
				throw new RuntimeException(
						"Unable to create object(InvocationTargetException) with constructor "
								+ classArray + " and class name "
								+ klass.getName());
			} catch (IllegalAccessException iae) {
				throw new RuntimeException(
						"Unable to create object(IllegalAccessException) with constructor "
								+ klass.getName() + "("
								+ Arrays.get(classArray) + ")");
			}
		} catch (InstantiationException ie) {
			throw new RuntimeException(
					"Unable to create object(InstantiationException) with constructor "
							+ classArray + " and class name " + klass.getName()
							+ " " + ie);
		} catch (InvocationTargetException ite) {
			throw new RuntimeException(
					"Unable to create object(InvocationTargetException) with constructor "
							+ classArray + " and class name " + klass.getName());
		} catch (IllegalAccessException iae) {
			throw new RuntimeException(
					"Unable to create object(IllegalAccessException) with constructor "
							+ klass.getName() + "(" + Arrays.get(classArray)
							+ ")");
		}

		return o; 
	}
	/**
	 * Given a Class, and Constructor signature(Class[]), the class will return the Constructor that
	 * matches. It does a "fuzzy" match in that if the signature has a type that is assignable to the
	 * constructor, it will match and return if the Constructor is Public.<br><br>
	 * 
	 * Example:<br>
	 *  Class[] array = { String.class };<br> 
	 * 	ReflectionHelper.getConstructor(String.class, array);
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static <T> Constructor<T> getConstructor(Class<T> theClass, Class<?>[] constructorArray) throws NoSuchMethodException
	{
		@SuppressWarnings("rawtypes")
		Constructor[] constructors = theClass.getDeclaredConstructors();
		Class<?>[] parameters = null;
		for (int i=0; i < constructors.length; i++)
		{
			if (constructors[i].getParameterTypes().length != constructorArray.length)
			{
				continue;
				
			} else
			{
				parameters = constructors[i].getParameterTypes();
			}
			
			if (parameters != null)
			{
				boolean isNotValidConstuctor = false;
				for (int j=0; j < parameters.length; j++)
				{
					Class<?> klass = parameters[j];
					if (!klass.isAssignableFrom(constructorArray[j]))
					{
						isNotValidConstuctor = true;
						break;
					}
				}
				if (isNotValidConstuctor) {
					continue;
				}
			}
			if (constructors[i].getModifiers() == Modifier.PUBLIC)
			{
				return constructors[i];
			} 
			{
				throw new NoSuchMethodException("Constructor is not Public ");
			}
			
		}
		throw new NoSuchMethodException("Constructor not found ");
	}
	
	/**
	 * Return List of Methods that are considered "accessors" aka "setters". 
	 */
	public static List<Method> getAccessorMethods(Class<?> c)
	{
		List<Method> list = new ArrayList<Method>();
		Method[] methods = c.getMethods();
		for (int i=0; i < methods.length; i++){
			if (isAccessor(methods[i])){
				list.add(methods[i]);
			}
		}
		return list;
	}
	
	/**
	 * Return List of Methods that are considered "mutators" aka "setters". 
	 */
	public static List<Method> getMutatorMethods(Class<?> c)
	{
		List<Method> list = new ArrayList<Method>();
		Method[] methods = c.getMethods();
		for (int i=0; i < methods.length; i++)
		{
			if (isMutator(methods[i]))
			{
				list.add(methods[i]);
			}
		}
		return list;
	}
	
	/**
	 * Return List of Methods that are considered "accessors" aka "setters". 
	 */
	public static List<Method> getAccessorMethods(Object o)
	{
		List<Method> list = new ArrayList<Method>();
		if (o != null)
		{
			list.addAll(getAccessorMethods(o.getClass()));
		}
		return list;
	}
	
	/**
	 * Return List of Methods that are considered "mutators" aka "setters". 
	 */
	
	public static List<Method> getMutatorMethods(Object o)
	{
		List<Method> list = new ArrayList<Method>();
		if (o != null)
		{
			list.addAll(getMutatorMethods(o.getClass()));
		}
		return list;
	}


	/**
	 * Scan a Class and return any method that is an Accessor as well as 
	 * implements one of the interfaces provided in the Class[] parameter
	 * <br><br>
	 * This is useful if for instance you wanted to retrieve a list of methods
	 * that implemented the Validation Interface (eg. DomainObject/Attribute)
	 * so you could invoke a common method.
	 */
	public static List<Method> getAccessorMethodsOfType(Object o,Class<?>[] classArray)
	{
		List<Method> list = new ArrayList<Method>();

		Method[] methods = o.getClass().getMethods();
		
		for (int i=0; i < methods.length; i++)
		{
			if (isAccessor(methods[i]) && 
				Arrays.isOfType(methods[i].getReturnType(),classArray))
			{
				list.add(methods[i]);
			}
		}
		return list;
	}
	/**
	 * This method will allow you to find a given no parm method on an object, this can be coupled with
	 * the bean Specification to find accessors.
	 * <br><br>
	 *	Example:<br>
	 *	Method m = ReflectionHelper.getAccessorMethod("","toString");
	 */
	public static Method getAccessorMethod(Object o, String aMethod) throws CheckedApplicationException 
	{
		
		Method theMethod = null;
		if (o != null)
		{
			theMethod = getAccessorMethod(o.getClass(),aMethod);
		} 
		return theMethod;
	}
	/**
	 * This method will allow you to find a given no parm method on an object, this can be coupled with
	 * the bean Specification to find accessors.
	 * <br><br>
	 *	Example:<br>
	 *	Method m = ReflectionHelper.getAccessorMethod("","toString");
	 */
	public static Method getAccessorMethod(Class<?> c, String aMethod) throws CheckedApplicationException 
	{
		Method theMethod = null;
		Method[] methods = c.getMethods();
		for (int i=0; i < methods.length; i++)
		{
			if ((methods[i].getName().equalsIgnoreCase(aMethod)||
				methods[i].getName().equalsIgnoreCase("get" +aMethod))||methods[i].getName().equalsIgnoreCase("is" +aMethod) &&
				methods[i].getParameterTypes().length == 0)
			{
				try 
				{
					theMethod = c.getMethod(methods[i].getName(),EMPTY_ARRAY);
				} catch (SecurityException e) 
				{
					throw new CheckedApplicationException(ReflectionHelper.class,"Unable to find or access that Method",e);
				} catch (NoSuchMethodException e) 
				{
					throw new CheckedApplicationException(ReflectionHelper.class,"Unable to find or access that Method",e);
				}
			}
		}
		return theMethod;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T invokeAccessorMethod(Object theObject, Method m,Object[] parameters)throws CheckedApplicationException
	{
		try 
		{
			return (T)m.invoke(theObject,parameters);
		} catch (IllegalArgumentException e) 
		{
			throw new CheckedApplicationException(ReflectionHelper.class,"Unable to invoke Accessor("+ m + ") on (" + theObject + ") with  Parameters(" + parameters + ")",e);
		} catch (IllegalAccessException e) 
		{
			throw new CheckedApplicationException(ReflectionHelper.class,"Unable to invoke Accessor",e);
		} catch (InvocationTargetException e) 
		{
			throw new CheckedApplicationException(ReflectionHelper.class,"Unable to invoke Accessor",e);
		}
		
	}
	




	/**
	 * Returns a specific method name that takes a single Object as its parameter<br><br>
	 * Example:<br>
	 * Method m = getMutatorMethod("","compareTo")
	 */
	public static Method getMutatorMethod(Object o, String aMethod) 
		   throws CheckedApplicationException
	{
		Method theMethod = null;
		if (o != null)
		{
			Method[] methods = o.getClass().getMethods();
		for (int i=0; i < methods.length && theMethod == null; i++)
			{
				if (methods[i].getName().equalsIgnoreCase(aMethod) ||
					methods[i].getName().equalsIgnoreCase("set" +aMethod) &&
					methods[i].getParameterTypes().length == 1)
				{
					theMethod = methods[i];
				}
			}
		} 
		return theMethod;
	}

	/**
	 * Given an Object, Method and Parameter, invoke.
	 */
	
	public static void invokeMutatorMethod(Object o, Method m, Object parameter) throws CheckedApplicationException
	{
		Object[] array = { parameter };
		try 
		{ 
			if (m != null && o != null)
			{
				m.invoke(o,array);
			} else
			{
				throw new CheckedApplicationException(ReflectionHelper.class,"Null Method("+ (m == null)+") or Object("+ (o == null)+ ") can not be invoked");
			}
		} catch (InvocationTargetException e)
		{
			throw new CheckedApplicationException(ReflectionHelper.class,"Unable to invoke Mutator",e);
		} catch (IllegalAccessException e)
		{
			throw new CheckedApplicationException(ReflectionHelper.class,"Unable to invoke Mutator",e);
		}
	}

	/**
	 * Method allows you to get the return value of a method invoked via a nested call.
	 * <br><br>
	 * Example:<br>
	 * String value = ReflectionHelper.invokeNestedAccessorMethods("","class.name");<br>
	 * Value returned would be "java.lang.String".
	 */
	public static String invokeNestedAccessorMethods(Object o, String theGraph)
	{
		return invokeNestedAccessorMethods(o,getNestedAccessorMethods(o,theGraph));
	}

	/**
	 * Method allows you to get the return value of a method invoked via a nested call of methods provided.
	 * Typically this would be coupled with the other invokeNestedAccessorMethods call, since you must
	 * have the method in hand first.
	 */
	public static String invokeNestedAccessorMethods(Object o, List<Method> theMethods)
	{
		String theResult = "";
		Object theObject = o;
		boolean canContinue = true;
		for (Method theMethod:theMethods){
			if(!canContinue){
				break;
			}
			try {
					if (theMethod != null)
					{
						theObject = theMethod.invoke(theObject,EMPTY_OBJ_ARRAY);
					} else
					{
						canContinue = false;
					}
				} catch (InvocationTargetException ite)
				{
					canContinue = false;
				} catch (IllegalAccessException iae)
				{
					canContinue = false;
				}
		}
		if (canContinue && theObject != null)
		{
			theResult = theObject.toString();
		}
		return theResult;
	}
	/**
	 * Method allows you to get the return value of a method invoked via a nested call. The
	 * nested calls are expected to have "get" as the prefix.
	 * <br><br>
	 * Example:<br>
	 * List  values = ReflectionHelper.getNestedAccessorMethods("","class.name");<br>
	 * Values returned would be method getClass() and method getName() in the list.
	 */
	
	public static List<Method> getNestedAccessorMethods(Object o,String theMethods)
	{
		Object theObject = o;
		StringTokenizer stt = new StringTokenizer(theMethods,".");
		List<Method> methods = new ArrayList<Method>();
		String m = null;
		Method theMethod = null;
		while (stt.hasMoreElements())
		{
			m = (String)stt.nextElement();
			if (!m.startsWith("get")&&!m.startsWith("is"))
			{
				m = "get" + m.substring(0,1).toUpperCase() + m.substring(1);
			}
			try 
			{
				boolean isGet=true;
				try
				{
				theMethod = getAccessorMethod(theObject,m);
				}
				catch(CheckedApplicationException cae)
				{
				isGet=false;
				if(cae.getCause().getClass().isAssignableFrom(NoSuchMethodException.class))
				{
				m = "is" + m.substring(3);
				}
				}
				
			// At this stage we know if there is an Accessor starting with a get .. or else we try with is 	
				if(!isGet)
				{
				theMethod = getAccessorMethod(theObject,m);
				}
				if (theMethod != null)
				{
					methods.add(getAccessorMethod(theObject,m));
					theObject = invokeAccessorMethod(theObject,getAccessorMethod(theObject,m),EMPTY_ARRAY);
				}
			} catch (CheckedApplicationException e)
			{
				throw new RuntimeException(e);
			}
		}
		return methods;
	}
	/**
	 * Makes up for a subliety of java's reflection implementation that does not walk up the inheritance
	 * tree when calling getInterfaces(). This often hides the fact that a subclass can stand in for an
	 * interface implemented by a superclass.
	 */
	public static Class<?>[] getDeclaredInterfaces(Class<?> c)
	{
		Class<?> theClass = c;
		List<Class<?>> allInterfaces = new ArrayList<Class<?>>();
		if (theClass != null)
		{
			for ( Class<?> top = Object.class; !theClass.equals(top); )
			{
				Class<?>[] interfaces = theClass.getInterfaces();
				for (int i=0; i < interfaces.length; i++)
				{
					if (!allInterfaces.contains(interfaces[i]))
					{
						allInterfaces.add(interfaces[i]);
					}
				}
				theClass = theClass.getSuperclass();
			}
		}
		return (Class[])allInterfaces.toArray(EMPTY_ARRAY);
	}
	
	public static boolean isAccessor(Method method)
	{
		if ((method.getName().toLowerCase().startsWith("get")||method.getName().toLowerCase().startsWith("is")) &&
			method.getParameterTypes().length == 0)
			{
					return true;
			}
		return false;
	
	}
	
	public static boolean isMutator(Method method)
	{
		if (
			method.getName().toLowerCase().startsWith("set") &&
			method.getParameterTypes().length == 1 &&
			method.getReturnType() == void.class
			)
			{
					return true;
			}
		return false;	
	}
	
	/**
	 * Method allows a quick way to determine if the parameters can 
	 * be un/boxed. Using Java wrappers and primitives
	 */
	public static  boolean canConvert(Class<?> theClass,Class<?> primitive)
	{
		boolean theAnswer = false;
		if (
		   (Integer.class  == theClass || Long.class    == theClass ||
			Float.class    == theClass || Byte.class    == theClass ||
			Short.class    == theClass || Double.class  == theClass ||
			Boolean.class  == theClass|| Character.class==theClass)
			&&
			(int.class     == primitive || long.class   == primitive ||
			 float.class   == primitive || byte.class   == primitive ||
			 short.class   == primitive || double.class == primitive ||
			 boolean.class == primitive || char.class==primitive)
			 )
		{
			theAnswer = true;
		}
		return theAnswer;
	}

}
