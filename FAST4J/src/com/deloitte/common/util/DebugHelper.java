package com.deloitte.common.util;

/**
 * DebugHelper is a ThreadLocal list of items. It allows a static call
 * to the method receiveEvent(String m), which is unique to a running 
 * Thread. This is helpful in attempting to isolate one thread execution
 * and dumping out all of the events together instead of interspersed in a
 * general log
 * <br><br>
 * Usage<br>
 *  DebugHelper.logLocal("Entering Method");<br>
 *  DebugHelper.logLocal(Exiting Method");<br>
 *  System.err.println(CollectionHelper.toList(DebugHelper.getLocalLog()));<br><br>
 *  Output: [2006/03/13 11:45:09.982-Duration(0ms)-Entering Method, 2006/03/13 11:45:10.042-Duration(60ms)-Exiting Method]
 * @author SIDT Framework Team
 * 
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DebugHelper
{
  private static class ThreadLocalList extends ThreadLocal<InnerCollector> 
  {
    public InnerCollector initialValue() 
    {
      return new InnerCollector();
    }

    public InnerCollector getCollector() 
    { 
      return (InnerCollector) super.get(); 
    }

  }
  
  private static class InnerCollector
  {
	  List<String> list = new ArrayList<String>();
	  long startTime = System.currentTimeMillis();
  }

  private static ThreadLocalList events = new ThreadLocalList();
  private static String[] theArray = new String[0];
  private static String dateFormat = "yyyy/MM/dd HH:mm:ss.SSS";

  public void clear() 
  {
	  events.getCollector().startTime = System.currentTimeMillis();
	  events.getCollector().list.clear();
  }

  public static void setFormat(String format)
  {
	  dateFormat = format;
  }
  
  public static void logLocal(String text) 
  {
	Date now = new Date();
	DateFormat fmt = new SimpleDateFormat(dateFormat);
	events.getCollector().list.add( fmt.format(new Date()) + "-Duration(" + (now.getTime()-events.getCollector().startTime) + "ms)-" + text);
  }

  public static String[]  getLocalLog() 
  {
    return (String[])events.getCollector().list.toArray(theArray);
  }
}