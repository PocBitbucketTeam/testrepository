package com.deloitte.common.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.Command;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * The CommandManager will perform either, a single Command or a CommandList of
 * commands. Assuming an Exception is not thrown, it will continue to execute
 * commands and add their results to the Collection to be returned.
 * 
 * @author SIDT Framework Team
 */
public class CommandManager {

	private static final Logger logger = Logger.getLogger(CommandManager.class.getName());

	public Collection<?> perform(Command theCommand) throws CheckedApplicationException {
		logger.info("About to perform a Command " + theCommand.getClass());
		return theCommand.execute();
	}

	public  Collection<?> perform(CommandList theCommands) throws CheckedApplicationException {
		List<Object> results = new ArrayList<Object>();
		for(Command aCommand: theCommands.get()){
			results.addAll(perform(aCommand));
		}
		return results;
	}
}
