package com.deloitte.common.commands.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.Command;
import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.persistence.DefaultDAOFactory;
import com.deloitte.common.persistence.interfaces.DataAccessObject;
/**
 * PersistObjectAbstractCommand provides the base Persistence Command
 * and is used to Creating, Update and Deleting DomainObjects.
 * <p>
 * Implementation subclasses must implement two methods
 * <p>
 *	protected abstract void executeDAO(DataAccessObject theDAO) throws CheckedApplicationException;
 *
 *		<li>Example:
 *			<li><li>theDAO.add(theObject);
 * <p>
 *	protected abstract Intent getIntent();
 *
 *		Example:
 *
 *			return ValidationIntent.INSERT;
 *	@author SIDT Framework Team 
 */

public abstract class PersistObjectAbstractCommand<T extends DomainObject> implements Command
{
	private static final Logger logger = Logger.getLogger(PersistObjectAbstractCommand.class.getName());
	
	protected T theObject;
	private DataAccessObject<T> dao;
	private Object aDAOKey;
	
	PersistObjectAbstractCommand(T anObject,Object aDAOKey)
	{
		this(anObject);
		this.aDAOKey = aDAOKey;
	}
	
	PersistObjectAbstractCommand(T anObject)
	{
		if (anObject == null){
			throw new UncheckedApplicationException(this.getClass(), "Cannot persist a null object");
		}
		logger.info("Creating the command to persist an Object of type " + anObject.getClass());	
		theObject 	= anObject;
	}

	/*
	 * Implement the Command Interface method. 
	 * 
	 * This method verifies the object is not null and is validated prior
	 * to getting the DAO and delegating the DAO's method to the subclass. 
	 */

	public final Collection<?> execute() throws CheckedApplicationException {
		List<Object> theErrors = new ArrayList<Object>();
		logger.info("Executing the command to persist an Object of type " + theObject.getClass());	
		theErrors.addAll(prepare());
		if (theErrors.size() == 0)
		{
			theErrors.addAll(theObject.getValidationErrors(getIntent()));
		}
		if (theErrors.size() == 0)
		{
			executeDAO();
		}
		
		return theErrors;
	}

	public Collection<Error> prepare() throws CheckedApplicationException{
		return Collections.emptyList();
	}
	
	/**
	 * If no DAO is supplied by the final implementation, try to do 
	 * a lookup using the Object and it's class as the keys.
	 */
	@SuppressWarnings("unchecked")
	protected DataAccessObject<T> getDAO()
	{
		if (dao == null)
		{
			Class<T> klass = (Class<T>)theObject.getClass();
			if (aDAOKey != null)
			{
				dao = DefaultDAOFactory.getInstance().createFor(aDAOKey, klass);
			} else
			{
				dao = DefaultDAOFactory.getInstance().createFor(klass);
				//TODO is this condition still necessary? I'd like to get rid of this
				if (dao == null)
				{
					dao = DefaultDAOFactory.getInstance().createFor(theObject, klass);
				}
			}
		}
		return dao;
	}
	
	protected abstract void executeDAO() throws CheckedApplicationException;
	
	protected abstract Intent getIntent();

	public String toString()
	{
		return getIntent() + "ObjectCommand, Target = " + theObject;
	}

}
