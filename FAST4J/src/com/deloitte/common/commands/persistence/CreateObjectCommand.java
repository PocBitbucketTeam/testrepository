package com.deloitte.common.commands.persistence;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.ValidationIntent;

/**
 * CreateObjectCommand accepts a DomainObject for <b>Creation</b>.
 * <p>
 * The getDAO(DomainObject theObject) method may be overriden to use a  
 * different strategy for returning the DAO based on the DomainObject passed in.
 *  
 * @author SIDT Framework Team
 */

public class CreateObjectCommand<T extends DomainObject> extends PersistObjectAbstractCommand<T> {
	
	public CreateObjectCommand(final T anObject, final Object aDAOKey) 
	{
		super(anObject, aDAOKey);
	}

	public CreateObjectCommand(final T anObject)
	{
		super(anObject);
	}
	
	protected final void executeDAO() throws CheckedApplicationException
	{
		getDAO().add(theObject);
	}
	
	protected final Intent getIntent()
	{
		return ValidationIntent.INSERT;
	}
}

