package com.deloitte.common.commands.persistence;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.objects.framework.ValidationIntent;

/**
 * UpsetObjectCommand accepts a DomainObject for <b>Updating or inserting</b>.
 * <p>
 * The getDAO(DomainObject theObject) method may be overridden to use a
 * different strategy for returning the DAO based on the DomainObject passed in.
 * 
 * @author Fast4J Framework Team
 */

public class UpsertObjectCommand<T extends DomainObject> extends
		PersistObjectAbstractCommand<T> {
	private boolean isUpdatable;

	public UpsertObjectCommand(final T anObject, final Object aDAOKey) {
		super(anObject, aDAOKey);
	}

	public UpsertObjectCommand(final T anObject) {
		super(anObject);
	}

	protected final void executeDAO() throws CheckedApplicationException {
		if (isUpdatable) {
			getDAO().update(theObject);
		} else {
			getDAO().add(theObject);
		}
	}

	protected final Intent getIntent() {
		Map<String, UUID> parameters = new HashMap<String, UUID>();
		parameters.put("ID", theObject.getID());
		try {
			Collection<T> domainObjects = getDAO().getAll(parameters);
			if (domainObjects.size() > 0) {
				// if we have some record/s, update one/All.
				isUpdatable = true;
				return ValidationIntent.UPDATE;
			}
		} catch (CheckedApplicationException e) {
			throw new UncheckedApplicationException(this.getClass(), e);
		}
		// otherwise use Insert.
		return ValidationIntent.INSERT;
	}

}
