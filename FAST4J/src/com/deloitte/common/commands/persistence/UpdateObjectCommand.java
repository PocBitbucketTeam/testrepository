package com.deloitte.common.commands.persistence;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Intent;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.ValidationIntent;
/**
 * UpdateObjectCommand accepts a DomainObject for <b>Updating</b>.
 * <p>
 * The getDAO(DomainObject theObject) method may be overriden to use a  
 * different strategy for returning the DAO based on the DomainObject passed in.
 * @author SIDT Framework Team
 */

public class UpdateObjectCommand<T extends DomainObject> extends PersistObjectAbstractCommand<T> 
{
	public UpdateObjectCommand(final T anObject, final Object aDAOKey) 
	{
		super(anObject, aDAOKey);
	}

	public UpdateObjectCommand(final T anObject)
	{
		super(anObject);
	}
	
	protected final void executeDAO() throws CheckedApplicationException
	{
		getDAO().update(theObject);
	}
	
	protected final Intent getIntent()
	{
		return ValidationIntent.UPDATE;
	}
	
}
