package com.deloitte.common.commands.persistence;

import java.util.List;

import com.deloitte.common.persistence.ConnectionFactory;


/**
 * The SQLCommandManager will perform either a single Command or a
 * CommandList of commands in a transaction. Assuming an Exception is
 * not thrown, it will continue to execute commands and add their results
 * to the Collection to be returned. The command will be rolled back in
 * the event of an exception or if the transaction is explicitely set for
 * rollback with the TransactionContext object.
 * @author SIDT Framework Team
 */
public class SQLCommandManager extends TransactionalCommandManager
{
	public SQLCommandManager()
	{
		super();
	}
	
	public SQLCommandManager(ConnectionFactory factory)
	{
		super(factory);
	}


	public boolean rollback(List<?> results) {
		return false;
	}
	
}
