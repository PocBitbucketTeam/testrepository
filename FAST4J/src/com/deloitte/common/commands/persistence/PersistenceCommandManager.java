package com.deloitte.common.commands.persistence;

import java.util.List;

import com.deloitte.common.persistence.ConnectionFactory;

/**
 * The PersistenceCommandManager will perform either a single Command or a
 * CommandList of commands. Assuming an Exception is not thrown, it
 * will continue to execute commands and add their results to
 * the Collection to be returned. Any returned values from the execute()
 * method are assumed to be errors. In the event any errorts are returned
 * by any of the commands, the transaction will be aborted and rolled back. 
 * <p>
 * <b>NOTE: Commands with under normal circumstances return values from the execute
 * method should not be used with PersistenceCommandManager as the returned values
 * will cause the transaction to be aborted.
 * </b>
 * @author SIDT Framework Team
 */
public final class PersistenceCommandManager extends TransactionalCommandManager 
{
	public PersistenceCommandManager()
	{
		super();
	}
	
	public PersistenceCommandManager(ConnectionFactory factory)
	{
		super(factory);
	}

	public boolean rollback(List<?> results) {
		return results.size() > 0;
	}

}
