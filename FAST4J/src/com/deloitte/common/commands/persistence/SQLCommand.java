package com.deloitte.common.commands.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.deloitte.common.interfaces.Command;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.persistence.ConnectionFactory;
import com.deloitte.common.persistence.SQLBatchUpdate;
import com.deloitte.common.persistence.SQLProcessor;
import com.deloitte.common.persistence.SQLQuery;
import com.deloitte.common.persistence.SQLUpdate;
import com.deloitte.common.persistence.interfaces.Transactional;

/**
 * Allows for execution of the frameworks three different SQLProcessor implementations
 * for ad-hoc SQL in a Command. This supports all of SQLQuery, SQLUpdate, and 
 * SQLBatchUpdate. 
 */
public class SQLCommand implements Command, Transactional {
	
	private String sql;
	private SQLProcessor processor;
	private List<?> parms;

	/**
	 * Creates the SQLCommand
	 * 
	 * @param ds the DataSource to use
	 * @param processor the SQLProcessor to use for executing the SQL. Should be one of 
	 * 			either SQLQuery, SQLUpdate, or SQLBatchUpdate
	 * @param sql the SQL to execute
	 * @param parms the parameters for the SQL. If using SQLBatchUpdate, the parameters
	 * provided must be a List<List<?>> as is defined and necessary by the SQLBatchUpdate interface.
	 */
	public SQLCommand(DataSource ds, SQLProcessor processor, String sql, List<?> parms) {
		if ((processor instanceof SQLBatchUpdate) && parms.size() > 0
				&& !(parms.get(0) instanceof List<?>)) {
			throw new IllegalArgumentException("SQLBatchUpdate can only accept List<List<?>> for parms"
					+ ", instead it was provided a List of " + parms.get(0).getClass().getName());
		}
		this.processor = processor;
		this.processor.setDataSource(ds);
		this.parms = parms;
		this.sql = sql;
	}

	public SQLCommand(DataSource ds,SQLProcessor processor, String sql)	{
		this(ds, processor, sql, Collections.emptyList());
	}

	/**
	 * @param ds
	 * @param sql
	 */
	public SQLCommand(DataSource ds, String sql) {	
		this(ds, new SQLQuery(), sql);
	}

	/**
	 * Creates the SQLCommand
	 * 
	 * @param factory the ConnectionFactory to get the DataSource to use from
	 * @param processor the SQLProcessor to use for executing the SQL. Should be one of 
	 * 			either SQLQuery, SQLUpdate, or SQLBatchUpdate
	 * @param sql the SQL to execute
	 * @param parms the parameters for the SQL. If using SQLBatchUpdate, the parameters
	 * provided must be a List<List<?>> as is defined and necessary by the SQLBatchUpdate interface.
	 */
	public SQLCommand(ConnectionFactory factory, SQLProcessor processor, String sql, List<?> parms) {
		this(factory.getDataSource(factory.getClass()), processor, sql, parms);
	}

	public SQLCommand(ConnectionFactory factory, SQLProcessor processor, String sql) {
		this(factory, processor, sql, Collections.emptyList());
	}
	
	public SQLCommand(ConnectionFactory factory, String sql, List<?> parms) {
		this(factory, new SQLQuery(), sql, parms);
	}
	
	public SQLCommand(ConnectionFactory factory, String sql) {	
		this(factory, sql, Collections.emptyList());
	}


	public void setDataSource(DataSource ds) {
		getProcessor().setDataSource(ds);
	}
	
	public DataSource getDataSource() {
		return getProcessor().getDataSource();
	}

	/** 
	 * Executes the SQL with the given SQLProcessor. The Collection returned if using
	 * as SQLQuery will be the results unchanged from the SQLQuery as defined by
	 * the ResultSetProcessor it is using. The result from a SQLUpdate will be
	 * a list containing a single Map<String, Integer> entry, that itself has one entry
	 * with the key "RESULTS" and a value representing the total number of rows
	 * updated in the call. The result from a SQLBatchUpdate will be a List<int[]> with
	 * one entry containing the result of the call to getBatchUpdateResults().
	 */
	public final Collection<?> execute() throws CheckedApplicationException
	{
		if (SQLQuery.class.isAssignableFrom(getProcessor().getClass())){
			return ((SQLQuery)getProcessor()).getQueryResults(sql, parms);	
		} else if (SQLUpdate.class.isAssignableFrom(getProcessor().getClass())) {
			int results = ((SQLUpdate)getProcessor()).getUpdateResults(sql, parms);

			Map<String, Integer> result = new HashMap<String, Integer>();
			result.put("RESULTS",new Integer(results));
			List<Map<String, Integer>> list = new ArrayList<Map<String, Integer>>();
			list.add(result);
			
			return list;
		} else if (SQLBatchUpdate.class.isAssignableFrom(getProcessor().getClass())) {
			@SuppressWarnings("unchecked")
			int[] results = ((SQLBatchUpdate)getProcessor()).getBatchUpdateResults(sql, (List<List<?>>)parms);

			List<int[]> list = new ArrayList<int[]>();
			list.add(results);
			
			return list;
		} else {
			throw new IllegalArgumentException("Unexpected Processor type: " + getProcessor().getClass().getName());
		}
	}
	
	private SQLProcessor getProcessor() {
		return processor;
	}

	public Collection<Error> prepare() throws CheckedApplicationException {
		return Collections.emptyList();
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("SQLCommand with [");
		sb.append("SQLProcessor = ").append(processor);
		sb.append(", SQL = '").append(sql).append("'");
		sb.append("]");
		return sb.toString();

	}
	
}
