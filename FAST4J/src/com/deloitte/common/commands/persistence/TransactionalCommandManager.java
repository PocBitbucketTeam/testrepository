package com.deloitte.common.commands.persistence;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.commands.CommandList;
import com.deloitte.common.commands.CommandManager;
import com.deloitte.common.interfaces.Command;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.ConnectionFactory;
import com.deloitte.common.persistence.TransactionContext;
import com.deloitte.common.util.CollectionHelper;

/**
 * Will execute either a single Command or a CommandList of commands. 
 * It will continue to execute commands and add their results to
 * the Collection to be returned. Subclasses must implement the rollback method
 * which will indicate if the transaction must be aborted 
 * @author SIDT Framework Team
 */
public abstract class TransactionalCommandManager extends CommandManager {

	private static final Logger logger = Logger.getLogger(TransactionalCommandManager.class.getName());

	private ConnectionFactory factory;

	public TransactionalCommandManager(){
		super();
	}
	
	public TransactionalCommandManager(ConnectionFactory factory) {
		super();
		this.factory = factory;
	}
	

	public final Collection<?> perform(CommandList theCommands) throws CheckedApplicationException {
		logger.info("Command List Received: " + CollectionHelper.format(theCommands.get()));

		boolean startedTransaction = false;
		List<Object> theResults = new ArrayList<Object>();
		
		try {
			startedTransaction = TransactionContext.startTransaction();
			
			if (factory != null){
				//use the user-provided factory to execute the statements on
				try {
					TransactionContext.setTransactionConnection(factory.getDataSource(factory.getClass()).getConnection());
				} catch (SQLException sqle) {
					throw new CheckedApplicationException(this.getClass(), "Unable to create connection with provided factory" + factory, sqle);
				}
			}
			
			for(Command aCommand: theCommands.get()){
				logger.info("About to perform a Command " + aCommand.getClass());
				theResults.addAll(aCommand.execute());
			}
			
			if (rollback(theResults)) {
				TransactionContext.setRollbackOnly();
			}
			TransactionContext.markTransactionComplete(startedTransaction);
		} catch (CheckedApplicationException cae) {
			logger.info("Setting transaction to rollback due to exception:  " + cae);
			TransactionContext.setRollbackOnly();
			throw new CheckedApplicationException(this.getClass(), "Error occured in execution of command list", cae);
		} finally {
			if (startedTransaction){
				TransactionContext.finishTransaction(startedTransaction);
			}
		}
		return theResults;
	}
	
	public final Collection<?> perform(Command command) throws CheckedApplicationException	{
		CommandList commands = new CommandList();
		commands.add(command);
		return perform(commands);
	}
	
	/**
	 * This method may be implemented with logic that will conditionally abort and roll 
	 * back the transaction depending on the compiled results from the command. Will be
	 * invoked by TransactionalCommandManager after each command is executed being passed
	 * the aggregate results of all commands.
	 * @param results The List containing the compiled results from the commands
	 * @return indicates if the TransactionalCommandManager should rollback the transaction
	 */
	public abstract boolean rollback(List<?> results);
	
}
