package com.deloitte.common.commands;

import java.util.ArrayList;
import java.util.List;

import com.deloitte.common.interfaces.Command;

/** 
 * <p>
 * Command List is a Collection of Commands. This allows a series
 * of Commands to be executed in order.
 * <p>
 * Example Usage: 
 * 	<li>CommandList list = new CommandList();
 *  <li>list.add(aCommand);
 *  <li>list.add(aCommand);
 * 
 * @author SIDT Framework Team

 */

public class CommandList{
	
	private List<Command> commandList;
	
	public CommandList(){
		super();
		commandList = new ArrayList<Command>();
	}
	
	public final void add(Command aCommand){
		commandList.add(aCommand);
	}
	
	public final List<Command> get(){
		return java.util.Collections.unmodifiableList(commandList);
	}
}
