package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import com.deloitte.common.interfaces.Document;
import com.deloitte.common.objects.domain.BinaryDocument;
import com.deloitte.common.objects.domain.TextDocument;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.util.Arrays;

public class SQLBindHelper {

	static PreparedStatement bindParameters(Connection theConnection, String sql, List<?> parameters) throws SQLException, CheckedApplicationException {

		PreparedStatement theStatement = getPreparedStatement(theConnection, sql);

		if (!canBind(sql, parameters)) {
			throw new SQLException(
					"Unable to bind PreparedStatement, the place holder count (?) does not match the number of parameters\n "
							+ resolveStatement(sql, parameters));
		}

		if (parameters != null && parameters.size() > 0) {
			int position = 1;
			for (Object obj : parameters) {
				handleStatementBinds(theStatement, obj, position++);
			}

		}

		return theStatement;

	}

	static final PreparedStatement bindBatchParameters(Connection theConnection, String sql, List<List<?>> parameters) throws SQLException, CheckedApplicationException {

		PreparedStatement theStatement = getPreparedStatement(theConnection, sql);

		for (List<?> parmList : parameters) {
			if (!canBind(sql, parmList)) {
				throw new SQLException(
						"Unable to bind PreparedStatement, the place holder count (?) does not match the number of parameters\n "
								+ resolveStatement(sql, parameters));
			}

			if (parmList != null && parmList.size() > 0) {
				int position = 1;
				for (Object obj : parmList) {
					SQLBindHelper.handleStatementBinds(theStatement, obj, position++);
				}
			}
			theStatement.addBatch();
		}

		return theStatement;

	}

	private static PreparedStatement getPreparedStatement(Connection theConnection, String sql) throws CheckedPersistenceException {
		PreparedStatement theStatement = null;
		try {
			theStatement = theConnection.prepareStatement(sql);
		} catch (SQLException se) {
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
			throw new CheckedPersistenceException(SQLBindHelper.class,
					"An error has occured in the Data Layer, unable to bind the parameters to the SQL: "
					+ se.getMessage(), se);
		}
		return theStatement;
	}
	
	static void handleStatementBinds(PreparedStatement theStatement,
			Object theObject, int position) throws SQLException {
		if (theObject != null && Calendar.class.isAssignableFrom(theObject.getClass())) {
			Calendar theCalendar = (Calendar) theObject;
			Timestamp theTimestamp = new Timestamp(theCalendar.getTimeInMillis());
			theStatement.setTimestamp(position, theTimestamp);
		} else if (theObject != null && theObject instanceof BinaryDocument) {
			BinaryDocument theDocument = (BinaryDocument) theObject;
			theStatement.setBinaryStream(position, theDocument.getStream(), theDocument.length());
		} else if (theObject != null && theObject instanceof TextDocument) {
			TextDocument theDocument = (TextDocument) theObject;
			theStatement.setAsciiStream(position, theDocument.getStream(), theDocument.length());
		} else {
			theStatement.setObject(position, theObject);
		}
	}

	public static String resolveStatement(String sql, List<?> parameters)
	{
		StringBuffer sb = new StringBuffer();
		if (!canBind(sql,parameters))
		{
			sb.append("Unresolvable Statement: " + sql + " "/* + CollectionHelper.format(parameters)*/);
			
		} else
		{
			sb.append(sql);
			int index = (parameters != null ? parameters.size()-1 : 0);
			for (int i=sb.length()-1; i > 0; i--)
			{
				if (sb.charAt(i) == '?')
				{
					if (parameters.get(index) != null && Calendar.class.isAssignableFrom(parameters.get(index).getClass()))
					{
						Calendar cal = (Calendar)parameters.get(index);
						sb.replace(i,i+1,"'" + cal.getTimeInMillis()+"'");
					} 
					else if (parameters.get(index) != null && Document.class.isAssignableFrom(parameters.get(index).getClass()))
					{
						Document doc = (Document)parameters.get(index);
						// The Document is no longer implements the DomainObject
						// Hence the doc.getID() is removed
						sb.replace(i,i+1,"'" + doc.getClass().getName()+" '");
					} 
					else
					{
						sb.replace(i,i+1,"'" + parameters.get(index) + "'");	
					}
					index--;
				}
			}
		}
		return sb.toString();
	}

	public static boolean canBind(String sql, List<?> parameters)
	{
		int count = Arrays.contains(sql.toCharArray(),'?');
		int parameterSize = 0;
		if (parameters != null)
		{
			parameterSize = parameters.size();
		}
		if ( count != parameterSize)
		{
			return false;
		}
		return true;
	}

}
