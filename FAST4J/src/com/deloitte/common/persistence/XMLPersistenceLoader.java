package com.deloitte.common.persistence;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import com.deloitte.common.objects.AbstractXMLLoader;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * Uses either external XML file or Document object to register DAO with the DefaultDAOFactory
 * 
 * The format of the xml file must be
 * <pre>
 * <configuration>
 * <registration>
 *	<dao  key="com.deloitte.common.interfaces.DomainObject" 
 *	  factory="com.deloitte.common.persistence.HsqldbConnectionFactory"
 *	     dao="com.deloitte.common.persistence.DomainObjectDAO"
 *    cached="true" maxRows="50" fetchSize="50"/>
 * </registration>
 * </configuration> 
 * </pre>
 * 
 * @author SIDT Framework Team
 *
 */
public class XMLPersistenceLoader extends AbstractXMLLoader {
	
	public XMLPersistenceLoader(Document document){
		super(document);
	}
	
	public XMLPersistenceLoader(String fileName) {
		super(fileName);
	}

	protected void loadValues(Document xmlDoc) throws CheckedApplicationException {
		NodeList theDAOs = xmlDoc.getElementsByTagName("dao");
		Node node = null;
		NamedNodeMap map = null;
		String key = null;
		String factory = null;
		String dao = null;
		boolean cache=false;
		Integer fetchSize = null;
		Integer maxRows = null;

		if (theDAOs != null) {
			for (int i = 0; i < theDAOs.getLength(); i++) {
				node = theDAOs.item(i);
				map = node.getAttributes();
				key = map.getNamedItem("key").getNodeValue();
				factory =map.getNamedItem("factory").getNodeValue();
				dao = map.getNamedItem("dao").getNodeValue();
				
				if (null != map.getNamedItem("cached")) {
					cache = Boolean.valueOf(map.getNamedItem("cached").getNodeValue());
				}
				if (null != map.getNamedItem("maxRows")) {
					try{
						maxRows = Integer.valueOf(map.getNamedItem("maxRows").getNodeValue().trim());
					} catch(NumberFormatException ex){
						throw new UncheckedApplicationException(PropertiesPersistenceLoader.class, "Invalid value configured for fetch size property in dao.properties", ex);
					}				
				}
				if (null != map.getNamedItem("fetchSize")) {
					try{
						fetchSize = Integer.valueOf(map.getNamedItem("fetchSize").getNodeValue().trim());
					} catch(NumberFormatException ex){
						throw new UncheckedApplicationException(PropertiesPersistenceLoader.class, "Invalid value configured for fetch size property in dao.properties", ex);
					}
				}
				DAOConfiguration configuration = new DAOConfiguration(key, factory, dao, cache, fetchSize, maxRows); 
				DefaultDAOFactory.getInstance().register(configuration
						.getKey(), configuration.getFactory(), configuration.getDao());
				
			}
		}
	}
}
