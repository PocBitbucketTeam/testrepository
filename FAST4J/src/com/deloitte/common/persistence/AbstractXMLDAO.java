package com.deloitte.common.persistence;

import com.deloitte.common.objects.domain.XMLContainer;
import com.deloitte.common.persistence.database.XMLResultSetProcessor;
import com.deloitte.common.persistence.interfaces.ResultSetProcessor;

public abstract class AbstractXMLDAO extends AbstractDAO<XMLContainer>
{
	protected ResultSetProcessor<XMLContainer> getResultSetProcessor()
	{
		return new XMLResultSetProcessor();
	}

}
