package com.deloitte.common.persistence;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.logging.Logger;

import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;
/**
 * 
 * To facilitate Transactional control, a Dynamic Proxy is created around the
 * java.sql.Connection object. 
 * 
 * The primary purpose is to wrap the Statement object that is returned with a
 * Proxy. This allows us to control access to the Connection and keep subclasses
 * from interfering with the transaction.
 * 
 * @author SIDT Framework Team
 *
 */
final class TransactionalConnection implements java.lang.reflect.InvocationHandler
{
	private static final Logger logger = Logger.getLogger(TransactionalConnection.class.getName());

	private final static String CLOSE = "close";
	private final static String IS_CLOSED = "isClosed";
	private final static String PREPARE_STATEMENT = "prepareStatement";
	
	private Object obj;

    public static Object newInstance(Object obj) 
	{
	return java.lang.reflect.Proxy.newProxyInstance(
	    obj.getClass().getClassLoader(),
	    ReflectionHelper.getDeclaredInterfaces(obj.getClass()),
	    new TransactionalConnection(obj));
    }

    private TransactionalConnection(Object obj) 
	{
    	if (obj instanceof Connection){
    		this.obj = obj;
    	} else {
    		throw new UncheckedApplicationException(this.getClass(), "Only Connection objects may be wrapped with this proxy");
    	}
    }

    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable
    {
        Object result = null;
		try 
		{
			if (CLOSE.equals(m.getName()))
			{
				logger.info("In Transaction, bypassing close of Connection") ;
			} else if (IS_CLOSED.equals(m.getName()))
			{
				result = Boolean.FALSE;
			}
			else if (PREPARE_STATEMENT.equals(m.getName()))
			{
				result = TransactionalStatement.newInstance(m.invoke(obj, args));

			} else
			{
				result = m.invoke(obj,args);
			}
	        } catch (InvocationTargetException e) 
			{
				throw e.getTargetException();
	        } catch (Exception e) 
	        {
				throw new UncheckedApplicationException(getClass(),"Unable to invoke on " + proxy + ":" + m,e);
	    } 
	return result;
    }
    
}
