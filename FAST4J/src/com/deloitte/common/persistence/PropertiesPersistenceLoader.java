package com.deloitte.common.persistence;

import java.util.Properties;
import java.util.StringTokenizer;

import com.deloitte.common.objects.AbstractPropertiesLoader;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * Uses either external properties file or Properties object to register DAO with the DefaultDAOFactory
 * 
 * The format of the properties file must be
 * <br/><b>
 * dao1=com.deloitte.common.interfaces.DomainObject,com.deloitte.common.persistence.HsqldbConnectionFactory,com.deloitte.common.persistence.DomainObjectDAO, false, ${maxRows}, ${fetchSize} <br/>
 * dao2=com.deloitte.common.objects.domain.XMLContainer,com.deloitte.common.persistence.PostgreSQLConnectionFactory,com.deloitte.common.persistence.DomainObjectXMLDAO,true<br/>
 *</b>
 * @author SIDT Framework Team
 *
 */
public class PropertiesPersistenceLoader extends AbstractPropertiesLoader {

	public PropertiesPersistenceLoader(String fileName) {
		super(fileName);
	}
	
	public PropertiesPersistenceLoader(Properties props){
		super(props);
	}
	
	protected void loadValues(String propKey, String propVal){
		if(propKey.toLowerCase().startsWith("dao")) {
			StringTokenizer stt = null;
			String key = null;
			String factory = null;
			String dao = null;
			boolean cache=false;
			Integer fetchSize = null;
			Integer maxRows = null;

			propVal = propVal.replaceAll(",,", ", ,"); 
			/*StringTokenizer does not identify a token between adjacent delimiters*/
			stt = new StringTokenizer(propVal,",");
			key = stt.nextToken();
			factory = stt.nextToken();
			dao = stt.nextToken();
			
			if (stt.hasMoreElements()) {
				cache = Boolean.valueOf(stt.nextToken());
			}

			if (stt.hasMoreElements()) {
				try{
					maxRows = Integer.valueOf(stt.nextToken().trim());
				} catch(NumberFormatException ex){
					throw new UncheckedApplicationException(PropertiesPersistenceLoader.class, "Invalid value configured for max rows property in dao.properties", ex);
				}
			}			
			if (stt.hasMoreElements()) {
				try{
					fetchSize = Integer.valueOf(stt.nextToken().trim());
				} catch(NumberFormatException ex){
					throw new UncheckedApplicationException(PropertiesPersistenceLoader.class, "Invalid value configured for fetch size property in dao.properties", ex);
				}
			}			

			DAOConfiguration configuration = new DAOConfiguration(key, factory, dao, cache, fetchSize, maxRows); 
			DefaultDAOFactory.getInstance().register(configuration
					.getKey(), configuration.getFactory(), configuration.getDao());
		} 
		
	}

}
