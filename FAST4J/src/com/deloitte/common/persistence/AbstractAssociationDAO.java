package com.deloitte.common.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.deloitte.common.persistence.interfaces.ResultSetProcessor;

/**
 * 
 * <p>
 * AbstractAssociationDAO provides a Data Access default implementation to
 * persist associations i.e. many to many relationships. It requires
 * the subclass implementor to provide SQL Statements and Parameters. It inherits the 
 * functionalities like Database connections retrieve/release, 
 * statement binding and execution from AbstractDAO.
 * 
 * @see AbstractDAO
 * 
 * </p>
 *
 */
public abstract class AbstractAssociationDAO
		extends AbstractDAO<Association> {

	/**
	 * Automatically adds two params to the parameters list, first
	 * the ParentID, then the ChildId
	 * @see AbstractDAO#getInsertParameters(DomainObject)
	 */
	@Override
	protected List<?> getInsertParameters(Association theObject) {
		List<Object> params = new ArrayList<Object>();
		params.add(theObject.getParentId().getValue());
		params.add(theObject.getChildId().getValue());
		return params;
	}

	/**
	 * Automatically adds two params to the parameters list, first
	 * the ParentID, then the ChildId
	 * @see AbstractDAO#getDeleteParameters(DomainObject)
	 */
	@Override 
	protected List<?> getDeleteParameters(Association theObject) {
		List<Object> params = new ArrayList<Object>();
		params.add(theObject.getParentId().getValue());
		params.add(theObject.getChildId().getValue());
		return params;
	}

	/**
	 * Will throw an UnsupportedOperationException when invoked
	 */
	@Override
	protected ResultSetProcessor<Association> getResultSetProcessor() {
		throw new UnsupportedOperationException("Not Implemented");
	}

	/**
	 * Will throw an UnsupportedOperationException when invoked
	 */
	@Override
	protected String getSelectStatement(Map<?, ?> arg0) {
		throw new UnsupportedOperationException("Not Implemented");
	}
	

}
