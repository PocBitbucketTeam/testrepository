package com.deloitte.common.persistence;

import java.util.logging.Logger;

import javax.sql.DataSource;

import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.persistence.interfaces.Transactional;

/**
 * 
 */
public abstract class SQLProcessor implements Transactional {
	
	private static final Logger logger = Logger.getLogger(SQLProcessor.class.getName());

	private DataSource datasource;
	
	public final void setDataSource(DataSource datasource) {
		if (datasource == null) {
			throw new UncheckedApplicationException(this.getClass(),
					"Can not set Datasource, the datasource is null");
		}
		logger.info("Received Datasource " + datasource.getClass().getName());
		this.datasource = datasource;
	}

	public final DataSource getDataSource() {
		return this.datasource;
	}
	
}
