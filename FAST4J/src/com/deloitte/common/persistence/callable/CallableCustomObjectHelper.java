package com.deloitte.common.persistence.callable;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A convenient helper class containing methods to handle DB custom object types
 */
public class CallableCustomObjectHelper {

	/**
	 * Constructs the java.sql.Array object from the Callable Array object
	 * @param connection
	 * @param callArray Callable Array representing the array of custom objects in DB
	 * @return java.sql.Array of custom object in DB
	 * @throws SQLException
	 */
	public static Array getArray(Connection connection, CallableArray callArray) throws SQLException{
		if(callArray.isPrimitiveArray()){
			return getPrimitiveArray(callArray, connection);
		}else{
			CustomObject custObject = callArray.getCustomObject();
			registerCustomObject(connection, custObject.getSqlObjectTypeName());
			return getCustomObjectArray(callArray, connection);
		}
	}
	
	/**
	 * Constructs the CustomObjectDecorator object from the Callable Struct object
	 * @param connection
	 * @param callStruct Callable Struct representing the custom object in DB
	 * @return CustomObjectDecorator
	 * @throws SQLException
	 */
	public static CustomObjectDecorator getCustomObjectAsStruct(Connection connection, CallableStruct<?> callStruct) throws SQLException{		
		CustomObject custObject = callStruct.getCustomObject();
		registerCustomObject(connection, custObject.getSqlObjectTypeName());
		return new CustomObjectDecorator(custObject.getSqlObjectTypeName(), callStruct.getValue(), custObject.getMappedProperties());		
	}	
	
	/**
	 * Constructs the list of java custom object from the Array object returned from the DB 
	 * @param customObjectArray java.sql.Array object returned from the DB
	 * @param isPrimitiveArray indicates whether the returned array is of primitives or not
	 * @return List of java primitives or custom object or the list of pojo mapped to custom object in DB
	 * @throws SQLException
	 */
	public static List<?> getCustomObjectsAsList(Array customObjectArray, boolean isPrimitiveArray) throws SQLException{
		Object[] objects = (Object[])customObjectArray.getArray();
		List<Object> objectList = new ArrayList<Object>();		
		for(int index = 0; index < objects.length; index++){
			if(isPrimitiveArray){
				objectList.add(objects[index]);
			}else{
				CustomObjectDecorator decorator = (CustomObjectDecorator)objects[index];
				objectList.add(decorator.getCustomObject());
			}
		}
		return objectList;
	}
	
	/**
	 * Registers the sql object type with CustomObjectDecorator 
	 * @param connection DB connection
	 * @param sqlObjectType DB sql object type
	 * @throws SQLException
	 */
	public static void registerCustomObject(Connection connection, String sqlObjectType) throws SQLException{
		Map<String,Class<?>> typeMap = connection.getTypeMap();
		if(typeMap == null){
			typeMap = new HashMap<String, Class<?>>();
		}
		typeMap.put(sqlObjectType, CustomObjectDecorator.class);
		connection.setTypeMap(typeMap);
	}
	
	/*
	 * Constructs the java.sql.Array object from the given Callable Array object
	 */
	private static Array getCustomObjectArray(CallableArray callableArray, Connection connection){
		Collection<?> customObjectCollection = callableArray.getValue();
		CustomObject customObject = callableArray.getCustomObject();
		CustomObjectDecorator[] objectDecorator = new CustomObjectDecorator[customObjectCollection.size()];
		int count=0;
		for(Object object: customObjectCollection){						
			objectDecorator[count++] = new CustomObjectDecorator(customObject.getSqlObjectTypeName(), object, customObject.getMappedProperties());
		}
		SQLCustomType sqlType = SQLCustomTypeFactory.getSQLCustomType();
		return sqlType.createArray(connection,customObject.getSqlArrayTypeName(), objectDecorator);
	}
	
	/*
	 * Constructs the java.sql.Array object from the given Callable Array primitive objects
	 */	
	private static Array getPrimitiveArray(CallableArray callableArray, Connection connection){
		Collection<?> collValue = callableArray.getValue();
		Object[] objArray = collValue.toArray(new Object[collValue.size()]);
		SQLCustomType sqlType = SQLCustomTypeFactory.getSQLCustomType();
		return sqlType.createArray(connection,callableArray.getSqlObjectType(), objArray);				
	}
	
}
