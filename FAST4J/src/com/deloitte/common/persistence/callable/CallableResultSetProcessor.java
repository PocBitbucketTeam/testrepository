package com.deloitte.common.persistence.callable;


import java.util.List;
import java.util.Map;

import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.database.AbstractResultSetProcessor;
import com.deloitte.common.persistence.interfaces.ObjectProcessor;

/**
 * Used to convert a ResultSet into a list of specific object types using specific ObjectProcessor. The
 * CallableResultSetProcessor is instantiated by handleResultSet method of CallableResultSetHandler to process the ResultSets.  
 * CallableResultSetProcessor tells how to map the entries in the List representing the row to the corresponding
 * specific object type properties by using the specific ObjectProcessor.
 * 
 * @param <T> the type of specific object that the ResultSet will be converted into
 */
public class CallableResultSetProcessor<T> extends AbstractResultSetProcessor<T> {
	
	private ObjectProcessor<T> processor;
	
	public CallableResultSetProcessor(ObjectProcessor<T> processor) {
		this.processor=processor;
	}
			
	@Override
	protected void processTheRow(List<T> resultsList, Map<String, ?> paramMap)
			throws CheckedApplicationException {
		T theObject = processor.map(paramMap);
		if (getFilter().include(theObject)){
			resultsList.add(theObject);
		}
	}
	
	
}
