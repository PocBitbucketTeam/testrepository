package com.deloitte.common.persistence.callable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.objects.mapper.DefaultPropertyMapper;
import com.deloitte.common.util.ReflectionHelper;
/**
 * Responsible for loading custom object configuration file provided by the user as an xml file
 * and stores the information in a map with sql object type name as key and {@link CustomObject } as value.
 * The client can provide the custom object class with it's properties in the configuration file or the
 * class may have been defined in the mapper xml file. In either case, the custom object class properties must be
 * defined in the same order in which the custom object type is defined in the DB while creating the
 * custom object in the DB.
 *
 */
final public class CustomObjectConfig {

	private static Logger logger = Logger.getLogger(CustomObjectConfig.class.getName());
	private static CustomObjectConfig me = new CustomObjectConfig();
	
	private static Map<String, CustomObject> customObjectConfig = new HashMap<String, CustomObject>();
	private static String sqlCustomType;
	
	private CustomObjectConfig(){}
	
	/**
	 * Must be used only to fetch the details of custom object
	 * @return CustomObjectConfig
	 */
	public static CustomObjectConfig getInstance(){
		return me;
	}
	
	/**
	 * Must be used in all cases whenever Arrays are used
	 * @param configFileName Configuration xml file having all the information of the 
	 * custom object defined in the DB
	 * @return CustomObjectConfig
	 */
	public static CustomObjectConfig getInstance(String configFileName){
		loadConfig(configFileName, null);
		return getInstance();
	}
	
	/**
	 * Must be used if the custom object class properties are alreday defined in the mapper file.
	 * The same mapper must be passed as an argument here.
	 * @param configFileName
	 * @param mapper
	 * @return
	 */
	public static CustomObjectConfig getInstance(String configFileName, DefaultPropertyMapper mapper){		
		loadConfig(configFileName, mapper);		
		return getInstance();
	}	
	
	/**
	 * Returns the custom object with all the information as provided in the config file.
	 * @param type Sql Object type name created and defined in the DB
	 * @return CustomObject
	 */
	public CustomObject getCustomObject(String type){
		if(customObjectConfig.containsKey(type)){
			return customObjectConfig.get(type);
		}else{
			throw new UncheckedApplicationException(CustomObjectConfig.class,
					"Custom Object configuration not found for type "+type);
		}
	}
	
	/**
	 * Returns the SqlCustomType value defined in the config file
	 * @return String
	 */
	public String getSqlCustomType(){
		return sqlCustomType;
	}
	
	/*
	 * Loads the user provided custom object configuration file and stores the
	 * information in CustomObject to be used later in the procedure call.
	 */
	private static void loadConfig(String configFileName, DefaultPropertyMapper mapper){
		DocumentBuilder builder;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			logger.info("Loading CustomObjectConfigFile --> "+configFileName);
			Document document = builder.parse(CustomObjectConfig.class.getResourceAsStream(configFileName));
			NodeList customObjects = document.getElementsByTagName("CustomObject");
			for(int index1=0;index1 < customObjects.getLength(); index1++){
				Node customObjectNode = customObjects.item(index1);
				String type = customObjectNode.getAttributes().getNamedItem("SqlObjectType").getNodeValue();
				logger.info("Loading config for SqlObjectType->"+type);
				CustomObject customObject = getConfig(customObjectNode, mapper);				
				customObjectConfig.put(type, customObject);
			}
			NodeList sqlCustomTypeNode = document.getElementsByTagName("SqlCustomType");
			if(sqlCustomTypeNode != null && sqlCustomTypeNode.getLength() > 0){
				sqlCustomType = sqlCustomTypeNode.item(0).getFirstChild().getNodeValue();							
			}
		} catch (ParserConfigurationException e) {
			throw new UncheckedApplicationException(CustomObjectConfig.class,
					"Unexpected error while parsing the CustomObjectConfig XML", e);
		} catch (SAXException e) {
			throw new UncheckedApplicationException(CustomObjectConfig.class,
					"Unexpected error while parsing the CustomObjectConfig XML", e);
		} catch (IOException e) {
			throw new UncheckedApplicationException(CustomObjectConfig.class,
					"Unexpected error while parsing the CustomObjectConfig XML", e);
		}
	}
	
	/*
	 * Constructs the CustomObject from the xml custom object node
	 */
	private static CustomObject getConfig(Node customObjectNode,  DefaultPropertyMapper mapper){
		String sqlObjectType=customObjectNode.getAttributes().getNamedItem("SqlObjectType").getNodeValue();
		String sqlArrayType=null;
		Map<String,List<String>> classMap=null;
		Map<String, String> dateTypeMap = null;
		NodeList childNodes = customObjectNode.getChildNodes();
		for(int index2=0; index2< childNodes.getLength();index2++){
			Node node = childNodes.item(index2);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if(node.getNodeName().equals("SqlArrayType")){
					sqlArrayType = node.getTextContent();
				}	
				if(node.getNodeName().equals("Class")){		
					List<Map<String,?>> list = getClassMap(node);
					classMap = (Map<String, List<String>>) list.get(0);
					dateTypeMap = (Map<String, String>) list.get(1);
				}
			}
		}
		/*
		 * If the custom object properties are defined in the config
		 * it will be loaded and stored for use in the procedure call.
		 * If the properties are not defined in the config file, then
		 * it will try to load properties from the mapper if provided.
		 */
		String className = classMap.keySet().iterator().next();
		List<String> properties = classMap.get(className);
		Class<?> classType = ReflectionHelper.createFor(className);		
		if(properties.isEmpty() && mapper != null){
			properties = mapper.getMappedPropertyKeys(classType);
			if(properties == null || properties.isEmpty()){
				throw new UncheckedApplicationException(CustomObjectConfig.class,
						"Custom object mapping not configured for class "+className);
			}
		}else if(properties.isEmpty() && mapper == null){
			throw new UncheckedApplicationException(CustomObjectConfig.class,
					"Custom object mapping not configured for class "+className);			
		}
		if(dateTypeMap == null){
			return new CustomObject(sqlArrayType, sqlObjectType, classType, properties);
		}else{
			return new CustomObject(sqlArrayType, sqlObjectType, classType, properties, dateTypeMap);
		}
	}
	
	/*
	 * Loads the custom object class properties defined in the config file 
	 */
	private static List<Map<String, ?>> getClassMap(Node classNode){
		List<Map<String, ?>> classPropertyList = new ArrayList<Map<String, ?>>();
		Map<String,List<String>> map = new HashMap<String, List<String>>();
		String className = classNode.getAttributes().getNamedItem("name").getNodeValue();
		NodeList properties = classNode.getChildNodes();
		List<String> propertiesList = new ArrayList<String>();
		Map<String, String> dateTypeProperties = new HashMap<String, String>();
		for(int index=0;index < properties.getLength(); index++){
			Node propertyNode = properties.item(index);
			if(propertyNode.getNodeType() == Node.ELEMENT_NODE){
				if(propertyNode.getNodeName().equals("property")){
					propertiesList.add(propertyNode.getTextContent().toUpperCase());
					
					if(propertyNode.getAttributes() != null && propertyNode.getAttributes()
							.getNamedItem("dateType") != null){
						String dateType = propertyNode.getAttributes()
						.getNamedItem("dateType").getNodeValue();
						dateTypeProperties.put(propertyNode.getTextContent().toUpperCase(), dateType.toUpperCase());
					}
				}
			}
		}
		map.put(className, propertiesList);
		classPropertyList.add(map);
		classPropertyList.add(dateTypeProperties);
		return classPropertyList;
	}
	
}
