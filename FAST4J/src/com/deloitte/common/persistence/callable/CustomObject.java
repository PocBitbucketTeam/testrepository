package com.deloitte.common.persistence.callable;

import java.util.List;
import java.util.Map;

/**
 * A Custom Object pojo representing the custom object configuration data
 * containing sql object type name, sql array type name and the class details
 * with class properties mapped to the db sql object type.
 *
 */
public class CustomObject {

	private String sqlArrayTypeName;
	private String sqlObjectTypeName;
	private Class<?> classType;
	private List<String> mappedProperties;
	private Map<String, String> dateTypeMap;
	
	public CustomObject(String sqlArrayTypeName, String sqlObjectTypeName, Class<?> classType, List<String> mappedProperties){
		this.sqlArrayTypeName = sqlArrayTypeName;
		this.sqlObjectTypeName = sqlObjectTypeName;
		this.classType = classType;
		this.mappedProperties = mappedProperties;
	}

	public CustomObject(String sqlArrayTypeName, String sqlObjectTypeName, Class<?> classType, List<String> mappedProperties, Map<String, String> dateTypeMap){
		this(sqlArrayTypeName, sqlObjectTypeName, classType, mappedProperties);
		this.dateTypeMap = dateTypeMap;
	}

	/**
	 * @return String
	 */
	public String getSqlArrayTypeName() {
		return sqlArrayTypeName;
	}

	/**
	 * @return String
	 */
	public String getSqlObjectTypeName() {
		return sqlObjectTypeName;
	}

	/**
	 * 
	 * @return Class<?>
	 */
	public Class<?> getClassType() {
		return classType;
	}

	/**
	 * @return List<String> 
	 */
	public List<String> getMappedProperties() {
		return mappedProperties;
	}
	
	public DateType getDateType(String property){
		if(dateTypeMap == null || !dateTypeMap.containsKey(property)){
			return DateType.DEFAULT;
		}else{
			return DateType.valueOf(dateTypeMap.get(property));
		}
	}
	

}
