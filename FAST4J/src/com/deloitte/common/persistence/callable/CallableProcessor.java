package com.deloitte.common.persistence.callable;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import java.sql.Types;

import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.objects.mapper.DefaultPropertyMapper;
import com.deloitte.common.persistence.CheckedPersistenceException;
import com.deloitte.common.persistence.ConnectionFactory;
import com.deloitte.common.persistence.SQLProcessor;
import com.deloitte.common.persistence.interfaces.ObjectProcessor;
import com.deloitte.common.util.Arrays;

/**
 * Used to execute Database stored procedure or function.
 * All parameters (IN, OUT and INOUT) must be added in the same order of 
 * the stored procedure defined in the database.
 * 
 * Example: Assuming that you have the following stored procedure in the DB
 * GET_EMPLOYEE_COUNT(deptno IN NUMBER, empcount OUT NUMBER)
 * Then you must add the parameters to the CallableProcessor in the same order as shown below
 * <code>
 *  CallableProcessor calProcessor = new CallableProcessor("GET_EMPLOYEE_COUNT");
 *  calProcessor.addInParameter(new Integer(100)); //for deptno
 *  calProcessor.addOUTParameter(new CallableOutParameter(Types.NUMERIC)); //for empcount
 * </code>  
 */
public class CallableProcessor extends SQLProcessor {

	private static Logger logger = Logger.getLogger(CallableProcessor.class.getName());
	
	private final String procedureName;
	private final boolean isFunction;
	private int parameterCount = 0;
	private Map<Integer, Object> inParameters = new HashMap<Integer, Object>();
	private Map<Integer, CallableOutParameter> outParameters = new HashMap<Integer, CallableOutParameter>();
	private CallableResultSetHandler handler = new CallableResultSetHandler();
	private Map<Class<?>, Integer> processorTypeIndexMap = new HashMap<Class<?>, Integer>();
	private final Map<Integer, ?> emptyMap = Collections.emptyMap();
	private DefaultPropertyMapper mapper;
	private String configFileName;	
	private boolean containsCallableCustomObjects = false;	
	
	/**
	 * @param procedureName Database procedure name
	 */
	public CallableProcessor(String procedureName) {
		this(procedureName, false);
	}
	
	/**
	 * @param procedureName Database procedure name or function name
	 * @param isFunction Must be true if it is a function
	 */
	public CallableProcessor(String procedureName, boolean isFunction) {
		this.procedureName = procedureName;
		this.isFunction= isFunction;
	}	
	
	/**	
	 * @param inParameter IN parameter value that will be passed to the stored procedure
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public CallableProcessor addInParameter(Object inParameter) {
		inParameters.put(++parameterCount, inParameter);
		return this;
	}
	
	/**
	 * @param callableArray CallableArray containing all the information of the custom object
	 * and the IN parameter value that will be passed to the stored procedure
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public CallableProcessor addInParameter(CallableArray callableArray) {
		if(!callableArray.isPrimitiveArray()){
			updateContainsCallableCustomObjects();
		}
		inParameters.put(++parameterCount, callableArray);
		return this;
	}	
	
	/**
	 * @param callableStruct CallableArray containing all the information of the custom object
	 * and the IN parameter value that will be passed to the stored procedure
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public CallableProcessor addInParameter(CallableStruct<?> callableStruct) {
		updateContainsCallableCustomObjects();
		inParameters.put(++parameterCount, callableStruct);
		return this;
	}	
	
	/** 
	 * @param callableOutParameter OUT parameter that will passed to the stored procedure. The parameter
	 * is of simple primitive object types
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public CallableProcessor addOutParameter(CallableOutParameter callableOutParameter) {
		outParameters.put(++parameterCount, callableOutParameter);		
		return this;
	}		
	
	/**
	 * @param callableOutParameter OUT parameter that will be passed to the stored procedure. The parameter
	 * is of other object types or custom defined types like OracleTypes.CURSOR
	 * @param processorType Class type for which the OUT parameter is being set
	 * @param objectProcessor The type of ObjectProcessor that will be used to process this OUT parameter
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public <T> CallableProcessor addOutParameter(CallableOutParameter callableOutParameter, Class<T> processorType, ObjectProcessor<T> objectProcessor) {
		outParameters.put(++parameterCount, callableOutParameter);
		handler.addProcessor(parameterCount, objectProcessor);
		processorTypeIndexMap.put(processorType, parameterCount);		
		return this;
	}
	
	/**
	 * @param callableArray CallableArray containing all the information of the custom object
	 * @return CallableProcessor This instance of the CallableProcessor
	 */	
	public  CallableProcessor addOutParameter(CallableArray callableArray) {
		if(!callableArray.isPrimitiveArray()){
			updateContainsCallableCustomObjects();
		}
		CallableOutParameter callableOutParameter = new CallableOutParameter(Types.ARRAY,callableArray);
		outParameters.put(++parameterCount, callableOutParameter);				
		return this;
	}	
	
	/**
	 * @param callableStruct CallableStruct containing all the information of the custom object
	 * @return CallableProcessor This instance of the CallableProcessor
	 */	
	public  CallableProcessor addOutParameter(CallableStruct<?> callableStruct) {
		updateContainsCallableCustomObjects();
		CallableOutParameter callableOutParameter = new CallableOutParameter(Types.STRUCT,callableStruct);
		outParameters.put(++parameterCount, callableOutParameter);				
		return this;
	}	
	
	/**
	 * @param inParameter IN parameter value that will be passed to the stored procedure
	 * @param callableOutParameter OUT parameter that will be passed to the stored procedure. The parameter
	 * is of other object types or custom defined types like OracleTypes.CURSOR
	 * @param processorType Class type for which the OUT parameter is being set
	 * @param objectProcessor The type of ObjectProcessor that will be used to process this OUT parameter
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public <T> CallableProcessor addInOutParameter(Object inParameter, CallableOutParameter callableOutParameter, Class<T> processorType, ObjectProcessor<T> objectProcessor) {
		addInParameter(inParameter);
		
		/* need to decrement internal parameter count since it's modified in both
		 * addInParameter and addOutParameter 
		 */
		--parameterCount;
		addOutParameter(callableOutParameter, processorType, objectProcessor);
		return this;
	}	
	
	/**
	 * @param inParameter IN parameter value that will be passed to the stored procedure
	 * @param callableOutParameter OUT parameter that will passed to the stored procedure. The parameter
	 * is of simple primitive object types
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public CallableProcessor addInOutParameter(Object inParameter, CallableOutParameter callableOutParameter) {
		addInParameter(inParameter);
		--parameterCount;
		addOutParameter(callableOutParameter);
		return this;
	}	
	
	/**
	 * @param callableArray CallableArray containing all the information of the custom object
	 * IN parameter value that will be passed to the stored procedure
	 * @return CallableProcessor This instance of the CallableProcessor
	 */	
	public CallableProcessor addInOutParameter(CallableArray callableArray) {
		if(!callableArray.isPrimitiveArray()){
			updateContainsCallableCustomObjects();
		}
		addInParameter(callableArray);
		
		/* need to decrement internal parameter count since it's modified in both
		 * addInParameter and addOutParameter 
		 */
		--parameterCount;
		addOutParameter(callableArray);
		return this;
	}	
	
	/**
	 * @param callableStruct CallableStruct containing all the information of the custom object
	 * IN parameter value that will be passed to the stored procedure
	 * @return CallableProcessor This instance of the CallableProcessor
	 */	
	public CallableProcessor addInOutParameter(CallableStruct<?> callableStruct) {
		updateContainsCallableCustomObjects();
		addInParameter(callableStruct);
		
		/* need to decrement internal parameter count since it's modified in both
		 * addInParameter and addOutParameter 
		 */
		--parameterCount;
		addOutParameter(callableStruct);
		return this;
	}	
	
	/**
	 * Registers the custom objects defined in the DB with the respective java object mapped to it.
	 * All custom objects and their properties must be defined in an xml and must be registerd 
	 * by calling this method.
	 * Sample configuration details:
	 *  <CustomObject SqlObjectType="RD.PROJECT">
	 *	  <SqlArrayType>RD.PROJECTLIST</SqlArrayType>
	 *	  <Class name="com.deloitte.common.persistence.callable.Project">
	 *	   <property>projectNumber</property>
	 *	   <property>title</property>
	 *	   <property>cost</property>
	 *	  </Class>
	 *	 </CustomObject> 
	 * @param configFileName Config xml file name conatianing sql object type, array type name and the 
	 * custom object properties to which the sql type name is mapped.
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public CallableProcessor registerCustomObjects(String configFileName){
		this.configFileName = configFileName;
		return this;
	}
	
	/**
	 * Registers the mapper conatining the Java object properties mapped to the custom object in DB.
	 * This method can be used if the java object properties are already defined in the mapper file.
	 * @param mapper conatining the Java object properties mapped to the custom object in DB.
	 * @return CallableProcessor This instance of the CallableProcessor
	 */
	public CallableProcessor registerMapper(DefaultPropertyMapper mapper){
		this.mapper = mapper;
		return this;
	}	
	
	/**
	 * Executes the specified stored procedure or function
	 * @return CallableResult Result object containing the values for all the OUT parameters if any
	 * @throws CheckedApplicationException
	 */
	public CallableResult executeCall() throws CheckedApplicationException {
		CallableResult result;	
		/*
		 * Custom configuration must be loaded first before executing the procedure
		 */
		loadCustomObjectConfiguration();
		if(parameterCount == 0) {
			executeCallWithNoParams();
			result = new CallableResult(emptyMap, processorTypeIndexMap);
		} else {
			Map<Integer,?> resultMap = executeCallWithParams();
			result = new CallableResult(resultMap, processorTypeIndexMap);
		}		
		return result;
	}
	
	/*
	 * NO IN and NO OUT. This method simply executes a stored procedure that
	 * doesn't return any value or take any parameters as input. This is the
	 * simplest form of all the methods in this class to execute a procedure.
	 */
	private void executeCallWithNoParams() throws CheckedApplicationException {
		CallableStatement theStatement = null;
		Connection theConnection = null;
		String statement = "{call " + procedureName + "() }";

		try {
			logger.info("About to Execute a procedure: " + procedureName);
			theConnection = getDataSource().getConnection();
			theStatement = theConnection.prepareCall(statement);
			theStatement.execute();
		} catch (SQLException e) {
			throw new CheckedPersistenceException(this.getClass(),
					"An error (DB Specific Information: "+ e.getErrorCode()
					+ " "+ e.getMessage()
					+ ") has occured in the Data Layer, while executing a procedure :"
					+ procedureName, e);
		} finally {
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
		}

	}

	/*
	 * <p>This form of executeCall() accepts two Map objects as inputs, out of
	 * which one represents the IN parameters for the stored procedure and the
	 * other OUT. INOUT parameters should be present in both the maps.
	 * 
	 * The users of this method are expected to use Integer objects as key with
	 * their intValue corresponding to the index of the parameter in the call string.
	 * </p>
	 * <p>
	 * Example usage :
	 * 
	 * Say we have to call a procedure getName with one IN parameter name we
	 * write it as
	 * <code>
	 * {call getName(?)}
	 * 
	 * If we know that we want to replace the value of first occurance of ? in
	 * the callable statement with "Bill Gates" , then we will have the Map with
	 * the key as Integer[1] and "Bill Gates" as bind value.
	 * 
	 * Similarly for OUT parameters too.
	 * <code>
	 * </p>
	 * <p>
	 * There may be a case where we have IN OUT parameters, in that case we will
	 * have the same key in both these maps. 
	 *
	 * This method is to be used if we have IN or OUT or IN OUT parameters.
	 * </p>
	 */
	private Map<Integer, ?> executeCallWithParams() throws CheckedApplicationException {
		CallableStatement theStatement = null;
		Connection theConnection = null;

		try {
			logger.info("About to Execute a procedure: " + procedureName);
			theConnection = getDataSource().getConnection();
			theStatement = constructCallableStatement(theConnection);

			theStatement.execute();
			
			for (Integer key : outParameters.keySet()) {
				Object outObject = (Object) theStatement.getObject(key.intValue());
				if (outObject == null){
					handler.addResult(key, outObject);
				}else{
					// This is to check if there exists a registered OUT like a cursor -- ResultSet
					if (ResultSet.class.isAssignableFrom(outObject.getClass())) {
						logger.info("Handler to handle ResultSet returned from an OUT parameter at Index "+ key.intValue());
						ResultSet rs = (ResultSet) outObject;
						handler.handleResultSet(key.intValue(), rs);
						rs.close();
					} else if(Array.class.isAssignableFrom(outObject.getClass())){
						CallableOutParameter callOut = outParameters.get(key);
						List<?> customObjects = CallableCustomObjectHelper.getCustomObjectsAsList((Array)outObject, callOut.getCallableArray().isPrimitiveArray());
						handler.addResult(key, customObjects);	
					} else if(CustomObjectDecorator.class.isAssignableFrom(outObject.getClass())){
						CustomObjectDecorator custObj = (CustomObjectDecorator)outObject;					
						handler.addResult(key, custObj.getCustomObject());						
					} else {
						handler.addResult(key, outObject);
					}
				}
			}
		} catch (SQLException e) {
			throw new CheckedPersistenceException(this.getClass(),
					"An error (DB Specific Information: "+ e.getErrorCode()
					+ " "+ e.getMessage()
					+ ") has occured in the Data Layer, while executing a procedure :"
					+ procedureName, e);
		} finally {
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
		}
		return handler.getResults();
	}

	private CallableStatement constructCallableStatement(Connection con)
			throws CheckedApplicationException {
		CallableStatement cstmt;
		int paramCount = parameterCount;
		StringBuffer sb = new StringBuffer();

		if (isFunction) {
			sb.append("{? = call " + procedureName + "(");
			paramCount--;
		} else {
			sb.append("{call " + procedureName + "(");
		}
		
		sb.append(Arrays.get(Arrays.duplicate(paramCount, '?').toCharArray(), ","));
		sb.append(")}");

		if (isFunction) {
			paramCount++;
		}

		String statement = sb.toString();
		logger.info("About to Execute a procedure: " + statement);

		try {
			cstmt = con.prepareCall(statement);
		} catch (SQLException e) {
			throw new CheckedPersistenceException(this.getClass(),
					"An error (DB Specific Information: "+ e.getErrorCode()
					+ " "+ e.getMessage()
					+ ") has occured in the Data Layer, while trying to create a callable statement",e);
		}
		
		// We have to now bind the input parameters
		logger.info("<ConstructCallableStatement>Successfully constructed the call String");
		handleStatementInputBinds(cstmt, paramCount);

		// we have to now bind the output parameters
		registerStatementOUTParams(cstmt);

		return cstmt;
	}

	private void handleStatementInputBinds(CallableStatement theStatement, int maxSize)
			throws CheckedApplicationException {
		Set<Integer> inKeys = inParameters.keySet();
		Object theObject;
		int position = 0;
		
		for (Integer positionWrapper : inKeys) {
			position = positionWrapper.intValue();
			theObject = inParameters.get(positionWrapper);
			try {
				if (theObject != null && Calendar.class.isAssignableFrom(theObject.getClass())) {
					Calendar theCalendar = (Calendar)  theObject;
					Timestamp theTimestamp = new Timestamp(theCalendar.getTimeInMillis());
					theStatement.setTimestamp(position, theTimestamp);
				}else if(theObject != null && CallableArray.class.isAssignableFrom(theObject.getClass())){
					Array array = CallableCustomObjectHelper.getArray(theStatement.getConnection(), (CallableArray)theObject);
					theStatement.setArray(position,array);	
				}else if(theObject != null && CallableStruct.class.isAssignableFrom(theObject.getClass())){					
					CustomObjectDecorator structObject = CallableCustomObjectHelper.getCustomObjectAsStruct(theStatement.getConnection(), (CallableStruct<?>)theObject);
					theStatement.setObject(position,structObject);					
				} else {
					theStatement.setObject(position, theObject);
				}
			} catch (SQLException e) {
				throw new CheckedPersistenceException(this.getClass(),
						"An error (DB Specific Information: "+ e.getErrorCode()
						+ " "+ e.getMessage()
						+ ") has occured in the Data Layer, while trying to set in parameter in a callable statement",e);
			}
		}
		logger.info("<handleStatementInputBinds>Successfully bound the input parameters");
	}

	/* All the output params are to be registered before executing a stored
	 * procedure.
	 */
	private void registerStatementOUTParams(CallableStatement theStatement)
			throws CheckedApplicationException {
		CallableOutParameter outParam;
		
		for (Integer positionWrapper : outParameters.keySet()) {		
			 outParam =  outParameters.get(positionWrapper);
			int position = positionWrapper.intValue();

			try {
				if(outParam.getSqlType() == Types.ARRAY){	
					if(outParam.getCallableArray().isPrimitiveArray()){
						theStatement.registerOutParameter(position, outParam.getSqlType(),outParam.getCallableArray().getSqlObjectType());	
					}else{
						CustomObject custObject = outParam.getCallableArray().getCustomObject();
						CallableCustomObjectHelper.registerCustomObject(theStatement.getConnection(), custObject.getSqlObjectTypeName());
						theStatement.registerOutParameter(position, outParam.getSqlType(),custObject.getSqlArrayTypeName());					
						processorTypeIndexMap.put(custObject.getClassType(), position);						
					}
				}else if(outParam.getSqlType() == Types.STRUCT){					
					CustomObject custObject = outParam.getCallableStruct().getCustomObject();
					CallableCustomObjectHelper.registerCustomObject(theStatement.getConnection(), custObject.getSqlObjectTypeName());
					theStatement.registerOutParameter(position, outParam.getSqlType(),custObject.getSqlObjectTypeName());					
					processorTypeIndexMap.put(custObject.getClassType(), position);					
				}else if (outParam.getScale() == -1 && outParam.getTypeName().equals("DEFAULT")) {
					theStatement.registerOutParameter(position, outParam.getSqlType());
				} else if (outParam.getScale() != -1) {
					theStatement.registerOutParameter(position, outParam.getSqlType(), 
									outParam.getScale());
				} else if (!outParam.getTypeName().equals("DEFAULT")) {
					theStatement.registerOutParameter(position, outParam.getSqlType(), 
									outParam.getTypeName());
				}
			} catch (SQLException e) {
				throw new CheckedPersistenceException(
						this.getClass(),
						"An error (DB Specific Information: "+ e.getErrorCode()
								+ " "+ e.getMessage()
								+ ") has occured in the Data Layer, while trying to registering out put parameters in CallableStatement",
						e);
			}
		}
		logger.info("<registerStatementOUTParams>Successfully bound the out put parameters on the callable statement");
	}	
	
	/*
	 * Loads the custom object configuration defined in the config file
	 *  to be used in executing the the stored procedure and processing the results.
	 */
	private void loadCustomObjectConfiguration(){
		if(containsCallableCustomObjects){
			if(configFileName != null){
				if(mapper != null){
					CustomObjectConfig.getInstance(configFileName, mapper);
				}else{
					CustomObjectConfig.getInstance(configFileName);
				}
			}else{				
				throw new UncheckedApplicationException(CallableProcessor.class,
						"Custom object configuration not found. Specify custom object config file name to register custom objects.");
			}
		}
	}
	
	/*
	 * A convenient method used to update the flag indicating callable custom objects are
	 * used in the processor and that the stored procedure contains DB Arrays.
	 */
	private void updateContainsCallableCustomObjects(){
		containsCallableCustomObjects = true;
	}	
}
