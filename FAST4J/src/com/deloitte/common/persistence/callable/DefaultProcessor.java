package com.deloitte.common.persistence.callable;

import java.util.Map;
import com.deloitte.common.persistence.interfaces.ObjectProcessor;

/**
 * Default Processor implementation that returns back the same Map object thats been sent to this processor.
 */
public class DefaultProcessor implements ObjectProcessor<Map<String, ?>> {

	/**
	 * Returns back the same map that is been passed in.
	 */
	public Map<String, ?> map(Map<String, ?> map) {
		return map;
	}

}
