package com.deloitte.common.persistence.callable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;

/**
 * Represents the callable configuration specified in the CallableConfig.xml file
 * This implementations contains the SqlData information conatining java.sql.SqlInput, java.sql.SqlOutput
 * methods for the respective datatype as defined in the JDBC specification
 */
final public class CallableConfiguration {

	private static final Logger logger = Logger.getLogger(CallableConfiguration.class.getName());
	private static 	CallableConfiguration me = new CallableConfiguration();
	private Map<Class<?>, CallableSQLData> sqlDataMap = new HashMap<Class<?>, CallableSQLData>();
	
	private CallableConfiguration(){
		loadValues();
	}
	
	public static CallableConfiguration getInstance(){
		return me;
	}
	
	public String getSqlInputMethodName(Class<?> type){
		CallableSQLData sqlData = sqlDataMap.get(type);
		if(sqlData == null){
			return null;
		}else{
			return sqlData.getSqlInput();
		}
	}
	
	public String getSqlOutputMethodName(Class<?> type){		
		CallableSQLData sqlData = sqlDataMap.get(type);
		if(sqlData == null){
			return null;
		}else{
			return sqlData.getSqlOutput();
		}
	}
	
	
	/*
	 * Loads the configuration information from CallableConfig.xml and stores the information 
	 * in the CallableSQLData object
	 */	
	private void loadValues(){
		try {
			String configFileName = "/com/deloitte/common/persistence/callable/CallableConfig.xml";			
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			logger.info("Loading callable configuration.......");
			Document document = builder.parse(CallableConfiguration.class.getResourceAsStream(configFileName));			
			NodeList sqlData = document.getElementsByTagName("SqlData");
			logger.info("callable configuration..number of entries for SqlData--> "+sqlData.getLength());
			for(int index=0;index < sqlData.getLength(); index++){				
				NodeList childNodes = sqlData.item(index).getChildNodes();
				String sqlInput=null;
				String sqlOutput=null;
				List<String> allTypes = new ArrayList<String>();
				for(int count=0;count < childNodes.getLength(); count++){
					Node childNode = childNodes.item(count);
					if(childNode.getNodeType() == Node.ELEMENT_NODE) {
						if(childNode.getNodeName().equals("SqlInput")){
							sqlInput = childNode.getTextContent();
						}
						if(childNode.getNodeName().equals("SqlOutput")){
							sqlOutput = childNode.getTextContent();
						}
						if(childNode.getNodeName().equals("type")){
							allTypes.add(childNode.getTextContent());
						}						
					}
				}
				for(String type : allTypes){
					sqlDataMap.put(ReflectionHelper.createFor(type), new CallableSQLData(sqlInput,sqlOutput));
				}				
			}			
		} catch (ParserConfigurationException e) {
			throw new UncheckedApplicationException(CallableConfiguration.class,
					"Unexpected error while parsing the CallableConfiguration XML", e);
		} catch (SAXException e) {
			throw new UncheckedApplicationException(CallableConfiguration.class,
					"Unexpected error while parsing the CallableConfiguration XML", e);
		} catch (IOException e) {
			throw new UncheckedApplicationException(CallableConfiguration.class,
					"Unexpected error while parsing the CallableConfiguration XML", e);
		}		
	}
	
	
	/*
	 * Simple representation of SQLData containing respective input, output and data type method details
	 */
	private class CallableSQLData{
		
		String sqlInput;
		String sqlOutput;
		
		CallableSQLData(String sqlInput, String sqlOutput){
			this.sqlInput = sqlInput;
			this.sqlOutput = sqlOutput;
		}

		public String getSqlInput() {
			return sqlInput;
		}

		public String getSqlOutput() {
			return sqlOutput;
		}						
	}	
}
