package com.deloitte.common.persistence.callable;

/**
 * Represents a generic custom type class.
 *
 */
public abstract class CallableCustomType {

	private String sqlObjectType;
	
	public CallableCustomType(String sqlObjectType){
		this.sqlObjectType = sqlObjectType;		
	}	
	
	public String getSqlObjectType(){
		return sqlObjectType;
	}
	
	/** 
	 * @return CustomObject 
	 */
	public CustomObject getCustomObject(){
		return CustomObjectConfig.getInstance().getCustomObject(sqlObjectType);
	}	
	
}
