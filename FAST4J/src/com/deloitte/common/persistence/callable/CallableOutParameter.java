package com.deloitte.common.persistence.callable;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * This class wraps the parameter index, sqlType and scale of the OUT parameter used by a Callable statement.
 * Typically this parameter class will not be used to represent a ResultSet OUT type. The Constructor with TypeName 
 * should be used for the out params like REF, or any custom type, as specified in JDBC api.
 */
public class CallableOutParameter {

   /*	 
	* We have to make sure that this is Type safe. How to acheive this? We have to make
	* sure that we are only sending the allowed values that are in java.sql.Types class.
	* May be we can write a helper class which constructs a list of those values and the input
	* can be checked against those values and see if it exists. if not throw  CAE. 
	* 
	* Anyway if the SQLType value is some thing not valid, SQL exception will be thrown while registering output.
    */ 
	private int sqlType; 
	private int scale=-1;
	private String typeName="DEFAULT";
	private CallableArray callableArray;
	private CallableStruct<?> callableStruct;
	
	public CallableOutParameter(int sqlType, int scale) throws CheckedApplicationException	{
		super();
		this.sqlType=sqlType; // this variable is not type safe ... we need to validate this JDBC.Types
		this.scale=scale;
	}
	
	public CallableOutParameter(int sqlType) throws CheckedApplicationException	{
		super();
		this.sqlType=sqlType;		
	}
	
	public CallableOutParameter(int sqlType, String typeName) {
		this.sqlType=sqlType; // this variable is not type safe ... we need to validate this JDBC.Types
		this.typeName=typeName;
	}
	
	public CallableOutParameter(int sqlType, CallableArray callableArray) {
		this.sqlType=sqlType; 
		this.callableArray=callableArray;
	}	
	
	public CallableOutParameter(int sqlType, CallableStruct<?> callableStruct) {
		this.sqlType=sqlType; 
		this.callableStruct=callableStruct;
	}	
	
	public CallableOutParameter(int sqlType, String typeName, int scale) {
		this.sqlType=sqlType; // this variable is not type safe ... we need to validate this JDBC.Types
		this.scale=scale;
		this.typeName=typeName;
	}
	
   /**
	* @return Returns the typeName.
	*/
	public final String getTypeName() {
		return typeName;
	}
	
   /**
	* @return Returns the scale.
    */
	public final int getScale() {
		return scale;
	}

   /**
	* @return Returns the sqlType.
	*/
	public final int getSqlType() {
		return sqlType;
	}
	
	public final CallableArray getCallableArray(){
		return callableArray;
	}	
	
	public final CallableStruct<?> getCallableStruct(){
		return callableStruct;
	}	
}
