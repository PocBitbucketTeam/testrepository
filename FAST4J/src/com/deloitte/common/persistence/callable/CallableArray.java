package com.deloitte.common.persistence.callable;

import java.util.Collection;

/**
 * Contains the SQL array information.
 *  
 */
public class CallableArray extends CallableCustomType{

	private Collection<?> value;
	private boolean primitiveArray = false;
	
	public CallableArray(String sqlObjectType){
		super(sqlObjectType);		
	}
	
	/**
	 * Uses boolean flag to differentiate between primitive values or 
	 * custom object values in the collection.
	 * @param sqlObjectType
	 * @param primitiveArray
	 */
	public CallableArray(String sqlObjectType, boolean primitiveArray){
		super(sqlObjectType);	
		this.primitiveArray = primitiveArray;
	}	
	
	public CallableArray(String sqlObjectType, Collection<?> value){
		super(sqlObjectType);		
		this.value = value;
	}	
	
	/**
	 * Uses boolean flag to differentiate between primitive values or 
	 * custom object values in the collection.	
	 * @param sqlObjectType
	 * @param value
	 * @param primitiveArray
	 */
	public CallableArray(String sqlObjectType, Collection<?> value, boolean primitiveArray){
		this(sqlObjectType,primitiveArray);		
		this.value = value;		
	}	

	public Collection<?> getValue(){
		return value;
	}
	
	public boolean isPrimitiveArray(){
		return primitiveArray;
	}
		
}
