package com.deloitte.common.persistence.callable;

import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;
/**
 * A factory method responsible for creating the SQL custom type
 * specified in the custom config file
 */
public class SQLCustomTypeFactory {

	public static SQLCustomType getSQLCustomType(){
		try {
			String sqlCustomTypeName = CustomObjectConfig.getInstance().getSqlCustomType();
			if(sqlCustomTypeName != null){
				return (SQLCustomType)ReflectionHelper.createFor(sqlCustomTypeName, new Class[0]);
			}else{
				throw new UncheckedApplicationException(SQLCustomTypeFactory.class,
						"Can't create SQLCustomType. SqlCustomType is not specified in the Custom config file.");
			}			
		} catch (Exception e) {
			throw new UncheckedApplicationException(SQLCustomTypeFactory.class, "SQLException while reading metadata information", e);
		}
	}
		
}
