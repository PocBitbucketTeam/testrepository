package com.deloitte.common.persistence.callable;

/**
 * Contains the SQL custom object (as Struct) information.
 *
 */
public class CallableStruct<T> extends CallableCustomType{
	
	private T value;
	
	public CallableStruct(String sqlObjectType){
		super(sqlObjectType);	
	}
	
	public CallableStruct(String sqlObjectType, T value){
		super(sqlObjectType);	
		this.value = value;
	}	

	public T getValue(){
		return value;
	}	
		
}
