package com.deloitte.common.persistence.callable;

import java.sql.Array;
import java.sql.Connection;

public interface SQLCustomType {

	public Array createArray(Connection connection,String arrayTypeName, Object[] customObjects);
	
}
