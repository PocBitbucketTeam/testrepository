package com.deloitte.common.persistence.callable;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;
/**
 * A decorator for the custom object class and is responsible for reading
 * and writing the sql struct data into the custom class type object. 
 */
public class CustomObjectDecorator implements SQLData{

	private Object customObject;
	private String sqlTypeName;
	private List<String> mappedProperties;	
	
	public CustomObjectDecorator(){}
	
	public CustomObjectDecorator(String sqlTypeName, Object customObject, List<String> mappedProperties){
		this.customObject = customObject;
		this.sqlTypeName = sqlTypeName;
		this.mappedProperties = mappedProperties;
	}

	public String getSQLTypeName() throws SQLException {		
		return sqlTypeName;
	}
	
	public Object getCustomObject(){
		return customObject;
	}

	/**
	 * Reads the SQLInput stream and populates the custom object using reflection mechanism
	 */
	public void readSQL(SQLInput stream, String typeName) throws SQLException {					
		CustomObject custObjConfig = CustomObjectConfig.getInstance().getCustomObject(typeName);				
		Class<?> klass = custObjConfig.getClassType();
		customObject =  ReflectionHelper.createFor(klass,new Class[0]);		
		mappedProperties = custObjConfig.getMappedProperties();		
		Map<String,String> methodsMap = getMutatorMethods(customObject.getClass());
		for(String propertyName : mappedProperties){
			String methodName = methodsMap.get(propertyName);
			String dateType = custObjConfig.getDateType(propertyName).toString();
			try {				
				Method method = ReflectionHelper.getMutatorMethod(customObject, methodName);
				Class<?>[] types = method.getParameterTypes();
				Object value = getValueForType(types[0], stream, dateType);				
				ReflectionHelper.invokeMutatorMethod(customObject, method, value);
			} catch (CheckedApplicationException e) {						
				throw new UncheckedApplicationException(CustomObjectDecorator.class,
						"Unknown error while invoking a method from class "+customObject.getClass(),e);								
			} catch (SecurityException e) {				
				throw new UncheckedApplicationException(CustomObjectDecorator.class,
						"Unknown SecurityException while invoking a method from class "+customObject.getClass(),e);
			}	
		}
	}

	/**
	 * Reads custom object data and writes the data to the SQLOutput stream
	 */
	public void writeSQL(SQLOutput stream) throws SQLException {		
		CustomObject custObjConfig = CustomObjectConfig.getInstance().getCustomObject(getSQLTypeName());		
		Map<String,String> methodsMap = getAccessorMethods(customObject.getClass());		
		for(String propertyName : mappedProperties){			
			String methodName = methodsMap.get(propertyName);
			String dateType = custObjConfig.getDateType(propertyName).toString();
			try {				
				Method method = customObject.getClass().getMethod(methodName, new Class[0]);
				Class<?> type = method.getReturnType();
				setValueForType(type,method.invoke(customObject,new Object[0]), stream, dateType);
			} catch (SecurityException e) {
				throw new UncheckedApplicationException(CustomObjectDecorator.class,
						"SecurityException while invoking a method from class "+customObject.getClass(),e);
			} catch (NoSuchMethodException e) {
				throw new UncheckedApplicationException(CustomObjectDecorator.class,
						"NoSuchMethodException while invoking a method from class "+customObject.getClass(),e);
			} catch (IllegalArgumentException e) {
				throw new UncheckedApplicationException(CustomObjectDecorator.class,
						"IllegalArgumentException while invoking a method from class "+customObject.getClass(),e);
			} catch (IllegalAccessException e) {
				throw new UncheckedApplicationException(CustomObjectDecorator.class,
						"IllegalAccessException while invoking a method from class "+customObject.getClass(),e);
			} catch (InvocationTargetException e) {
				throw new UncheckedApplicationException(CustomObjectDecorator.class,
						"InvocationTargetException while invoking a method from class "+customObject.getClass(),e);
			}
		}		
	}
	
	/*
	 * Reads the data from the SQLInput stream using appropriate method 
	 * for the defined type in the configuration file
	 */
	private Object getValueForType(Class<?> type, SQLInput stream, String dateType) throws SQLException{	
		Class<?> actualType = getWrapperType(type,dateType);
		String sqlInputMethodName = CallableConfiguration.getInstance().getSqlInputMethodName(actualType);
		try {
			Method method = stream.getClass().getMethod(sqlInputMethodName, new Class[0]);
			Object obj = method.invoke(stream, new Object[0]);			
			return getJavaTypeValue(obj,type);			
		} catch (SecurityException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"SecurityException while invoking a method from class "+customObject.getClass(),e);
		} catch (NoSuchMethodException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"NoSuchMethodException while invoking a method from class "+customObject.getClass(),e);
		} catch (IllegalArgumentException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"IllegalArgumentException while invoking a method from class "+customObject.getClass(),e);
		} catch (IllegalAccessException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"IllegalAccessException while invoking a method from class "+customObject.getClass(),e);
		} catch (InvocationTargetException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"InvocationTargetException while invoking a method from class "+customObject.getClass(),e);
		}	
		
	}
	
	/*
	 * Writes the value to SQLOutput stream using appropriate method 
	 * for the defined type in the configuration file
	 */
	private void setValueForType(Class<?> type, Object value,SQLOutput stream, String dateType) throws SQLException{
		Class<?> actualType = getWrapperType(type, dateType);
		String sqlOutputMethodName = CallableConfiguration.getInstance().getSqlOutputMethodName(actualType);		
		try {	
			Method method = stream.getClass().getMethod(sqlOutputMethodName, getStreamParameterType(actualType));
			method.invoke(stream, getSqlTypeValue(value,dateType));
		} catch (SecurityException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"SecurityException while invoking a method from class "+customObject.getClass(),e);
		} catch (NoSuchMethodException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"NoSuchMethodException while invoking a method from class "+customObject.getClass(),e);
		} catch (IllegalArgumentException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"IllegalArgumentException while invoking a method from class "+customObject.getClass(),e);
		} catch (IllegalAccessException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"IllegalAccessException while invoking a method from class "+customObject.getClass(),e);
		} catch (InvocationTargetException e) {
			throw new UncheckedApplicationException(CustomObjectDecorator.class,
					"InvocationTargetException while invoking a method from class "+customObject.getClass(),e);
		}		
	}
	
	/*
	 * Returns the value as java.util.Date or Calendar depending on the
	 * date type mentioned in the config file
	 */
	private Object getJavaTypeValue(Object value, Class<?> type){
		if(value.getClass().equals(java.sql.Date.class)){
			java.sql.Date dtIn = (java.sql.Date)value;		
			return getConvertedValue(new java.util.Date(dtIn.getTime()), type,dtIn);
		}
		if(value.getClass().equals(java.sql.Timestamp.class)){
			java.sql.Timestamp dtIn = (java.sql.Timestamp)value;
			return getConvertedValue(new java.util.Date(dtIn.getTime()), type, dtIn);
		}		
		return value;
	}
	/*
	 * Converts the value as java.util.Date or Calendar depending on the
	 * date type mentioned in the config file
	 */	
	private Object getConvertedValue(Date date, Class<?> type, Object defaultValue){
		if(type.equals(java.util.Date.class)){
			return date;
		}
		if(type.equals(java.util.Calendar.class) || 
		   type.equals(java.util.GregorianCalendar.class)){
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);			
			return calendar;
		}
		return defaultValue;
	}
	/*
	 * Returns the value as java.sql.Date or java.sql.Timestamp depending on the
	 * date type mentioned in the config file
	 */	
	private Object getSqlTypeValue(Object value, String dateType){
		if(value instanceof java.util.Date){
			java.util.Date dtIn = (java.util.Date)value;
			return getJavaToSqlTypeValue(dtIn, dateType);
		}
		if(value instanceof java.util.Calendar){
			java.util.Calendar dtIn = (java.util.Calendar)value;			
			return getJavaToSqlTypeValue(dtIn.getTime(), dateType);			
		}		
		return value;
	}
	/*
	 * Converts the value as java.sql.Date or java.sql.Timestamp depending on the
	 * date type mentioned in the config file
	 */	
	private Object getJavaToSqlTypeValue(java.util.Date date, String dateType){
		DateType newType = DateType.valueOf(dateType);
		if(newType.equals(DateType.DEFAULT) || newType.equals(DateType.DATE)){
			return new java.sql.Date(date.getTime());
		}
		if(newType.equals(DateType.TIMESTAMP)){
			return new java.sql.Timestamp(date.getTime());
		}
		return date;
	}
		
	private Map<String, String> getAccessorMethods(Class<?> type){		
		List<Method> methods = ReflectionHelper.getAccessorMethods(type);
		return getMethodsMap(methods, false);		
	}
	
	private Map<String, String> getMutatorMethods(Class<?> type){		
		List<Method> methods = ReflectionHelper.getMutatorMethods(type);
		return getMethodsMap(methods, true);
	}
	
	private Map<String, String> getMethodsMap(List<Method> methods, boolean mutator){
		Map<String, String> methodsMap = new HashMap<String, String>();
		for(Method method : methods){
			String methodName = method.getName();
			String key = methodName.toUpperCase();
			if(mutator){
				key = methodName.toUpperCase().substring(3);
			}else{
				if(key.startsWith("GET")){
					key = methodName.toUpperCase().substring(3);
				}else if(key.startsWith("IS")){
					key = methodName.toUpperCase().substring(2);
				}
			}
			methodsMap.put(methodName.toUpperCase().substring(3), methodName);
		}		
		return methodsMap;
	}	
	
	private Class<?> getStreamParameterType(Class<?> type){		
		if(type.equals(Integer.class)){
			return int.class;
		}
		if(type.equals(Long.class)){
			return long.class;
		}	
		if(type.equals(Double.class)){
			return double.class;
		}
		if(type.equals(Float.class)){
			return float.class;
		}	
		if(type.equals(Short.class)){
			return short.class;
		}	
		if(type.equals(Byte.class)){
			return byte.class;
		}
		return type;
	}
	
	private Class<?> getWrapperType(Class<?> type, String dateType){
		if(type.equals(long.class)){
			return Long.class;
		}
		if(type.equals(int.class)){
			return Integer.class;
		}
		if(type.equals(double.class)){
			return Double.class;
		}
		if(type.equals(float.class)){
			return Float.class;
		}	
		if(type.equals(byte.class)){
			return Byte.class;
		}
		if(type.equals(short.class)){
			return Short.class;
		}	
		if(type.equals(char.class)){			
			return Character.class;
		}	
		if(type.equals(java.util.Date.class)||
		   type.equals(java.util.Calendar.class) || 
		   type.equals(java.util.GregorianCalendar.class)){
			DateType newType = DateType.valueOf(dateType);
			if(newType.equals(DateType.DEFAULT) || newType.equals(DateType.DATE)){
				return java.sql.Date.class;
			}
			if(newType.equals(DateType.TIMESTAMP)){
				return java.sql.Timestamp.class;
			}
		}
		return type;
	}
	
}
