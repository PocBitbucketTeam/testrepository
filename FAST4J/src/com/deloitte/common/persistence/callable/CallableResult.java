package com.deloitte.common.persistence.callable;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.deloitte.common.objects.framework.UncheckedApplicationException;


/**
 * A convenient class containing values of the OUT parameters for the executed stored procedure or function.
 *
 * For example if the stored procedure is GET_ALL_EMPLOYEES(deptno IN NUMBER, ecursor OUT emp_cursor, empcount OUT NUMBER)
 * and emp_cursor is "type emp_cursor is ref cursor" that will fetch all employees in the given department
 * and we have Employee POJO then when the procedure is executed, we can access the OUT parameter values in the following way
 * <code>
 *  CallableResult result = calProcessor.execute();
 *  List<Employee> empList = result.getOutValueAsListFor(Employee.class);
 *  Integer empCount = (Integer)result.getOutValue(2); //Here 2 means second OUT parameter
 * </code>
 */
public class CallableResult {
	
	/*
	 * resultMap is the actual result returned from the CallableProcessor.
	 * For the above example the resultMap will contain the values as
	 * [[2,List<Employee>],[3,40]]
	 * The key values in the map are the index values of the parameters 
	 */
	private Map<Integer, Object> resultMap = new TreeMap<Integer, Object>();
	
	/*
	 * processorTypeIndexMap will map the processor type to index map of the actual result map
	 * For the above example processorTypeIndexMap will contain values as
	 * [Employee.class, 2]
	 */
	private Map<Class<?>, Integer> processorTypeIndexMap;
	private int resultMapSize = 0;
	
	public CallableResult(Map<Integer,?> resultMap, Map<Class<?>, Integer> processorTypeIndexMap){
		this.resultMap.putAll(resultMap);
		this.processorTypeIndexMap = processorTypeIndexMap;
		this.resultMapSize = resultMap.size();		
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> getOutValueAsListFor(Class<T> processorType) {
		Integer key = processorTypeIndexMap.get(processorType);
		return (List<T>) resultMap.get(key);		
	}
	
	/**
	 * @return Object the returned value of the First OUT parameter
	 * @throws UncheckedApplicationException
	 */
	public Object getFirstOutValue() throws IndexOutOfBoundsException {
		return get(1);
	}
	
	/**
	 * @return Object the returned value of the Last OUT parameter
	 * @throws UncheckedApplicationException
	 */
	public Object getLastOutValue() throws IndexOutOfBoundsException {		
		return get(resultMapSize);
	}
	
	/**
	 * @param index index index of the OUT parameter in the stored procedure. Index starts from 1
	 * @return Object the returned value of the OUT parameter
	 */
	public Object getOutValue(int index) throws IndexOutOfBoundsException {
		return get(index);
	}
	
	/**
	 * @param index index of the OUT parameter in the stored procedure. Index starts from 1
	 * @return Object the returned value of the OUT parameter
	 */
	private Object get(int index) throws IndexOutOfBoundsException {
		
		if(resultMapSize == 0 || index <=0 || index > resultMapSize){
			 throw new IndexOutOfBoundsException("Invalid Out parameter index of " + index);     
		} else {
			Set<Integer> resultKeySet = resultMap.keySet();
			Integer[] keys = new Integer[resultMapSize];
			keys = resultKeySet.toArray(keys);
			return resultMap.get(keys[index - 1]);
		}
	}
}
