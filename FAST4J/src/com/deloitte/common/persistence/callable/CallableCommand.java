package com.deloitte.common.persistence.callable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import com.deloitte.common.interfaces.Command;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.Error;
import com.deloitte.common.persistence.callable.CallableProcessor;
import com.deloitte.common.persistence.callable.CallableResult;

/**
 * A command implementation to invoke Database stored procedure or function
 */
public class CallableCommand implements Command {
	
	private CallableProcessor processor;

	public CallableCommand(CallableProcessor processor) {
		this.processor = processor;		
	}

	public final Collection<CallableResult> execute() throws CheckedApplicationException {
		CallableResult result = processor.executeCall();
		List<CallableResult> list = new ArrayList<CallableResult>();
		list.add(result);
		return list;
	}

	public final Collection<Error> prepare()throws CheckedApplicationException {
		return Collections.emptyList();
	} 

}
