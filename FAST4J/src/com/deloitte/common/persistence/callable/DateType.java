package com.deloitte.common.persistence.callable;

/**
 * A convenient enum to represent date type 
 *
 */
public enum DateType {
	
	/*
	 * Represents a default value when datetype is not specified in the config file.
	 * The default value will be DATE
	 */
	DEFAULT,
	/*
	 * Represents java.sql.Date type and the conversion between java.util.Date or java.util.Calendar
	 * to java.sql.Date and vice versa will be handled in the DB layer
	 */
	DATE,
	/*
	 * Represents java.sql.Timestamp type and the conversion between java.util.Date or java.util.Calendar
	 * to java.sql.Timestamp  vice versa will be handled in the DB layer
	 */
	TIMESTAMP;
			
}
