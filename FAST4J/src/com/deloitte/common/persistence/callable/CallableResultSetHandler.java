package com.deloitte.common.persistence.callable;

import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.deloitte.common.objects.collection.NullFilter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.persistence.callable.CallableResultSetProcessor;
import com.deloitte.common.persistence.interfaces.ObjectProcessor;


/**
 * The handler will register all the CallableResultSetProcessors using the out Parameter index in the callable
 * statement.
 */
public class CallableResultSetHandler {

	private Map<Integer, CallableResultSetProcessor<?>> handlers = new LinkedHashMap<Integer, CallableResultSetProcessor<?>>();
	private Map<Integer, Object> results = new LinkedHashMap<Integer, Object>();
	
	/**
	 * This method would be used by the application calling the stored procedure. They have to register the
	 * handlers in a sequence. The key used should be Integer(wrapping Parameter index) in case of a registered
	 * out put result set or Integer(with a negative value of the order of retrieval of the ResultSets if any).
	 */
	public <T> void addProcessor(int parameterIndex, ObjectProcessor<T> processor) {
		handlers.put(parameterIndex, new CallableResultSetProcessor<T>(processor));
	}
	
   /* This method would be called by the CallableProcessor while handling the resultset objects it has obtained.
	* As the below methods are used by the CallableProcessor alone with in the package they are not visible to the 
	* application.
	*/
	@SuppressWarnings({"rawtypes", "unchecked"})
	protected void handleResultSet(int index, ResultSet rs) {
			CallableResultSetProcessor<?> resultSetProcessor = handlers.get(index);
			List<?> result;
			try {
				result = resultSetProcessor.handleResultSet(rs, new NullFilter());
			} catch (CheckedApplicationException e) {
				throw new UncheckedApplicationException(this.getClass(), e);
			}
			results.put(index, result);
	}
	
	/* 
	 * Returns the handled results from one of the outpout parameters identified by the index
	 */
	protected Map<Integer, ?> getResults() {
		return results;
	}
	
	/* 
	 * Adds a value to the results map at the specified index. Used to place
	 * results that are not from a ResultSet generated mapped objects.
	 */
	protected void addResult(Integer key,  Object value) {
		results.put(key,value);
	}
	
	
}
