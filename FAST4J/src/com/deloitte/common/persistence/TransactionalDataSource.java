package com.deloitte.common.persistence;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;

/** 
  * To facilitate Transactional control, this class is used to decorate a
  * java.sql.DataSource object. When within the context of a transaction as
  * defined by the TransactionContext, the TransactionalDataSource wrapper 
  * will return the active connection for the transaction, rather than
  * returning a new Connection from the underlying DataSource
*/
public final class TransactionalDataSource implements DataSource
{
	private static final Logger logger = Logger.getLogger(TransactionalDataSource.class.getName());

	private DataSource datasource;
	
	public TransactionalDataSource(DataSource datasource)
	{
		this.datasource = datasource;
	}
	
	public int getLoginTimeout() throws SQLException
	{
		return datasource.getLoginTimeout();
	}

	public void setLoginTimeout(int arg0) throws SQLException
	{
		datasource.setLoginTimeout(arg0);

	}

	public PrintWriter getLogWriter() throws SQLException
	{
		return datasource.getLogWriter();
	}

	public void setLogWriter(PrintWriter arg0) throws SQLException
	{
		datasource.setLogWriter(arg0);
	}

	//TODO need to comment back in when we start compiling in Java 6
//	public <T> T unwrap(Class<T> iface) throws SQLException {
//		return datasource.unwrap(iface);
//	}
//
//	public boolean isWrapperFor(Class<?> iface) throws SQLException {
//		return datasource.isWrapperFor(iface);
//	}

	public Connection getConnection() throws SQLException
	{
		if (TransactionContext.isActiveTransaction()) {
			logger.info("Joining an active transaction");
			Connection transactionalConnection = TransactionContext.getActiveConnection();
			if (transactionalConnection == null){ //this is the first statement in the transaction
				//begin the transaction and set it in the TransactionContext
				TransactionContext.setTransactionConnection(datasource.getConnection());
			}
			return TransactionContext.getActiveConnection();
			//TODO Is there a way here to check if this is a connection for the same DB?
		} else {
			return datasource.getConnection();
		}
	}

	public Connection getConnection(String arg0, String arg1) throws SQLException
	{
		if (TransactionContext.isActiveTransaction()) {
			logger.info("Joining an active transaction");
			Connection transactionalConnection = TransactionContext.getActiveConnection();
			if (transactionalConnection == null){ //this is the first statement in the transaction
				//begin the transaction and set it in the TransactionContext
				TransactionContext.setTransactionConnection(datasource.getConnection(arg0,arg1));
			}
			return TransactionContext.getActiveConnection();
			//TODO Is there a way here to check if this is a connection for the same DB?
		} else {
			return datasource.getConnection(arg0,arg1);
		}
	}

	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	
}
