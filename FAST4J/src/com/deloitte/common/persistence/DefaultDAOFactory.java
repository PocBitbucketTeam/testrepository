package com.deloitte.common.persistence;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.sql.DataSource;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.persistence.interfaces.BatchDataAccessObject;
import com.deloitte.common.persistence.interfaces.DataAccessObject;
import com.deloitte.common.persistence.interfaces.Transactional;

/**
 * DefaultDAOFactory provides a lookup for DAOs/DataSources, with default behavior for
 * AbstractDAO where it sets the DataSource from the DataHandler.
 * <p>
 * Usage:<br>
 * <li>Step 1 - DefaultDAOFactory.register(key, connectionFactory, dao);
 * <li>Step 2 - DataAccessObject dao = DefaultDAOFactory.getInstance().createFor(key);
 * 
 * The DefaultDAOFactory is designed to work with the transaction support features of Fast4J.
 * As such, all created DAOs are returned configured with their DataSources properly 
 * wrapped in TransactionalDataSource decorators so all operations on the DAO will
 * be properly enlisted in any running transactions.
 * 
 * DefaultDAOFactory also supports DAO implementations which do not take DataSource objects
 * and hence do not implement the Transactional interface. To register these DAOs simply
 * register them with the overloaded 2-arg version of the register method, or register them
 * with a NullConnectionFactory. This will prevent the DefaultDAOFactory from trying to inject
 * a DataSource before the DAO is returned.
 * 
 * There are two specific methods for returning DAOs which implement the BatchDataAccessObject
 * interface and provide that supported behavior.
 * 
 * DefaultDAOFactory treats all DAOs as singletons (i.e. the same DAO instance is returned for
 * each key).
 */
public class DefaultDAOFactory 
{
	private static final Logger logger = Logger.getLogger(DefaultDAOFactory.class.getName());

	private static Map<Object, DataAccessObject<?>> daos = new HashMap<Object, DataAccessObject<?>>();
	private static DefaultDAOFactory me = new DefaultDAOFactory();;
	
	public static DefaultDAOFactory getInstance()
	{
		return me;
	}
	
	protected DefaultDAOFactory()
	{
		// Singleton, only construct from instance();
	}
	
	/**
	 * Unregister the DAO for the corresponding key value
	 */
	public void deRegister(Object key)
	{
		daos.remove(key);
	}
	
	/**
	 * This is the preferred method for registering DAOs. Allows the DAO to be registered with a lookup
	 * key of the class of the DomainObject type which the DAO handles persistence for.
	 * @param <T> The type of the DomainObject which the DAO is used for
	 * @param key The key which will later be used to look up the DAO in the various factory methods.
	 * @param factory The ConnectionFactory from which the DAO will get it's connections. If the DAO implementation
	 * does not require a ConnectionFactory, NullConnectionFactory can be supplied.
	 * @param dao The DAO to register
	 */
	public <T extends DomainObject> void register(Class<T> key, ConnectionFactory factory, DataAccessObject<T> dao){
		registerByArtificialKey(key, factory, dao);
	}
	
	/**
	 * Allows DAOs to be registered using any value as the key. Would allow one to register the DAO by something
	 * other than the DomainObject type the DAO is used to access. Useful in scenarios where the same DAO must be
	 * registered multiple times (such as with different ConnectionFactories).
	 * @param key The key which will later be used to look up the DAO in the various factory methods.
	 * @param factory The ConnectionFactory from which the DAO will get it's connections. If the DAO implementation
	 * does not require a ConnectionFactory, NullConnectionFactory can be supplied.
	 * @param dao The DAO to register
	 */
	public <T extends DomainObject> void registerByArtificialKey(Object key, ConnectionFactory factory, DataAccessObject<T> dao){
		//set the DataSource on the DAO
		dao = assignDataSource(dao, factory);
		
		logger.info("Registering a DataAccessHandler(" + key + ")");
		
		daos.put(key, dao);
	}
	
	/**
	 * This version of register allows DefaultDAOFactory to be used with DAO implementations that do not 
	 * implement the Transactional interface and thus cannot have a DataSource injected into them. This
	 * is also equivalent to calling register(thKey, new NullConnectionFactory(), theDao), which will also
	 * allow DAO implementations which do not implement Transactional to be registered.
	 */
	public <T extends DomainObject> void register(Class<T> key, DataAccessObject<T> dao)
	{
		register(key, new NullConnectionFactory(), dao);
	}
	
	/**
	 * This version of register allows DefaultDAOFactory to be used with DAO implementations that do not 
	 * implement the Transactional interface and thus cannot have a DataSource injected into them. This
	 * is also equivalent to calling register(thKey, new NullConnectionFactory(), theDao), which will also
	 * allow DAO implementations which do not implement Transactional to be registered.
	 */
	public <T extends DomainObject> void registerByArtificialKey(Object key, DataAccessObject<T> dao)
	{
		registerByArtificialKey(key, new NullConnectionFactory(), dao);
	}
	
	/**
	 * @param <T> the DomainObject type which the DAO is used to persist
	 * @param key The key used to look up the DAO. Should match the key value used to register the DAO.
	 * @param klass the type of the DataAccessObject that will be returned by the factory
	 * @return the DAO for the specified key
	 */
	public <T extends DomainObject> DataAccessObject<T> createFor(Object key, Class<T> klass)
	{
		return getDAO(key, klass);
	}
	
	/**
	 * @param <T> the DomainObject type which the DAO is used to persist
	 * @param key The key used to look up the DAO. Should match the key value used to register the DAO.
	 * @param klass the type of the DataAccessObject that will be returned by the factory
	 * @return the DAO for the specified key
	 * @throws UncheckedApplicationException if the requested DAO is not a BatchDataAccessObject
	 */
	public <T extends DomainObject> BatchDataAccessObject<T> createBatchDAOFor(Object key, Class<T> klass){
		return getBatchDAO(key, klass);
	}
	
	/**
	 * Used to create a DAO which was registered in the factory using the class of the DomainObject as the 
	 * registration key.
	 * @param <T> The subclass of DomainObject which will be returned from the DAO produced by the factory
	 * @param key The key used to look up the DAO. Should match the key value used to register the DAO. Also
	 * 		used to set the type of the DomainObjects which will be returned by DAO returned by the method.
	 * @return The DataAccessObject associated with the key.
	 */
	public <T extends DomainObject> DataAccessObject<T> createFor(Class<T> key)
	{
		return getDAO(key, key);
	}
	
	/**
	 * Used to create a BatchDAO which was registered in the factory using the class of the DomainObject as the 
	 * registration key.
	 * @param <T> The subclass of DomainObject which will be returned from the DAO produced by the factory
	 * @param key The key used to look up the DAO. Should match the key value used to register the DAO. Also
	 * 		used to set the type of the DomainObjects which will be returned by DAO returned by the method.
	 * @return The DataAccessObject associated with the key.
	 * @throws UncheckedApplicationException if the requested DAO is not a BatchDataAccessObject
	 */
	public <T extends DomainObject> BatchDataAccessObject<T> createBatchDAOFor(Class<T> key){
		return getBatchDAO(key, key);
	}
	
	@SuppressWarnings("unchecked")
	private <T extends DomainObject> DataAccessObject<T> getDAO(Object key, Class<T> klass){
		logger.info("Looking for a DAO to handle " + key);
		
		DataAccessObject<T> theDAO = (DataAccessObject<T>)daos.get(key);
		if (theDAO == null )
		{
			logger.info("Throwing a RuntimeException, No DAO was found to handle " + key);
			throw new RuntimeException("No DAO was found to handle " + key + " Check to make sure DataAccessHandler was registered with the DefaultDAOFactory");
		} 
		
		return theDAO;
	}
	
	private <T extends DomainObject> BatchDataAccessObject<T> getBatchDAO(Object key, Class<T> klass){
		logger.info("Looking for a BatchDAO to handle " + key);
		
		DataAccessObject<T> theDAO = getDAO(key, klass);
		if (!(theDAO instanceof BatchDataAccessObject<?>)){
			logger.info("Throwing a RuntimeException, the DAO for key value " + key + " is not a BatchDataAccessObject");
			throw new UncheckedApplicationException(this.getClass(), "The DAO registered for key " + key + " is not a BatchDataAccessObject");
		} else {
			return (BatchDataAccessObject<T>)theDAO;
		}
	}
	
	protected <T extends DomainObject> DataAccessObject<T> assignDataSource(DataAccessObject<T> theDAO, ConnectionFactory connFactory){
		if (!(connFactory instanceof NullConnectionFactory)){
			//Don't try to assign the DataSource if registered with NullConnectionFactory
			logger.info(theDAO.getClass().getName() + " is setting the Datasource using " + connFactory.getClass().getName());
			DataSource theDataSource = connFactory.getDataSource(connFactory.getClass());
			if (theDataSource == null) {
				// Try again using the String version of the Key
				theDataSource = connFactory.getDataSource(connFactory.getClass().getName());
			}
			if (theDAO instanceof CachedDAO) {
				((Transactional)((CachedDAO<T>)theDAO).getDAO()).setDataSource(new TransactionalDataSource(theDataSource));
			} else {
				((Transactional)theDAO).setDataSource(new TransactionalDataSource(theDataSource));
			}
		}
		return theDAO;
	}
	
}
