package com.deloitte.common.persistence;

import java.util.Collection;
import java.util.logging.Logger;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Use {@link TransactionTemplate} instead. <br><br>
 * 
 * Utility class Provides a hook to execute a code in a single transaction.
 * 
 * {@link #executeInTransaction()} method should be implemented with the code that requires
 * transactional behavior. This method must throw CheckedApplicationException
 * for the code needs to rollback.
 * 
 * NOTE: Be sure to use TransactionalDataSource implementations for proper behavior
 */
@Deprecated
public abstract class TransactionalWrapper {

	private static final Logger logger = Logger
			.getLogger(TransactionalWrapper.class.getName());

	public TransactionalWrapper() {
		super();
	}

	public final Collection<?> execute() throws CheckedApplicationException {
		boolean startedTransaction = false;
		Collection<?> theResults;
		try {
			startedTransaction = TransactionContext.startTransaction();
			theResults = executeInTransaction();
			TransactionContext.markTransactionComplete(startedTransaction);
		} catch (CheckedApplicationException cae) {
			logger.info("Setting transaction to rollback due to exception:  "
					+ cae);
			TransactionContext.setRollbackOnly();
			throw new CheckedApplicationException(this.getClass(),
					"Error occured in executing the code in a transaction", cae);
		} finally {
			if (startedTransaction) {
				TransactionContext.finishTransaction(startedTransaction);
			}
		}
		return theResults;
	}

	public abstract Collection<?> executeInTransaction()
			throws CheckedApplicationException;

}
