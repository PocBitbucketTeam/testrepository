package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Used to perform a batch insert/update operation. Ideal for use in batch environments.
 */
public class SQLBatchUpdate extends SQLProcessor
{
	private static final Logger logger = Logger.getLogger(SQLUpdate.class.getName());

	/**
	 * @param sql The SQL operation to execute
	 * @param parms The parameters for the SQL batch operation. Each outer list represents
	 * one set of params to pass to the SQL call. Thus, if you want to insert or update 10 records
	 * in a table with 4 colums, pass a list with 10 elements, where each item is a list with the 4
	 * values for each column in the DB (i.e. a 10x4 lists of lists)
	 * @return The update count for each individual SQL operation in the batch
	 * @throws CheckedApplicationException
	 */
	public final int[] getBatchUpdateResults(String sql, List<List<?>> parms) throws CheckedApplicationException
	{
		long begin = System.currentTimeMillis();
		PreparedStatement theStatement = null;
		Connection theConnection = null;
		int[] theResults;
		try 
		{
			theConnection = getDataSource().getConnection();
			logger.info("SQLBatchUpdate About to Execute: " + sql + " with " + parms.size() + " records.");
			theStatement = SQLBindHelper.bindBatchParameters(theConnection,sql,parms);
			theResults = theStatement.executeBatch();
		} catch (SQLException e)
		{
			throw new CheckedPersistenceException(this.getClass(),"An error (DB Specific Information: " + e.getErrorCode() + " " + e.getMessage()+  ") has occured in the Data Layer, while retrieving the data SQL:\n\t" + SQLBindHelper.resolveStatement(sql,parms),e);
		} finally 
		{
			logger.info("Persistence Execution Complete, Duration = " + (System.currentTimeMillis() - begin) + " milliseconds");
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
		}
		logger.info("SQLBatchUpdate returning " + Arrays.toString(theResults) + " rows updated" );

		return theResults;
	}	

}
