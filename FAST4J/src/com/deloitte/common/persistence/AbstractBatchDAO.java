package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.interfaces.BatchDataAccessObject;

/**
 * <p>
 * AbstractBatchDAO provides a Data Access default implementation for any 
 * DAO that requires Bulk Insert, Update and Delete operations there by 
 * avoiding the time taken for round trips to database. The subclass implementor 
 * will continue to implement the same methods as Abstract DAO class. Database
 * connections retrieve/release, statement binding and execution are handled
 * by this class.
 * 
 * <p>
 *  There is a slightly greater restriction on the template methods for
 *  getXXXParameters() and getXXXStatement in the AbstractBatchDAO. Due
 *  to the nature of batch updates, one SQL statement will be used for all
 *  objects passed to the call. This one SQL statement will be retrived by
 *  passing the first DomainObject in the List passed to the create, update,
 *  or delete call to the corresponding getXXXStatement method. As a result,
 *  the calls to getXXXParameters() for every entry in the input list must
 *  return a parameters List that contains parameters of the correct size and
 *  type for that statement. Or to state another way, the statement generated
 *  by the first object in the input list must be appropriate for the 
 *  parameters returned by getXXXParameters() for all objects in the input.
 */
public abstract class AbstractBatchDAO<T extends DomainObject> extends AbstractDAO<T> implements BatchDataAccessObject<T>{
	
	private static final Logger logger = Logger.getLogger(AbstractBatchDAO.class.getName());

	public final int[] remove(List<T> theObjects) throws CheckedApplicationException {
		if (theObjects == null || theObjects.size()==0){
			throw new CheckedApplicationException(getClass(),"No objects were provided to be removed");
		}
		List<List<?>> parameters = new ArrayList<List<?>>();
		for(T object : theObjects){
			parameters.add(getDeleteParameters(object));
		}
		return execute(getDeleteStatement(theObjects.get(0)), parameters);
	}

	public final int[] add(List<T> theObjects) throws CheckedApplicationException {
		if (theObjects == null || theObjects.size()==0){
			throw new CheckedApplicationException(getClass(),"No objects were provided to be added");
		}
		List<List<?>> parameters = new ArrayList<List<?>>();
		for(T object : theObjects){
			parameters.add(getInsertParameters(object));
		}
		return execute(getInsertStatement(theObjects.get(0)), parameters);
	}

	public final int[] update(List<T> theObjects) throws CheckedApplicationException {
		if (theObjects == null || theObjects.size()==0){
			throw new CheckedApplicationException(getClass(),"No objects were provided to be updated");
		}
		List<List<?>> parameters = new ArrayList<List<?>>();
		for(T object : theObjects){
			parameters.add(getUpdateParameters(object));
		}
		return execute(getUpdateStatement(theObjects.get(0)), parameters);
	}
	
	private int[] execute(String sql, List<List<?>> parameters) throws CheckedApplicationException {
		long begin = System.currentTimeMillis();
		PreparedStatement theStatement = null;
		Connection theConnection = null;
		try {
				logger.info("About to Execute: " + sql + " with " + parameters.size() + " records.");
				theConnection = getDataSource().getConnection();
				theStatement = (PreparedStatement)SQLBindHelper.bindBatchParameters(theConnection,sql,parameters);
				return theStatement.executeBatch();
		} catch (SQLException e) {
			throw new CheckedPersistenceException(this.getClass(),"An error has occured in the Data Layer, while parsing the Query", e);
		} finally {
			logger.info("Persistence Execution Complete, Duration = " + (System.currentTimeMillis() - begin) + " milliseconds");
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
		}		
	}

	
}
