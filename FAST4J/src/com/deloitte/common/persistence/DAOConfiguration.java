package com.deloitte.common.persistence;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.persistence.interfaces.ConfigurableDataAccessObject;
import com.deloitte.common.persistence.interfaces.DataAccessObject;
import com.deloitte.common.util.ReflectionHelper;

/**
 * Used to store configuration entries for DataAccessObjects
 */
final class DAOConfiguration {
	
	private String key;
	private String factory;
	private String dao;
	private boolean cached;
	private Integer fetchSize;
	private Integer maxRows;
	
	public DAOConfiguration(String key, String factory, String dao, boolean cached) {
		this.key = key;
		this.factory = factory;
		this.dao = dao;
		this.cached = cached;
	}

	public DAOConfiguration(String key, String factory, String dao, boolean cached, Integer fetchSize, Integer maxRows) {
		this.key = key;
		this.factory = factory;
		this.dao = dao;
		this.cached = cached;
		this.fetchSize = fetchSize;
		this.maxRows = maxRows;
	}
	
	@SuppressWarnings("unchecked")
	public DataAccessObject<DomainObject> getDao(){ 
		Object theObj = ReflectionHelper.createFor(dao,new Object[0]);
		if(ConfigurableDataAccessObject.class.isAssignableFrom(theObj.getClass())){
			ConfigurableDataAccessObject theDAO = (ConfigurableDataAccessObject) theObj;
			theDAO.setFetchSize(fetchSize);
			theDAO.setMaxRows(maxRows);
		}
		if(DataAccessObject.class.isAssignableFrom(theObj.getClass())){
			DataAccessObject<DomainObject> theDAO = (DataAccessObject<DomainObject>) theObj;
			if (cached){
				return new CachedDAO<DomainObject>(theDAO);	
			}else{
				return theDAO;
			} 
		}else{
			throw new UncheckedApplicationException(this.getClass(),  "DataAccessObject type is expected, but found "+dao);
		}
	}
	
	public ConnectionFactory getFactory(){
		Object theObj = ReflectionHelper.createFor(factory,new Object[0]);
		if(ConnectionFactory.class.isAssignableFrom(theObj.getClass())){
			return (ConnectionFactory) theObj;
		}else{
			throw new UncheckedApplicationException(this.getClass(), "ConnectionFactory type is expected, but found "+factory);
		}
	}

	@SuppressWarnings("unchecked")
	public Class<DomainObject> getKey(){
		Class<DomainObject> theKey = (Class<DomainObject>) ReflectionHelper.createFor(key);
		return theKey; 	
	}


	public String toString(){
		return "\n\tKey=" + getKey() + ",\n\tFactory=" + getFactory() + ",\n\tDAO="+ getDao() + ",\n\tCached=" + cached;
	}

}
