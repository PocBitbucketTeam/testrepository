package com.deloitte.common.persistence;

/**
 * Used to demarcate a transaction which should be executed with transactional
 * semantics.
 *
 * @param <T> the type of object which will be returned from the transaction
 * execution
 */
public abstract class Transaction<T> {

	/**
	 * Defines the operation which should be performed as a transaction
	 */
	protected abstract T doInTrans();

	/**
	 * Will force the transaction to be rolled back after {@link #doInTrans()}
	 * is completed.
	 */
	protected final void setRollBackOnly(){
		TransactionContext.setRollbackOnly();
	}

	/**
	 * Queries the rollback status of the transaction
	 */
	protected final boolean isRollBackOnly(){
		return TransactionContext.isRollbackOnly();
	}

}
