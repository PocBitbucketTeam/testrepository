package com.deloitte.common.persistence.interfaces;

import javax.sql.DataSource;

public interface Transactional
{
	public DataSource getDataSource();
	public void setDataSource(DataSource ds);
}
