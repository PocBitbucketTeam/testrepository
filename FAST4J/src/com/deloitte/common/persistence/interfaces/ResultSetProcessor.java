package com.deloitte.common.persistence.interfaces;

import java.sql.ResultSet;
import java.util.List;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Defines the behavior which will be used to transform JDBC ResultSets
 * into objects.
 *
 * @param <T> The type of object which will be returned in the transformed list
 */
public interface ResultSetProcessor<T>
{
	public List<T> handleResultSet(ResultSet rs, Filter<T> f) throws CheckedApplicationException;
}
