package com.deloitte.common.persistence.interfaces;

import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;

public interface DomainObjectProcessor<T extends DomainObject>
{
	public T map(Map<String,?> theRow) throws CheckedApplicationException;
}
