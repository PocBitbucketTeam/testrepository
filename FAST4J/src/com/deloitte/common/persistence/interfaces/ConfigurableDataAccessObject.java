package com.deloitte.common.persistence.interfaces;

/**
 * Describes the additional JDBC parameters that will be used in the invocation
 * to the underlying driver. 
 */
public interface ConfigurableDataAccessObject {
	
	/**
	 * @return the fetch size that will be used with this DAO
	 */
	public Integer getFetchSize();
	
	/**
	 * @param fetchSize the fetch size that will be used by default with this DAO.
	 * If not specified, no value will be passed to the JDBC driver
	 */
	public void setFetchSize(Integer fetchSize);
	
	/**
	 * @return the maxRows value that will be used when invoking the underlying JDBC call.
	 */
	public Integer getMaxRows();

	/**
	 * @param maxRows the maxRows value that will be used when invoking the underlying JDBC call.
	 * If not specified, no value will be passed to the JDBC driver
	 */
	public void setMaxRows(Integer maxRows);

}
