package com.deloitte.common.persistence.interfaces;

import java.util.List;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Defines additional set of data operations that can act upon a DomainObject using. The essential
 * bulk operations are defined and should be supported by concrete implementations.
 */
public interface BatchDataAccessObject<T extends DomainObject> extends DataAccessObject<T> {

	public int[] update(List<T> theObject) throws CheckedApplicationException;
	
	public int[] add(List<T> theObject) throws CheckedApplicationException;
	
	public int[] remove(List<T> theObject) throws CheckedApplicationException;

}
