package com.deloitte.common.persistence.interfaces;

import java.util.Collection;
import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * Defines set of data operations that can act upon a DomainObject. The essential
 * CRUD operations are defined and should be supported by concrete
 * implementations.
 * @author SIDT Framework Team
 *
 */
public interface DataAccessObject<T extends DomainObject>
{
	public Collection<T>   getAll(Map<?, ?> parameters) throws CheckedApplicationException;
	public Collection<T> 	getAll(Map<?, ?> parameters, Filter<T> theFilter) throws CheckedApplicationException;
	public T            get(Map<?, ?> parameters) throws CheckedApplicationException;
	public T            findByKey(Object key, Object value) throws CheckedApplicationException;	
	public void			update(T theObject) throws CheckedApplicationException;
	public void			add(T theObject) throws CheckedApplicationException;
	public void 		remove(T theObject) throws CheckedApplicationException;

}
