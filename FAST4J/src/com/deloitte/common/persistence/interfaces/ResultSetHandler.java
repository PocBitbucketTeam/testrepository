package com.deloitte.common.persistence.interfaces;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Provides callback handler for manipulation of ResultSets prior to being
 * passed into AbstractDAO.map method.
 *  
 * @author SIDT Framework Team
 *
 */
public interface ResultSetHandler
{
	public Object getResultSetValue(Object o) throws CheckedApplicationException;
}
