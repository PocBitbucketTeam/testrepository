package com.deloitte.common.persistence.interfaces;


import java.util.Map;

import com.deloitte.common.objects.framework.CheckedApplicationException;


/**
 * Used to define mapping logic from a Map to a different type of Java object.
 * This is most commonly used in the framework OR mapping mechanism where an ObjectProcessor
 * is used to define the conversion of map representing a row received from the database query
 * into a java object.
 */
public interface ObjectProcessor<T> {

	/**
	 * Defines how to convert the data received in a Map
	 * into a Java object. It can be any Object - ex : DomainObject  
	 * <br><br>
	 * Example: Assuming we got a map with userid and password, we are creating a credential object.<p>
	 * <code>
	 * public class MyObjectMapper implements ObjectMapper {<br>
	 * public Object map(Map map)throws CheckedApplicationException
	 * {
	 * OID oid = (OID) map.get("USERID");
	 * String password=(String)map.get("PASSWORD");
	 * 
	 * return new UserCredential(oid, password);
	 * }
	 * </code>
	 */	
	public T map(Map<String, ?> map) throws CheckedApplicationException;

}