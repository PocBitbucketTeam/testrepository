package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.objects.collection.NullFilter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.database.BasicResultSetProcessor;
import com.deloitte.common.persistence.interfaces.ResultSetProcessor;

/**
 * Used to execute ad-hoc SQL. If a ResultSetProcessor is not provided,
 * they will be returned in the format of a {@link BasicResultSetProcessor}<br><br>
 * 
 * If a fetch size is specified, it will be passed to the DB driver when the statement is
 * executed. If a fetch size is not specified, no fetch size value will be specified when
 * executing the statement and thus the default behavior for the driver will be used.
 * The effect of this is driver-specific, so refer to your DB documentation for details.
 * This would most likely be used if you are retrieving a very large result set and need 
 * to tune the fetch size for performance reasons.
 *  
 * @see com.deloitte.common.persistence.interfaces.ResultSetProcessor ResultSetProcessor
 * @see com.deloitte.common.persistence.database.BasicResultSetProcessor BasicResultSetProcessor
 */
public class SQLQuery extends SQLProcessor
{
	private static final Logger logger = Logger.getLogger(SQLQuery.class.getName());

	private ResultSetProcessor<?> processor;
	private Integer fetchSize = null;  //need to distinguish between 0 and an unspecified value
	private Integer maxRows = null;  //need to distinguish between 0 and an unspecified value

	public SQLQuery(){
		this(new BasicResultSetProcessor());
	}
	
	public SQLQuery(int fetchSize){
		this(new BasicResultSetProcessor(), fetchSize);
	}
	
	public SQLQuery(ResultSetProcessor<?> processor){
		this.processor = processor;
	}
	
	/**
	 * @param processor The ResultSetProcessor used to tranform query results into 
	 * 				the results List returned
	 * @param fetchSize The fetchSize parameter to set when executing the statement
	 */
	public SQLQuery(ResultSetProcessor<?> processor, int fetchSize){
		this(processor);
		this.fetchSize = fetchSize;
	}
	
	/**
	 * @param processor The ResultSetProcessor used to tranform query results into 
	 * 				the results List returned
	 * @param maxRows The maxRows parameter to set when executing the statement
	 */
	public SQLQuery(int maxRows, ResultSetProcessor<?> processor){
		this(processor);
		this.maxRows = maxRows;
	}
	
	/**
	 * @param processor The ResultSetProcessor used to tranform query results into 
	 * 				the results List returned
	 * @param fetchSize The fetchSize parameter to set when executing the statement
	 * @param maxRows The maxRows parameter to set when executing the statement
	 */
	public SQLQuery(ResultSetProcessor<?> processor, int fetchSize, int maxRows){
		this(processor);
		this.fetchSize = fetchSize;
		this.maxRows = maxRows;
	}
		
	public Integer getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(Integer maxRows) {
		this.maxRows = maxRows;
	}

	/**
	 * Same as {@link #getQueryResults(String, List)}, but used for
	 * SQL statements with no parameters
	 */
	public final Collection<?> getQueryResults(String sql) throws CheckedApplicationException
	{
		return getQueryResults(sql,Collections.EMPTY_LIST);
	}
	
	/**
	 * Executes the query and returns the results.
	 * @param sql the SQL statement to be executed
	 * @param parms the parameters which will be passed to the SQL statement
	 * @return the result of the statement execution. The entries in the list
	 * are determined by the type of ResultSetProcessor the SQLQuery is using
	 * ({@link BasicResultSetProcessor} by default)
	 * @throws CheckedApplicationException
	 */
	public final Collection<?> getQueryResults(String sql, List<?> parms) throws CheckedApplicationException {
		long begin = System.currentTimeMillis();
		PreparedStatement theStatement = null;
		Connection theConnection = null;
		ResultSet theResultSet = null;
		List<?> theResults;
		try {
			theConnection = getDataSource().getConnection();
			logger.info("SQLProcessor about to Execute: " + SQLBindHelper.resolveStatement(sql,parms));
			theStatement = SQLBindHelper.bindParameters(theConnection,sql,parms);
			if (fetchSize != null){
				logger.info("SQLProcessor setting statement fetchSize to " + fetchSize);
				theStatement.setFetchSize(fetchSize);
			}
			if (maxRows != null){
				logger.info("SQLProcessor setting statement maxRows to " + maxRows);
				theStatement.setMaxRows(maxRows);
			}
			theResultSet = theStatement.executeQuery();
			theResults = handleResultSet(theResultSet);
		} catch (SQLException e) {
			throw new CheckedPersistenceException(this.getClass(),"An error (DB Specific Information: " + e.getErrorCode() + " " + e.getMessage()+  ") has occured in the Data Layer, while retrieving the data SQL:\n\t" + SQLBindHelper.resolveStatement(sql,parms),e);
		} finally {
			logger.info("Persistence Execution Complete, Duration = " + (System.currentTimeMillis() - begin) + " milliseconds");
			ConnectionFactory.releaseResultSet(theResultSet);
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
		}
			
		logger.info("SQLProcessor Returning " + theResults.size() + " rows");
		
		return theResults;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	protected final List<?> handleResultSet(ResultSet rs) throws CheckedApplicationException {
		return getResultSetProcessor().handleResultSet(rs, new NullFilter());
	}

	protected ResultSetProcessor<?> getResultSetProcessor()
	{
		return this.processor;
	}
	
}
