package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * 
 */
//TODO this needs Javadoc
public class SQLUpdate extends SQLProcessor
{
	private static final Logger logger = Logger.getLogger(SQLUpdate.class.getName());

	public SQLUpdate()
	{
		super();
	}
	
	public final int getUpdateResults(String sql) throws CheckedApplicationException
	{
		return getUpdateResults(sql,Collections.EMPTY_LIST);
	}
	
	public final int getUpdateResults(String sql, List<?> parms) throws CheckedApplicationException
	{
		long begin = System.currentTimeMillis();
		PreparedStatement theStatement = null;
		Connection theConnection = null;
		int theResults = -1;
		try 
		{
			theConnection = getDataSource().getConnection();
			logger.info("SQLProcessor About to Execute: " + SQLBindHelper.resolveStatement(sql,parms));
			theStatement = SQLBindHelper.bindParameters(theConnection,sql,parms);
			theResults = theStatement.executeUpdate();
		} catch (SQLException e)
		{
			throw new CheckedPersistenceException(this.getClass(),"An error (DB Specific Information: " + e.getErrorCode() + " " + e.getMessage()+  ") has occured in the Data Layer, while retrieving the data SQL:\n\t" + SQLBindHelper.resolveStatement(sql,parms),e);
		} finally 
		{
			logger.info("Persistence Execution Complete, Duration = " + (System.currentTimeMillis() - begin) + " milliseconds");
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
		}
		logger.info("SQLProcessor Returning " + theResults + " Rows updated" );

		return theResults;
	}
	
}
