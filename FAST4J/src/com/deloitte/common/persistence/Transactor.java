package com.deloitte.common.persistence;

/**
 * Used to execute a set of operations in a transaction. In order to work properly,
 * all the DataSources participating in the transaction must be wrapped with a
 * {@link TransactionalDataSource}
 */
public class Transactor {

	/**
	 * Invokes the {@link Transaction#doInTrans()} method, including the operation
	 * with transactional behavior.<br/><br/>
	 * 
	 * If a runtime Exception occurs, the transaction will be automatically rolled back.
	 * 
	 * @param transaction the transaction to execute
	 * @return the result of the call from {@link Transaction#doInTrans()}
	 */
	public static <T> T execute(Transaction<T> transaction) {
		boolean startedTransaction = false;
		try {
			startedTransaction = TransactionContext.startTransaction();

			T result = transaction.doInTrans();

			TransactionContext.markTransactionComplete(startedTransaction);
			
			return result;
		} finally {
			TransactionContext.finishTransaction(startedTransaction);
		}
	}
	
}
