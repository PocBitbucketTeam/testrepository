package com.deloitte.common.persistence;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.util.ReflectionHelper;

/** 
  * To facilitate Transactional control, a Dynamic Proxy is created around the
  * java.sql.Statement object. 
  * 
  * The primary purpose is to return the NullObjectConnection object.
  * This allows us to control access to the Connection and keep subclasses
  * from interfering with the transaction.
  * 
  * @see GRAND98 NullObject Pattern
  * 
  * @author SIDT Framework Team
 **/

final class TransactionalStatement implements java.lang.reflect.InvocationHandler
{
	 private Object obj;
	 private static NullObjectConnection nullObject = new NullObjectConnection();

    public static Object newInstance(Object obj) 
	{
	return java.lang.reflect.Proxy.newProxyInstance(
	    obj.getClass().getClassLoader(),
	    ReflectionHelper.getDeclaredInterfaces(obj.getClass()),
	    new TransactionalStatement(obj));
    }

    private TransactionalStatement(Object obj) 
	{
		this.obj = obj;
    }

    public Object invoke(Object proxy, Method m, Object[] args) throws Throwable
    {
        Object result;
		try 
		{
			if ("getConnection".equals(m.getName()))
			{
				result = nullObject;
			} else
			{
				result = m.invoke(obj, args);
			}
	     } catch (InvocationTargetException e) 
		 {
			throw e.getTargetException();
	     } catch (Exception e) 
	     {
	       	throw new UncheckedApplicationException(getClass(),"Unable to invoke on " + proxy + ":" + m,e);
	     }
	return result;
    }

}
