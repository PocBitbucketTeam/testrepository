package com.deloitte.common.persistence;

import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.domain.AbstractDomainObject;
import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * DomainObject to represent a pair [UUID,UUID]. May be used as a standard
 * DomainObject implementation for the association tables in many-to-many
 * relational mappings.
 */
public class Association extends AbstractDomainObject {

	//there are no fields in association as the entirety of the state
	//for this class is contained in it's UUID implementation
	
	private static final long serialVersionUID = 1258054942718145942L;
	
	public Association(UUID parentId, UUID childId) {
		super(new ID(parentId, childId));
	}

	public UUID getParentId() {
		return ((ID)getID()).getParentId();
	}

	public UUID getChildId() {
		return ((ID)getID()).getChildId();
	}
	

	
	/**
	 * Provides a valid implementation of a UUID for the Association class. It incorporates
	 * both UUID values into a composite ID value which has valid implementations of all
	 * UUID methods
	 */
	public static class ID implements UUID {

		private static final long serialVersionUID = -3385170005321006984L;
		
		private UUID parentId;
		private UUID childId;

		private int	hashcode = -1;
		
		
		public ID(UUID parentId, UUID childId) {
			super();
			if (parentId == null || childId == null){
				throw new UncheckedApplicationException(this.getClass(), "Key values cannot be null.");
			}
			this.parentId = parentId;
			this.childId = childId;
		}

		public UUID getParentId() {
			return parentId;
		}

		public UUID getChildId() {
			return childId;
		}

		public boolean equals(Object obj) {
			if (obj == null || !(obj instanceof ID)){
				return false;
			} else {
				return (this.compareTo((UUID)obj) == 0);
			}
		}

		public int compareTo(UUID arg0) {
			if (arg0 == null){
				throw new UncheckedApplicationException(this.getClass(), "Invalid argument. Cannot compare to null");
			} else if (!(arg0 instanceof ID)){
				throw new UncheckedApplicationException(this.getClass(), "Invalid argument. Instances of "
						+ arg0.getClass().getName() + " not comparable with " + this.getClass().getName());
			} else {
				ID otherID = (ID)arg0;
				int parentCompareResult = this.getParentId().compareTo(otherID.getParentId());
				if ( parentCompareResult != 0){
					return parentCompareResult;
				} else {
					return this.getChildId().compareTo(otherID.getChildId());
				}
			}
		}

		public int hashCode() {
			if (hashcode == -1) {
				hashcode = parentId.hashCode() + childId.hashCode();
			}
			return hashcode;
		}

		public String toString() {
			return "[" + parentId + "/" + childId + "]";
		}

		public Object getValue(){
			return this;
		}

	}
}
