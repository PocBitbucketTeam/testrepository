package com.deloitte.common.persistence.database;

import java.util.List;
import java.util.Map;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * BasicResultSetProcessor returns a List of Map Objects, each Map represents a
 * single row of the resultSet, with column name as the key and column value as
 * value
 * 
 * @author SIDT Team
 * 
 */
public class BasicResultSetProcessor extends AbstractResultSetProcessor<Map<String, ?>> {

	protected void processTheRow(List<Map<String, ?>> theList, Map<String, ?> theRow) throws CheckedApplicationException {
		if (this.getFilter().include(theRow)){
			theList.add(theRow);
		}
	}

}
