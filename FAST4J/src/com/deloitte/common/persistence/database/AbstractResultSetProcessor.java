package com.deloitte.common.persistence.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.CheckedPersistenceException;
import com.deloitte.common.persistence.interfaces.ResultSetProcessor;

/**
 * Used to convert a JDBC ResultSet into a list of objects.<br><br>
 * 
 * Note: AbstractResultSetProcessor and it's subclasses are not thread-safe. Do
 * not re-use instances across multiple threads for processing ResultSets.
 *
 * @param <T> the type of objects that the ResultSet will be converted into
 */
public abstract class AbstractResultSetProcessor<T> implements ResultSetProcessor<T> {

	private Filter<T> theFilter;
	private String tableName;

	public List<T> handleResultSet(ResultSet rs, Filter<T> filter) throws CheckedApplicationException {
		theFilter = filter;
		return handleResultSet(rs);
	}

	private List<T> handleResultSet(ResultSet rs) throws CheckedApplicationException {
		List<T> theList = getListToPopulate();
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			tableName = rsmd.getTableName(1);
			int columnCount = rsmd.getColumnCount();
			List<String> rowLabels = new ArrayList<String>();
			for (int i = 0; i < columnCount; i++) {
				rowLabels.add(rsmd.getColumnLabel(i + 1).toUpperCase());
			}
			ResultSetManager manager = new ResultSetManager();
			while (rs.next()) {
				Map<String, Object> theRow = new HashMap<String, Object>();
				Object theColumnValue = null;
				for (String theColumnName : rowLabels) {
					theColumnValue = manager.getResultSetValue(rs.getObject(theColumnName));
					theRow.put(theColumnName, theColumnValue);
				}
				processTheRow(theList, theRow);
			}
		} catch (SQLException e) {
			throw new CheckedPersistenceException(
					this.getClass(),
					"An error has occured in the Data Layer, while handling the results",
					e);
		}
		return theList;
	}

	protected abstract void processTheRow(List<T> theList, Map<String, ?> theRow) throws CheckedApplicationException;

	/**
	 * This factory method can be overridden in subclasses in order to return a different concrete
	 * List type from the ResultSetProcessor. By default, it uses an ArrayList.
	 * @return The empty List implementation that will be populated by the result set processing
	 */
	protected List<T> getListToPopulate(){
		return new ArrayList<T>();
	}

	protected Filter<T> getFilter()	{
		return theFilter;
	}

	protected String getTableName()	{
		return tableName;
	}
}
