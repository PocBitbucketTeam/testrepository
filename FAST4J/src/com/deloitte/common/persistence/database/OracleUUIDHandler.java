package com.deloitte.common.persistence.database;

import com.deloitte.common.persistence.ConnectionFactory;

/**
 * Use ({@link DefaultUUIDHandler}) instead <br><br>
 *
 * OracleUUIDHandler class is a implementation of UUIDHandler for Oracle database to
 * generate sequence ID's.
 *
 */
@Deprecated
public class OracleUUIDHandler extends UUIDHandler
{
	public OracleUUIDHandler(Object key, ConnectionFactory factory)
	{
		super(key,factory);
	}

	public OracleUUIDHandler(Object key, ConnectionFactory factory, long refreshRate)
	{
		super(key,factory,refreshRate);
	}

	public String getSql()
	{
		return "select " + getKey() + ".nextval from dual";
	}
}
