package com.deloitte.common.persistence.database;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.deloitte.common.objects.domain.BinaryDocument;
import com.deloitte.common.objects.domain.TextDocument;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.interfaces.ResultSetHandler;

/**
 * The manager will invoke all of the registered ResultSetHandlers in the order with which they
 * were registered.
 */
public class ResultSetManager implements ResultSetHandler
{
	private static final Logger logger = Logger.getLogger(ResultSetManager.class.getName());

	private static List<ResultSetHandler> handlers = new ArrayList<ResultSetHandler>();
	
	@Deprecated 
	public static void register(Object o)
	{
		if (ResultSetHandler.class.isAssignableFrom(o.getClass()))
		{
			logger.info(o.getClass() + " has been registered as a ResultSetHander");
			handlers.add((ResultSetHandler)o);
		}
	}
	
	public static void register(ResultSetHandler o)	{
		handlers.add(o);
	}
	
	public static void clear() {
		for (ResultSetHandler i : handlers) {
			logger.info(i.getClass() + " has been deregistered as a ResultSetHander");	
		}
		handlers.clear();
		
	}
	/**
	 * 
	 * Call all the registered handler, if there is one and process
	 * Note, the order of registration will affect the order of handler invocation, 
	 * so a chaining effect can be achieved
	 *  
	 */
	public Object getResultSetValue(Object o) throws CheckedApplicationException
	{
		Object theReturnObject = o;
		if (o != null && o instanceof java.sql.Clob)
		{
			try
			{
				Reader reader = ((Clob) o).getCharacterStream();
				theReturnObject = new TextDocument(reader);
				reader.close();
			}
			catch(SQLException e)
			{
				throw new CheckedApplicationException(this.getClass(),e.getMessage());
			}
			catch(IOException e)
			{
				throw new CheckedApplicationException(this.getClass(),e.getMessage());
			}
		} else if (o != null && ( o instanceof java.sql.Blob || o instanceof byte[]))
		{
			try
			{
				if(o instanceof Blob)
				{
					InputStream stream = ((Blob)o).getBinaryStream();
					theReturnObject = new BinaryDocument(stream);
					stream.close();
				}
				else if (o instanceof byte[])
				{
					theReturnObject = new BinaryDocument((byte[])o);
				}
			}
			catch(SQLException e)
			{
				throw new CheckedApplicationException(this.getClass(),e.getMessage());
			}
			catch(IOException e)
			{
				throw new CheckedApplicationException(this.getClass(),e.getMessage());
			}
		}
		
		for (ResultSetHandler rsHandler : handlers) {
			theReturnObject = rsHandler.getResultSetValue(theReturnObject);
		}

		return theReturnObject;
	}

}
