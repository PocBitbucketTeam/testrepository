package com.deloitte.common.persistence.database;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.persistence.ConnectionFactory;
import com.deloitte.common.util.ReflectionHelper;

final class UUIDHandlerConfiguration {
	
	private String key;
	private String factory;
	private String sql;
	public UUIDHandlerConfiguration(String key, String factory, String sql) {
		this.key = key;
		this.factory = factory;
		this.sql = sql;
	
	}
	
	@SuppressWarnings("unchecked")
	public String getSql(){ 
		return sql;
	}
	
	public ConnectionFactory getFactory(){
		Object theObj = ReflectionHelper.createFor(factory,new Object[0]);
		if(ConnectionFactory.class.isAssignableFrom(theObj.getClass())){
			return (ConnectionFactory) theObj;
		}else{
			throw new UncheckedApplicationException(this.getClass(), "ConnectionFactory type is expected, but found "+factory);
		}
	}

	@SuppressWarnings("unchecked")
	public Class<DomainObject> getKey(){
		Class<DomainObject> theKey = (Class<DomainObject>) ReflectionHelper.createFor(key);
		return theKey; 	
	}


	public String toString(){
		return "\n\tKey=" + getKey() + ",\n\tFactory=" + getFactory() + ",\n\tSql="+ getSql() ;
	}

}
