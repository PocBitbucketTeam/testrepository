package com.deloitte.common.persistence.database;

import com.deloitte.common.persistence.ConnectionFactory;

/**
 * Use ({@link DefaultUUIDHandler}) instead <br><br>
 * 
 * HsqlUUIDHandler class is a implementation of UUIDHandler for Hsql database to
 * generate sequence ID's.
 * 
 */
@Deprecated
public class HsqlUUIDHandler extends UUIDHandler
{
	public HsqlUUIDHandler(Object key, ConnectionFactory factory)
	{
		super(key,factory);
	}
	
	public HsqlUUIDHandler(Object key, ConnectionFactory factory, long refreshRate)
	{
		super(key,factory,refreshRate);
	}	
	
	public String getSql()
	{
		return "select next value for "+getKey()+" as sequence from information_schema.system_sequences where sequence_name = '" + getKey().toString().toUpperCase() +"'"; 
		//return "select next value for " + getKey() + " from sequence"; 

	}
}
