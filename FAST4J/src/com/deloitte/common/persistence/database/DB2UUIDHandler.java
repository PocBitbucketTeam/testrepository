package com.deloitte.common.persistence.database;

import com.deloitte.common.persistence.ConnectionFactory;

/**
 * Use ({@link DefaultUUIDHandler}) instead <br><br>
 * 
 * DB2UUIDHandler class is a implementation of  UUIDHandler for DB2 database to generate sequence ID's.
 * 
 */
@Deprecated
public class DB2UUIDHandler extends UUIDHandler
{
	public DB2UUIDHandler(Object key, ConnectionFactory factory)
	{
		super(key,factory);
	}
		
	public DB2UUIDHandler(Object key, ConnectionFactory factory, long refreshRate)
	{
		super(key,factory,refreshRate);
	}
		
	public String getSql()
	{
		return "values nextval for " + getKey();
	}
}
