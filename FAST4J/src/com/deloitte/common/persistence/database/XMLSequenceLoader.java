package com.deloitte.common.persistence.database;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.AbstractXMLLoader;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.UncheckedApplicationException;
import com.deloitte.common.persistence.ConnectionFactory;
import com.deloitte.common.persistence.database.DefaultUUIDFactory;
import com.deloitte.common.persistence.database.UUIDHandler;
import com.deloitte.common.util.ReflectionHelper;

/**
 * Uses either external XML file or Document object to register sequence with
 * the DefaultUUIDFactory
 * 
 * The format of the xml file must be
 * 
 * <pre>
 * <configuration>
 * <registration>
 * <sql> select ${sequence}.nextval from dual</sql>
 * <sequence key="com.deloitte.bo.EmployeeBO" 
 * 			factory="com.deloitte.bo.DataConnectionFactory" 
 * 			sequence="EMP_SEQ" 
 * 			refresh="2" 
 * 			descending="true"/>
 * </registration>
 * </configuration>
 * </pre>
 * 
 * @author SIDT Framework Team
 * 
 */

public class XMLSequenceLoader extends AbstractXMLLoader {

	private static String SEQUENCE_EXPRN = "\\$\\{sequence}";

	public XMLSequenceLoader(Document document) {

		super(document);
	}

	public XMLSequenceLoader(String fileName) {

		super(fileName);
	}

	@Override
	protected void loadValues(Document xmlDoc)
			throws CheckedApplicationException {

		NodeList sqlNodeList = xmlDoc.getElementsByTagName("sql");
		String sqlString = sqlNodeList.item(0).getFirstChild().getNodeValue();
		NodeList sequences = xmlDoc.getElementsByTagName("sequence");
		Node node = null;
		NamedNodeMap map = null;
		String key = null;
		String factory = null;
		String sequence = null;

		if (sequences != null) {
			for (int i = 0; i < sequences.getLength(); ++i) {

				node = sequences.item(i);
				map = node.getAttributes();
				key = map.getNamedItem("key").getNodeValue();
				factory = map.getNamedItem("factory").getNodeValue();
				sequence = map.getNamedItem("sequence").getNodeValue();
				String sql = new String(sqlString.toString());
				sql = sql.replaceAll(SEQUENCE_EXPRN, sequence);
				UUIDHandlerConfiguration configuration = new UUIDHandlerConfiguration(
						key, factory, sql);
				UUIDHandler handler = new DefaultUUIDHandler(
						configuration.getKey(), configuration.getSql(),
						configuration.getFactory());

				if (null != map.getNamedItem("refresh")) {
					long refreshRate = Long.valueOf(
							map.getNamedItem("refresh").getNodeValue())
							.longValue();
					boolean descending = false;

					if (null != map.getNamedItem("descending")) {
						descending = Boolean.valueOf(map.getNamedItem(
								"descending").getNodeValue());
					}

					handler = new DefaultUUIDHandler(configuration.getKey(),
							configuration.getSql(), configuration.getFactory(),
							refreshRate, descending);
				}
				DefaultUUIDFactory.register(handler);
			}

		}

	}

	private static class UUIDHandlerConfiguration {

		private String key;
		private String factory;
		private String sql;

		public UUIDHandlerConfiguration(String key, String factory, String sql) {
			this.key = key;
			this.factory = factory;
			this.sql = sql;
		}

		public String getSql() {
			return sql;
		}

		public ConnectionFactory getFactory() {
			Object theObj = ReflectionHelper.createFor(factory, new Object[0]);
			if (ConnectionFactory.class.isAssignableFrom(theObj.getClass())) {
				return (ConnectionFactory) theObj;
			} else {
				throw new UncheckedApplicationException(this.getClass(),
						"ConnectionFactory type is expected, but found "
								+ factory);
			}
		}

		@SuppressWarnings("unchecked")
		public Class<DomainObject> getKey() {
			Class<DomainObject> theKey = (Class<DomainObject>) ReflectionHelper
					.createFor(key);
			return theKey;
		}

		public String toString() {
			return "\n\tKey=" + getKey() + ",\n\tFactory=" + getFactory()
					+ ",\n\tSql=" + getSql();
		}
	}
}
