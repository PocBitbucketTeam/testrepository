package com.deloitte.common.persistence.database;

import java.util.Collection;

import com.deloitte.common.commands.CommandManager;
import com.deloitte.common.commands.persistence.SQLCommand;
import com.deloitte.common.commands.persistence.SQLCommandManager;
import com.deloitte.common.interfaces.Loader;
import com.deloitte.common.interfaces.SimpleObject;
import com.deloitte.common.objects.business.SimpleObjectAccessor;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.ConnectionFactory;
import com.deloitte.common.persistence.SQLQuery;

/**
 * Use a {@link com.deloitte.common.objects.reftable.ReferenceTable ReferenceTable}
 * instead <br><br>
 */
@Deprecated
public class UnitedStates extends com.deloitte.common.objects.business.UnitedStates implements Loader
{
	private boolean valuesLoaded = false;
	private ConnectionFactory factory;
	private static Loader me;
	
	/**
	 * @deprecated
	 * @see #UnitedStates(ConnectionFactory)
	 */
	protected UnitedStates()
	{
		super();
	}
	
	protected UnitedStates(ConnectionFactory factory)
	{
		this.factory = factory;
	}

	public static Loader getInstance(ConnectionFactory factory)
	{
		if (me == null)
		{
			me = new UnitedStates(factory);
		}
		return me;
	}
	/**
	 * @deprecated Calling this method without ConnectionFactory
	 *  can't load the values from the DB.
	 * @see #getInstance(ConnectionFactory)
	 */
	public static Loader getInstance()
	{
		if (me == null)
		{
			me = new UnitedStates();
		}
		return me;
		
	}
	
	//TODO think this not a good way to remove the warning here
	@SuppressWarnings("unchecked")
	public void loadValues() throws CheckedApplicationException
	{
		if (!valuesLoaded)
		{
			CommandManager manager = new SQLCommandManager();
			Collection<SimpleObject> values = (Collection<SimpleObject>) manager.perform(new SQLCommand(factory,new SQLQuery(new SimpleObjectResultSetProcessor()),"select '" + getType()+ "' as type,name,value from UnitedStates"));
			valuesLoaded = SimpleObjectAccessor.getInstance().addAll(values);;
		}
	}
}
