package com.deloitte.common.persistence.database;

import java.sql.Timestamp;
import java.util.Calendar;

import com.deloitte.common.persistence.interfaces.ResultSetHandler;

/**
 * This was written to handle the Oracle database, but it might be valid acrosss
 * other implementations.
 * <p>
 */
public class OracleResultSetHandler implements ResultSetHandler
{

	static 
	{
		ResultSetManager.register(new OracleResultSetHandler());
	}

	public OracleResultSetHandler()
	{
		super();
	}
	/**
	 * Return a java.util.Date/Calendar instead of the associated Timestamp/java.sql.Date
	 *  
	 */
	public Object getResultSetValue(Object o)
	{
		Object theReturnObject = o;

		if (o != null && o instanceof Timestamp)
		{
			Timestamp theTimestamp = (Timestamp) o;
			Calendar theCalendar = Calendar.getInstance();
			theCalendar.setTimeInMillis(theTimestamp.getTime());
			theReturnObject = theCalendar;
		} else if (o != null && o instanceof java.sql.Date)
		{
			java.sql.Date date = (java.sql.Date)o;
			theReturnObject = new java.util.Date(date.getTime());
		} /*else if (o != null && o instanceof Blob)
		{
			
			Blob theBlob = (Blob)o;
			//BufferedOutputStream baos = new BufferedOutputStream(new ByteArrayOutputStream());
	        try {
				//baos.write(theBlob.getBytes(0, (int) theBlob.length()), 0, (int) theBlob.length());
				byte[] theArray = theBlob.getBytes(0,(int)theBlob.length());
				theReturnObject = new org.hsqldb.jdbc.jdbcBlob(theArray);
		        //baos.flush();
		        //baos.close();
	        } catch (IOException e) 
			{
	    		logger.info("Unable to handle Blob " + e.toString());
			} catch (SQLException e) 
			{
				logger.info("Unable to handle Blob " + e.toString());
			}
		}*/
		return theReturnObject;
	}

}
