package com.deloitte.common.persistence.database;

import java.util.List;
import java.util.Map;

import com.deloitte.common.objects.DefaultSimpleObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Used to return SimpleObjects from a ResultSet. The constructors will determine the processing
 * of the ResultSet. <br><br>
 * Documentation for the various constructors are based upon querying a table
 * with the following basic structure:<br>
 * 	type	varchar<br>
 *  name	varchar<br>
 *  value	varchar<br>
 */
@Deprecated
public class SimpleObjectResultSetProcessor extends AbstractResultSetProcessor<DefaultSimpleObject>
{
	private   String  nameColumnAlias;
	private   String  valueColumnAlias;
	private   String  typeColumnAlias;
	private   boolean typeAsLiteral;
	private   boolean typeAsTableName;
	
	/**
	 *  Name attribute in the SimpleObject comes from a column named:  NAME <br>
	 *  Value attribute in the SimpleObject comes from a column named: VALUE <br>
	 *  Type attribute in the SimpleObject comes from a column named: TYPE<br>
	 *  <br><br>
	 *  The following query will produce a result set that can be used by the
	 *  SimpleObjectResultSetProcessor instantiated from this constuctor: <br>
	 *  Select type, name,value from tablename<br>
 	 *  Contructor: new SimpleObjectResultSetProcessor()
	 */
	public SimpleObjectResultSetProcessor()
	{
		this("NAME","VALUE","TYPE");
	}

	/**
	 * NameColumn provides the name of the column that will map to the Name attribute in the SimpleObject<br>
	 * ValueColumn provides the name of the column that will map to the Value attribute in the SimpleObject<br>
	 * TypeColumn provides the name of the column that will map to the Type attribute in the SimpleObject<br>
	 * <br><br>
	 * Example: Select 'Literal' as type, column1, column2 from tablename<br>
	 * Contructor: new SimpleObjectResultSetProcessor("column1","column2", "type")
	 * 
	 */
	public SimpleObjectResultSetProcessor(String nameColumn, String valueColumn, String typeColumn)
	{
		this(nameColumn,valueColumn,typeColumn,false);
	}

	/**
	 * NameColumn provides the name of the column that will map to the Name attribute in the SimpleObject<br>
	 * ValueColumn provides the name of the column that will map to the Value attribute in the SimpleObject<br>
	 * Type provides either the name of the column that will map to the Type attribute in the SimpleObject,
	 * or a literal value that will be placed in the Type attribute in the SimpleObject<br>
	 * typeAsLiteral indicates if the type parameter supplied the column name or a literal value
	 * <br><br>
	 * Example: Select column1, column2 from tablename<br>
	 * Contructor: new SimpleObjectResultSetProcessor("column1","column2","MySimpleObject",true)<br>
	 * SimpleObject type attrib value will be "MySimpleObject"
	 * 
	 * <br><br>
	 * Example: Select column1, column2, column3 from tablename<br>
	 * Contructor: new SimpleObjectResultSetProcessor("column1","column2","column3",false)<br>
	 * SimpleObject type attrib value will be value for record in column3
	 */
	public SimpleObjectResultSetProcessor(String nameColumn, String valueColumn, String type, boolean typeAsLiteral)
	{
		this(nameColumn,valueColumn,type,typeAsLiteral,false);
	}

	/**
	 * NameColumn provides the name of the column that will map to the Name attribute in the SimpleObject<br>
	 * ValueColumn provides the name of the column that will map to the Value attribute in the SimpleObject<br>
	 * Type provides either the name of the column that will map to the Type attribute in the SimpleObject,
	 * or a literal value that will be placed in the Type attribute in the SimpleObject<br>
	 * typeAsLiteral indicates if the type parameter supplied the column name or a literal value
	 * typeAsTableName indicates if the name of the table from the query should be used as the Type attribute in the SimpleObject.
	 * If true, typeAsLiteral and type params are ignored.
	 * <br><br>
	 * Example: Select column1, column2 from tablename<br>
	 * Contructor: new SimpleObjectResultSetProcessor("column1","column2","MySimpleObject",true,false)<br>
	 * SimpleObject type attrib value will be "MySimpleObject"
	 * 
	 * <br><br>
	 * Example: Select column1, column2, column3 from tablename<br>
	 * Contructor: new SimpleObjectResultSetProcessor("column1","column2","column3",false,false)<br>
	 * SimpleObject type attrib value will be value for record in column3
	 *
	 * <br><br>
	 * Example: Select column1, column2 from myTableName<br>
	 * Contructor: new SimpleObjectResultSetProcessor("column1","column2",null,false,true)
	 * SimpleObject type attrib value will be 'myTableName'
	 */
	public SimpleObjectResultSetProcessor(String nameColumn, String valueColumn,String type, boolean typeAsLiteral,boolean typeAsTableName)
	{
		this.nameColumnAlias  = nameColumn.toUpperCase();
		this.valueColumnAlias = valueColumn.toUpperCase();
		this.typeAsLiteral 	  = typeAsLiteral;
		this.typeAsTableName  = typeAsTableName;
		if (!typeAsLiteral && !typeAsTableName)
		{
			this.typeColumnAlias  = type.toUpperCase();

		} else
		{
			this.typeColumnAlias  = type;
		}		
	}	

	protected  void processTheRow(List<DefaultSimpleObject> theList, Map<String,?> theRow) throws CheckedApplicationException {
		String type = null;
		String name = theRow.get(nameColumnAlias).toString();
		String value = theRow.get(valueColumnAlias).toString();
	//	List<DefaultSimpleObject> theList1 = (List<DefaultSimpleObject>)theList;
		if (typeAsTableName)
		{
			type = getTableName();
		}
		else if (typeAsLiteral)
		{
			type = typeColumnAlias;
		} else
		{
			type = theRow.get(typeColumnAlias).toString();
		}
		theList.add(new DefaultSimpleObject(type,name,value));
	}

}
