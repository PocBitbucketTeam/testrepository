package com.deloitte.common.persistence.database;

import java.util.List;
import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.interfaces.DomainObjectProcessor;

/**
 * Used to convert a JDBC ResultSet into a list of DomainObjects. The
 * DomainObjectProcessor is is instantiated with tells DomainObjectResultSetProcessor
 * how to map the entries in the List representing the row to the corresponding
 * domain object properties.<br><br>
 * 
 * Note: DomainObjectResultSetProcessor is not thread-safe. Do not re-use
 * instances across multiple threads for processing ResultSets.
 * 
 * @param <T> the type of domain objects that the ResultSet will be converted into
 */
public class DomainObjectResultSetProcessor<T extends DomainObject> extends AbstractResultSetProcessor<T> {
	
	DomainObjectProcessor<T> theProcessor;
	
	/**
	 * Should never be used. Must always use
	 * {@link DomainObjectResultSetProcessor#DomainObjectResultSetProcessor(DomainObjectProcessor)}
	 * instead
	 */
	@Deprecated
	public DomainObjectResultSetProcessor()	{
		super();
	}
	
	public DomainObjectResultSetProcessor(DomainObjectProcessor<T> processor){
		super();
		theProcessor = processor;
	}

	protected void processTheRow(List<T> theList, Map<String,?> theRow) throws CheckedApplicationException {
		T theObject = theProcessor.map(theRow);
		if (getFilter().include(theObject)){
			theList.add(theObject);
		}
	}
}
