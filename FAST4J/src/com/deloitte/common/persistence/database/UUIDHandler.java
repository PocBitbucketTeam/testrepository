package com.deloitte.common.persistence.database;

import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.framework.OID;
import com.deloitte.common.persistence.ConnectionFactory;

/**
 * UUIDHandler class is a abstract class to generate a UUID based on a database
 * Sequence for a given Database type, extend this class to create a DB specific
 * UUID Handler.
 * 
 * <pre>
 * class DB2UUIDHandler extends UUIDHandler{
 * public DB2UUIDHandler(Object key, ConnectionFactory factory){
 *		super(key,factory);
 *	}
 *	
 * Override the getSql method to return sequence sql
 *	public String getSql(){
 *		return "values nextval for " + getKey();
 *	}
 * </pre>
 *  
 */
public abstract class UUIDHandler
{
	private Object key;
	private ConnectionFactory factory;
	private long refreshRate = 100l;
	private long invocations = 0l;
	private long currentValue;
	private boolean descending;
	private boolean isFirstTime = true;

	/**
	 * Creates a UUIDHandler with refreshRate of 1001 that generates in ascending order
	 */
	public UUIDHandler(Object key,ConnectionFactory factory)
	{
		this.key = key;
		this.factory = factory;
	}
	
	/**
	 * Creates a UUIDHandler that generates in ascending order
	 */
	public UUIDHandler(Object key, ConnectionFactory factory, long refreshRate)
	{
		this(key,factory);
		this.refreshRate = refreshRate;
	}
	
	public UUIDHandler(Object key, ConnectionFactory factory, long refreshRate,boolean descending)
	{
		this(key,factory,refreshRate);
		this.descending = descending;
	}
	
	/**
	 * Returns ConnectionFactory that will be used to invoke the sequence.
	 * @return ConnectionFactory - the Connection factory used to get the DB Connection.
	 */
	public final ConnectionFactory getFactory()
	{
		return factory;
	}

	/**
	 * Returns the key that can be used to identify the UUIDHandler.
	 * @return Object - the key mapped to the handler.
	 */
	public final Object getKey()
	{
		return key;
	}

	public abstract String getSql();
	
	/**
	 * Sets the Current UUID value to new value.
	 * @param l - the new UUID value.
	 */
	public final void setCurrentValue(long l)
	{
		this.currentValue = l;
	}
	
	/**	 
	 * Returns true if it is time for the UUID value to be seeded/re-seeded by
	 * invoking the DB sequence. 
	 * @return boolean
	 */
	public final boolean isTimeToRefresh()
	{
		boolean theAnswer = isFirstTime || currentValue == 0 || invocations == refreshRate;
		isFirstTime = false;
		return theAnswer;
	}

	/**
	 * Resets the refresh count value to zero.
	 */
	public final void resetTimeToRefresh()
	{
		synchronized(key)
		{
			invocations = 0;
		}
		
	}

	/**
	 * Returns the current UUID value.
	 * @return UUID
	 */
	public UUID getCurrentID()
	{
		OID theAnswer = null;
		synchronized(key)
		{
			theAnswer = new OID(currentValue);
		}
		return theAnswer;
	}
	
	/**
	 * Generates the next UUID, which could either be the next increment value or next decrement value based on the order type.
	 */
	public final void incrementCurrentValue()
	{
		synchronized(key)
		{
			if (descending)
			{
				currentValue--;
			} else
			{
				currentValue++;
			}
			invocations++;
		}
	}

}
