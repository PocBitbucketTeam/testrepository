package com.deloitte.common.persistence.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.domain.XMLContainer;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.CheckedPersistenceException;
import com.deloitte.common.persistence.interfaces.ResultSetProcessor;
import com.deloitte.common.util.Strings;

public class XMLResultSetProcessor implements ResultSetProcessor<XMLContainer>
{
	private static final Logger logger = Logger.getLogger(XMLResultSetProcessor.class.getName());

	private String root = "ResultSet";
	private String row = "Row";
	
	public XMLResultSetProcessor()
	{
		super();
	}
	
	public XMLResultSetProcessor(String root, String row)
	{
		this.root = root;
		this.row = row;
	}
	
	public List<XMLContainer> handleResultSet(List<Map<String, ?>> resultSet, Filter<XMLContainer> filter) throws CheckedApplicationException
	{
		List<XMLContainer> theList = new ArrayList<XMLContainer>();
		
		StringBuffer xml = new StringBuffer();
		if (!Strings.isEmpty(getRootLabel()))
		{
			xml.append("<" + getRootLabel() + ">\n");
		}
		logger.info("Handling the Result set in XMLRSProcesor");
				
				Object theColumnValue = null;
				Map<String, ?> row = null;
				ResultSetManager manager = new ResultSetManager();
				for(Map<String, ?> obj:resultSet){
					if (!Strings.isEmpty(getRowLabel()))
					{
						xml.append("\t<" + getRowLabel() + ">\n");
					}
					row =(Map<String, ?>)obj;
					
					for(String theColumnName: row.keySet()){
						theColumnValue = manager.getResultSetValue(row.get(theColumnName));
						xml.append("\t\t<" + theColumnName +">"+ theColumnValue + "</" + theColumnName + ">\n");
					}
					
					if (!Strings.isEmpty(getRowLabel()))
					{
						xml.append("\t</" + getRowLabel() + ">\n");
					}
				}
			if (!Strings.isEmpty(getRootLabel()))
			{
				xml.append("</" + getRootLabel() + ">\n");
			}
			XMLContainer xmlContainer = new XMLContainer(xml.toString());
			theList.add(xmlContainer);

		return theList;
	}

	public List<XMLContainer> handleResultSet(ResultSet rs, Filter<XMLContainer> filter) throws CheckedApplicationException
	{
		List<XMLContainer> theList = new ArrayList<XMLContainer>();
		
		StringBuffer xml = new StringBuffer();
		if (!Strings.isEmpty(getRootLabel()))
		{
			xml.append("<" + getRootLabel() + ">\n");
		}
		try {

				ResultSetMetaData rsmd = rs.getMetaData();
				logger.info("Handling the Result set in XMLRSProcesor");
				int columnCount = rsmd.getColumnCount();
				List<String> rowLabels = new ArrayList<String>();
				for (int i=0; i < columnCount; i++)
				{
					rowLabels.add(rsmd.getColumnLabel(i+1).toUpperCase());
				}
				//logger.info("Column Labels: " + CollectionHelper.format(CowLabels));
				ResultSetManager manager = new ResultSetManager();
				while(rs.next()) {

					Object theColumnValue = null;
					if (!Strings.isEmpty(getRowLabel()))
					{
						xml.append("\t<" + getRowLabel() + ">\n");
					}
					for(String theColumnName: rowLabels){
						theColumnValue = manager.getResultSetValue(rs.getObject(theColumnName));
						xml.append("\t\t<" + theColumnName +">"+ theColumnValue + "</" + theColumnName + ">\n");
					}
					
					if (!Strings.isEmpty(getRowLabel()))
					{
						xml.append("\t</" + getRowLabel() + ">\n");
					}
				}
	
			if (!Strings.isEmpty(getRootLabel()))
			{
				xml.append("</" + getRootLabel() + ">\n");
			}
			XMLContainer xmlContainer = new XMLContainer(xml.toString());
			theList.add(xmlContainer);
	
		} catch (SQLException e) {
				throw new CheckedPersistenceException(this.getClass(),"An error has occured in the Data Layer, while handling the results",e);
			}
		return theList;
	}
	
	
	protected String getRootLabel()
	{
		return root;
	}
	
	protected String getRowLabel()
	{
		return row;
	}
}
