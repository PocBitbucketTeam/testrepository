package com.deloitte.common.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.UUID;

/**
 * Helper class to build SQL Update/Insert statements
 * @author SIDT Framework Team
 *
 */
public class SQLUpdateHelper
{
	private static final List<String> EMPTY_STRING_LIST = Collections.emptyList();
	private static final Map<String, ?> emptyMap = Collections.emptyMap();
	
	/**
	 * Returns list of parameter values for a given map of column's, skiplist and staticElements.
	 * @param columns
	 * @param skipList - the list of column's that has to be skipped from sql statement.
	 * @param staticElements -the static elements are static literals of DB like SYSDATE,CURRENT_DATE, SEQUENCE.NEXTVal. 
	 * @return - the list of parameter value's
	 */
	public static List<Object> getUpdateParameters(Map<String,?> columns, List<String> skipList, Map<String,?> staticElements) {
		SortedSet<String> sortedKeys = new TreeSet<String>(columns.keySet());
		List<Object> theParameters = new ArrayList<Object>();
		
		for (String key : sortedKeys) {
			if (!skipList.contains(key) && !addAndHandleDomainOrUUID(columns.get(key), theParameters) && !staticElements.containsKey(key)) {
				theParameters.add(columns.get(key));
			}
		}
		return theParameters;		
	}
	
	public static List<Object> getUpdateParameters(Map<String, ?> columns){
		return getUpdateParameters(columns, EMPTY_STRING_LIST);
	}

	public static List<Object> getUpdateParameters(Map<String,?> columns, List<String> skipList) {		
		return getUpdateParameters(columns, skipList, emptyMap);		
	}
	
	/**
	 * 
	 * Generates an sql update statement for a given tablename, columns, skiplist and staticelements.
	 * @param tableName - name of the table as defined in the Database. 
	 * @param columns - map of table column's and it's values. 
	 * @param skipList - the list of column's that has to be skipped from sql statement.
	 * @param staticElements - the static elements are static literals of DB like SYSDATE,CURRENT_DATE, SEQUENCE.NEXTVal. 
	 * @return string - sql statement
	 * 
	 */
	public static String getUpdateStatement(String tableName, Map<String,?> columns, List<String> skipList, Map<String,?> staticElements) {
		StringBuffer sql = new StringBuffer();
		sql.append("Update " + tableName + " set");
		SortedSet<String> sortedKeys = new TreeSet<String>(columns.keySet());
		boolean first = true;
		
		for (String key : sortedKeys) {			
			if (!skipList.contains(key)) {
				if (first) {
					first = false;
				} else {
					sql.append(",");
				}
				
				if (staticElements.containsKey(key)) {
                     sql.append(" " + key + " = "+staticElements.get(key));
				} else {
                     sql.append(" " + key + " = ?");
				}
			}
		}
		sql.append(" where 1=1 ");
		return sql.toString();
	}
		
	public static String getUpdateStatement(String tableName, Map<String,?> columns){
		return getUpdateStatement(tableName, columns, EMPTY_STRING_LIST);
	}
	
	public static String getUpdateStatement(String tableName, Map<String,?> columns, List<String> skipList) {
		return getUpdateStatement(tableName, columns, skipList, emptyMap);
	}
	
	public static List<Object> getInsertParameters(Map<String,?> columns, List<String> skipList, Map<String,?> staticElements) {
		SortedSet<String> sortedKeys = new TreeSet<String>(columns.keySet());
		List<Object> theParameters = new ArrayList<Object>();
		
		for (String key : sortedKeys) {			
			if (!skipList.contains(key) && !addAndHandleDomainOrUUID(columns.get(key),theParameters) && !staticElements.containsKey(key)) {
				theParameters.add(columns.get(key));
			}
		}
		return theParameters;		
	}
	
	public static List<Object> getInsertParameters(Map<String,?> columns) {
		return getInsertParameters(columns, EMPTY_STRING_LIST);
	}

	public static List<Object> getInsertParameters(Map<String,?> columns, List<String> skipList) {
		return getInsertParameters(columns, skipList, emptyMap);
	}
	
	/**
	 * 
	 * Generates an sql insert statement for a given tablename, columns, skiplist and staticelements.
	 * @param tableName - name of the table as defined in the Database. 
	 * @param columns - map of table column's and it's values. 
	 * @param skipList - the list of column's that has to be skipped from sql statement.
	 * @param staticElements - the static elements are static literals of DB like SYSDATE,CURRENT_DATE, SEQUENCE.NEXTVal. 
	 * @return string - sql query
	 * 
	 */
	public static String getInsertStatement(String tableName, Map<String,?> columns, List<String> skipList, Map<String,?> staticElements) {
		StringBuffer sql = new StringBuffer();
		StringBuffer bindParams = new StringBuffer();
		
		sql.append("insert into " + tableName + "(");
		SortedSet<String> sortedKeys = new TreeSet<String>(columns.keySet());
		boolean first = true;
	
		for (String key :sortedKeys) {
			if (!skipList.contains(key)) {
				if (first) {
					first = false;
				} else {
					sql.append(",");
					bindParams.append(",");
				}		
				
				if (staticElements.containsKey(key)) {
                    bindParams.append(staticElements.get(key));
				} else {
					bindParams.append("?");
				}
				sql.append(key);
			}
		}
		
		sql.append(") values ("); 
		sql.append(bindParams.toString());
		sql.append(")");		
		return sql.toString();
	}
	
	public static String getInsertStatement(String tableName, Map<String,?> columns, List<String> skipList) {
		return getInsertStatement(tableName, columns, skipList, emptyMap);
	}
	
	public static String getInsertStatement(String tableName, Map<String,?> columns){
		return getInsertStatement(tableName, columns, EMPTY_STRING_LIST);
	}

	
	/**
	 * If theObject provided is a UUID handle the logic to translate it to a 
	 * String and add it to the bind list.
	 */
	static boolean addAndHandleUUID(Object theObject, List<Object> bindValues)
	{
		if (theObject != null && UUID.class.isAssignableFrom(theObject.getClass()))
		{
			UUID uuid = (UUID) theObject;
			bindValues.add(uuid.getValue().toString());
			return true;
		}
		return false;
	}

	/**
	 * If theObject provided is a DomainObject handle the logic to get the UUID, 
	 * and call addAndHandleUUID.
	 */

	static boolean addAndHandleDomainObject(Object theObject, List<Object> bindValues)
	{
		if (theObject != null && DomainObject.class.isAssignableFrom(theObject.getClass()))
		{
			DomainObject domainObject = (DomainObject) theObject;
			return addAndHandleUUID(domainObject.getID(),bindValues);
		}
		return false;
	}

	/**
	 * Determine if the statement will bind properly when passed to the database
	 */
	public static boolean addAndHandleDomainOrUUID(Object theObject, List<Object> bindValues)
	{
		boolean theResult = addAndHandleDomainObject(theObject,bindValues);
		if (!theResult)
		{
			theResult = addAndHandleUUID(theObject,bindValues); 
		}
		return theResult;
	}
	
	/**
	 * Convenience Method that will assist in providing a bound set of objects
	 * to be used in preparing a SQL PreparedStatment
	 */
	public static void addBindValue(List<Object> inputList, List<Object> bindValues)
	{
		if (inputList != null)
		{
			for (Object theObject : inputList)
			{
				if (!addAndHandleDomainOrUUID(theObject,bindValues))
				{
					bindValues.add(theObject);
				}
			}
		}
	}
	

}
