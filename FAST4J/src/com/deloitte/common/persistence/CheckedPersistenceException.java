package com.deloitte.common.persistence;

import java.sql.SQLException;
import java.util.logging.Logger;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public class CheckedPersistenceException extends CheckedApplicationException
{
	private String sqlState;
	private int	vendorErrorCode;
	
	public CheckedPersistenceException(Class<?> c)
	{
		super(DEFAULT_WARNING);
		Logger.getLogger(c.getName()).severe(DEFAULT_WARNING);
	}
	
	public CheckedPersistenceException(Class<?> c,String message)
	{
		super(message);
	}

	public CheckedPersistenceException(Class<?> c,String message,Exception theCause)
	{
        super(message,theCause);
		handleException(theCause);
 	}

	private void handleException(Exception se)
	{
		if (se instanceof SQLException)
		{
			this.sqlState = ((SQLException)se).getSQLState();
			this.vendorErrorCode = ((SQLException)se).getErrorCode();
			Logger.getLogger(this.getClass().getName()).severe("SQLException specific Data: SQLState = " + sqlState + " ErrorCode = " + vendorErrorCode);
		}
	}
	
	public int getErrorCode()
	{
		return this.vendorErrorCode;
	}
	
	public String getSQLState()
	{
		return this.sqlState;
	}
	
	private static final long serialVersionUID = 1L;

}
