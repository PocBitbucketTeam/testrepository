package com.deloitte.common.persistence;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import com.deloitte.common.interfaces.Cache;
import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.interfaces.UUID;
import com.deloitte.common.objects.collection.FifoCache;
import com.deloitte.common.objects.collection.NullFilter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.persistence.interfaces.DataAccessObject;

/**
 * A decorator object used to implement caching on a DataAccessobject.
 *
 * The CachedDAO has specific usage patterns that need to be noted for proper
 * behavior.
 *
 * 	1) If a DAO is wrapped by this class, ALL access should be through the
 *     Cached implementation unless access is only Read/Only
 *  <br><br>
 		Know your data!
 *  <br><br>
 *  2) Node the key is found by looking for "ID" in the Cache
 *  3) Casting to a CachedDAO can allow access to the Cache statistics
 *  <br><br>
 * @author SIDT Framework Team
 *
 */
public class CachedDAO<T extends DomainObject> implements DataAccessObject<T>
{
	private final DataAccessObject<T> theDao;
	private final Cache<UUID, T> theCache;
	private final CacheStats stats;

	public CachedDAO(DataAccessObject<T> theDao)
	{
		this(theDao, new FifoCache<UUID, T>());
	}

	/**
	 * Can be used to create a CachedDAO with an alternative implementation of
	 * the caching strategy, or to specify custom parameters in the cache configuration.
	 * eg. to change the default max size for the FifoCache you can execute:<br>
	 * dao = new CachedDAO(dao, new FifoCache(5000));<br>
	 * @param theDao
	 * @param theCache
	 */
	public CachedDAO(DataAccessObject<T> theDao, Cache<UUID, T> theCache)
	{
		this.theDao = theDao;
		this.theCache = theCache;
		this.stats = new CacheStats();
	}

	public final Collection<T> getAll(Map<?, ?> parameters) throws CheckedApplicationException {
		return getAll(parameters, new NullFilter<T>());
	}

	public final Collection<T> getAll(Map<?, ?> parameters, Filter<T> filter) throws CheckedApplicationException
	{
		Collection<T> theObjects = theDao.getAll(parameters,filter);
		putAll(theObjects);
		return theObjects;
	}

	public final T get(Map<?, ?> parameters) throws CheckedApplicationException
	{
		T theObject = null;
		if (parameters.get("ID") != null && this.theCache.containsKey((UUID)parameters.get("ID")))
		{
			theObject = this.theCache.get((UUID)parameters.get("ID"));
			stats.hit++;
		} else if(parameters.get("ID") != null && !this.theCache.containsKey((UUID)parameters.get("ID")))
		{
			stats.miss++;
			theObject = theDao.get(parameters);
		} else
		{
			theObject = theDao.get(parameters);
		}
		return theObject;
	}

	public final T findByKey(Object key, Object value) throws CheckedApplicationException
	{
		T theObject = null;
		if ("ID".equals(key.toString()) && this.theCache.containsKey((UUID)value))
		{
			theObject = this.theCache.get((UUID)value);
			stats.hit++;
		} else
		{
			theObject = theDao.findByKey(key,value);
			if (theObject != null)
			{
				put(theObject);
			}
			stats.miss++;
		}
		return theObject;
	}

	public final void update(T theObject) throws CheckedApplicationException
	{
		theDao.update(theObject);
		UUID theKey = theObject.getID();
		if (this.theCache.containsKey(theKey))
		{
			put(theObject);
			stats.update++;
		}

	}

	public final void add(T theObject) throws CheckedApplicationException
	{
		theDao.add(theObject);
		put(theObject);
		stats.add++;
	}

	public final void remove(T theObject) throws CheckedApplicationException
	{
		theDao.remove(theObject);
		UUID theKey = theObject.getID();
		if (this.theCache.containsKey(theKey))
		{
			synchronized ( this.theCache )
			{
				this.theCache.remove(theKey);
				stats.delete++;
			}
		}
	}

	protected void  putAll(Collection<T> theCollection){
		for (T o : theCollection){
			put(o);
		}
	}

	protected void  put(T theObject)
	{
		synchronized ( this.theCache )
		{
			this.theCache.put(theObject.getID(),theObject);
		}
	}

	public DataAccessObject<T> getDAO(){
		return theDao;
	}

	public final String getStatistics()
	{
		double total = stats.hit + stats.miss;
		if (total == 0) total++;
		BigDecimal hitPercentage = new BigDecimal(100*(stats.hit/total));
		BigDecimal missPercentage = new BigDecimal(100*(stats.miss/total));

		return "Cache Performance\n\tTotal Accesses = " + (stats.hit+stats.miss) +"\n\tCache Hits     = " + stats.hit + "("+hitPercentage.setScale(2,BigDecimal.ROUND_HALF_UP) + "%)" + "\n\tCache Misses   = " + (stats.miss) + "(" + missPercentage.setScale(2,BigDecimal.ROUND_HALF_UP) + "%)" +"\n\t\tAdds    = " + stats.add + "\n\t\tUpdates = " + stats.update + "\n\t\tDeletes = " + stats.delete;
	}

	private static class CacheStats {
		public int hit;
		public int miss;
		public int add;
		public int update;
		public int delete;
	}
}
