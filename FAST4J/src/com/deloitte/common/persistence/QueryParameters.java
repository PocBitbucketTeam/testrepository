package com.deloitte.common.persistence;


/**
 * The Query Parameters enum describes the additional JDBC parameters that 
 * can be passed in on the Runtime. 
 * 
 * As an example, Max Rows - property to limit the number of results sent 
 * back by the JDBC driver.
 * 
 */
public enum QueryParameters {

	MAX_ROWS, FETCH_SIZE;
	
}
