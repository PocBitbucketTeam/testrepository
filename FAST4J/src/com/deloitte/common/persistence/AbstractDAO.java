package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.sql.DataSource;

import com.deloitte.common.interfaces.DomainObject;
import com.deloitte.common.interfaces.Filter;
import com.deloitte.common.objects.collection.NullFilter;
import com.deloitte.common.objects.framework.CheckedApplicationException;
import com.deloitte.common.objects.framework.OID;
import com.deloitte.common.persistence.interfaces.ConfigurableDataAccessObject;
import com.deloitte.common.persistence.interfaces.DataAccessObject;
import com.deloitte.common.persistence.interfaces.ResultSetProcessor;
import com.deloitte.common.persistence.interfaces.Transactional;

/**
 * <p>
 * AbstractDAO provides a Data Access default implementation that requires
 * the subclass implementor to provide Sql Statements and Parameters. Database
 * connections retrieve/release, statement binding and execution are handled
 * by this class. 
 * </p>
 * <br>
 * <IMG SRC="../../../../resources/ClassDiagramPersistence.jpg" ALT="UML Diagram">
 * <br>
 * Subclass implementor should do the following
 * <br/>
 * 	1) Create subclass that extends this class<br/>
 *  2) Implement the following methods...<br/>
 * 
 * <p>
 *  Depending on the behavior of the DAO, implement the following pairs of
 *  methods
 *  </p>
 *  <br><br>
 *  Create (Add a new object to the database)<br/>
 *  	<li>String getInsertStatement(DomainObject o)<br/>
 *  	<li>List   getInsertParameters(DomainObject o)<br/>
 *  <br><br>	
 *  Read<br/>
 *  	<li>String getSelectStatement(DomainObject o)<br/>
 *  	<li>List   getSelectParameters(DomainObject o)<br/>
 *  	<li>DomainObject map(Map m) <br/>
 *  <p>
 *  	The map will contain the values from the ResultSet and will get called<br/>
 *  	for each row obtained from the database query.
 * </p>
 * <p>
 *  Update<br>
 *  	<li>String getUpdateStatement(DomainObject o)<br/>
 *  	<li>List   getUpdateParameters(DomainObject o)<br/>
 *  </p>
 *  <p>
 *  Delete<br>
 *  	<li>String getDeleteStatement(DomainObject o)</br>
 *  	<li>List   getDeleteParameters(DomainObject o)</br>
 *  </p>	
 * 
 */
public abstract class AbstractDAO<T extends DomainObject> implements DataAccessObject<T>,Transactional, ConfigurableDataAccessObject
{
	private static final Logger logger = Logger.getLogger(AbstractDAO.class.getName());

	private DataSource datasource;
	private int exceptionRowCount = 1;
	Integer fetchSize;
	Integer maxRows;

	public Integer getFetchSize() {
		return fetchSize;
	}

	public void setFetchSize(Integer fetchSize) {
		this.fetchSize = fetchSize;
	}

	public Integer getMaxRows() {
		return maxRows;
	}

	public void setMaxRows(Integer maxRows) {
		this.maxRows = maxRows;
	}
	
	public final void setDataSource(DataSource datasource)
	{
		if (datasource == null)
		{
			throw new RuntimeException("Can not set Datasource, the datasource  is null");
		}
		logger.info("Received Datasource " + datasource.getClass().getName());
		this.datasource = datasource;
	}

	public final DataSource getDataSource()
	{
		return this.datasource;
	}
	
	/**
	 * @see com.deloitte.common.persistence.AbstractDAO#getAll(java.util.Map, com.deloitte.common.interfaces.Filter)
	 */
	public final Collection<T> getAll(Map<?, ?> parameters) throws CheckedApplicationException {
		return getAll(parameters, new NullFilter<T>());
	}
	
	/**
	 * In addition to the core behavior defined by the DAO interface, this implementation also supports 
	 * specifyin the maxRows and fetchSize values which will be passed to the DB driver. These can be
	 * specified by including them in in the parameters map with the key values from the QueryParameters
	 * enum and an Integer in the associated value.
	 * 
	 * @see com.deloitte.common.persistence.interfaces.DataAccessObject#getAll(java.util.Map, com.deloitte.common.interfaces.Filter)
	 */
	public Collection<T> getAll(Map<?, ?> parameters, Filter<T> filter) throws CheckedApplicationException 
	{
		
		long begin = System.currentTimeMillis();

		String sql = getSelectStatement(parameters);
		List<?> parms = getSelectParameters(parameters);
		Statement theStatement = null;
		Connection theConnection = null;
		ResultSet theResultSet = null;
		List<T> theResults = new ArrayList<T>();
		Integer fetchSize = this.fetchSize;
		Integer maxRows = this.maxRows;

		if (parameters.containsKey(QueryParameters.FETCH_SIZE)){
			fetchSize = (Integer)parameters.get(QueryParameters.FETCH_SIZE);
		} 
		if (parameters.containsKey(QueryParameters.MAX_ROWS)){
			maxRows = (Integer)parameters.get(QueryParameters.MAX_ROWS);
		} 
		
		try{ 
		
			logger.info("Persistence About to Execute: " + SQLBindHelper.resolveStatement(sql,parms));
			theConnection = datasource.getConnection();
			theStatement = SQLBindHelper.bindParameters(theConnection,sql,parms);
			if(fetchSize != null){
				theStatement.setFetchSize(fetchSize);
			}
			if(maxRows != null){
				theStatement.setMaxRows(maxRows);
			}
			theResultSet = ((PreparedStatement)theStatement).executeQuery();
			theResults.addAll(handleResultSet(theResultSet,filter));
		} catch (SQLException e)
		{
			throw new CheckedPersistenceException(this.getClass(),"An error (DB Specific Information: " + e.getErrorCode() + " " + e.getMessage()+  ") has occured in the Data Layer, while retrieving the data SQL:\n\t" + SQLBindHelper.resolveStatement(sql,parms),e);
		} finally 
		{
			logger.info("Persistence Execution Complete, Duration = " + (System.currentTimeMillis() - begin) + " milliseconds");
			ConnectionFactory.releaseResultSet(theResultSet);
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
			
		}
		return theResults;
	}

	
	public final T get(Map<?, ?> parameters) throws CheckedApplicationException 
	{
		Collection<T> c = getAll(parameters);
		if (c == null || c.size() == 0)
		{
			throw new CheckedApplicationException(this.getClass(),"Unable to locate Object (" + parameters + ") \nResolved SQL Statement:\n\t" + SQLBindHelper.resolveStatement(getSelectStatement(parameters),getSelectParameters(parameters)));
		} else if (c.size() > 1)
		{
			throw new CheckedApplicationException(this.getClass(),"Multiple Objects returned, expecting only 1 (" + parameters + ") \nSQL:\n\t" + SQLBindHelper.resolveStatement(getSelectStatement(parameters),getSelectParameters(parameters)));
		}
			
		return c.iterator().next();
	}

	public final T findByKey(Object key, Object value) throws CheckedApplicationException 
	{
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put(key.toString().toUpperCase(),value);
		return get(map);
	}

	public final void remove(T theObject) throws CheckedApplicationException
	{
		if (theObject == null)
		{
			throw new CheckedApplicationException(getClass(),"A Null Object can not be Removed");
		}
		execute(theObject,getDeleteStatement(theObject),getDeleteParameters(theObject));
	}

	public final void add(T theObject) throws CheckedApplicationException
	{
		if (theObject == null)
		{
			throw new CheckedApplicationException(getClass(),"A Null Object can not be Added");
		}
	
		execute(theObject,getInsertStatement(theObject),getInsertParameters(theObject));
	}

	public final void update(T theObject) throws CheckedApplicationException 
	{
		if (theObject == null)
		{
			throw new CheckedApplicationException(getClass(),"A Null Object can not be Updated");
		}

		execute(theObject,getUpdateStatement(theObject),getUpdateParameters(theObject));
	}
	
	private void execute(T theObject,String sql, List<?> parameters) throws CheckedApplicationException
	{
		
		long begin = System.currentTimeMillis();
		PreparedStatement theStatement = null;
		Connection theConnection = null;
		try 
		{
				logger.info("Persistence About to Execute: " + SQLBindHelper.resolveStatement(sql,parameters));
				theConnection = datasource.getConnection();
				theStatement = (PreparedStatement)SQLBindHelper.bindParameters(theConnection,sql,parameters);
				int rowsUpdated = theStatement.executeUpdate();
				if (rowsUpdated < exceptionRowCount)
				{
					throw new CheckedApplicationException(this.getClass(),"The database was not updated");
				}
		} catch (SQLException e) 
		{
			throw new CheckedPersistenceException(this.getClass(),"An error has occured in the Data Layer, while parsing the Query",e);
		} finally
		{
			logger.info("Persistence Execution Complete, Duration = " + (System.currentTimeMillis() - begin) + " milliseconds");
			ConnectionFactory.releaseStatement(theStatement);
			ConnectionFactory.releaseConnection(theConnection);
		}		
	}

	/**
	 * Select statement/parameters
	 */
	protected String getSelectStatement(Map<?, ?> theParameters)
	{
		return "";
	}
	
	/**
	 * Note the default behavior for getSelectParameters is to get the OID
	 * from the Map and add it to the Query. This should either be overridden
	 * or called as super depending on the behavior that is desired.
	 * <p>
	 * Example Code:<br>
	 * <code>
	 * 	List bindValues = new ArrayList();
	 * 	Object oid = (UUID)theParameters.get("ID");
	 * 	bindValues.add(oid.getValue().toString());
	 * </code>
	 */
	protected List<?> getSelectParameters(Map<?, ?> theParameters)
	{
		List<Object> bindValues = new ArrayList<Object>();
		Object oid = theParameters.get("ID");
		if (oid != null)
		{
			if (oid instanceof OID)
			{
				oid = ((OID)oid).getValue().toString();
			}
			bindValues.add(oid);
		}
		return bindValues;
	}

	/**
	 * Update Statement/Parameters
	 */
	protected String getUpdateStatement(T theObject)
	{
		throw new UnsupportedOperationException("Update not implemented in DAO");
	}
	
	protected List<?> getUpdateParameters(T theObject)
	{
		return Collections.EMPTY_LIST;
	}

	/*
	 * Insert Statement/Parameters
	 */
	protected String getInsertStatement(T theObject)
	{
		throw new UnsupportedOperationException("Insert not implemented in DAO");
	}

	protected List<?> getInsertParameters(T theObject)
	{
		return Collections.EMPTY_LIST;
	}
	
	/*
	 * Delete Statement/Parameters
	 */
	protected String getDeleteStatement(T theObject)
	{
		throw new UnsupportedOperationException("Delete not implemented in DAO");
	}

	protected List<?> getDeleteParameters(T theObject)
	{
		List<Object> bindValues = new ArrayList<Object>();
		bindValues.add(theObject.getID().getValue().toString());
		return bindValues;
	}

	/**
	 * The DAO assumes by default that the add, update, and delete operations
	 * will affect at least one row in the DB. If no rows are updated, the DAO
	 * will return an exception. setting ignoreRowsUpdated to true will override
	 * this behavior so no Exception will be thrown if no rows are affected by
	 * the call  
	 * @param ignore
	 */
	public void setIgnoreRowsUpdated(boolean ignore)
	{
		if (ignore)
		{
			exceptionRowCount = 0;
		} else
		{
			exceptionRowCount = 1;
		}
	}
	
	protected abstract ResultSetProcessor<T> getResultSetProcessor();
	
	protected List<T> handleResultSet(ResultSet rs, Filter<T> filter) throws CheckedApplicationException {
		return getResultSetProcessor().handleResultSet(rs,filter);
	}

}
