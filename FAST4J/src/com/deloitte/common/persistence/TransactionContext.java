package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

import com.deloitte.common.objects.framework.UncheckedApplicationException;

/**
 * Used to handle transactions for the application against a single data source.
 * Provides methods to demarcate the beginning and end point of transactions,
 * and methods for retrieving the shared connection object all operations must be
 * performed on in order to achieve transactional behavior. 
 */
public final class TransactionContext {
	
	private static final Logger logger = Logger.getLogger(TransactionContext.class.getName());

	private TransactionContext(){} //prevent instantiation

	private static ThreadLocal<TransactionState> transactionState = new ThreadLocal<TransactionState>() {
		        protected synchronized TransactionState initialValue() {
		            return new TransactionState();
		        }
		    };

	private static TransactionState getTransState(){
		return transactionState.get();
	}
	
	/**
	 * Used to demarcate the start of a transacational area.
	 * @return Indicates if a new transaction was started, or if the demarcated
	 * 		area was joined into an already running transaction. Returns true
	 * 		if a new transaction was initiated. If true, if is the callers responsibility
	 * 		to call finishTransaction() to that the Transaction context is cleaned up. This
	 * 		should be done in a finally block to insure cleanup occurs. If false, the caller
	 * 		must not call finishTransaction, as this would prematurely terminate the transaction
	 * 		and will likely result in an error.
	 */
	public static boolean startTransaction() {
		TransactionState transState = getTransState();
		if (isActiveTransaction()){
			logger.info("Transaction already active, new transaction will join the existing transaction context");
			return false;
		} else {
			logger.info("Starting a new transaction");
			transState.setActiveTransaction(true);
			return true;
		}
	}
	
	/**
	 * Indicates if the running thread is currently in a zone that has
	 * been demarcated as transactional
	 */
	public static boolean isActiveTransaction() {
		return getTransState().isActiveTransaction();
	}

	/**
	 * @return the Connection object which must be used in order to achieve
	 * transactional behavior
	 */
	public static Connection getActiveConnection() {
		if (isActiveTransaction()){
			return getTransState().getTransactionalConnection();
		} else {
			throw new UncheckedApplicationException(TransactionContext.class,
			"Attempted to retrieve a transactionl connection when a transaction is not active");
		}
	}

	/**
	 * used to set the Connection object on which all operations in the transaction will be performed
	 */
	public static void setTransactionConnection(Connection connection) {
		if (isActiveTransaction()){
			if (getTransState().isConnectionSet()){
				logger.info("Transaction connection already set, will join existing transaction.");
			} else {
				logger.info("Setting connection for the transaction");
				try {
					connection.setAutoCommit(false);
				} catch (SQLException sqle) {
					throw new UncheckedApplicationException(TransactionContext.class,
							"Unexpected error initializing the connection for transaction", sqle);
				}
				Connection transactionalConnection = (Connection) TransactionalConnection.newInstance(connection);
				getTransState().setConnections(transactionalConnection, connection);
			}
		} else {
			throw new UncheckedApplicationException(TransactionContext.class.getClass(), "Cannot set connection for a transaction outside a transaction demarcation boundary");
		}
	}
	
	/**
	 * used to complete the transaction. Will either commit or rollback the transaction
	 * accordingly. This method must be called in order to clean up and reset the transaction
	 * context information. This should always be set in a finally block so this is
	 * guaranteed to occur.<br><br>
	 * It is the responsibility of the caller to only invoke finishTransation if it was
	 * the object that initiated the transaction
	 * @param startedTransaction pass true if the calling object is the same one
	 * that started the transaction. This should be the value returned by the
	 * startTransaction() method.
	 */
	public static void finishTransaction(boolean startedTransaction)
	{
		if (isActiveTransaction()){
			if (startedTransaction) { //only commit or rollback if the caller started the transaction
				try {
					Connection connection = getTransState().getWrappedConnection();
					if (connection==null){
						logger.info("Connection for the transaction not set. Nothing to commit or roll back.");
					} else {
						if (!isRollbackOnly() && getTransState().isTransactionComplete())
						{
							commit(connection);
						} else {
							rollback(connection);
						}
					}
				} finally {
					getTransState().reset();
				}
			}
		} else {
			throw new UncheckedApplicationException(TransactionContext.class,
			"Attempted to finish a transaction when a transaction is not active");
		}
	}
	
	public static boolean isRollbackOnly()
	{
		if (isActiveTransaction()){
			return getTransState().isRollBackOnly();
		} else {
			throw new UncheckedApplicationException(TransactionContext.class,
			"Cannot query state of transaction when a transaction is not active");
		}
	}
		
	public static void setRollbackOnly()
	{
		if (isActiveTransaction()){
			getTransState().setRollBackOnly(true);
		} else {
			throw new UncheckedApplicationException(TransactionContext.class,
			"Cannot roll back a transaction when a transaction is not active");
		}
	}
		
	/**
	 * Used to indicate to the TransactionContext object that all operations
	 * completed successfully and that the transaction may be committed. If not
	 * invoked before finishTransaction(), the transaction will be rolled back.
	 * @param startedTransaction pass true if the calling object is the same one
	 * that started the transaction. This should be the value returned by the
	 * startTransaction() method.
	 */
	public static void markTransactionComplete(boolean startedTransaction)
	{
		if (isActiveTransaction()){
			if(startedTransaction){
				getTransState().setTransactionComplete(true);
			}
		} else {
			throw new UncheckedApplicationException(TransactionContext.class,
			"Cannot mark a transaction complete when a transaction is not active");
		}
	}
		
	private static void rollback(Connection connection)
	{
		try 
		{
			connection.rollback();
			logger.info("Transaction rolled back");

		} catch (SQLException e) 
		{
			throw new UncheckedApplicationException(TransactionalConnection.class, "Unable to get Rollback JDBC Transaction ",e);
		} finally
		{
			ConnectionFactory.releaseConnection(connection);
		}
		
	}
	
	private static void commit(Connection connection)
	{
		try 
		{
			connection.commit();
			logger.info("Transaction committed on "+ connection);

		} catch (SQLException e) 
		{
			rollback(connection);
			throw new UncheckedApplicationException(TransactionContext.class, "Unable to get Commit JDBC Transaction ",e);
		} finally
		{
			ConnectionFactory.releaseConnection(connection);
		}
	}
	
	
	
	/**
	 * Used to hold the thread-local state for the running transaction.
	 */
	private static class TransactionState {
		private boolean activeTransaction = false;
		private boolean rollBackOnly = false;
		private boolean transactionComplete = false;
		private Connection transactionalConnection;
		private Connection wrappedConnection;
		
		
		public boolean isActiveTransaction() {
			return activeTransaction;
		}

		public void setActiveTransaction(boolean activeTransaction) {
			this.activeTransaction = activeTransaction;
		}

		public boolean isRollBackOnly() {
			return rollBackOnly;
		}

		public void setRollBackOnly(boolean rollBackOnly) {
			this.rollBackOnly = rollBackOnly;
		}

		public boolean isTransactionComplete() {
			return transactionComplete;
		}

		public void setTransactionComplete(boolean transactionComplete) {
			this.transactionComplete = transactionComplete;
		}

		public Connection getTransactionalConnection() {
			return (Connection)transactionalConnection;
		}

		public void setConnections(Connection transactionalConnection, Connection wrappedConnection) {
			this.transactionalConnection = transactionalConnection;
			this.wrappedConnection = wrappedConnection;
		}

		public Connection getWrappedConnection() {
			return wrappedConnection;
		}

		public void reset(){
			this.transactionalConnection = null;
			this.wrappedConnection = null;
			setActiveTransaction(false);
			setRollBackOnly(false);
			setTransactionComplete(false);
		}

		public boolean isConnectionSet(){
			return getTransactionalConnection() != null;
		}
	}
	
}
