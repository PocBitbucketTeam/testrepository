package com.deloitte.common.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.sql.DataSource;


/**
 * Base class for creating Connection Factories.
 * 
 * <p><b>Example Implementation</b>
 * <pre> 
 * import javax.naming.Context;
 * import javax.naming.InitialContext;
 * import javax.naming.NamingException;
 * import javax.sql.DataSource;
 *
 * public class SampleConnectionFactory extends ConnectionFactory 
 * {
 * 	private static final Logger logger = Logger.getLogger(SampleConnectionFactory.class.getName());
 *
 *	public static final String ORACLE_JNDI = "DataSource_Name";
 *	
   ***	public SampleConnectionFactory()
 *	{
 *		super();
 *	}
 *	
 *	static
 *	{
 *		try {
 *			Context ctx = new InitialContext();
  **			if (ctx != null)
 *			{	
 *				logger.info("InitialContext found");
 *				DataSource ds = (DataSource) ctx.lookup("jdbc/" + ORACLE_JNDI);
 *				setDataSource(ds);
 *			} else
 *			{
 *				logger.info("No InitialContext found, Datasource not initialized");
 *			} 
 *
 *		} catch (NamingException e) 
 *		{
 *			logger.info(e.toString());
 *			tryDevelopmentDatasource();
 *		}
 *	}
 *	</pre>
 * @author SIDT Framework Team
 *
 */
public abstract class ConnectionFactory 
{
	private static final Logger logger = Logger.getLogger(ConnectionFactory.class.getName());

	protected static Map<Object, DataSource> dataSources = new HashMap<Object, DataSource>();

	protected ConnectionFactory()
	{
		// Only Instantiate from subclass
	}
	
	public static void clear()
	{
		dataSources.clear();
	}
	
	public DataSource getDataSource(Object factoryname)
	{
		logger.info("Attempting to retrieve "+ factoryname + " from registered Connection Factories");
		return dataSources.get(factoryname);
	}
	
	public static void setDataSource(Object factoryname,DataSource datasource)
	{
		logger.info(factoryname + " Datasource has been set in ConnectionFactory " + datasource.getClass().getName());
		dataSources.put(factoryname,datasource);

	}
	
	public static void releaseResultSet(ResultSet theResultSet)
	{
		try 
		{
			if (theResultSet != null)
			{
				theResultSet.close();
				logger.info("Closed ResultSet "+ theResultSet.toString());
			}
		} catch (SQLException ignoreThis) 
		{
			logger.info("Error closing ResultSet " + ignoreThis) ;

		}	
	}
	
	/**
	 * Per JDBC doc, closing a Statement automatically closes the ResultSet
	 * @param theStatement
	 */
	
	public static void releaseStatement(Statement theStatement)
	{
		try 
		{
			if (theStatement != null)
			{
				logger.info("Closed Statement  " + theStatement.toString() + " Connection = " + theStatement.getConnection().toString());

				theStatement.close();
	
			}
		} catch (SQLException ignoreThis) 
		{
			logger.info("Error closing Statement " + ignoreThis) ;

		}	
	}
	
	public static void releaseConnection(Connection theConnection)
	{
		try 
		{
			if (theConnection != null && !theConnection.isClosed())
			{
				theConnection.close();
				if (theConnection.isClosed())
				{
					logger.info("Closed Connection " + theConnection.toString());
				}
			}
		} catch (SQLException ignoreThis) 
		{
			logger.info("Error closing Connection " + ignoreThis) ;
		}
	}
	
}
