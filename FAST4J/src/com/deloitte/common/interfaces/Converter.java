package com.deloitte.common.interfaces;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public interface Converter 
{
		public boolean accepts(Object from, Class<?> to);
		public Object convert(Object from) throws CheckedApplicationException;
		public Class<?> getReturnType();
		public Class<?>[] getParameterTypes();
}
