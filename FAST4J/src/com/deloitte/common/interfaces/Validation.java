package com.deloitte.common.interfaces;

import java.util.Collection;

import com.deloitte.common.objects.framework.Error;

/**
 * Based on an intent, perform validation on the object that
 * implements this behavior and return the results. For example
 * this is crucial to accepting input from a UI and prior to
 * persisting an object to the repository.
 * 
 * @author SIDT Framework Team
 */
public interface Validation 
{
	public Collection<Error> getValidationErrors(Intent intent);
	
}
