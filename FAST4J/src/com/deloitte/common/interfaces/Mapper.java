package com.deloitte.common.interfaces;

/**
 * Allows mapping strategies to be defined, creating mappings between an input
 * tuple of a class and string identifer to an output string. Is commonly used
 * to define property name mappings between classes.
 */
public interface Mapper {
	
	/**
	 * Given a particular class and String identifier and input, will return a
	 * mapped string in response.
	 */
	public String getName(Class<?> klass, String name);
}
