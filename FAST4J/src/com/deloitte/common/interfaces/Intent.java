package com.deloitte.common.interfaces;

/**
 * Intent is used in situations like persistence where a 
 * type-safe enumeration will be passed in to communicate
 * why the method is being called.
 * 
 * @author SIDT Framework Team
 *
 */
public interface Intent{	
}
