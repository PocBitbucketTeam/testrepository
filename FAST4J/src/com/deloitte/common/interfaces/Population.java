package com.deloitte.common.interfaces;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Used to determine if particular a value may be used to populate a property
 * on a given object.
 */
public interface Population {
	
		/**
		 * Indicates if the toObject can have the specified property populated using
		 * the supplied fromValue
		 * @param toObject the destination object whose property is to be populated
		 * @param fromValue the value which should be used to populate the property
		 * @param propertyName the name of the property on the destination object
		 */
		public boolean accepts(Object toObject,Object fromValue,String propertyName);
		
		/**
		 * Populates the specified property on the toObject using the supplied value
		 * @param toObject the destination object whose property is to be populated
		 * @param fromValue the value which should be used to populate the property
		 * @param propertyName the name of the property on the destination object
		 */
		public void   populate(Object toObject, Object fromValue, String propertyName) throws CheckedApplicationException;
		
}
