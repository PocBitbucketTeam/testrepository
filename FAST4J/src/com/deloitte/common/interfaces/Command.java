package com.deloitte.common.interfaces;

import java.util.Collection;
import com.deloitte.common.objects.framework.Error;

import com.deloitte.common.objects.framework.CheckedApplicationException;
/**
 * Supports the Command pattern by seperating the invocation of
 * the command from the implementation.
 * <p>
 * Concrete classes that implement the Command interface will
 * consolidate behaviour that be invoked seperately or as a 
 * collection of commands.
 * 
 * @author SIDT Framework
 *
 */

public interface Command {
	
	public Collection<Error> prepare() throws CheckedApplicationException;
	public Collection<?> execute() throws CheckedApplicationException;
}
