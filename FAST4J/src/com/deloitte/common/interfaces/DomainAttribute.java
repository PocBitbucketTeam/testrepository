package com.deloitte.common.interfaces;

import java.text.Format;
import java.util.Collection;

import com.deloitte.common.objects.framework.Error;

public interface DomainAttribute extends Validation //extends Comparable
{
	public Collection<Error> getValidationErrors();
	public String toDisplay();
	public String toDisplay(Format format);
	public Object getValue();
}
