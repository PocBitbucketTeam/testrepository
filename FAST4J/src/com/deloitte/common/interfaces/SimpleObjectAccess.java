package com.deloitte.common.interfaces;

import java.util.Collection;

import com.deloitte.common.objects.framework.CheckedApplicationException;

/**
 * Use {@link com.deloitte.common.objects.reftable.ReferenceTable ReferenceTable}
 * instead <br><br>
 */
@Deprecated
public interface SimpleObjectAccess
{
	public SimpleObject get(String type,String name) throws CheckedApplicationException;
	public Collection<SimpleObject>   getAll(SimpleFilter filter)  throws CheckedApplicationException;
	public Collection<String>   getAllTypes(SimpleFilter filter)  throws CheckedApplicationException;
	public Collection<String>   getAllNames(SimpleFilter filter)  throws CheckedApplicationException;
	public Collection<String>   getAllValues(SimpleFilter filter)  throws CheckedApplicationException;
	
}
