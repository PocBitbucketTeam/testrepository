package com.deloitte.common.interfaces;

import java.util.Map;

import com.deloitte.common.interfaces.DomainObject;

public interface DomainObjectDecorator extends DomainObject 
{
	public Map<String, ?> getProperties();

	//TODO shouldn't this be DomainObject? Should this maybe be parameterized? Should this even be here?
	public Object getObject();
}
