package com.deloitte.common.interfaces;

import java.io.InputStream;
import java.io.Serializable;

public interface Document extends Validation, Serializable{
	public int length();
	public InputStream getStream();
	public String getType();
	public String getFileName();
}
