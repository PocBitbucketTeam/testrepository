package com.deloitte.common.interfaces;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public interface Factory
{
	public Object createFor(Object o)throws CheckedApplicationException;
}
