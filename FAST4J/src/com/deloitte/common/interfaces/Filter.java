package com.deloitte.common.interfaces;

public interface Filter<T> {
	public boolean include(T theObject);
}
