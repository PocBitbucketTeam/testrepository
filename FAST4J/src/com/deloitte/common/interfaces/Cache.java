package com.deloitte.common.interfaces;

public interface Cache<K, V>
{
	public V get(K index);
	public V put(K index, V o);
	public boolean containsKey(K index);
	public V remove(K index);
	public void clear();
}