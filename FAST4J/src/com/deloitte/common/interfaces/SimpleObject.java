package com.deloitte.common.interfaces;

/**
 * Use {@link com.deloitte.common.objects.reftable.ReferenceTableRow ReferenceTableRow}
 * instead <br><br>
 * 
 * Second class business objects. This would be used whenever an
 * object has no requirements that would indicate persistence.<br><br>
 * An example would be a simple lookup object such as this:
 * <p>
 *  <li>getType() returns "State"
 * 	<li>getName() returns "AK"
 *  <li>getValue() returns "Alaska"
 *  
 */
@Deprecated
public interface SimpleObject extends Comparable<SimpleObject>
{
	public String getType();
	public String getName();
	public String getValue();
}

