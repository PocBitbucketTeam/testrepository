package com.deloitte.common.interfaces;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public interface Generation<T>
{
	public T getNextValue() throws CheckedApplicationException;
}
