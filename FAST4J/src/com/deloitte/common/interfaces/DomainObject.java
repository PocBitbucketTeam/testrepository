package com.deloitte.common.interfaces;

/**
 * A DomainObject is the first class business object of all systems. Upon
 * Identification and Validation it can be persisted into a repository
 * or accepted as input from a UI.
 * <IMG SRC="../../../../resources/ClassDiagramInterfaces.jpg" ALT="UML Diagram">
 * <IMG SRC="../../../../resources/ClassDiagramObjects.jpg" ALT="UML Diagram">
 * @author SIDT Framework Team
 *
 */

public interface DomainObject extends Comparable<DomainObject>,Identification, Validation 
{
}
