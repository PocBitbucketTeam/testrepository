package com.deloitte.common.interfaces;

import java.io.Serializable;

/**
 * Universally Unique Identification, implements the concept of
 * a unique identifier for each object that implements this
 * interface.
 * 
 *  @author SIDT Framework Team
 */
public interface UUID extends Serializable, Comparable<UUID> {
	public Object getValue();
}
