package com.deloitte.common.interfaces;

/**
 * Supports the notion of one and only one item, which can be
 * reliably identified. For example this concept is crucial
 * to persisting objects into a repository and being able to
 * retrieve them via a key.
 * @author SIDT Framework Team
 *
 */
public interface Identification 
{
	public UUID	getID();	

}
