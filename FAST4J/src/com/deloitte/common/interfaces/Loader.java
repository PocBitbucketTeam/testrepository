package com.deloitte.common.interfaces;

import com.deloitte.common.objects.framework.CheckedApplicationException;

public interface Loader
{
	public void loadValues() throws CheckedApplicationException;
}
