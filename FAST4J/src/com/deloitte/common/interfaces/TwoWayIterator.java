package com.deloitte.common.interfaces;

/**
 * Two-Way Iterator supports bidirectional traversal of a
 * List.
 */
import java.util.ListIterator;

public interface TwoWayIterator<E> extends ListIterator<E> 
{
	public E current();
}
