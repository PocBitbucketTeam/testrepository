import java.sql.Timestamp;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringToTimestampConversion {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String args[]) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			Date date = format.parse("2008-09-01 00:00:00");
			Timestamp timestamp = new Timestamp(date.getTime());
			Object date2 = format.parseObject("2008-09-01 00:00:00", new ParsePosition(0));
			System.out.println("date :: " + date);
			System.out.println("timestamp :: " + timestamp);
			System.out.println("date2 :: " + date2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
