package gov.state.nextgen.util;

import static gov.state.nextgen.util.UtilityConstants.inputJSPFolderPath;
import static gov.state.nextgen.util.UtilityConstants.outputJSPFolderPath;
import static gov.state.nextgen.util.UtilityConstants.pageElementFolderPath;
import static gov.state.nextgen.util.UtilityConstants.propertiesPattern;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class ScriptGenerationUtility {

	
	public static void main(String[] args) {

		String inputPageElementsFolderPath = args.length == 0 ? pageElementFolderPath:args[0];
		System.out.println("Using the input Page Elements folder location " + inputPageElementsFolderPath);
		
		
		String inputFolderPath = args.length < 2 ? inputJSPFolderPath : args[1];
		System.out.println("Processing the input JSP folder location " + inputFolderPath);
		
		String outputFolder = args.length < 3 ? outputJSPFolderPath : args[2];
		System.out.println("Output will be generated at folder location " + outputFolder);
		
		generatePageElementsScripts(inputFolderPath, inputPageElementsFolderPath, outputJSPFolderPath);		
	}

	public static void generatePageElementsScripts(String inputJSPFolderPath, String inputPageElementsFolderPath, String outputFolderLoc) {
		
		File inputJSPFolder = new  File(inputJSPFolderPath);
		
		if(!inputJSPFolder.exists()) {
			System.out.println("Input JSP folder does not exists...");
			return;
		}
		
		File inputPageElementFolder = new  File(inputPageElementsFolderPath);
		
		if(!inputPageElementFolder.exists()) {
			System.out.println("Input Page Element folder does not exists...");
			return;
		}
		
		File outputFolderPath = new  File(outputFolderLoc + "/script");
		
		if(!outputFolderPath.exists()) {
			outputFolderPath.mkdirs();
		}
		
		Map<String, List<Long>> textIdMap = new HashMap<>();
		
		JspAnalysis.processFolder(inputJSPFolder, textIdMap);
		 
		long eleDplyId = 1;
		long txtId = 0;
		PrintWriter out = null;
		try {
			out = new PrintWriter(new File(outputFolderPath + "\\ELE_DPLY_script.sql" ));
			for (File file : inputPageElementFolder.listFiles()) {
				try {
					Map<String, String> attributes = new HashMap<String, String>(); 
					System.out.println("-- File name - " + file.getName());
					out.println("-- File name - " + file.getName());
					Scanner sc = new Scanner(file);
					String line = null;
					while((line = sc.findWithinHorizon(propertiesPattern,0)) != null) {
						String[] tokens = line.split(" *= *");
						attributes.put(tokens[0].substring(tokens[0].lastIndexOf(" ")+1).toUpperCase(), tokens[1]);
					}
					sc.close();
					//System.out.println(file.getName());
					String fileName = file.getName().replaceAll("\\.java", "");
					if(textIdMap.get(fileName) == null || textIdMap.get(fileName).size() != 1) {
						txtId = 0;	
					}
					else {
						txtId = textIdMap.get(fileName).get(0);
					}
					
					System.out.println("insert into ELE_DPLY(ELE_DPLY_ID, ELE_SIZE_QTY, TXT_ID, HTML_NAM) values (" + eleDplyId + ", " + attributes.get("WIDTH").replace("\"", "") + ", " + txtId + ", " + attributes.get("FIELD_NAME").replace('"', '\'') + ");");
					out.println("insert into ELE_DPLY(ELE_DPLY_ID, ELE_SIZE_QTY, TXT_ID, HTML_NAM) values (" + eleDplyId + ", " + attributes.get("WIDTH").replace("\"", "") + ", " + txtId + ", " + attributes.get("FIELD_NAME").replace('"', '\'') + ");");
					eleDplyId++;
					//break;
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			if(out != null) {
				out.close();
			}
		}
		
	}
}
