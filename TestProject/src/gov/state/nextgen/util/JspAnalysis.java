package gov.state.nextgen.util;

import static gov.state.nextgen.util.UtilityConstants.inputJSPFolderPath;
import static gov.state.nextgen.util.UtilityConstants.outputJSPFolderPath;
import static gov.state.nextgen.util.UtilityConstants.useBeanPattern;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * The Class JspAnalysis.
 */
public class JspAnalysis {



	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		System.out.println("Jira #2 Commit # 1");
		System.out.println("Jira #4 commit 1");
		String inputFolderPath = args.length == 0 ? inputJSPFolderPath:args[0];
		System.out.println("Processing the input folder location " + inputFolderPath);

		System.out.println("Jira #3 -- Commit #1");
		String outputFolderLoc = (args.length < 2 ? outputJSPFolderPath:args[1]);
		System.out.println("Analysis will be generated at folder location " + outputFolderLoc);

		performJSPAnalysis(inputFolderPath, outputFolderLoc);
		System.out.println("Jira #5 commit 1");
	}
	
	/**
	 * Perform jsp analysis.
	 *
	 * @param inputFolderPath the input folder path
	 * @param outputFolderLoc the output folder loc
	 */
	public static void performJSPAnalysis(String inputFolderPath, String outputFolderLoc) {
		
		File inputFolder = new  File(inputFolderPath);

		if(!inputFolder.exists()) {
			System.out.println("Input JSP folder does not exists...");
			return;
		}
		
		File outputFolderPath = new  File(outputFolderLoc + "/analysis/");

		if(!outputFolderPath.exists()) {
			outputFolderPath.mkdirs();
		}
		
		Map<String, List<Long>> textIdMap = new TreeMap<String, List<Long>>();

		processFolder(inputFolder, textIdMap);
		
		PrintWriter out = null;
		try {
			out = new PrintWriter(new File(outputFolderPath + "\\All_Page_Elements.txt" ));
			for (String className : textIdMap.keySet()) {
				if(textIdMap.get(className).size() == 0) {
					continue;
				}
				out.println(className + " = " + textIdMap.get(className).toString().replaceAll("[\\[\\]]", ""));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				out.close();
			}
		}
	}

	/**
	 * Process folder.
	 *
	 * @param inputFolder the input folder
	 * @param map the map
	 */
	public static void processFolder(File inputFolder, Map<String, List<Long>> map) {
		for (File file : inputFolder.listFiles()) {
			if(file.isDirectory()) {
				processFolder(file, map);
			}
			else if(file.isFile() && file.getName().endsWith(".jsp")) {
				processJsp(file, map);
			}
		}
	}

	/**
	 * Process jsp.
	 *
	 * @param file the file
	 * @param textIdMap the text id map
	 */
	public static void processJsp(File file, Map<String, List<Long>> textIdMap) {
		try {
			System.out.println("Processing jsp " + file);
			Map<String, String> attributes = new HashMap<String, String>(); 
			Scanner sc = new Scanner(file);
			String line = null;
			while((line = sc.findWithinHorizon(useBeanPattern,0)) != null) {
				line = line.replaceAll("[\\t\\n\\r ]+", " ").replaceAll(" *= *", "=");
				//System.out.println(line);
				String[] properties = line.substring(line.indexOf(" ") + 1, line.lastIndexOf(" ")).split(" ");
				String className = null;
				String id = null;
				boolean isPageElement = false;
				for (String string : properties) {
					String[] prop = string.split("=");
					if(prop[0].startsWith("class")) {
						if(!prop[1].startsWith("\"gov.state.nextgen.access.presentation.pageElements")) {
							continue;
						}

						className = prop[1].replaceAll("\"", "").substring(prop[1].lastIndexOf("."));
						isPageElement = true;
					}
					if(prop[0].startsWith("id")) {
						id = prop[1].replaceAll("\"", "");
					}
				}
				if(isPageElement)
				{
					attributes.put(id, className);
				}
			}
			sc.close();
			for (String key : attributes.keySet()) {
				sc = new Scanner(file);
				//while((line = sc.findWithinHorizon("<[^ >]* *bean=\"<%="+key+"%>\"[^/]*/>",0)) != null) {
				//<(AP|TL|DS)(.*\\W*|\\W*.*)bean[^/]*/
				while((line = sc.findWithinHorizon("<(AP|TL|DS)[:=\"<%>\\w\\r\\n\\s.()';\\\\,+\\[\\]\\- ]*bean *= *\"<% *= *"+key+"[:=\"<%>\\w\\r\\n\\s.()';\\\\,+\\[\\]\\- ]*/",0)) != null) {
					line = line.replaceAll("[\\t\\n\\r ]+", " ").replaceAll(" *= *", "=");;
					//System.out.println(line);
					if(textIdMap.get(attributes.get(key)) == null) {
						textIdMap.put(attributes.get(key), new ArrayList<Long>());
					}
					String[] properties = line.substring(line.indexOf(" ") + 1, line.length() - 1).split(" ");
					for (String string : properties) {
						String[] prop = string.split("=");
						if(prop[0].toUpperCase().startsWith("textId".toUpperCase())
								|| prop[0].toUpperCase().startsWith("displayTextId".toUpperCase()) 
								|| prop[0].toUpperCase().startsWith("pageTitleTextID".toUpperCase()) 
								|| prop[0].toUpperCase().startsWith("noRecordTextId".toUpperCase())) {
							Long textId = null;
							try {
								textId = Long.parseLong(prop[1].replaceAll("\"", ""));
							}
							catch(NumberFormatException e) {
								continue;
							}
							if(!textIdMap.get(attributes.get(key)).contains(textId)) {
								textIdMap.get(attributes.get(key)).add(textId);	
							}
						}
					}
				}
				sc.close();
			}
			//break;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
	}
}
