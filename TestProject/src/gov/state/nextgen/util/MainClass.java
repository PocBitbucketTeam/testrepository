package gov.state.nextgen.util;

import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {

		Scanner scanIn = new Scanner(System.in);
		System.out.println("//////////////////////Page element and JSP replacement utility////////////////////////////");
		
		System.out.println("Please enter input JSP folder path: ");
	    String inputJSPFolder = scanIn.nextLine();
	    if(inputJSPFolder == null || inputJSPFolder.isEmpty()) {
	    	inputJSPFolder = UtilityConstants.inputJSPFolderPath;
	    	System.out.println("No JSP folder location specified. Using default JSP input folder location " + UtilityConstants.inputJSPFolderPath);
	    }

	    System.out.println("Please enter page elements folder path: ");
	    String pageElementFolderPath = scanIn.nextLine();
	    if(pageElementFolderPath == null || pageElementFolderPath.isEmpty()) {
	    	pageElementFolderPath = UtilityConstants.pageElementFolderPath;
	    	System.out.println("No page elements folder location specified. Using default page elements input folder location " + UtilityConstants.pageElementFolderPath);
	    }
	    
	    System.out.println("Please enter output folder path: ");
	    String outputFolderPath = scanIn.nextLine();
	    if(outputFolderPath == null || outputFolderPath.isEmpty()) {
	    	outputFolderPath = UtilityConstants.outputJSPFolderPath;
	    	System.out.println("No output folder location specified. Using default output folder location " + UtilityConstants.outputJSPFolderPath);
	    }
	    
	    System.out.println();
	    System.out.println("1. Perform JSP analysis (Generates page elements and their corresponding text ids used) ");
	    System.out.println("2. Generate Script for page elements ");
	    System.out.println("3. Run JSP bean replacement utility ");
	    System.out.println("Enter your choice: ");
	    int choice = scanIn.nextInt();
	    
	    switch(choice)
	    {
	    	case 1: JspAnalysis.performJSPAnalysis(inputJSPFolder, outputFolderPath);
	    			System.out.println("JSP analysis utility completed successfully.");
	    			break;
	    	case 2: ScriptGenerationUtility.generatePageElementsScripts(inputJSPFolder, pageElementFolderPath, outputFolderPath);
				    System.out.println("Page elements scripts generated successfully.");
				    break;
	    	case 3: JSPBeanReplacementUtility.replaceJSPTags(inputJSPFolder, pageElementFolderPath, outputFolderPath);
	    			break;
	    	default:
	    		System.out.println("Invalid input !!");
	    }
	    scanIn.close();            
	       
	}

}
