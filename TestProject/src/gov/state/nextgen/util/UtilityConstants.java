package gov.state.nextgen.util;

import java.util.regex.Pattern;

public class UtilityConstants {

	public static String inputJSPFolderPath = "C:\\SSPWorkspace\\access\\WebContent\\jsp\\access";
	//public static String inputJSPFolderPath = "C:\\NGJSPtestOutput\\backup\\access";
	
	//public static String outputFolder = "C:\\NGJSPtestOutput\\analysis";
	
	public static String outputJSPFolderPath = "C:\\NGJSPtestOutput";
	
	
	public static String pageElementFolderPath = "C:\\SSPWorkspace\\access\\JavaSource\\gov\\state\\nextgen\\access\\presentation\\pageElements";

//	Pattern useBeanPattern=Pattern.compile("<jsp:usebean.*[^/]*/>",Pattern.CASE_INSENSITIVE);
	public static Pattern useBeanPattern=Pattern.compile("<jsp:usebean[^>]*>(</jsp:useBean>)?",Pattern.CASE_INSENSITIVE);
	public static Pattern beanAttrPattern=Pattern.compile("<(AP|TL|DS)[:=\"<%>\\w\\r\\n\\s.()';\\\\,+\\[\\]\\- ]*bean[:=\"<%>\\w\\r\\n\\s.()';\\\\,+\\[\\]\\- ]*/",Pattern.CASE_INSENSITIVE);

//	public static Pattern innerBeanAttrPat=Pattern.compile("bean=\"<%=\\w*%>\"",Pattern.CASE_INSENSITIVE);
	public static Pattern innerBeanAttrPat=Pattern.compile("bean *= *\"<%= *\\w* *%> *\"",Pattern.CASE_INSENSITIVE);
//	public static Pattern textIdAttrPat=Pattern.compile("(textId|displaytextId)=\"\\d*\"",Pattern.CASE_INSENSITIVE);
	public static Pattern textIdAttrPat=Pattern.compile("(textId|displaytextId|pageTitleTextID|noRecordTextId) *= *\"\\d*\"",Pattern.CASE_INSENSITIVE);

	public static Pattern propertiesPattern = Pattern.compile("(private|protected|public)? *(static)? *(final)? *String *(WIDTH|LABEL|FIELD_NAME) *= *\"[^\"]*\"", Pattern.CASE_INSENSITIVE);

	public static String newLinesRegex="^\r\n\r\n";
}
