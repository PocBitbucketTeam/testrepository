package gov.state.nextgen.util;

import static gov.state.nextgen.util.UtilityConstants.beanAttrPattern;
import static gov.state.nextgen.util.UtilityConstants.innerBeanAttrPat;
import static gov.state.nextgen.util.UtilityConstants.inputJSPFolderPath;
import static gov.state.nextgen.util.UtilityConstants.newLinesRegex;
import static gov.state.nextgen.util.UtilityConstants.outputJSPFolderPath;
import static gov.state.nextgen.util.UtilityConstants.pageElementFolderPath;
import static gov.state.nextgen.util.UtilityConstants.textIdAttrPat;
import static gov.state.nextgen.util.UtilityConstants.useBeanPattern;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;
public class JSPBeanReplacementUtility {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		String inputFolderPath = args.length == 0 ? inputJSPFolderPath:args[0];
		
		System.out.println("Jira #2 Commit # 1");
		System.out.println("Jira #2 Commit # 2");
		System.out.println("Processing the input folder location " + inputFolderPath);
		File inputFolder = new  File(inputFolderPath);

		if(!inputFolder.exists()) {
			System.out.println("Input folder does not exists...");
			return;
		}
		
		String outputFolderLoc = args.length < 2 ? outputJSPFolderPath:args[1];
		
		System.out.println("Output will be generated at folder location " + outputFolderLoc);

		File outputFolderPath = new  File(outputFolderLoc);

		if(!outputFolderPath.exists()) {
			outputFolderPath.mkdirs();
		}
		
		replaceJSPTags(inputFolderPath, pageElementFolderPath, outputFolderLoc);

	}


	/**
	 * @param jspInputPackagePath
	 * @param javaBeanPackagePath
	 * @param jspOutputPackagePath
	 * 
	 * The package path should already have the directory separators escaped. 
	 */
	public static void replaceJSPTags(String jspInputPackagePath, String javaBeanPackagePath, String jspOutputPackagePath)
	{	
		
		if(jspInputPackagePath.equalsIgnoreCase(jspOutputPackagePath))
		{
			System.out.println("Please ensure you give separate input and output folders to maintain a copy of original JSPs.\nUtility exiting...");
			return;
		}
		
		System.out.println("--Entering replaceJSPTags with JSP package: "+jspInputPackagePath+" and beanClasses packagePath: "+javaBeanPackagePath);
		System.out.println("--The resulting JSPs will be placed at: "+jspOutputPackagePath);

		File inputJSPFolder = new  File(jspInputPackagePath);
		
		if(!inputJSPFolder.exists()) {
			System.out.println("Input JSP folder does not exists...");
			return;
		}
		
		File inputBeanClassFolder = new  File(javaBeanPackagePath);

		if(!inputBeanClassFolder.exists()) {
			System.out.println("Input Bean classes folder does not exists...");
			return;
		}
		
		System.out.println("Getting BeanClass=FieldName Map for all Java classes @ "+javaBeanPackagePath);
		Map<String,String> fieldNameMap=getFieldNameMap(inputBeanClassFolder);
		
		System.out.println("Getting BeanClass=List<TextIds> Map for all Java classes @ "+javaBeanPackagePath);
		Map<String,List<Long>> textIdMap=new HashMap<>();
		JspAnalysis.processFolder(inputJSPFolder, textIdMap);
		
		
		File outputFolderPath = new  File(jspOutputPackagePath + jspInputPackagePath.substring(jspInputPackagePath.lastIndexOf("\\")));
		File logfolderPath = new  File(jspOutputPackagePath + "\\logs\\" + jspInputPackagePath.substring(jspInputPackagePath.lastIndexOf("\\")));
		
		if(!outputFolderPath.exists()) {
			outputFolderPath.mkdirs();
		}
		
		if(!logfolderPath.exists()) {
			logfolderPath.mkdirs();
		}
		
		processFolder(inputJSPFolder, fieldNameMap, textIdMap, outputFolderPath, logfolderPath);
		

	}
	
	
	private static Map<String, String> getFieldNameMap(File inputFolder) {
        
		Pattern pattern = Pattern.compile("(private|protected|public)? *(static)? *(final)? *String *(WIDTH|LABEL|FIELD_NAME) *= *\"[^\"]*\"", Pattern.CASE_INSENSITIVE);
		Map<String, String> returnMap = new HashMap<>();
        for (File file : inputFolder.listFiles()) {
               try {
                     Map<String, String> attributes = new HashMap<String, String>(); 
//                     System.out.println("-- File name - " + file.getName());
                     Scanner sc = new Scanner(file);
                     String line = null;
                     while((line = sc.findWithinHorizon(pattern,0)) != null) {
                            String[] tokens = line.split(" *= *");
                            attributes.put(tokens[0].substring(tokens[0].lastIndexOf(" ")+1).toUpperCase(), tokens[1]);
                            if(tokens[0].substring(tokens[0].lastIndexOf(" ")+1).equals("FIELD_NAME")) {
                                   returnMap.put(file.getName().replaceAll(".java", ""), tokens[1]);
                            }
                     }
                     sc.close();
                     
               } catch (FileNotFoundException e) {
                     e.printStackTrace();
               } 
        }
        System.out.println(returnMap);
        return returnMap;
 }

	private static void processFolder(File inputJSPFolder, Map<String,String> fieldNameMap, Map<String,List<Long>> textIdMap, File outputFolderPath, File logFolderPath) {
		for (File file : inputJSPFolder.listFiles()) {
			if(file.isDirectory()) {
				
				File childOutputFolder=new File(outputFolderPath, file.getName());
				
				if(!childOutputFolder.exists()) {
					childOutputFolder.mkdirs();
				}
				File childLogOutputFolder=new File(logFolderPath, file.getName());
				
				if(!childLogOutputFolder.exists()) {
					childLogOutputFolder.mkdirs();
				}
				processFolder(file,fieldNameMap, textIdMap,childOutputFolder,childLogOutputFolder);
			}
			else if(file.isFile() && file.getName().endsWith(".jsp")) {
				processJsp(file, fieldNameMap, textIdMap, outputFolderPath, logFolderPath);

			}
		}
	}
	
	private static void processJsp(File inputJSPFile, Map<String,String> fieldNameMap, Map<String,List<Long>> textIdMap, File outputFolderPath, File logFolderPath) {
		
		System.out.println("-Processing jsp -- " + inputJSPFile);
		FileReader fr=null;
		Scanner sc=null;
		PrintWriter out=null,logWriter=null;
		

		try {

			File outputFile=new File(outputFolderPath,inputJSPFile.getName());
			outputFile.createNewFile();
			out=new PrintWriter(outputFile);
			File logOutputFile=new File(logFolderPath,inputJSPFile.getName()+".log");
			logOutputFile.createNewFile();
			logWriter=new PrintWriter(logOutputFile);

			fr=new FileReader(inputJSPFile);
			sc=new Scanner(fr);
			
			List<String> useBeanmatchList=new ArrayList<>(); 
			List<String> beanAttrmatchList=new ArrayList<>(); 
			Map<String,String> beanIdClassNameMap=new HashMap<>();
			String match=null;
			do{
				match=sc.findWithinHorizon(useBeanPattern, 0);
				if(match!=null && match.contains("gov.state.nextgen.access.presentation.pageElements")) {
					useBeanmatchList.add(match!=null?match.trim():match);
					beanIdClassNameMap.put(match!=null?match.substring(match.indexOf("id=\"")+4, match.indexOf("\"", match.indexOf("id=\"")+4)):"",(match!=null?match.substring(match.lastIndexOf(".")+1,match.indexOf("\"", match.lastIndexOf(".")+1)):""));
				}

			}while(match!=null);

			match=null;
			do{
				match=sc.findWithinHorizon(beanAttrPattern, 0);
				beanAttrmatchList.add(match!=null?match.trim():match);

			}while(match!=null);

			System.out.println(useBeanmatchList.size()+" "+useBeanmatchList.toString());
			System.out.println(beanAttrmatchList.size()+" "+beanAttrmatchList.toString());
			System.out.println(beanIdClassNameMap.size()+" "+beanIdClassNameMap.toString());
			System.out.println();		
			
			byte[] JSPData = Files.readAllBytes(Paths.get(inputJSPFile.toURI()));
			String fullJSP=new String(JSPData);

			String useBeanmodifiedJSP=fullJSP;
			for(String useBeanString:useBeanmatchList)
			{
				useBeanmodifiedJSP=useBeanmodifiedJSP.replace(useBeanString,"");
			}

			String beanAttrModifiedJSP=useBeanmodifiedJSP;
			Scanner scn=null;
			List<Long> textIdList=null;
			List<String> modBeanAttrAndTextId=new ArrayList<>();
			List<String> modBeanAttrOnly=new ArrayList<>();

			//for(String beanAttrString:beanAttrmatchList)
			for(Iterator<String> i = beanAttrmatchList.iterator(); i.hasNext();)
			{
				String beanAttrString = i.next();
				if(beanAttrString!=null)
				{
					scn=new Scanner(beanAttrString);
					String beanAttr=scn.findWithinHorizon(innerBeanAttrPat, 0);
					String newString=new String(beanAttrString);
					if(beanAttr!=null) {
						
						String beanId=beanAttr.substring(beanAttr.lastIndexOf("=")+1, beanAttr.lastIndexOf("%"));
						String elementName=fieldNameMap.get(beanIdClassNameMap.get(beanId.trim()));
						textIdList=textIdMap.get(beanIdClassNameMap.get(beanId.trim()));
						if(elementName!=null)
						{
							newString=newString.replace(beanAttr, "elementName="+elementName);
						}
						
						String textIdAttr=scn.findWithinHorizon(textIdAttrPat, 0);
						if(textIdAttr!=null) {
							if(textIdList.size()==1) {
								newString=newString.replace(textIdAttr, "");
								modBeanAttrAndTextId.add(beanAttrString);
							}
							else{
								modBeanAttrOnly.add(beanAttrString);							
							}
						}
						
						beanAttrModifiedJSP=beanAttrModifiedJSP.replace(beanAttrString, newString);
					}
					else {
						i.remove();
					}
					
				}

			}

			
			//replace all empty lines
			beanAttrModifiedJSP=beanAttrModifiedJSP.replaceAll(newLinesRegex, "");

			out.write(beanAttrModifiedJSP);
			
			System.out.println("Logged the statistics to file: "+logOutputFile.getName());
			String logOutput="\n\n~~~~~~~Log for changes in JSP: "+outputFile.getName()+" -- Generated on: "+new Date()+"--~~~~~~~\n\n";
			logOutput=logOutput+useBeanmatchList.size()+" "+useBeanmatchList.toString()+"\n~~~~~~~STATS 1: ~~~~~~~\nThe above "+ useBeanmatchList.size()+" useBean tags were removed from the JSP\n";
			logOutput=logOutput+beanAttrmatchList.size()+" "+beanAttrmatchList.toString()+"\n~~~~~~~STATS 2: ~~~~~~~\nThe above "+ beanAttrmatchList.size()+" element tags were modified to replace 'bean=<%...%>' attribute with 'elementName=\"...\" attribute \n\n";
			if(modBeanAttrAndTextId.size()>0) {
				logOutput=logOutput+modBeanAttrAndTextId.size()+" "+modBeanAttrAndTextId.toString()+"\n~~~~~~~STATS 3: ~~~~~~~\nThe above "+ modBeanAttrAndTextId.size()+" element tags were modified to remove 'textId/displayTextId' attribute \n\n";
			}
			if(modBeanAttrOnly.size()>0) {
				logOutput=logOutput+modBeanAttrOnly.size()+" "+modBeanAttrOnly.toString()+"\n~~~~~~~STATS 4: ~~~~~~~\nThe above "+ modBeanAttrOnly.size()+" element tags were not modified to remove 'textId/displayTextId' attribute as corresponding Java Classes have different textIds being used for different elements \n\n";
			}
			logWriter.write(logOutput);


		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(out!=null)
				{
					out.close();
				}
			if(logWriter!=null)
			{
				logWriter.close();
			}
		
			try {
				if(fr!=null) {
					fr.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(sc!=null) {
				sc.close();
			}

		}
		
		
	}




}
